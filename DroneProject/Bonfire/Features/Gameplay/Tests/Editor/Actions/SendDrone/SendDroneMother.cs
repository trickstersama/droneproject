﻿using Assets.Bonfire.Features.Gameplay.Domain;
using System;
using static Bonfire.Features.Gameplay.Tests.Editor.Actions.SendDrone.GameplayEventMother;

namespace Bonfire.Features.Gameplay.Tests.Editor.Actions.SendDrone
{
    public static class SendDroneMother
    {
        public static SendDrone ASendDrone(
            GameplayEvents? withEvents = null
        ){
            var events = withEvents ?? SomeEvents();
            
            return new SendDrone(
                gameplayEvents: events 
                );
        }
    }
}

