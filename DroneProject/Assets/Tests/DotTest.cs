using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Tests
{
    public class DotTest : MonoBehaviour
    {
        [SerializeField] Button animation1;
        [SerializeField] Button animation2;
        [SerializeField] Button animation3;
        [SerializeField] Button animation4;
        void Start()
        {
            animation1.OnClickAsObservable().Subscribe(_ => Animation1());
            animation2.OnClickAsObservable().Subscribe(_ => Animation2());
            animation3.OnClickAsObservable().Subscribe(_ => Animation3());
            animation4.OnClickAsObservable().Subscribe(_ => Animation4());
        
        }

        void Animation1()
        {
            transform.DOMoveY(100f, 5f, true)
                .OnComplete(() => transform
                    .DOShakeRotation(3f, 90f, 10, 50f));
        } 
        void Animation2()
        {
            var mySequence = DOTween.Sequence();

            mySequence
                .Append(transform.DOMoveX(45, 1))
                .Append(transform.DORotate(new Vector3(0, 180, 0), 1))
                //.PrependInterval(10)
                .Insert(0, transform.DOScale(new Vector3(3, 3, 3), mySequence.Duration()));
        }

        void Animation3()
        {
            //usando generic
            DOTween.To(()=> transform.position, x=> transform.position = x, new Vector3(3,4,8), 1);
        }
        void Animation4()
        {

        }

        void GenericDot()
        {
            //DOTween.To(getter, setter, to, float duration)
        
            // Tween a Vector3 called myVector to 3,4,8 in 1 second
            Vector3 myVector = Vector3.one;
            DOTween.To(()=> myVector, x=> myVector = x, new Vector3(3,4,8), 1);
        
            // Tween a float called myFloat to 52 in 1 second
            float myFloat = 0;
            DOTween.To(()=> myFloat, x=> myFloat = x, 52, 1);
        }

        void ShorcutWay()
        {
#pragma warning disable 618
            Material material = new Material("asd");
#pragma warning restore 618
            transform.DOMove(new Vector3(2,3,4), 1);
            material.DOColor(Color.green, 1);
        
            transform.DOMove(new Vector3(2,3,4), 1).From();
            material.DOColor(Color.green, 1).From();
        }
    }
}
