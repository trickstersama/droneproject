﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Delivery.Presentation;
using Bonfire.Gameplay.Delivery.Presentation.Balcony;
using Bonfire.Gameplay.Delivery.Presentation.Computer;
using Bonfire.Gameplay.Delivery.Presentation.House;
using Bonfire.Gameplay.Delivery.Presentation.Missions;
using Bonfire.Gameplay.Delivery.Presentation.Workstation;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Domain.Actions;
using Bonfire.Gameplay.Domain.Actions.BalconyContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Status;
using Bonfire.Gameplay.Domain.Actions.HouseBatteryContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Frames;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules;
using Bonfire.Gameplay.Domain.Configuration;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.Reactions.House;
using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay
{
    public static class Context
    {
        public static void Initialize(
            CanvasViews canvasViews,
            WorldViews worldViews,
            IEnumerable<Drone> loadedDrones,
            IEnumerable<Frame> loadedFrames,
            IEnumerable<Module> loadedModules,
            IEnumerable<Destination> loadedDestinations,
            IEnumerable<SolarGenerator> loadedSolarGenerators,
            IEnumerable<HouseBattery> loadedHouseBatteries,
            MissionsModifiers loadedTimeCostsModifiers,
            MissionConfiguration missionConfiguration
        ) {
            var events = CreateEvents();
            var houseEvents = CreateHouseEvents();
            var repositories = CreateRepositories(
                loadedDrones: loadedDrones, 
                loadedFrames: loadedFrames,
                loadedModules: loadedModules,
                loadedDestinations: loadedDestinations, 
                loadedSolarGenerators: loadedSolarGenerators,
                loadedHouseBatteries: loadedHouseBatteries,
                loadedTimeCostsModifiers: loadedTimeCostsModifiers
            );
            var services = CreateServices(missionConfiguration);
            var reactions = CreateReactions(events: events, repositories: repositories, services: services, houseEvents: houseEvents);
            var actions = CreateActions(events: events,repositories: repositories, services: services);
            var presenters = CreatePresenters(
                actions: actions, 
                events: events, 
                reactions: reactions, 
                canvasViews: canvasViews, 
                worldViews: worldViews,
                houseEvents: houseEvents
            );
            
            DoSubscriptions(presenters);
        }

        static GameplayServices CreateServices(MissionConfiguration missionConfiguration) =>
            new GameplayServices
            (
                encounterDeciderService: new UnityEncounterDeciderService(),
                frameSlotsAmountService: new LocalFrameSlotsAmountService(),
                droneAssemblerService: new LocalDroneAssemblerService(),
                destinationService: new UnityDestinationService(),
                droneService: new LocalDroneService(),
                missionService: new LocalMissionService(missionConfiguration)
            );

        static GameplayEvents CreateEvents() =>
            new GameplayEvents
            (
                onDroneSent: new Subject<Unit>(),
                onEncounterStart: new Subject<Encounter>(),
                onDroneReturning: new Subject<Drone>(),
                onModuleRepaired: new Subject<Module>(),
                onSendDroneTabSelected: new Subject<IEnumerable<Drone>>(),
                onMissionDroneSelected: new Subject<MissionOrders>(),
                onOpenComputer: new Subject<Unit>(),
                onDroneStatusTabSelected: new Subject<IEnumerable<Drone>>(),
                onDroneInStatusSelected: new Subject<Drone>(),
                onOpenWorkstation: new Subject<Unit>(),
                onFramePanelLoaded: new Subject<IEnumerable<Frame>>(),
                onModulesTabOpened: new Subject<IEnumerable<Module>>(),
                onAssemblyTabSelected: new Subject<IEnumerable<Frame>>(),
                onFrameSelected: new Subject<Frame>(),
                onModulesInPanelsLoaded: new Subject<IEnumerable<Module>>(),
                onAssemblyFrameConfirmed: new Subject<Frame>(),
                onAssemblyModuleInstalled: new Subject<DroneModifiers>(),
                onDroneInProgressUpdated: new Subject<DroneModifiers>(),
                onAssemblyModuleSlotsUpdated: new Subject<Frame>(),
                onDroneBuilt: new Subject<Drone>(),
                onModulesInInventoryRemoved: new Subject<IEnumerable<Module>>(),
                onFrameFromInventoryRemoved: new Subject<IEnumerable<Frame>>(),
                onDroneAddedToInventory: new Subject<IEnumerable<Drone>>(),
                onModuleTypeToDisplaySelected: new Subject<IEnumerable<Module>>(),
                onFrameCardSelected: new Subject<Frame>(),
                onFramesTabSelected: new Subject<IEnumerable<Frame>>(),
                onMissionTypeSelected: new Subject<MissionOrders>(),
                onDestinationSelected: new Subject<MissionOrders>(),
                onWorkstationDronesTabSelected: new Subject<IEnumerable<Drone>>(),
                onMapTabSelected: new Subject<IEnumerable<Destination>>(),
                onNewPinSelected: new Subject<Unit>(),
                onMissionsTimeUpdated: new Subject<IEnumerable<Mission>>(),
                onNewPinCoordinatesSelected: new Subject<Destination>(),
                onPinNameChosen: new Subject<Destination>(),
                onPinTypeChosen: new Subject<Destination>(),
                onNewPinSaved: new Subject<Unit>(),
                onPinnedLocationsSectionSelected: new Subject<IEnumerable<Destination>>(),
                onDroneNamed: new Subject<Unit>(),
                onDroneNameInvalid: new Subject<Unit>(),
                onMapOpened: new Subject<IEnumerable<Destination>>(),
                onRepairNeeded: new Subject<Unit>(),
                onDroneRecharged: new Subject<Drone>(),
                onRechargeNeeded: new Subject<Unit>(),
                onDroneRepaired: new Subject<Drone>(),
                onDroneDisassembled: new Subject<Unit>(),
                onPinnedLocationSelected: new Subject<Destination>(),
                onMissionServicesReadyToCalculate: new Subject<Unit>(),
                onEnergyCostsCalculated: new Subject<MissionOrders>(),
                onMissionMaxWeightAllowed: new Subject<MissionOrders>(),
                onMissionTimeCostsCalculated: new Subject<MissionOrders>(),
                onHouseBatteryOpened: new Subject<HouseEnergyStatus>(),
                onDronesInMissionUpdated: new Subject<Unit>(),
                onMissionsTimelineEmitted: new Subject<float>(),
                onMissionTargetReached: new Subject<Mission>(),
                onMissionTargetCompleted: new Subject<Mission>(),
                onMissionTargetStarted: new Subject<Unit>(),
                onMissionReturnStarted: new Subject<Unit>(),
                onMissionReturnedHome: new Subject<Mission>(),
                onMissionCompleted: new Subject<Unit>(),
                onBalconyOpened: new Subject<IEnumerable<MissionResults>>(),
                onAssemblyModuleUninstalled: new Subject<Unit>(),
                onNewPinReadyToSave: new Subject<Unit>(),
                onNewPinNotReadyToSave: new Subject<Unit>()
            );


        static HouseGameplayEvents CreateHouseEvents() =>
            new HouseGameplayEvents
            {
                OnEnergyProduced = new Subject<float>(),
                OnEnergyTimelineEmitted = new Subject<float>(),
                OnEnergyOverheadProduced = new Subject<float>(),
                OnInsufficientEnergyIsProduced = new Subject<Unit>(),
                OnEnergyStored = new Subject<float>(),
                OnEnergyStorageFull = new Subject<Unit>(),
                OnHouseBatteryUpgraded = new Subject<Unit>()
            };

        static Presenters CreatePresenters(
            GameplayActions actions,
            GameplayEvents events,
            GameplayReactions reactions,
            CanvasViews canvasViews,
            WorldViews worldViews,
            HouseGameplayEvents houseEvents
        ) =>
            new Presenters
            (
                contextsPresenter: new ContextsPresenter(canvasViews: canvasViews, worldViews: worldViews, actions: actions, events: events),
                computerPresenter: new ComputerPresenter(actions: actions, events: events, canvasViews: canvasViews),
                workstationPresenter: new WorkstationPresenter(events: events, actions: actions, canvasViews: canvasViews),
                assemblyPresenter: new AssemblyPresenter(events: events, reactions: reactions, actions: actions, canvasViews: canvasViews),
                mapPresenter: new MapPresenter(canvasViews: canvasViews, actions: actions, events: events, reactions: reactions), 
                sendDronePresenter: new SendDronePresenter(canvasViews: canvasViews, actions: actions, events: events, reactions: reactions),
                houseEnergyPresenter: new HouseEnergyPresenter(reactions: reactions, houseEvents: houseEvents, canvasViews: canvasViews),
                timelinesPresenter: new TimelinesPresenter(reactions: reactions, worldViews: worldViews, houseEvents: houseEvents),
                missionsPresenter: new MissionsPresenter(events: events, reactions: reactions),
                balconyPresenter: new BalconyPresenter(events: events, canvasViews: canvasViews),
                workstationDronesPresenter: new WorkstationDronesPresenter(actions: actions, events: events, views: canvasViews)
            );

        static GameplayReactions CreateReactions(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services,
            HouseGameplayEvents houseEvents
        ) =>
            new GameplayReactions
            (
                loadModulesInPanels: new LoadModulesInPanels(events: events, repositories: repositories),
                updateDroneInProgress: new UpdateDroneInProgress(events: events, repositories: repositories),
                removeFrameFromInventory: new RemoveFrameFromInventory(events: events, repositories: repositories),
                removeModulesFromInventory: new RemoveModulesFromInventory(events: events, repositories: repositories),
                addDroneToInventory: new AddDroneToInventory(events: events, repositories: repositories),
                buildDrone: new BuildDrone(events: events, services: services, repositories: repositories),
                produceEnergy: new ProduceEnergy(houseEvents: houseEvents, repositories: repositories),
                emitTimelines: new EmitTimelines(houseEvents: houseEvents, gameplayEvents: events),
                distributeEnergy: new DistributeEnergy(houseEvents: houseEvents, repositories: repositories),
                storeEnergy: new StoreEnergy(houseEvents: houseEvents, repositories: repositories),
                missionServicesReadyToCalculate: new MissionServicesReadyToCalculate(events: events, repositories: repositories),
                calculateMissionEnergyCosts: new CalculateMissionEnergyCosts(events: events, services: services, repositories: repositories),
                calculateMissionTimeCosts: new CalculateMissionTimeCosts(events: events, services: services, repositories: repositories),
                calculateMissionMaxWeight: new CalculateMissionMaxWeight(events: events, repositories: repositories, services: services),
                updateMissionsTime: new UpdateMissionsTime(events: events, repositories: repositories, services: services),
                missionTargetReached: new MissionTargetReached(events: events, repositories: repositories, services: services),
                startMissionTarget: new StartMissionTarget(events: events, repositories: repositories),
                missionTargetCompleted: new MissionTargetCompleted(events: events, repositories: repositories, services: services),
                startMissionReturn: new StartMissionReturn(events: events, repositories: repositories),
                missionReturnsHome: new MissionReturnsHome(events: events, repositories: repositories, services: services),
                completeMission: new CompleteMission(events: events, repositories: repositories),
                newPinReadyToSave: new NewPinReadyToSave(events: events, services: services)
            );

        static GameplayActions CreateActions(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) =>
            new GameplayActions
            (
                sendDrone: new SendDrone(events: events, repositories: repositories, services: services),
                openComputer: new OpenComputer(events: events),
                collectCargo: new CollectCargo(repositories: repositories),
                selectMissionDrone:  new SelectMissionDrone(events: events, repositories: repositories),
                selectSendDroneTab: new SelectSendDroneTab(events: events, repositories: repositories),
                selectDroneStatusTab: new SelectDroneStatusTab(events: events, repositories: repositories),
                selectDroneCardInStatus: new SelectDroneCardInStatus(events: events),
                returnDrone: new ReturnDrone(events: events, repositories: repositories),
                openWorkstation: new OpenWorkstation(events: events),
                selectModulesTab: new SelectModulesTab(events: events, repositories: repositories),
                selectAssemblyTab: new SelectAssemblyTab(events: events, repositories),
                selectFrame: new SelectFrame(events: events, repositories: repositories),
                confirmAssemblyFrame: new ConfirmAssemblyFrame(events: events, repositories: repositories),
                installAssemblyModule: new InstallAssemblyModule(events: events, repositories: repositories),
                selectModuleTypeToDisplay: new SelectModuleTypeToDisplay(events: events, repositories: repositories),
                selectFramesTab: new SelectFramesTab(events: events, repositories: repositories),
                selectFrameCard: new SelectFrameCard(events: events),
                selectMissionType: new SelectMissionType(events: events, repositories: repositories),
                selectMapTab: new SelectMapTab(events: events, repositories: repositories),
                selectNewPin: new SelectNewPin(events: events, repositories: repositories),
                selectNewPinCoordinates: new SelectNewPinCoordinates(events: events, repositories: repositories),
                choosePinName: new ChoosePinName(events: events, repositories: repositories),
                choosePinType: new ChoosePinType(events: events, repositories: repositories),
                saveNewPin: new SaveNewPin(events: events, repositories: repositories, services: services),
                selectPinnedLocationsSection: new SelectPinnedLocationsSection(events: events, repositories: repositories),
                nameDrone: new NameDrone(events: events, repositories: repositories, services: services),
                openMap: new OpenMap(events: events, repositories: repositories),
                selectDestination: new SelectDestination(events: events, repositories: repositories),
                selectWorkstationDronesTab: new SelectWorkstationDronesTab(events: events, gameplayRepositories: repositories),
                selectWorkstationDrone: new SelectWorkstationDrone(events: events, repositories: repositories, services: services),
                rechargeDrone: new RechargeDrone(events: events, repositories: repositories, services: services),
                repairDrone: new RepairDrone(events: events, repositories: repositories, services: services),
                disassemblyDrone: new DisassemblyDrone(events: events, repositories: repositories, services: services),
                selectPinnedLocation: new SelectPinnedLocation(events: events),
                missionServicesReadyToCalculate: new MissionServicesReadyToCalculate(events: events, repositories: repositories) ,
                openHouseBattery: new OpenHouseBattery(events: events, repositories: repositories),
                openBalcony: new OpenBalcony(events: events, repositories: repositories),
                uninstallAssemblyModule: new UninstallAssemblyModule(events: events, repositories: repositories)
            );

        static GameplayRepositories CreateRepositories(
            IEnumerable<Drone> loadedDrones,
            IEnumerable<Frame> loadedFrames,
            IEnumerable<Module> loadedModules,
            IEnumerable<Destination> loadedDestinations,
            IEnumerable<SolarGenerator> loadedSolarGenerators,
            IEnumerable<HouseBattery> loadedHouseBatteries,
            MissionsModifiers loadedTimeCostsModifiers
        ) =>
            new GameplayRepositories
            (
                dronesRepository: new InMemoryDronesRepository(loadedDrones),
                houseItemsRepository: new InMemoryHouseItemsRepository(),
                componentsRepository: new InMemoryComponentRepository(frames: loadedFrames, modules: loadedModules),
                droneInProgressRepository: new InMemoryDroneInProgressRepository(),
                mapRepository: new InMemoryMapRepository(withDestinations: loadedDestinations),
                pinInProgressRepository: new InMemoryPinInProgressRepository(),
                missionBuilderRepository: new InMemoryMissionBuilderRepository(),
                houseResourcesRepository: new InMemoryHouseResourcesRepository(
                    withSolarGenerators: loadedSolarGenerators,
                    withHouseBatteries: loadedHouseBatteries
                ),
                modifiersRepository: new InMemoryModifiersRepository(
                    withTimeCostsModifiers: loadedTimeCostsModifiers),
                missionsRepository: new InMemoryMissionsRepository()
            );

        static void DoSubscriptions(Presenters presenters)
        {
            var contextsPresenter = presenters.ContextsPresenter;
            var computerPresenter = presenters.ComputerPresenter;
            var workstationPresenter = presenters.WorkstationPresenter;
            var assemblyPresenter = presenters.AssemblyPresenter;
            var mapPresenter = presenters.MapPresenter;
            var sendDronePresenter = presenters.SendDronePresenter;
            var houseEnergyPresenter = presenters.HouseEnergyPresenter;
            var timelinesPresenter = presenters.TimelinesPresenter;
            var missionsPresenter = presenters.MissionsPresenter;
            var balconyPresenter = presenters.BalconyPresenter;
            var dronesPresenter = presenters.WorkstationDronesPresenter;

            contextsPresenter.DoSubscriptions(
                new CompositeDisposable(),
                computerPresenter.Disposables
                    .Concat(workstationPresenter.Disposables)
                    .Concat(assemblyPresenter.Disposables)
                    .Concat(mapPresenter.Disposables)
                    .Concat(sendDronePresenter.Disposables)
                    .Concat(houseEnergyPresenter.Disposables)
                    .Concat(timelinesPresenter.Disposables)
                    .Concat(missionsPresenter.Disposables)
                    .Concat(balconyPresenter.Disposables)
                    .Concat(dronesPresenter.Disposables)
            );
        }
        
    }
}