﻿using Bonfire.Gameplay.Delivery.Presentation;
using Bonfire.Gameplay.Delivery.Presentation.Balcony;
using Bonfire.Gameplay.Delivery.Presentation.Computer;
using Bonfire.Gameplay.Delivery.Presentation.House;
using Bonfire.Gameplay.Delivery.Presentation.Missions;
using Bonfire.Gameplay.Delivery.Presentation.Workstation;

namespace Bonfire.Gameplay
{
    public class Presenters
    {
        public readonly ContextsPresenter ContextsPresenter;
        public readonly ComputerPresenter ComputerPresenter;
        public readonly WorkstationPresenter WorkstationPresenter;
        public readonly AssemblyPresenter AssemblyPresenter;
        public readonly MapPresenter MapPresenter;
        public readonly SendDronePresenter SendDronePresenter;
        public readonly HouseEnergyPresenter HouseEnergyPresenter;
        public readonly TimelinesPresenter TimelinesPresenter;
        public readonly MissionsPresenter MissionsPresenter;
        public readonly BalconyPresenter BalconyPresenter;
        public readonly WorkstationDronesPresenter WorkstationDronesPresenter;

        public Presenters(
            ContextsPresenter contextsPresenter,
            ComputerPresenter computerPresenter, 
            WorkstationPresenter workstationPresenter,
            AssemblyPresenter assemblyPresenter, 
            MapPresenter mapPresenter, 
            SendDronePresenter sendDronePresenter,
            HouseEnergyPresenter houseEnergyPresenter,
            TimelinesPresenter timelinesPresenter, 
            MissionsPresenter missionsPresenter,
            BalconyPresenter balconyPresenter,
            WorkstationDronesPresenter workstationDronesPresenter
        ){
            ContextsPresenter = contextsPresenter;
            ComputerPresenter = computerPresenter;
            WorkstationPresenter = workstationPresenter;
            AssemblyPresenter = assemblyPresenter;
            MapPresenter = mapPresenter;
            SendDronePresenter = sendDronePresenter;
            HouseEnergyPresenter = houseEnergyPresenter;
            TimelinesPresenter = timelinesPresenter;
            MissionsPresenter = missionsPresenter;
            BalconyPresenter = balconyPresenter;
            WorkstationDronesPresenter = workstationDronesPresenter;
        }
    }
}