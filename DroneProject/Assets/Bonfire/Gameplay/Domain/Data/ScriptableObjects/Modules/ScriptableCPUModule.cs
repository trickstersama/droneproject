﻿using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.Modules
{
    [CreateAssetMenu(fileName = "DroneWeaponModule", menuName = "Data/Modules/CPU")]
    public class ScriptableCPUModule : ScriptableObject
    {
        public CPUModule CPU;
    }
}