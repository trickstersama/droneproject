﻿using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.Modules
{
    [CreateAssetMenu(fileName = "DroneBatteryModule", menuName = "Data/Modules/Battery")]
    public class ScriptableBatteryModule : ScriptableObject
    {
        public BatteryModule battery;
    }
    
    
}