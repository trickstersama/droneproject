﻿using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.Modules
{
    [CreateAssetMenu(fileName = "DroneArmorModule", menuName = "Data/Modules/Armor")]
    public class ScriptableArmorModule : ScriptableObject
    {
        public ArmorModule armor;
    }
}