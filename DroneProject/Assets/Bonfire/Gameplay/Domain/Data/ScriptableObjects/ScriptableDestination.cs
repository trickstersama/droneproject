﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects
{    
    [CreateAssetMenu(fileName = "Destination", menuName = "Data/Map/Destination")]
    public class ScriptableDestination : ScriptableObject
    {
        public Destination destination;
    }
}