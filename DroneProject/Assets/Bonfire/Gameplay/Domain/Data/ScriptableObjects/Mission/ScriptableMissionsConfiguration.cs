﻿using Bonfire.Gameplay.Domain.Configuration;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.Mission

{
    [CreateAssetMenu(fileName = "MissionConfig", menuName = "Data/MissionsConfigurations")]
    public class ScriptableMissionsConfiguration : ScriptableObject
    {
        public float secondsOnLootMission;
        public float secondsOnScoutMission;
        public float secondsOnBattleMission; 
        
        public MissionConfiguration GetMissionConfiguration() =>
            new MissionConfiguration(
                secondsOnBattleMission: secondsOnBattleMission,
                secondsOnLootMission: secondsOnLootMission,
                secondsOnScoutMission: secondsOnScoutMission
            );
    }
}