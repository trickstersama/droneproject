using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects
{
    [CreateAssetMenu(fileName = "ScriptableModifiers", menuName = "Data/Modifiers")]
    public class ScriptableMissionsModifiers : ScriptableObject
    {
        [Header("Mission Time")] 
        public float missionMultiplier;
        public float travelMultiplier;

        public MissionsModifiers GetMissionsModifiers()
            => new MissionsModifiers(
                missionMultiplier: missionMultiplier,
                travelMultiplier: travelMultiplier
            );
    }
}