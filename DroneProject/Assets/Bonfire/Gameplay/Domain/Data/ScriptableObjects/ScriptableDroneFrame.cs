﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects

{
    [CreateAssetMenu(fileName = "DroneFrame", menuName = "Data/Frame")]
    [Serializable]
    public class ScriptableDroneFrame : ScriptableObject
    {
        public Frame frame;
    }
}