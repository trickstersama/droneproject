﻿using Bonfire.Gameplay.Domain.ValueObjects.House;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.House

{
    [CreateAssetMenu(fileName = "SolarGenerator", menuName = "Data/House/Generators/Solar")]
    public class ScriptableSolarGenerator : ScriptableObject
    {
        public SolarGenerator generator;
    }
}