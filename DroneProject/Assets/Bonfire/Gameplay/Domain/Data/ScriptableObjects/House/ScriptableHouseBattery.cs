﻿using Bonfire.Gameplay.Domain.ValueObjects.House;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects.House

{
    [CreateAssetMenu(fileName = "HouseBattery", menuName = "Data/House/Battery")]
    public class ScriptableHouseBattery : ScriptableObject
    {
        public HouseBattery houseBattery;
    }
}