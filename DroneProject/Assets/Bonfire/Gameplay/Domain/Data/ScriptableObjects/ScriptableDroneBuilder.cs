using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Data.ScriptableObjects.Modules;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Drone", menuName = "Data/Drone")]
    public class ScriptableDroneBuilder : ScriptableObject
    {
        [SerializeField] string droneName;
        [SerializeField] float chargeAmount;
        [SerializeField] int damage;
        [SerializeField] ScriptableDroneFrame droneFrame;
        [SerializeField] DroneStatus droneStatus;
        
        [Header("Modules")] 
        [SerializeField] ScriptableWeaponModule[] weaponModules;
        [SerializeField] ScriptableBatteryModule[] batteryModules;
        [SerializeField] ScriptableSensorModule[] sensorModules;
        [SerializeField] ScriptableStorageModule[] storageModules;
        [SerializeField] ScriptablePropulsionModule[] propulsionModules;
        [SerializeField] ScriptableCPUModule[] CPUModules;
        [SerializeField] ScriptableArmorModule[] armorModules;
        
        public Drone GetDrone() => Build();

        Drone Build() =>
            new Drone
            {
                droneName = droneName,
                frame = droneFrame.frame,
                status = droneStatus,
                modules = GetModules(),
                charge = chargeAmount,
                damageReceived = damage,
                modifiers = GetModifiers()
            };

        DroneModifiers GetModifiers()
        {
            var allModules = GetModules();

            return allModules
                .Aggregate(new DroneModifiers(), (current, module) => current + module.GetDroneModifiers())
                + droneFrame.frame.GetModifiers();
        }

        List<Module> GetModules() =>
            armorModules.Select(module => module.armor as Module)
                .Concat(batteryModules.Select(module => module.battery))
                .Concat(sensorModules.Select(module => module.sensor))
                .Concat(storageModules.Select(module => module.storage))
                .Concat(propulsionModules.Select(module => module.propulsion))
                .Concat(CPUModules.Select(module => module.CPU))
                .Concat(weaponModules.Select(module => module.weapon)).ToList();
    }
}
