using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Configuration;
using Bonfire.Gameplay.Domain.Data.ScriptableObjects;
using Bonfire.Gameplay.Domain.Data.ScriptableObjects.House;
using Bonfire.Gameplay.Domain.Data.ScriptableObjects.Mission;
using Bonfire.Gameplay.Domain.Data.ScriptableObjects.Modules;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine;

namespace Bonfire.Gameplay.Domain.Data
{
    public static class ScriptableDataLoader
    {
        public static IEnumerable<Destination> LoadDestinations() => 
            Resources.LoadAll<ScriptableDestination>("Destinations")
                .Select(destination => destination.destination).ToList();

        public static IEnumerable<Drone> LoadDrones() =>
            Resources.LoadAll<ScriptableDroneBuilder>("Drones")
                .Select(drone => drone.GetDrone());

        public static IEnumerable<Module> LoadModules() =>
            Resources.LoadAll<ScriptableArmorModule>("Modules/Armor")
                .Select(module => module.armor as Module)
                .Concat(Resources.LoadAll<ScriptableBatteryModule>("Modules/Battery")
                    .Select(module => module.battery as Module))
                .Concat(Resources.LoadAll<ScriptableCPUModule>("Modules/CPU")
                    .Select(module => module.CPU as Module))
                .Concat(Resources.LoadAll<ScriptablePropulsionModule>("Modules/Propulsion")
                    .Select(module => module.propulsion as Module))
                .Concat(Resources.LoadAll<ScriptableSensorModule>("Modules/Sensor")
                    .Select(module => module.sensor as Module))
                .Concat(Resources.LoadAll<ScriptableStorageModule>("Modules/Storage")
                    .Select(module => module.storage as Module))
                .Concat(Resources.LoadAll<ScriptableWeaponModule>("Modules/Weapon")
                    .Select(module => module.weapon as Module));

        public static IEnumerable<Frame> LoadFrames() => 
            Resources.LoadAll<ScriptableDroneFrame>("Frames")
                .Select(frame => frame.frame).ToList();

        public static IEnumerable<SolarGenerator> LoadSolarGenerators() => 
            Resources.LoadAll<ScriptableSolarGenerator>("House/Generators")
                .Select(scriptableObject => scriptableObject.generator);

        public static IEnumerable<HouseBattery> LoadHouseBatteries() => 
            Resources.LoadAll<ScriptableHouseBattery>("House/Batteries")
                .Select(scriptableObject => scriptableObject.houseBattery);

        public static MissionConfiguration LoadMissionConfiguration() =>
            Resources.Load<ScriptableMissionsConfiguration>("Missions/Configuration/MissionsConfiguration").GetMissionConfiguration();

        public static MissionsModifiers LoadTimeCostModifiers() =>
            Resources.Load<ScriptableMissionsModifiers>("Missions/Modifiers/MissionModifiers").GetMissionsModifiers();
    }
}
