﻿namespace Bonfire.Gameplay.Domain.ValueObjects.House
{
    public readonly struct HouseEnergyStatus
    {
        public readonly float productionPerSecond;
        public readonly float consumptionPerSecond;
        public readonly float capacity;

        public HouseEnergyStatus(
            float productionPerSecond, 
            float consumptionPerSecond,
            float capacity
        ) {
            this.productionPerSecond = productionPerSecond;
            this.consumptionPerSecond = consumptionPerSecond;
            this.capacity = capacity;
        }

        public HouseEnergyStatus ModifyThis(
            float? withProduction = null,
            float? withConsumption = null,
            float? withCapacity = null
        ) =>
            new HouseEnergyStatus(
                productionPerSecond: withProduction ?? productionPerSecond,
                consumptionPerSecond: withConsumption ?? consumptionPerSecond,
                capacity: withCapacity ?? capacity
            );
    }
}