﻿using System;

namespace Bonfire.Gameplay.Domain.ValueObjects.House
{
    [Serializable]
    public struct SolarGenerator
    {
        public string generatorName;
        public int generatorSize;
        public float EnergyPerSecond;
    }
    
    public struct EnergyReactor
    {
        public float EnergyPerSecond;
    }

    public struct WindGenerator
    {
        public float EnergyPerSecond;
    }
}