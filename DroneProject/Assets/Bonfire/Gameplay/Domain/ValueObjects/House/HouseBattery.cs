﻿using System;

namespace Bonfire.Gameplay.Domain.ValueObjects.House

{
    [Serializable]
    public struct HouseBattery
    {
        public string name;
        public string description;
        public float storageSize;
    }
}