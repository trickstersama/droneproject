﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class PropulsionModule : Module
    {
        public int thrust; //grams
        public int stealth;
        public int consumption; //watts/h
        public EngineTechnology engineTechnology;
        
        public override DroneModifiers GetDroneModifiers()
        {
            return new DroneModifiers(thrust: thrust, stealth: stealth, consumption: consumption);
        }

        public override string GetFormattedStats() =>
            $"Thrust: {thrust} \n" +
            $"Consumption: {consumption} \n" +
            $"Stealth: {stealth} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is PropulsionModule && base.Equals(obj))
            {
                var otherModule = (PropulsionModule) obj;
                if (thrust == otherModule.thrust &&
                    stealth == otherModule.stealth &&
                    consumption == otherModule.consumption &&
                    engineTechnology?.Type() == otherModule?.engineTechnology?.Type()
                ) {
                    return true;
                }
            }
            return false;
        }
        
        public interface EngineTechnology
        {
            EngineTechType Type();
        }

        public class Multirotor : EngineTechnology
        {
            EngineTechType techType;

            public Multirotor()
            {
                techType = EngineTechType.Multirotor;
            }

            public EngineTechType Type()
            {
                return techType;
            }
        }
        public class Ionic : EngineTechnology
        {
            EngineTechType techType;

            public Ionic()
            {
                techType = EngineTechType.Ionic;
            }

            public EngineTechType Type()
            {
                return techType;
            }
        }
        public class AntiGravitational : EngineTechnology
        {
            EngineTechType techType;

            public AntiGravitational()
            {
                techType = EngineTechType.AntiGravitational;
            }

            public EngineTechType Type()
            {
                return techType;
            }
        }
    }
}