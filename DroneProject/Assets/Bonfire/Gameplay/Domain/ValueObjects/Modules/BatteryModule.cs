﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class BatteryModule : Module
    {
        const float Tolerance = 1f;

        public float weight;
        public int powerCapacity; //en watts
        public override DroneModifiers GetDroneModifiers() => 
            new DroneModifiers(
                weight: weight, 
                powerCapacity: powerCapacity
            );

        public override string GetFormattedStats() =>
            $"Weight: {weight} \n" +
            $"Power Capacity: {powerCapacity} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is BatteryModule otherModule && base.Equals(otherModule))
            {
                if (Math.Abs(weight - otherModule.weight) < Tolerance &&
                    powerCapacity == otherModule.powerCapacity
                )
                    return true;
            }
            return false;
        }
    }
}