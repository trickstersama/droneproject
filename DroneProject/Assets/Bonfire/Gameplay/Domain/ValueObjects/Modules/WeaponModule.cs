﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class WeaponModule : Module
    {
        const float Tolerance = 1f;

        public int damage;
        public float weight;
        public WeaponType weaponType;
        public override DroneModifiers GetDroneModifiers() => 
            new DroneModifiers(
                damage: damage, 
                weight: weight, 
                weaponType: weaponType
            );

        public override string GetFormattedStats()
        {
            return $" Damage: {damage} \n" +
                   $" Weight: {weight} \n" +
                   $" Weapon Type: {weaponType}";
        }

        public override bool Equals(object obj)
        {
            if (obj is WeaponModule otherModule && base.Equals(otherModule))
            {
                if (Math.Abs(weight - otherModule.weight) < Tolerance &&
                    damage == otherModule.damage &&
                    weaponType == otherModule.weaponType
                )
                    return true;
            }
            return false;
        }
        
    }
}