﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class StorageModule : Module
    {
        public int reservedWeight;
        public StorageType storageType;
        public override DroneModifiers GetDroneModifiers()
        {
            return new DroneModifiers(
                reservedWeight: reservedWeight, 
                storageType: storageType);
        }

        public override string GetFormattedStats() =>
            $"Reserved weight: {reservedWeight} \n" +
            $"Storage Type: {storageType} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is StorageModule && base.Equals(obj))
            {
                var otherModule = (StorageModule) obj;
                if (reservedWeight == otherModule.reservedWeight &&
                    storageType == otherModule.storageType
                ) {
                    return true;
                }
            }
            return false;
        }
    }
}