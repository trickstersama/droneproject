﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class CPUModule: Module
    {
        public int stealth;
        public int damage;
        public int consumption;
        public override DroneModifiers GetDroneModifiers() =>
            new DroneModifiers(
                damage: damage, 
                stealth: stealth, 
                consumption: consumption
            );

        public override string GetFormattedStats() =>
            $"Damage: {damage} \n" +
            $"Consumption: {consumption} \n" +
            $"Stealth: {stealth} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is CPUModule && base.Equals(obj))
            {
                var otherModule = (CPUModule) obj;
                if (damage == otherModule.damage &&
                    stealth == otherModule.stealth &&
                    consumption == otherModule.consumption
                ) {
                    return true;
                }
            }
            return false;
        }

    }
}