﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public abstract class Module
    {
        public ModuleType moduleType;
        public string description;
        public string name;
        public abstract DroneModifiers GetDroneModifiers();
        public abstract string GetFormattedStats();
        public override bool Equals(object obj)
        {
            if (obj is Module)
            {
                var otherModule = (Module) obj;
                if (moduleType == otherModule.moduleType && 
                    description == otherModule.description &&
                    name == otherModule.name)
                {
                    return true;
                }
            }
            return false;
        }
    }
}