﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class ArmorModule : Module
    {
        const float Tolerance = 1f;

        public float weight;
        public int hardness;
        public int stealth;
        public int hitPoints;
        
        public override DroneModifiers GetDroneModifiers() => 
            new DroneModifiers(
                hardness: hardness,
                weight: weight,
                stealth: stealth,
                hitPoints: hitPoints
            );

        public override string GetFormattedStats()
        {
            return $"Hardness: {hardness} \n" +
                   $"Weight: {weight} \n" +
                   $"Stealth {stealth} \n" +
                   $"HitPoints {hitPoints}";
        }

        public override bool Equals(object obj)
        {
            if (obj is ArmorModule otherModule && base.Equals(otherModule))
            {
                if (
                    Math.Abs(weight - otherModule.weight) < Tolerance &&
                    hardness == otherModule.hardness && 
                    hitPoints == otherModule.hitPoints && 
                    stealth == otherModule.stealth)
                {
                    return true;
                }
            }
            return false;
        }
    }
}