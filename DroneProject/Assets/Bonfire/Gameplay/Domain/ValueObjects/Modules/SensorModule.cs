﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Modules
{
    [Serializable]
    public class SensorModule : Module
    {
        const float Tolerance = 1f;

        public float weight;
        public int damage;
        public int stealth;
        public override DroneModifiers GetDroneModifiers() => 
            new DroneModifiers(weight: weight, damage: damage, stealth: stealth);

        public override string GetFormattedStats() =>
            $"Weight: {weight} \n" +
            $"Damage: {damage} \n" +
            $"Stealth {stealth} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is SensorModule && base.Equals(obj))
            {
                var otherModule = (SensorModule) obj;
                if (Math.Abs(weight - otherModule.weight) < Tolerance &&
                    damage == otherModule.damage && 
                    stealth == otherModule.stealth
                )
                    return true;
            }
            return false;
        }
    }
}