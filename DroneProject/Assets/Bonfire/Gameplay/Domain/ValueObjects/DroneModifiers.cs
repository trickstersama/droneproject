﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    public readonly struct DroneModifiers
    {
        const float Tolerance = 1f;

        public readonly int hardness;
        public readonly float weight;
        public readonly int damage;
        public readonly int stealth;
        public readonly int powerCapacity;
        public readonly float reservedWeight;
        public readonly float thrust;
        public readonly int consumption;
        public readonly int hitPoints;
        public readonly StorageType storageType;
        public readonly WeaponType weaponType;
        public readonly float aerodynamics;
        public readonly float crossSectionArea;

        public DroneModifiers(
            int hardness = 0, 
            float weight = 0,
            int damage = 0,
            int stealth = 0,
            int powerCapacity = 0,
            float reservedWeight = 0,
            float thrust = 0,
            int consumption = 0,
            int hitPoints = 0,
            StorageType storageType = StorageType.Solid, 
            WeaponType weaponType = WeaponType.Laser,
            float aerodynamics = 0,
            float crossSectionArea = 0
        ) {
            this.hardness = hardness;
            this.weight = weight;
            this.damage = damage;
            this.stealth = stealth;
            this.hitPoints = hitPoints;
            this.powerCapacity = powerCapacity;
            this.reservedWeight = reservedWeight;
            this.thrust = thrust;
            this.consumption = consumption;

            this.storageType = storageType;
            this.weaponType = weaponType;
            this.aerodynamics = aerodynamics;
            this.crossSectionArea = crossSectionArea;
        }

        public static DroneModifiers operator +(DroneModifiers A, DroneModifiers B) =>
            new DroneModifiers(
                hardness: A.hardness + B.hardness,
                weight: A.weight + B.weight,
                damage: A.damage + B.damage,
                stealth: A.stealth + B.stealth,
                hitPoints: A.hitPoints + B.hitPoints,
                powerCapacity: A.powerCapacity + B.powerCapacity,
                reservedWeight: A.reservedWeight + B.reservedWeight,
                thrust: A.thrust + B.thrust,
                consumption: A.consumption + B.consumption,
                aerodynamics: A.aerodynamics + B.aerodynamics,
                crossSectionArea: A.crossSectionArea + B.crossSectionArea
            );

        public override string ToString() =>
            $"DroneModifiers-> hardness: {hardness}," +
            $" weight {weight}, \n" +
            $" damage: {damage}, \n" +
            $" stealth {stealth}, \n" +
            $" weaponType {weaponType}, \n" +
            $" powerCapacity {powerCapacity}, \n" +
            $" reserved Weight {reservedWeight} \n," +
            $" thrust {thrust} \n," +
            $" consumption {consumption} \n," +
            $" storage Type {storageType} \n" +
            $" aerodynamics: {aerodynamics} \n" +
            $" cross section area: {crossSectionArea} \n" +
            $" hit points {hitPoints}";
        
        public override bool Equals(object obj)
        {
            if (obj is DroneModifiers droneModifiers)
            {
                if (
                    hardness == droneModifiers.hardness  &&
                    Math.Abs(weight - droneModifiers.weight) < Tolerance &&
                    damage == droneModifiers.damage  &&
                    stealth == droneModifiers.stealth  &&
                    powerCapacity == droneModifiers.powerCapacity  &&
                    Math.Abs(reservedWeight - droneModifiers.reservedWeight) < Tolerance &&
                    Math.Abs(thrust - droneModifiers.thrust) < Tolerance &&
                    consumption == droneModifiers.consumption  &&
                    hitPoints == droneModifiers.hitPoints  &&
                    Math.Abs(aerodynamics - droneModifiers.aerodynamics) < Tolerance &&
                    Math.Abs(crossSectionArea - droneModifiers.crossSectionArea) < Tolerance &&
                    storageType == droneModifiers.storageType  &&
                    weaponType == droneModifiers.weaponType
                )
                    return true;
            }
            return false;
        }
    }
}