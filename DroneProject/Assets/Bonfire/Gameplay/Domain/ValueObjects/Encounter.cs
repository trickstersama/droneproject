﻿namespace Bonfire.Gameplay.Domain.ValueObjects
{
    public class Encounter
    {
        public Encounter()
        {
            Type = "default";
        }

        public string Type { get; set; }
    }
}