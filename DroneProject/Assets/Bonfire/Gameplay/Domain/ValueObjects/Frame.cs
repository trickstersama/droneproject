﻿using System;

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    //no puede ser readonly porque sino no puedo serializar en Unity
    [Serializable]
    public struct Frame
    {
        public string frameModel;
        public string description;
        public string name;
        public int weaponSlotsAmount;
        public int batterySlotsAmount;
        public int sensorSlotsAmount;
        public int propulsionSlotsAmount;
        public int cpuSlotsAmount;
        public int armorSlotsAmount;
        public int storageUnitSlotsAmount;

        public int hardness;
        public float weight;
        public int stealth;
        public int hitPoints;
        public float aerodynamics;
        public float crossSectionArea;

        public Frame(
            string description, 
            string name,
            string frameModel,
            
            int weaponSlotsAmount, 
            int batterySlotsAmount, 
            int sensorSlotsAmount, 
            int propulsionSlotsAmount, 
            int cpuSlotsAmount, 
            int armorSlotsAmount, 
            int storageUnitSlotsAmount,
            
            int hardness,
            float weight,
            int stealth,
            int hitPoints,
            float aerodynamics,
            float crossSectionArea
        ) {
            this.description = description;
            this.name = name;
            this.frameModel = frameModel;
            
            this.weaponSlotsAmount = weaponSlotsAmount;
            this.batterySlotsAmount = batterySlotsAmount;
            this.sensorSlotsAmount = sensorSlotsAmount;
            this.propulsionSlotsAmount = propulsionSlotsAmount;
            this.cpuSlotsAmount = cpuSlotsAmount;
            this.armorSlotsAmount = armorSlotsAmount;
            this.storageUnitSlotsAmount = storageUnitSlotsAmount;

            this.hardness = hardness;
            this.weight = weight;
            this.stealth = stealth;
            this.hitPoints = hitPoints;
            this.aerodynamics = aerodynamics;
            this.crossSectionArea = crossSectionArea;
        }

        public DroneModifiers GetModifiers() =>
            new DroneModifiers(
                hardness: hardness,
                weight: weight,
                hitPoints: hitPoints,
                stealth: stealth,
                aerodynamics: aerodynamics,
                crossSectionArea: crossSectionArea
            );

        public int GetSlotsAmountByModuleType(ModuleType moduleType) =>
            moduleType switch
            {
                ModuleType.Weapon => weaponSlotsAmount,
                ModuleType.Battery => batterySlotsAmount,
                ModuleType.Sensor => sensorSlotsAmount,
                ModuleType.Storage => storageUnitSlotsAmount,
                ModuleType.Propulsion => propulsionSlotsAmount,
                ModuleType.CPU => cpuSlotsAmount,
                ModuleType.Armor => armorSlotsAmount,
                _ => throw new ArgumentOutOfRangeException(nameof(moduleType), moduleType, null)
            };


        public Frame DecreaseSlot(ModuleType type)
        {
            return ModifyThis(
                weaponSlotsAmount: type == ModuleType.Weapon? weaponSlotsAmount - 1 : weaponSlotsAmount,
                batterySlotsAmount: type == ModuleType.Battery? batterySlotsAmount - 1 : batterySlotsAmount,
                sensorSlotsAmount: type == ModuleType.Sensor? sensorSlotsAmount - 1 : sensorSlotsAmount,
                storageUnitSlotsAmount: type == ModuleType.Storage? storageUnitSlotsAmount - 1 : storageUnitSlotsAmount,
                propulsionSlotsAmount: type == ModuleType.Propulsion? propulsionSlotsAmount - 1 : propulsionSlotsAmount,
                cpuSlotsAmount: type == ModuleType.CPU? cpuSlotsAmount - 1 : cpuSlotsAmount,
                armorSlotsAmount: type == ModuleType.Armor? armorSlotsAmount - 1 : armorSlotsAmount

            );
        }
        public Frame ResetSlotsAmount(ModuleType type, int originalAmount) =>
            ModifyThis(
                weaponSlotsAmount: type == ModuleType.Weapon? originalAmount : weaponSlotsAmount,
                batterySlotsAmount: type == ModuleType.Battery? originalAmount : batterySlotsAmount,
                sensorSlotsAmount: type == ModuleType.Sensor? originalAmount : sensorSlotsAmount,
                storageUnitSlotsAmount: type == ModuleType.Storage? originalAmount : storageUnitSlotsAmount,
                propulsionSlotsAmount: type == ModuleType.Propulsion? originalAmount : propulsionSlotsAmount,
                cpuSlotsAmount: type == ModuleType.CPU? originalAmount : cpuSlotsAmount,
                armorSlotsAmount: type == ModuleType.Armor? originalAmount : armorSlotsAmount
            );

        public Frame ReleaseSlot(ModuleType type) =>
            ModifyThis(
                weaponSlotsAmount: type == ModuleType.Weapon? weaponSlotsAmount + 1 : weaponSlotsAmount,
                batterySlotsAmount: type == ModuleType.Battery? batterySlotsAmount + 1 : batterySlotsAmount,
                sensorSlotsAmount: type == ModuleType.Sensor? sensorSlotsAmount + 1 : sensorSlotsAmount,
                storageUnitSlotsAmount: type == ModuleType.Storage? storageUnitSlotsAmount + 1 : storageUnitSlotsAmount,
                propulsionSlotsAmount: type == ModuleType.Propulsion? propulsionSlotsAmount + 1 : propulsionSlotsAmount,
                cpuSlotsAmount: type == ModuleType.CPU? cpuSlotsAmount + 1 : cpuSlotsAmount,
                armorSlotsAmount: type == ModuleType.Armor? armorSlotsAmount + 1 : armorSlotsAmount
            );
        
        public override string ToString()
        {
            return $"\n Slots Amounts: {weaponSlotsAmount} - {batterySlotsAmount} -{sensorSlotsAmount} - {storageUnitSlotsAmount} - {propulsionSlotsAmount} - {cpuSlotsAmount} - {armorSlotsAmount}" +
                   $"\n Stats: hardness : {hardness}, weight: {weight}, stealth: {stealth}, hitPoints: {hitPoints}, aerodynamics {aerodynamics}, crossSectionArea {crossSectionArea}" +
                   $"\n Rest: name {name}, description {description}, frameModel {frameModel}";
        }

        Frame ModifyThis(
                string name = null,
                string description = null,
                string frameModel = null,
                int? weaponSlotsAmount = null,
                int? batterySlotsAmount = null,
                int? sensorSlotsAmount = null,
                int? propulsionSlotsAmount = null,
                int? cpuSlotsAmount = null,
                int? armorSlotsAmount = null,
                int? storageUnitSlotsAmount = null,

                int? hardness = null,
                float? weight = null,
                int? stealth = null,
                int? hitPoints = null,
                float? aerodynamics = null,
                float? crossSectionArea = null
            ) =>
            new(
                name: name ?? this.name,
                description: description ?? this.description,
                frameModel: frameModel ?? this.frameModel,
                
                weaponSlotsAmount: weaponSlotsAmount ?? this.weaponSlotsAmount,
                batterySlotsAmount: batterySlotsAmount ?? this.batterySlotsAmount,
                sensorSlotsAmount: sensorSlotsAmount ?? this.sensorSlotsAmount,
                storageUnitSlotsAmount: storageUnitSlotsAmount ?? this.storageUnitSlotsAmount,
                propulsionSlotsAmount:  propulsionSlotsAmount ?? this.propulsionSlotsAmount,
                cpuSlotsAmount: cpuSlotsAmount ?? this.cpuSlotsAmount,
                armorSlotsAmount: armorSlotsAmount ?? this.armorSlotsAmount,
                
                hardness: hardness ?? this.hardness,
                weight: weight ?? this.weight,
                stealth: stealth ?? this.stealth,
                hitPoints: hitPoints ?? this.hitPoints,
                aerodynamics: aerodynamics ?? this.aerodynamics,
                crossSectionArea: crossSectionArea ?? this.crossSectionArea
            );
    }
}