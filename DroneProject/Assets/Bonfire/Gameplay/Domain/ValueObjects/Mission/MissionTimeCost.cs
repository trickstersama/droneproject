﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public struct MissionTimeCost
    {
        const float Tolerance = 1f;

        public readonly float toTarget;
        public readonly float onTarget;
        public readonly float toBaseWithMaxWeight;
        public float toBaseWithMaxAllowed;

        public MissionTimeCost(
            float toTarget, 
            float onTarget,
            float toBaseWithMaxWeight,
            float toBaseWithMaxAllowed
        ) {
            this.toTarget = toTarget;
            this.onTarget = onTarget;
            this.toBaseWithMaxWeight = toBaseWithMaxWeight;
            this.toBaseWithMaxAllowed = toBaseWithMaxAllowed;
        }
        
        public override bool Equals(object obj)
        {
            if (obj is MissionTimeCost missionTimeCost)
            {
                if (
                    Math.Abs(toTarget - missionTimeCost.toTarget) < Tolerance &&
                    Math.Abs(onTarget - missionTimeCost.onTarget) < Tolerance &&
                    Math.Abs(toBaseWithMaxAllowed - missionTimeCost.toBaseWithMaxAllowed) < Tolerance &&
                    Math.Abs(toBaseWithMaxWeight - missionTimeCost.toBaseWithMaxWeight) < Tolerance
                )
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"Time to mission: {toTarget} \n" +
                   $"on mission: {onTarget} \n" +
                   $"to base: {toBaseWithMaxWeight} \n" +
                   $"max return weight: {toBaseWithMaxAllowed}";
        }
    }
}