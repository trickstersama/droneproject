﻿using System;

#pragma warning disable CS0659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public struct MissionResults
    {        
        const float Threshold = .1f;

        public readonly string droneName;
        public readonly float damageReceived;
        public readonly MissionType missionType;
        public readonly TimeOnMission timeOnMission;
        
        public MissionResults(string droneName, float damageReceived, MissionType missionType, TimeOnMission timeOnMission)
        {
            this.droneName = droneName;
            this.damageReceived = damageReceived;
            this.missionType = missionType;
            this.timeOnMission = timeOnMission;
        }

        public override bool Equals(object obj)
        {
            if (obj is MissionResults missionResults)
            {
                if (
                    droneName == missionResults.droneName &&
                    Math.Abs(damageReceived - missionResults.damageReceived) < Threshold &&
                    missionType == missionResults.missionType &&
                    timeOnMission.Equals(missionResults.timeOnMission)
                )
                    return true;
            }
            return false;
        }

        public override string ToString() =>
            $"Drone Name: {droneName} \n" +
            $"Drone Damage: {damageReceived} \n" +
            $"Mission Type: {missionType} \n" +
            $"Time On Mission {timeOnMission} \n";
    }
}