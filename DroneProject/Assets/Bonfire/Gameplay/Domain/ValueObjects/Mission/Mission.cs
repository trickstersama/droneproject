﻿
namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public readonly struct Mission
    {
        public readonly MissionOrders missionOrders;
        public readonly TimeOnMission timeOnMission;
        public readonly MissionProgression missionProgression;

        public Mission(
            MissionOrders missionOrders, 
            TimeOnMission timeOnMission,
            MissionProgression missionProgression
        ) {
            this.missionOrders = missionOrders;
            this.timeOnMission = timeOnMission;
            this.missionProgression = missionProgression;
        }

        public override string ToString() => 
            $"Mission: {missionOrders} \n {timeOnMission} \n {missionProgression}";
    }
}