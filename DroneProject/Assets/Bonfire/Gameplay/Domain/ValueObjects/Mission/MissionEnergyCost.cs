﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public struct MissionEnergyCost
    {
        const float Tolerance = 1f;
        public readonly float energyToTarget;
        public readonly float tbd;
        public readonly float energyToReturnWithMaxLoad;

        public MissionEnergyCost(
            float energyToTarget,
            float TBD,
            float energyToReturnWithMaxLoad
        ) {
            this.energyToTarget = energyToTarget;
            tbd = TBD;
            this.energyToReturnWithMaxLoad = energyToReturnWithMaxLoad;
        }

        public override bool Equals(object obj)
        {
            if (obj is MissionEnergyCost missionEnergy)
            {
                if (
                    Math.Abs(energyToTarget - missionEnergy.energyToTarget) < Tolerance &&
                    Math.Abs(tbd - missionEnergy.tbd) < Tolerance &&
                    Math.Abs(energyToReturnWithMaxLoad - missionEnergy.energyToReturnWithMaxLoad) < Tolerance
                )
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"Energy cost to target: {energyToTarget} \n" +
                   $"energy to return with max load: {energyToReturnWithMaxLoad}";
        }
    }
}