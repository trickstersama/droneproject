﻿using System;

#pragma warning disable CS0659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public readonly struct TimeOnMission
    {
        const float Threshold = .1f;

        public readonly float timePassed;

        public TimeOnMission(float timePassed) => 
            this.timePassed = timePassed;

        public override string ToString() => 
            $"TimeOnMission: {timePassed} \n";
        
        public override bool Equals(object obj)
        {
            if (obj is TimeOnMission timeOnMission)
            {
                if (Math.Abs(timePassed - timeOnMission.timePassed) < Threshold)
                    return true;
            }
            return false;
        }
    }
}