namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public struct MissionsModifiers
    {
        public readonly float missionMultiplier;
        public readonly float travelMultiplier;

        public MissionsModifiers(float missionMultiplier, float travelMultiplier)
        {
            this.missionMultiplier = missionMultiplier;
            this.travelMultiplier = travelMultiplier;
        }
    }
}