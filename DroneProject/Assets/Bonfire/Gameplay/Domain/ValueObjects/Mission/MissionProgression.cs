﻿using System;

#pragma warning disable CS0659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public readonly struct MissionProgression
    {
        const float threshold = 1f;

        readonly float droneDamage;

        public MissionProgression(float DroneDamage) => 
            droneDamage = DroneDamage;

        public override bool Equals(object obj)
        {
            if (obj is MissionProgression missionProgression)
            {
                if (Math.Abs(droneDamage - missionProgression.droneDamage) < threshold) 
                    return true;
            }
            return false;
        }

        public override string ToString() => $"Drone damage: {droneDamage}";
    }
}