﻿using System;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects.Mission
{
    public readonly struct MissionOrders
    {
        const float threshold = 1f;
        public readonly MissionType missionType;
        public readonly MapCoordinates mapCoordinates;
        public readonly string droneName;
        public readonly MissionEnergyCost missionEnergyCost;
        public readonly MissionTimeCost missionTimeCost;
        public readonly float maxWeightAllowed;

        public MissionOrders(
            MissionType missionType, 
            MapCoordinates mapCoordinates,
            string droneName,
            MissionEnergyCost missionEnergyCost,
            MissionTimeCost missionTimeCost,
            float maxWeightAllowed
        ) {
            this.missionType = missionType;
            this.mapCoordinates = mapCoordinates;
            this.droneName = droneName;
            this.missionEnergyCost = missionEnergyCost;
            this.missionTimeCost = missionTimeCost;
            this.maxWeightAllowed = maxWeightAllowed;
        }

        public override bool Equals(object obj)
        {
            if (obj is MissionOrders)
            {
                var missionOrders = (MissionOrders) obj;
                if (
                    missionType.Equals(missionOrders.missionType) &&
                    mapCoordinates.Equals(missionOrders.mapCoordinates) &&
                    droneName == missionOrders.droneName &&
                    missionEnergyCost.Equals(missionOrders.missionEnergyCost) &&
                    missionTimeCost.Equals(missionOrders.missionTimeCost) &&
                    Math.Abs(maxWeightAllowed - missionOrders.maxWeightAllowed) < threshold
                ) 
                    return true;
            }
            return false;
        }

        public static MissionOrders Builder(
            MissionOrders original,
            MissionType? withMissionType = null,
            MapCoordinates? withDestination = null,
            string withDroneName = null,
            MissionEnergyCost?  withMissionEnergyCost = null,
            MissionTimeCost? withTimeCost = null,
            float? withMaxWeightAllowed = null
        ) =>
            new MissionOrders(
                missionType: withMissionType ?? original.missionType,
                mapCoordinates: withDestination ?? original.mapCoordinates,
                droneName: withDroneName ?? original.droneName,
                missionEnergyCost: withMissionEnergyCost ?? original.missionEnergyCost,
                missionTimeCost: withTimeCost  ?? original.missionTimeCost,
                maxWeightAllowed: withMaxWeightAllowed ?? original.maxWeightAllowed
            );

        public override string ToString() =>
            $"Mission orders: " +
            $"mission type: {missionType} \n" +
            $"and destination: {mapCoordinates} \n" +
            $"and drone assigned: {droneName} \n" +
            $"and mission energy cost: {missionEnergyCost} \n" +
            $"and mission time cost: {missionTimeCost} \n" +
            $"max weight allowed: {maxWeightAllowed} \n";
    }
}