﻿using System.Collections.Generic;

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    public enum WeaponType
    {
        debug, Laser, Plasma, Ballistic
    }

    public enum StorageType
    {
        debug, Solid, Liquid, Food, Living, Corrosive
    }

    public enum MissionType
    {
        Debug, Scout, Loot, Battle, 
    }
    public enum EngineTechType
    {
        debug, Multirotor, Ionic, AntiGravitational
    }

    public enum SensorType
    {
        debug, Infrared, Bionic, Metallic, Weather, Food, Liquid
    }

    public enum ModuleType
    {
        debug, Weapon, Battery, Sensor, Storage, Propulsion, CPU, Armor
    }
    
    public enum DroneStatus {
        debug,
        Idle, 
        Busy, 
        Returning, 
        Traveling,
        OnTarget,
        InBalcony
    }
    
    public enum ZoomType   
    {
        debug, ZoomIn, ZoomOut
    }

    public static class EnumeratesUtilities
    {
        public static Dictionary<string, ModuleType> ModuleTypeByStringSingular()
        {
            return new Dictionary<string, ModuleType>
            {
                { "Weapon", ModuleType.Weapon },
                { "Battery", ModuleType.Battery },
                { "Sensor", ModuleType.Sensor },
                { "Storage", ModuleType.Storage },
                {"Propulsion", ModuleType.Propulsion},
                { "CPU", ModuleType.CPU },
                { "Armor", ModuleType.Armor }
            };
        }
    }
}