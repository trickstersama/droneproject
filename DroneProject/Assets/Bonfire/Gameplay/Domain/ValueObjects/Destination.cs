﻿using System;

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    [Serializable]
    public struct Destination
    {
        public MapCoordinates mapCoordinates;
        public MissionType missionType;
        public string name;

        public Destination(
            MapCoordinates mapCoordinates,
            MissionType missionType, 
            string name
        ) {
            this.mapCoordinates = mapCoordinates;
            this.missionType = missionType;
            this.name = name;
        }

        public override string ToString() => 
            $"destination named {name} is: {mapCoordinates} coordinates and of type {missionType}.";

        public Destination ModifyThis(
            MapCoordinates? withMapCoordinates= null,
            MissionType? withMissionType = null,
            string withName = null
        ) =>
            new Destination(
                mapCoordinates: withMapCoordinates ?? mapCoordinates,
                missionType: withMissionType ?? missionType,
                name: withName ?? name
            );

        public string ToFormattedString()
        {
            var mapCoordinatesName = string.IsNullOrEmpty(name) 
                ? "Destination name not yet set. \n" 
                : $"Destination named {name} is:";

            var missionTypeText = missionType == MissionType.Debug 
                ? "not yet set" 
                : missionType.ToString();
            
            return $"{mapCoordinatesName} {mapCoordinates} coordinates of type {missionTypeText}.";
        }
    }
}