﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

#pragma warning disable 659

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    [Serializable]
    public struct Drone
    {
        const float Tolerance = 0.1f;
        
        public string droneName;
        public DroneStatus status;
        public Frame frame;
        public DroneModifiers modifiers;
        public List<Module> modules;
        public float charge;
        public int damageReceived;

        public Drone(
            string droneName,
            DroneStatus status,
            Frame frame,
            DroneModifiers modifiers,
            IEnumerable<Module> modules,
            float charge,
            int damageReceived = 0
        ) {
            this.droneName = droneName;
            this.status = status;
            this.frame = frame;
            this.modifiers = modifiers;
            this.modules = modules.ToList();
            this.charge = charge;
            this.damageReceived = damageReceived;
        }

        public Drone ModifyThis(
            string withName = null,
            DroneStatus? withStatus = null,
            Frame? withFrame = null,
            DroneModifiers? withModifiers = null,
            IEnumerable<Module> withModules = null,
            float? withCharge = null,
            int? withDamageReceived = null
        )
        {
            return new Drone
            {
                droneName = withName ?? droneName,
                status = withStatus ?? status,
                frame = withFrame ?? frame,
                modifiers = withModifiers ?? modifiers,
                modules = (List<Module>) withModules ?? modules,
                charge = withCharge ?? charge,
                damageReceived = withDamageReceived ?? damageReceived
            };
        }
        
        
        public override bool Equals(object obj)
        {
            if (obj is Drone)
            {
                var drone = (Drone) obj;
                if (droneName == drone.droneName && 
                    status == drone.status &&
                    Math.Abs(charge - drone.charge) < Tolerance &&
                    damageReceived == drone.damageReceived &&
                    modifiers.Equals(drone.modifiers) &&
                    frame.Equals(drone.frame) &&
                    HasSameModulesAs(drone)
                )
                    return true;
            }
            return false;
        }
        public override string ToString() => 
            $"Name: {droneName}, " +
            $"DroneStatus: {status}, " +
            $"DroneModifiers {modifiers}, " +
            $"frame {frame}, " +
            $"modules {modules} " +
            $"charge: {charge} " +
            $"damage received: {damageReceived}";

        bool HasSameModulesAs(Drone drone)
        {
            if (modules == null || modules.Count == 0)
            {
                if (drone.modules == null || drone.modules.Count == 0)
                {
                    return true;
                }
                return false;
            }

            if (drone.modules == null)
                return false;

            if (modules.Count == drone.modules.Count)
            {
                return modules.All(module => drone.modules.Contains(module));
            }
            return false;
        }
    }
}