﻿using System;

namespace Bonfire.Gameplay.Domain.ValueObjects
{
    [Serializable]
    public struct MapCoordinates
    {
        public float X;
        public float Y;

        public MapCoordinates(float x, float y)
        {
            X = (float)Math.Round((decimal)x,2);
            Y = (float)Math.Round((decimal)y,2);
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }

}