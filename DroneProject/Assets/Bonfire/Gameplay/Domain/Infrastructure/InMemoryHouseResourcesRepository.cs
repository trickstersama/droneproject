﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryHouseResourcesRepository : HouseResourcesRepository
    {
        readonly List<SolarGenerator> solarGenerators;
        readonly List<WindGenerator> windGenerators;
        readonly List<EnergyReactor> energyReactors;
        readonly List<HouseBattery> houseBatteries;

        const float threshold = 0.01f;

        float energyConsumption;
        
        float energyStored;
        
        public InMemoryHouseResourcesRepository(
            IEnumerable<SolarGenerator> withSolarGenerators = null,
            IEnumerable<WindGenerator> withWindGenerators = null,
            IEnumerable<EnergyReactor> withEnergyReactors = null,
            IEnumerable<HouseBattery> withHouseBatteries = null,
            float withEnergyConsumption = 0,
            float withEnergyStored = 0
        ) {
            solarGenerators = withSolarGenerators?.ToList() ?? new List<SolarGenerator>();
            windGenerators = withWindGenerators?.ToList() ?? new List<WindGenerator>();
            energyReactors = withEnergyReactors?.ToList() ?? new List<EnergyReactor>();
            houseBatteries = withHouseBatteries?.ToList() ?? new List<HouseBattery>();
            energyConsumption = withEnergyConsumption;
            energyStored = withEnergyStored;
        }

        public float GetEnergyProductionPerSecond() =>
            solarGenerators.Sum(generator => generator.EnergyPerSecond) +
            windGenerators.Sum(generator => generator.EnergyPerSecond) +
            energyReactors.Sum(generator => generator.EnergyPerSecond);

        public float GetEnergyConsumptionPerSecond() => 
            energyConsumption;

        public bool IsEnergyStorageFull() => 
            energyStorage - energyStored < threshold;

        public IObservable<float> StoreEnergy(float energy)
        {
            energyStored = Math.Min(energyStored + energy, energyStorage);
            return Observable.Return(energyStored);
        }

        public void AddHouseBattery(HouseBattery houseBattery) => 
            houseBatteries.Add(houseBattery);

        public float GetTotalStorage() => 
            houseBatteries.Sum(battery => battery.storageSize);

        float energyStorage =>
            houseBatteries.Sum(battery => battery.storageSize);
    }
}