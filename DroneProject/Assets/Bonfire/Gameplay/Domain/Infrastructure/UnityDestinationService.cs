﻿using System;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class UnityDestinationService : DestinationService
    {
        const int NameMinLenght = 4;
        readonly MapCoordinates InvalidCoordinates = new MapCoordinates {X = 0,Y = 0};
        public Destination CreateDefaultDestination()
        {
            return new Destination();
        }

        public IObservable<Destination> AddMissionType(Destination oldDestination, MissionType type)
        {
            return Observable.Return(new Destination
            {
                mapCoordinates = oldDestination.mapCoordinates,
                missionType =  type,
                name = oldDestination.name
            });
        }

        public IObservable<Destination> AddCoordinates(Destination oldDestination, MapCoordinates mapCoordinates)
        {
            return Observable.Return(new Destination
            {
                mapCoordinates = mapCoordinates,
                missionType =  oldDestination.missionType,
                name = oldDestination.name
            });
        }

        public IObservable<Destination> AddName(Destination oldDestination, string name)
        {
            return Observable.Return(new Destination
            {
                mapCoordinates = oldDestination.mapCoordinates,
                missionType = oldDestination.missionType,
                name = name
            });
        }

        public bool IsValid(Destination destination)
        {
            return !destination.mapCoordinates.Equals(InvalidCoordinates) 
                   && destination.name?.Length > NameMinLenght;
        }
    }
}