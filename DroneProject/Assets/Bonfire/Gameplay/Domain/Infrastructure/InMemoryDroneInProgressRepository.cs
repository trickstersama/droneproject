﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryDroneInProgressRepository : DroneInProgressRepository
    {
        DroneModifiers actualProgress;
        
        DroneModifiers frameModifiers;
        
        DroneModifiers armorProgress;
        DroneModifiers weaponProgress;
        DroneModifiers batteryProgress;
        DroneModifiers sensorProgress;
        DroneModifiers storageProgress;
        DroneModifiers CPUProgress;
        DroneModifiers propulsionProgress;

        List<Module> modulesInUse;
        List<Module> availableModules = new List<Module>();

        List<Frame> availableFrames = new List<Frame>();

        Frame originalFrame;
        Frame actualFrameState;

        string droneName = String.Empty;

        public InMemoryDroneInProgressRepository(
            Frame? actualFrameState = null,
            Frame? originalFrame = null,
            IEnumerable<Module> modulesInUse = null
        ) {
            this.modulesInUse = modulesInUse?.ToList() ?? new List<Module>();
            this.originalFrame = originalFrame ?? new Frame();
            this.actualFrameState = actualFrameState ?? new Frame();
            
            armorProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Armor);
            weaponProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Weapon);
            storageProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Storage);
            batteryProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Battery);
            CPUProgress = GetProgressFromModules(this.modulesInUse, ModuleType.CPU);
            sensorProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Sensor);
            propulsionProgress = GetProgressFromModules(this.modulesInUse, ModuleType.Propulsion);
            frameModifiers = this.originalFrame.GetModifiers();
            
            UpdateTotalProgress();
        }

        static DroneModifiers GetProgressFromModules(IEnumerable<Module> modulesInUse, ModuleType moduleType) =>
            modulesInUse
                .Where(module => module.moduleType == moduleType)
                .Select(module => module.GetDroneModifiers())
                .Aggregate(new DroneModifiers(), (total, next) => total + next);

        void UpdateProgressFromModules()
        {
            armorProgress = GetProgressFromModules(modulesInUse, ModuleType.Armor);
            weaponProgress = GetProgressFromModules(modulesInUse, ModuleType.Weapon);
            storageProgress = GetProgressFromModules(modulesInUse, ModuleType.Storage);
            batteryProgress = GetProgressFromModules(modulesInUse, ModuleType.Battery);
            CPUProgress = GetProgressFromModules(modulesInUse, ModuleType.CPU);
            sensorProgress = GetProgressFromModules(modulesInUse, ModuleType.Sensor);
            propulsionProgress = GetProgressFromModules(modulesInUse, ModuleType.Propulsion);

        }
        public void UpdateFrame(Frame newFrame)
        {
            actualFrameState = newFrame;
            originalFrame = newFrame;
            frameModifiers = actualFrameState.GetModifiers();
            UpdateTotalProgress();
        }

        public IObservable<DroneModifiers> AddModuleToProgress(Module newModule)
        {
            modulesInUse.Add(newModule);
            availableModules.Remove(newModule);
            UpdateProgressFromModules();
            UpdateTotalProgress();
            return Observable.Return(actualProgress);
        }
        
        public IObservable<DroneModifiers> RemoveModuleFromProgress(Module newModule)
        {
            modulesInUse.Remove(newModule);
            availableModules.Add(newModule);
            UpdateProgressFromModules();
            UpdateTotalProgress();
            return Observable.Return(actualProgress);
        }

        public IObservable<Frame> ClearFrame()
        {
            ClearGeneralProgress();
            UpdateTotalProgress();
            return Observable.Return(actualFrameState);
        }

        public int SlotsAvailableFor(Module module) => 
            actualFrameState.GetSlotsAmountByModuleType(module.moduleType);

        public IObservable<Frame> ConsumeSlot(Module module)
        {
            actualFrameState = actualFrameState.DecreaseSlot(module.moduleType);
            return Observable.Return(actualFrameState);
        }

        public IObservable<DroneModifiers> ClearModulesOfType(ModuleType moduleType)
        {
            modulesInUse.RemoveAll(module => module.moduleType == moduleType);
            ClearProgressOf(moduleType);
            UpdateTotalProgress();
            return Observable.Return(actualProgress);
        }

        public IObservable<DroneModifiers> GetDroneModifiers() => 
            Observable.Return(actualProgress);

        public IObservable<Frame> GetActualFrameState() => 
            Observable.Return(actualFrameState);

        public IObservable<IEnumerable<Module>> LoadModulesForAssembly(IEnumerable<Module> modules)
        {
            availableModules = new List<Module>(modules.ToList());
            return Observable.Return(availableModules);
        }

        public IObservable<IEnumerable<Frame>> LoadFramesForAssembly(IEnumerable<Frame> frames)
        {
            availableFrames = new List<Frame>(frames);
            return Observable.Return(availableFrames);
        }

        public Frame GetFrame() => 
            originalFrame;

        public IObservable<IEnumerable<Module>> GetModules()
        {
            return Observable.Return(modulesInUse);
        }

        public void ClearAll()
        {
            droneName = String.Empty;
            availableFrames.Clear();
            ClearGeneralProgress();
        }

        public void SetName(string name) => droneName = name;
        public string GetName() => droneName;
        public Frame ReleaseSlot(Module module) => 
            actualFrameState = actualFrameState.ReleaseSlot(module.moduleType);

        void UpdateTotalProgress() => 
            actualProgress = 
                frameModifiers + 
                armorProgress + 
                weaponProgress +
                batteryProgress +
                storageProgress +
                sensorProgress +
                CPUProgress +
                propulsionProgress;

        void ClearGeneralProgress()
        {
            
            actualProgress = new DroneModifiers();
            frameModifiers = new DroneModifiers();
        
            armorProgress = new DroneModifiers();
            weaponProgress = new DroneModifiers();

            modulesInUse.Clear();
            availableModules.Clear();

            originalFrame = new Frame();
            actualFrameState = new Frame();
        }

        void ClearProgressOf(ModuleType moduleType)
        {
            switch (moduleType)
            {
                case ModuleType.Weapon:
                    weaponProgress = new DroneModifiers();
                    actualFrameState = actualFrameState
                        .ResetSlotsAmount(ModuleType.Weapon, originalFrame.weaponSlotsAmount);
                    break;
                case ModuleType.Battery:
                    break;
                case ModuleType.Sensor:
                    break;
                case ModuleType.Storage:
                    break;
                case ModuleType.Propulsion:
                    break;
                case ModuleType.CPU:
                    break;
                case ModuleType.Armor:
                    armorProgress = new DroneModifiers();
                    actualFrameState = actualFrameState
                        .ResetSlotsAmount(ModuleType.Armor, originalFrame.armorSlotsAmount);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}