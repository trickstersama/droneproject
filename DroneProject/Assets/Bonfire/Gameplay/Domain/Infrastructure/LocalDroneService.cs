﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class LocalDroneService : DroneService
    {
        const float Threshold = 99.8f;
        const int damageThreshold = 99;
        const int fullCharge = 100;
        const int ZeroDamage = 0;

        public bool IsDroneDischarged(Drone drone) => 
            drone.charge <= Threshold;

        public bool IsDroneDamaged(Drone drone)
        {
            var maxHP = drone.modifiers.hitPoints;
            if (maxHP == 0)
                return false;
            var damageReceived = drone.damageReceived;
            return (maxHP - damageReceived) / maxHP * 100 <= damageThreshold;
        }

        public Drone RechargeDrone(Drone drone) => 
            drone.ModifyThis(withCharge: fullCharge);

        public Drone RepairDrone(Drone drone) => 
            drone.ModifyThis(withDamageReceived: ZeroDamage);

        public IEnumerable<Module> GetModulesFromDrone(Drone drone) => 
            drone.modules;

        public Frame GetFrameFromDrone(Drone drone) => 
            drone.frame;
    }
}