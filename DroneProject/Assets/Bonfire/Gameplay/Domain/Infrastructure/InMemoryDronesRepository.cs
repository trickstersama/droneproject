﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryDronesRepository : DronesRepository
    {
        readonly List<Drone> allDrones;
        Drone droneInWorkshop;
        //public int IdleDronesAmount => IdleDrones.Count();
        //public int BusyDronesAmount => BusyDrones.Count();
        public InMemoryDronesRepository(
            IEnumerable<Drone> withDrones = null,
            Drone? withDroneInWorkshop = null
        ) {
            allDrones = (withDrones ?? Array.Empty<Drone>()).ToList();
            
            if (withDroneInWorkshop != null) 
                allDrones.Add((Drone) withDroneInWorkshop);
            droneInWorkshop = withDroneInWorkshop ?? new Drone();
        }


        public void Remove(Drone drone) => allDrones.Remove(drone);

        public IObservable<IEnumerable<Drone>> RetrieveAllIdleDrones() => 
            Observable.Return(IdleDrones);

        public IObservable<IEnumerable<Drone>> RetrieveAllBusyDrones() => 
            Observable.Return(BusyDrones);

        public IObservable<Drone> SetDroneToBusy(Drone drone)
        {
            allDrones.Remove(GetDroneByName(drone.droneName));
            var busyDrone = drone.ModifyThis(withStatus: DroneStatus.Busy);
            allDrones.Add(busyDrone);
            return Observable.Return(busyDrone);
        }


        public IObservable<Drone> SetDroneToIdle(Drone drone)
        {
            allDrones.Remove(GetDroneByName(drone.droneName));
            var idleDrone = drone.ModifyThis(withStatus: DroneStatus.Idle);
            allDrones.Add(idleDrone);
            return Observable.Return(idleDrone);
        }

        public IObservable<IEnumerable<Drone>> AddNewDrone(Drone drone)
        {
            allDrones.Add(drone);
            return Observable.Return(IdleDrones);
        }

        public void SelectDroneForWorkshop(Drone drone) => 
            droneInWorkshop = drone;

        public IObservable<Drone> RetrieveDroneInWorkshop() => 
            Observable.Return(droneInWorkshop);

        public IObservable<Drone> UpdateDrone(Drone drone)
        {
            var oldDrone = GetDroneByName(drone.droneName);
            Remove(oldDrone);
            AddNewDrone(drone);
            return Observable.Return(drone);
        }

        public IObservable<Drone> SetDroneToTraveling(Drone drone)
        {
            allDrones.Remove(GetDroneByName(drone.droneName));
            var idleDrone = drone.ModifyThis(withStatus: DroneStatus.Traveling);
            allDrones.Add(idleDrone);
            return Observable.Return(idleDrone);
        }

        IEnumerable<Drone> IdleDrones => allDrones
            .Where(drone => drone.status.Equals(DroneStatus.Idle)); 
        IEnumerable<Drone> BusyDrones => allDrones
            .Where(drone => drone.status.Equals(DroneStatus.Busy) ||
                            drone.status.Equals(DroneStatus.Traveling) ||
                            drone.status.Equals(DroneStatus.Returning));

        public Drone GetDroneByName(string name) =>
            allDrones.First(drone => drone.droneName == name);

        public IEnumerable<Drone> GetDronesInBalcony() => 
            allDrones.Where(drone => drone.status == DroneStatus.InBalcony);
    }
    
}