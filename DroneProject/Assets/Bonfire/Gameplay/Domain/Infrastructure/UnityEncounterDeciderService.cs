﻿using System;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class UnityEncounterDeciderService : EncounterDeciderService
    {
        public IObservable<Encounter> DetermineEncounterType()
        {
            return new Subject<Encounter>();
        }
    }
}