﻿using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryPinInProgressRepository : PinInProgressRepository
    {
        public InMemoryPinInProgressRepository(
            Destination? withDestination = null
        ) => 
            destinationInProgress = withDestination ?? new Destination();
        
        Destination destinationInProgress;
        
        public void Initialize() => 
            destinationInProgress = new Destination();

        public void Save(Destination destination) => 
            destinationInProgress = destination;

        public Destination GetDestination() => 
            destinationInProgress;

        public void Clear() => 
            destinationInProgress = new Destination();

        public void UpdateMapCoordinates(MapCoordinates mapCoordinates) => 
            destinationInProgress = destinationInProgress.ModifyThis(withMapCoordinates: mapCoordinates);

        public void UpdateMissionType(MissionType missionType) => 
            destinationInProgress = destinationInProgress.ModifyThis(withMissionType: missionType);

        public void UpdateName(string name) => 
            destinationInProgress = destinationInProgress.ModifyThis(withName: name);
    }
}