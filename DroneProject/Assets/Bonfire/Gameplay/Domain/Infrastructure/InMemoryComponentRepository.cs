﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryComponentRepository : ComponentsRepository
    {
        readonly List<Module> availableModules;
        readonly List<Frame> availableFrames;

        public InMemoryComponentRepository(
            IEnumerable<Module> modules = null, 
            IEnumerable<Frame> frames = null
        ) {
            availableModules = (modules ?? Array.Empty<Module>()).ToList();
            availableFrames = (frames ?? Array.Empty<Frame>()).ToList();
        }

        public IObservable<IEnumerable<Frame>> GetFrames() => 
            Observable.Return(availableFrames);

        public IObservable<IEnumerable<Module>> GetAllModules() => 
            Observable.Return(availableModules);

        public IObservable<IEnumerable<Module>> RemoveModules(IEnumerable<Module> moduleToRemove)
        {
            foreach (var module in moduleToRemove)
            {
                availableModules.Remove(availableModules.First(storedModule => storedModule.Equals(module)));
            }
            return Observable.Return(availableModules);
        }

        public IObservable<IEnumerable<Frame>> RemoveFrame(Frame frame)
        {
            availableFrames.Remove(availableFrames.First(storedFrame => frame.Equals(storedFrame)));
            return Observable.Return(availableFrames);
        }

        public IObservable<IEnumerable<Module>> GetModulesOfType(ModuleType type)
        {
            return Observable.Return(availableModules.Where(module => module.moduleType == type));
        }

        public void SaveModules(IEnumerable<Module> modules)
        {
            foreach (var module in modules)
            {
                availableModules.Add(module);   
            }
        }

        public void SaveFrame(Frame frame) => 
            availableFrames.Add(frame);
    }
}