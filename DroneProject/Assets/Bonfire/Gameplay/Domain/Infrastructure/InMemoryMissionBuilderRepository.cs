﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using UniRx;
using static Bonfire.Gameplay.Domain.ValueObjects.Mission.MissionOrders;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryMissionBuilderRepository : MissionBuilderRepository
    {
        MissionOrders missionOrders;
        bool isDroneSelected;
        bool isMapCoordinatesSelected;
        bool isMissionTypeSelected;
        Drone actualDrone;

        public InMemoryMissionBuilderRepository(
            MissionOrders? withMissionOrders = null,
            Drone? drone = null,
            bool withMissionReadyToCalculate = false
        ) {
            missionOrders = withMissionOrders ?? new MissionOrders();
            actualDrone = drone ?? new Drone();
            if (withMissionReadyToCalculate)
            {
                isDroneSelected = true;
                isMapCoordinatesSelected = true;
                isMissionTypeSelected = true;
            }
        }

        public IObservable<MissionOrders> UpdateMissionType(MissionType missionType)
        {
            //TODO test this booleans
            isMissionTypeSelected = true;
            missionOrders = Builder(missionOrders, withMissionType: missionType);
            return Observable.Return(missionOrders);
        }

        public IObservable<MissionOrders> UpdateMapCoordinates(MapCoordinates destination)
        {
            isMapCoordinatesSelected = true;
            missionOrders = Builder(missionOrders, withDestination: destination);
            return Observable.Return(missionOrders);
        }

        public IObservable<MissionOrders> UpdateDrone(Drone drone)
        {
            actualDrone = drone;
            isDroneSelected = true;
            missionOrders = Builder(missionOrders, withDroneName: drone.droneName);
            return Observable.Return(missionOrders);
        }

        public MissionOrders Clear()
        {
            isDroneSelected = false;
            isMissionTypeSelected = false;
            isMapCoordinatesSelected = false;
            var missionOrdersToSend = missionOrders;
            missionOrders = new MissionOrders();

            return missionOrdersToSend;
        }

        public bool IsMissionReadyToCalculate() => 
            isDroneSelected && isMapCoordinatesSelected && isMissionTypeSelected;

        public IObservable<MissionOrders> AddEnergyCost(MissionEnergyCost missionEnergyCost)
        {
            missionOrders = Builder(missionOrders, withMissionEnergyCost: missionEnergyCost);
            return Observable.Return(missionOrders);
        }

        public IObservable<MissionOrders> GetMissionOrders() => 
            Observable.Return(missionOrders);

        public IObservable<MissionOrders> AddTimeCost(MissionTimeCost missionTimeCost)
        {
            missionOrders = Builder(missionOrders, withTimeCost: missionTimeCost);
            return Observable.Return(missionOrders);
        }

        public IObservable<MissionOrders> AddMaxWeightAllowed(float weight)
        {
            missionOrders = Builder(missionOrders, withMaxWeightAllowed: weight);
            return Observable.Return(missionOrders);
        }

        public Drone GetDrone() => actualDrone;
    }
}