﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryMapRepository : MapRepository
    {
        List<Destination> destinations;
        public InMemoryMapRepository(IEnumerable<Destination> withDestinations = null)
        {
            destinations = withDestinations != null ?
                new List<Destination>(withDestinations) :
                new List<Destination>();
        }

        public IObservable<IEnumerable<Destination>> RetrieveAllDestinations()
        {
            return Observable.Return(destinations);
        }

        public void AddNewDestination(Destination destination)
        {
            destinations.Add(destination);
        }
    }
}