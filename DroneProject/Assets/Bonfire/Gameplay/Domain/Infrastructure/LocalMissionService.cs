﻿using System;
using Bonfire.Gameplay.Domain.Configuration;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class LocalMissionService : MissionService
    {
        readonly MissionConfiguration missionConfiguration;
        const float gravity = 9.8f;
        const float airDensity = 1.225f;
        
        //TODO parametrizar estos valores
        public LocalMissionService(MissionConfiguration missionConfiguration) => 
            this.missionConfiguration = missionConfiguration;

        public float HorizontalAcceleration(Drone drone, float occupiedStorage = 0)
        {
            var acceleration =  (drone.modifiers.thrust - LiftForce(drone, occupiedStorage)) /
                                (drone.modifiers.weight + occupiedStorage);
            return acceleration > 0 ? acceleration : 0;
        }

        public float TravelJoulesWithNoLoss(Drone drone, MapCoordinates mapCoordinates, float occupiedStorage = 0) =>
            drone.modifiers.thrust *
            (drone.modifiers.weight + occupiedStorage) * 
            FlightTimeToTarget(drone, mapCoordinates, occupiedStorage);

        public float TerminalVelocity(Drone drone, float occupiedStorage = 0)
        {
            var m = drone.modifiers.weight + occupiedStorage;
            var a = HorizontalAcceleration(drone, occupiedStorage);
            var aerodynamics = drone.modifiers.aerodynamics;
            var crossSectionArea = drone.modifiers.crossSectionArea;
            return (float)Math.Sqrt(2 * m * a / (airDensity * crossSectionArea * aerodynamics));
        }

        public float AirDragForceByVelocity(Drone drone, float velocity) =>
            0.5f * (float) (airDensity * 
                            Math.Pow(velocity, 2) *
                            drone.modifiers.aerodynamics *
                            drone.modifiers.crossSectionArea);

        public float FlightTimeToTarget(Drone drone, MapCoordinates mapCoordinates, float occupiedStorage = 0)
        {
            var velocity = TerminalVelocity(drone, occupiedStorage);
            var distance = DistanceInMeters(mapCoordinates);
            return velocity > 0 ? distance / velocity : 0;
        }

        static float DistanceInMeters(MapCoordinates mapCoordinates) => 
            (float) Math.Sqrt(mapCoordinates.X * mapCoordinates.X + mapCoordinates.Y * mapCoordinates.Y) * 1000;

        public float LiftForce(Drone drone, float occupiedStorage = 0) => 
            (drone.modifiers.weight + occupiedStorage) * gravity;

        public float EfficientThrust(Drone drone, float occupiedStorage = 0) => 
            drone.modifiers.thrust - LiftForce(drone, occupiedStorage);
        
        public MissionEnergyCost CalculateEnergyCosts(MissionOrders missionOrders, Drone drone)
        {
            var travelCost = TravelJoulesWithNoLoss(drone, missionOrders.mapCoordinates);
            var returnWithFullStorage = TravelJoulesWithNoLoss(drone, missionOrders.mapCoordinates, occupiedStorage: drone.modifiers.reservedWeight);
            return new MissionEnergyCost(travelCost, 0, returnWithFullStorage);
        }

        public MissionTimeCost CalculateTimeCost(
            MissionOrders orders,
            MissionsModifiers missionsesModifiers,
            Drone drone
        ) {
            var toMission = FlightTimeToTarget(drone, orders.mapCoordinates);
            var onMission = ChooseTimeOnTarget(orders.missionType);
            var toBaseWithMaxWeight = FlightTimeToTarget(drone, orders.mapCoordinates, drone.modifiers.reservedWeight);
            var toBaseWithMaxAllowed = 1f;
            return new MissionTimeCost(
                toTarget: toMission * missionsesModifiers.travelMultiplier,
                onTarget: onMission * missionsesModifiers.missionMultiplier,
                toBaseWithMaxWeight: toBaseWithMaxWeight * missionsesModifiers.travelMultiplier,
                toBaseWithMaxAllowed: toBaseWithMaxAllowed * missionsesModifiers.travelMultiplier
            );
        }

        public float CalculateMaxExtraWeightAllowed(Drone drone)
        {
            var halfExtraThrust = EfficientThrust(drone) / 2;
            return  halfExtraThrust / gravity;
        }

        public Mission CreateMission(MissionOrders missionOrders) =>
            new Mission(
                missionOrders: missionOrders, 
                new TimeOnMission(0f),
                new MissionProgression()
            );

        public Mission UpdateTime(Mission mission, float timePassed)
        {
            var oldTime = mission.timeOnMission;
            return new Mission(
                mission.missionOrders, 
                new TimeOnMission(oldTime.timePassed + timePassed),
                mission.missionProgression
            );
        }

        public float TimeNeededToReturn(Mission mission) =>
            mission.missionOrders.missionTimeCost.toTarget +
            mission.missionOrders.missionTimeCost.onTarget +
            mission.missionOrders.missionTimeCost.toBaseWithMaxAllowed;

        public float TimeNeededToTarget(Mission mission) => 
            mission.missionOrders.missionTimeCost.toTarget;

        public float TimeNeededToCompleteTarget(Mission mission) =>
            mission.missionOrders.missionTimeCost.toTarget +
            mission.missionOrders.missionTimeCost.onTarget;

        float ChooseTimeOnTarget(MissionType missionType)
        {
            switch (missionType)
            {
                case MissionType.Scout:
                    return missionConfiguration.secondsOnScoutMission;
                case MissionType.Loot:
                    return missionConfiguration.secondsOnLootMission;
                case MissionType.Battle:
                    return missionConfiguration.secondsOnBattleMission;
                default:
                    throw new ArgumentOutOfRangeException(nameof(missionType), missionType, null);
            }
        }
    }
}