﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class LocalDroneAssemblerService : DroneAssemblerService
    {
        const int MinLenghtName = 4;
        const int MaxLenghtName = 16;
        const int StartingDamage = 0;
        const int StartingCharge = 100;
        public IObservable<Drone> CreateEmptyDrone(Frame frame)
        {
            var newDrone = new Drone();
            return Observable.Return(newDrone.ModifyThis(
                    withFrame: frame,
                    withCharge: StartingCharge,
                    withDamageReceived: StartingDamage,
                    withStatus: DroneStatus.Idle
                ));
        }

        public IObservable<Drone> AddModifiers(Drone drone, DroneModifiers modifiers) => 
            Observable.Return(drone.ModifyThis(withModifiers: modifiers));

        public IObservable<Drone> AddModules(Drone drone, IEnumerable<Module> modules) => 
            Observable.Return(drone.ModifyThis(withModules: modules));

        public IObservable<Drone> AddName(Drone drone, string name) => 
            Observable.Return(drone.ModifyThis(withName: name));

        public bool NameIsValid(string name) => 
            name.Length is > MinLenghtName and < MaxLenghtName;
    }
}