﻿using System;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class LocalFrameSlotsAmountService : FrameSlotsAmountService
    {
        public IObservable<int> GetAmountByModuleType(Frame frame, ModuleType moduleType) =>
            moduleType switch
            {
                ModuleType.Weapon => Observable.Return(frame.weaponSlotsAmount),
                ModuleType.Battery => Observable.Return(frame.batterySlotsAmount),
                ModuleType.Sensor => Observable.Return(frame.sensorSlotsAmount),
                ModuleType.Storage => Observable.Return(frame.storageUnitSlotsAmount),
                ModuleType.Propulsion => Observable.Return(frame.propulsionSlotsAmount),
                ModuleType.CPU => Observable.Return(frame.cpuSlotsAmount),
                ModuleType.Armor => Observable.Return(frame.armorSlotsAmount),
                _ => throw new ArgumentOutOfRangeException(nameof(moduleType), moduleType, null)
            };
    }
}