﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;

namespace Bonfire.Gameplay.Domain.Infrastructure
{
    public class InMemoryHouseItemsRepository : HouseItemsRepository
    {
        List<Item> houseItems = new List<Item>() {new Item(), new Item()};
        
        public void AddItems()
        {
            
        }
        public IObservable<IEnumerable<Item>> GetItems()
        {
            return Observable.Return(houseItems);
        }
    }
}