using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using UniRx;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public class InMemoryMissionsRepository : MissionsRepository
    {
        List<Mission> missions;

        public InMemoryMissionsRepository(Mission[] withMissions = null) => 
            missions = withMissions != null ? new List<Mission>(withMissions) : new List<Mission>();

        public void SaveNewMission(Mission mission) => 
            missions.Add(mission);

        public IObservable<IEnumerable<Mission>> GetAllMissions() => 
            Observable.Return(missions);

        public void UpdateMissions(IEnumerable<Mission> updatedMissions) => 
            missions = new List<Mission>(updatedMissions);

        public IEnumerable<Mission> GetFinishedMissions()
        {
            return missions; //TODO: not ready
        }
    }
}