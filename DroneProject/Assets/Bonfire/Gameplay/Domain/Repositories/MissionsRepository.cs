using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface MissionsRepository
    {
        void SaveNewMission(Mission missionOrders);
        IObservable<IEnumerable<Mission>> GetAllMissions();
        void UpdateMissions(IEnumerable<Mission> updatedMissions);
        IEnumerable<Mission> GetFinishedMissions();
    }
}