﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface MissionBuilderRepository
    {
        IObservable<MissionOrders> UpdateMissionType(MissionType missionType);
        IObservable<MissionOrders> UpdateMapCoordinates(MapCoordinates destination);
        IObservable<MissionOrders> UpdateDrone(Drone drone);
        MissionOrders Clear();
        bool IsMissionReadyToCalculate();
        IObservable<MissionOrders> AddEnergyCost(MissionEnergyCost missionEnergyCost);
        IObservable<MissionOrders> GetMissionOrders();
        IObservable<MissionOrders> AddTimeCost(MissionTimeCost missionTimeCost);
        IObservable<MissionOrders> AddMaxWeightAllowed(float weight);
        Drone GetDrone();
    }
}