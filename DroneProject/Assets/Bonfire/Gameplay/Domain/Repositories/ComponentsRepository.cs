﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface ComponentsRepository
    {
        IObservable<IEnumerable<Frame>> GetFrames();
        IObservable<IEnumerable<Module>> GetAllModules();
        IObservable<IEnumerable<Module>> RemoveModules(IEnumerable<Module> moduleToRemove);
        IObservable<IEnumerable<Frame>> RemoveFrame(Frame frame);
        IObservable<IEnumerable<Module>> GetModulesOfType(ModuleType type);
        void SaveModules(IEnumerable<Module> modules);
        void SaveFrame(Frame frame);
    }
}