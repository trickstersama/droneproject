﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects.House;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface HouseResourcesRepository
    {
        float GetEnergyProductionPerSecond();
        float GetEnergyConsumptionPerSecond();
        bool IsEnergyStorageFull();
        IObservable<float> StoreEnergy(float energy);
        void AddHouseBattery(HouseBattery houseBattery);
        float GetTotalStorage();
    }
}