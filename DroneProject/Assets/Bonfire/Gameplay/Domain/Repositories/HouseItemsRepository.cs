﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface HouseItemsRepository
    {
        void AddItems();
        IObservable<IEnumerable<Item>> GetItems();
    }
}
