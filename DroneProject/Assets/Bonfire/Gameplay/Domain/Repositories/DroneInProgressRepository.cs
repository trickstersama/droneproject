﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface DroneInProgressRepository
    {
        void UpdateFrame(Frame frame);
        IObservable<Frame> ClearFrame();
        IObservable<Frame> GetActualFrameState();
        IObservable<DroneModifiers> AddModuleToProgress(Module newModule);
        int SlotsAvailableFor(Module module);
        IObservable<Frame> ConsumeSlot(Module module);
        IObservable<DroneModifiers> ClearModulesOfType(ModuleType any);
        IObservable<DroneModifiers> GetDroneModifiers();
        IObservable<IEnumerable<Module>> LoadModulesForAssembly(IEnumerable<Module> modules);
        IObservable<IEnumerable<Frame>> LoadFramesForAssembly(IEnumerable<Frame> frames);
        Frame GetFrame();
        IObservable<IEnumerable<Module>> GetModules();
        void ClearAll();
        void SetName(string name);
        string GetName();
        Frame ReleaseSlot(Module module);
        IObservable<DroneModifiers> RemoveModuleFromProgress(Module newModule);
    }
}