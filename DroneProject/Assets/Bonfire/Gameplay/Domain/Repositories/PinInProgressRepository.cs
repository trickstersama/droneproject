﻿using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface PinInProgressRepository
    {
        void Initialize();
        void Save(Destination destination);
        Destination GetDestination();
        void Clear();
        void UpdateMapCoordinates(MapCoordinates mapCoordinates);
        void UpdateMissionType(MissionType missionType);
        void UpdateName(string name);
    }
}