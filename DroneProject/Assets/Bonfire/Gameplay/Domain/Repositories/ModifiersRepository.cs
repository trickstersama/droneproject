using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface ModifiersRepository
    {
        MissionsModifiers GetTimeCostModifiers();
    }
    
    public class InMemoryModifiersRepository : ModifiersRepository
    {
        readonly MissionsModifiers timeCostsModifiers;

        public InMemoryModifiersRepository(MissionsModifiers? withTimeCostsModifiers = null)
        {
            timeCostsModifiers = withTimeCostsModifiers ?? new MissionsModifiers(1,1);
        }

        public MissionsModifiers GetTimeCostModifiers() => 
            timeCostsModifiers;
    }
}