﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface MapRepository
    {
        IObservable<IEnumerable<Destination>> RetrieveAllDestinations();
        void AddNewDestination(Destination destination);
    }
}