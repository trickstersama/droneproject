﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Repositories
{
    public interface DronesRepository
    {
        void Remove(Drone drone);
        IObservable<Drone> SetDroneToBusy(Drone drone);
        IObservable<Drone> SetDroneToIdle(Drone drone);
        IObservable<IEnumerable<Drone>> AddNewDrone(Drone drone);
        void SelectDroneForWorkshop(Drone drone);
        IObservable<Drone> SetDroneToTraveling(Drone any);
        IObservable<IEnumerable<Drone>> RetrieveAllIdleDrones();
        IObservable<IEnumerable<Drone>> RetrieveAllBusyDrones();
        IObservable<Drone> RetrieveDroneInWorkshop();
        IObservable<Drone> UpdateDrone(Drone drone);
        Drone GetDroneByName(string name);
        IEnumerable<Drone> GetDronesInBalcony();
    }
}