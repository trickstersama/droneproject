﻿using System.Text.RegularExpressions;

namespace Bonfire.Gameplay.Domain.Support

{
    public static class InputUtilities
    {
        public static char Validate(char addedChar)
        {
            var rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
            return rg.IsMatch(addedChar.ToString()) ? addedChar : '\0';
        }
    }
}