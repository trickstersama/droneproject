﻿namespace Bonfire.Gameplay.Domain.Configuration
{
    public struct MissionConfiguration
    {
        public readonly float secondsOnLootMission;
        public readonly float secondsOnScoutMission;
        public readonly float secondsOnBattleMission;

        public MissionConfiguration(
            float secondsOnLootMission, 
            float secondsOnScoutMission, 
            float secondsOnBattleMission
        ) {
            this.secondsOnLootMission = secondsOnLootMission;
            this.secondsOnScoutMission = secondsOnScoutMission;
            this.secondsOnBattleMission = secondsOnBattleMission;
        }
    }
}