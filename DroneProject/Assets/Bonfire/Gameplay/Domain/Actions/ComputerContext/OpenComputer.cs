﻿using System;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext
{
    public class OpenComputer
    {
        readonly IObserver<Unit> onOpenComputer;

        public OpenComputer(GameplayEvents events) => 
            onOpenComputer = events.OnOpenComputer;

        public void Do() => onOpenComputer.OnNext(Unit.Default);
    }
}