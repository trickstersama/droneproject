﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext
{
    public class SelectWorkstationDronesTab
    {
        readonly ISubject<IEnumerable<Drone>> onWorkstationDronesTabSelected;
        readonly DronesRepository dronesRepository;

        public SelectWorkstationDronesTab(
            GameplayEvents events, 
            GameplayRepositories gameplayRepositories
        ) {
            onWorkstationDronesTabSelected = events.OnWorkstationDronesTabSelected;
            dronesRepository = gameplayRepositories.DronesRepository;
        }

        public void Do()
        {
            dronesRepository.RetrieveAllIdleDrones()
                .Subscribe(onWorkstationDronesTabSelected.OnNext);
        }
    }
}