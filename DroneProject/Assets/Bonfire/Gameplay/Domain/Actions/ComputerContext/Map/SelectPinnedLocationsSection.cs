﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class SelectPinnedLocationsSection
    {
        readonly IObserver<IEnumerable<Destination>> onPinnedLocationsSelected;
        readonly MapRepository mapRepository;

        public SelectPinnedLocationsSection(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onPinnedLocationsSelected = events.OnPinnedLocationsSectionSelected;
            mapRepository = repositories.MapRepository;
        }

        public void Do() =>
            mapRepository.RetrieveAllDestinations()
                .Subscribe(onPinnedLocationsSelected.OnNext);
    }
}