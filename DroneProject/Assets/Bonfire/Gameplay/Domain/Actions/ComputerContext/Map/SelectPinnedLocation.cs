﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class SelectPinnedLocation
    {
        readonly IObserver<Destination> onPinnedLocationSelected;

        public SelectPinnedLocation(GameplayEvents events) => 
            onPinnedLocationSelected = events.OnPinnedLocationSelected;

        public void Do(Destination destination) => 
            onPinnedLocationSelected.OnNext(destination);
    }
}