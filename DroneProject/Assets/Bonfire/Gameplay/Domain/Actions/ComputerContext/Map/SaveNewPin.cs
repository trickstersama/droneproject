﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class SaveNewPin
    {
        readonly IObserver<Unit> onNewPinSaved;
        readonly PinInProgressRepository pinInProgressRepository;
        readonly MapRepository mapRepository;
        readonly DestinationService destinationService;

        public SaveNewPin(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onNewPinSaved = events.OnNewPinSaved;
            pinInProgressRepository = repositories.PinInProgressRepository;
            mapRepository = repositories.MapRepository;
            destinationService = services.DestinationService;
        }

        public void Do()
        {
            Observable.Return(pinInProgressRepository.GetDestination())
                .Where(destination => destinationService.IsValid(destination))
                .Do(mapRepository.AddNewDestination)
                .Do( _ => pinInProgressRepository.Clear())
                .Subscribe(_ => onNewPinSaved.OnNext(Unit.Default));
        }
    }
}