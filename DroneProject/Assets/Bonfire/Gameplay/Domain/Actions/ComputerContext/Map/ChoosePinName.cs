﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class ChoosePinName
    {
        readonly IObserver<Destination> onPinNameChosen;
        readonly PinInProgressRepository pinInProgressRepository;

        public ChoosePinName(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onPinNameChosen = events.OnPinNameChosen;
            pinInProgressRepository = repositories.PinInProgressRepository;
        }

        public void Do(string name)
        {
           pinInProgressRepository.UpdateName(name);
           onPinNameChosen.OnNext(pinInProgressRepository.GetDestination());
        }
    }
}