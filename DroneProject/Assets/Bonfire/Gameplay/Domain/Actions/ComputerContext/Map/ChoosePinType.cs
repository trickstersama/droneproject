﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class ChoosePinType
    {
        readonly IObserver<Destination> onPinTypeChosen;
        readonly PinInProgressRepository pinInProgressRepository;

        public ChoosePinType(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onPinTypeChosen = events.OnPinTypeChosen;
            pinInProgressRepository = repositories.PinInProgressRepository;
        }

        public void Do(MissionType missionType)
        {
            pinInProgressRepository.UpdateMissionType(missionType);
            onPinTypeChosen.OnNext(pinInProgressRepository.GetDestination());
        }
    }
}