﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class SelectNewPinCoordinates
    {
        readonly IObserver<Destination> onNewPinCoordinatesSelected;
        readonly PinInProgressRepository pinInProgressRepository;

        public SelectNewPinCoordinates(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onNewPinCoordinatesSelected = events.OnNewPinCoordinatesSelected;
            pinInProgressRepository = repositories.PinInProgressRepository;
        }

        public void Do(MapCoordinates coordinates)
        {
            pinInProgressRepository.UpdateMapCoordinates(coordinates);
            onNewPinCoordinatesSelected.OnNext(pinInProgressRepository.GetDestination());
        }
    }
}