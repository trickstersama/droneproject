﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class SelectNewPin
    {
        readonly IObserver<Unit> onNewPinSelected;
        readonly PinInProgressRepository pinInProgressRepository;

        public SelectNewPin(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onNewPinSelected = events.OnNewPinSelected;
            pinInProgressRepository = repositories.PinInProgressRepository;
        }

        public void Do()
        {
            pinInProgressRepository.Initialize();
            onNewPinSelected.OnNext(Unit.Default);
        }
    }
}