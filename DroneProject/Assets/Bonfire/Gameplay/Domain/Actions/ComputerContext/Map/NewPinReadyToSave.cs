﻿using System;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Map
{
    public class NewPinReadyToSave
    {
        readonly IObserver<Unit> onNewPinReadyToSave;
        readonly IObserver<Unit> onNewPinNotReadyToSave;
        readonly DestinationService destinationService;

        public NewPinReadyToSave(
            GameplayEvents events, 
            GameplayServices services
        ) {
            onNewPinReadyToSave = events.OnNewPinReadyToSave;
            onNewPinNotReadyToSave = events.onNewPinNotReadyToSave;
            destinationService = services.DestinationService;
        }

        public void Do(Destination destination)
        {
            if (destinationService.IsValid(destination))
                onNewPinReadyToSave.OnNext(Unit.Default);
            else
                onNewPinNotReadyToSave.OnNext(Unit.Default);
        }
    }
}