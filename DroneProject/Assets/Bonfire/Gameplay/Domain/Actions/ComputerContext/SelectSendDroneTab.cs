﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext
{
    public class SelectSendDroneTab
    {
        readonly IObserver<IEnumerable<Drone>> onSendDroneTabSelected;

        readonly DronesRepository dronesRepository;
        readonly MissionBuilderRepository missionBuilderRepository;

        public SelectSendDroneTab(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onSendDroneTabSelected = events.OnSendDroneTabSelected;

            dronesRepository = repositories.DronesRepository;
            missionBuilderRepository = repositories.MissionBuilderRepository;
        }


        public void Do()
        {
            dronesRepository.RetrieveAllIdleDrones()
                .Do(_ => missionBuilderRepository.Clear())
                .Subscribe(onSendDroneTabSelected.OnNext);
        }
    }
}