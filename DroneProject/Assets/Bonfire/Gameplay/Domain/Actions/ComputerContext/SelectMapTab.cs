﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext
{
    public class SelectMapTab
    {
        readonly ISubject<IEnumerable<Destination>> onMapTabSelected;
        readonly MapRepository mapRepository;

        public SelectMapTab(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onMapTabSelected = events.OnMapTabSelected;
            mapRepository = repositories.MapRepository;
        }

        public void Do()
        {
            mapRepository.RetrieveAllDestinations()
                .Subscribe(onMapTabSelected.OnNext);
        }
    }
}