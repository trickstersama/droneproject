﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone

{
    public class SelectMissionDrone
    {
        readonly IObserver<MissionOrders> onMissionDroneSelected;
        readonly MissionBuilderRepository missionBuilderRepository;

        public SelectMissionDrone(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onMissionDroneSelected = events.OnMissionDroneSelected;
            missionBuilderRepository = repositories.MissionBuilderRepository;
        }

        public void Do(Drone drone)
        {
            missionBuilderRepository.UpdateDrone(drone)
                .Subscribe(onMissionDroneSelected.OnNext);
        }
    }
}