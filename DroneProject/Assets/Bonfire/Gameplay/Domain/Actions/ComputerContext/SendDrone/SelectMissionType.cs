﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone
{
    public class SelectMissionType
    {
        readonly IObserver<MissionOrders> onMissionTypeSelected;
        readonly MissionBuilderRepository missionBuilderRepository;

        public SelectMissionType(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onMissionTypeSelected = events.OnMissionTypeSelected;
            missionBuilderRepository = repositories.MissionBuilderRepository;
        }

        public void Do(MissionType missionType)
        {
            missionBuilderRepository.UpdateMissionType(missionType)
                .Subscribe(onMissionTypeSelected.OnNext);
        }
    }
}