﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone
{
    public class SendDrone
    {
        readonly IObserver<Unit> onDroneSent;
        readonly DronesRepository dronesRepository;
        readonly MissionBuilderRepository missionBuilderRepository;
        readonly MissionsRepository missionsRepository;
        readonly MissionService missionService;

        public SendDrone(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onDroneSent = events.OnDroneSent;
            dronesRepository = repositories.DronesRepository;
            missionBuilderRepository = repositories.MissionBuilderRepository;
            missionsRepository = repositories.MissionsRepository;
            missionService = services.MissionService;
        }
        public void Do() =>
            dronesRepository.SetDroneToTraveling(missionBuilderRepository.GetDrone())
                .Select(_ => missionBuilderRepository.Clear())
                .Select(missionOrders => missionService.CreateMission(missionOrders))
                .Do(missionsRepository.SaveNewMission)
                .Subscribe(_ => onDroneSent.OnNext(Unit.Default));
    }
}