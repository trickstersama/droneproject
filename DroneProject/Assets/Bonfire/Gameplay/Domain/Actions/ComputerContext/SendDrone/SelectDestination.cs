﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone
{
    public class SelectDestination
    {
        readonly IObserver<MissionOrders> onDestinationSelected;
        readonly MissionBuilderRepository missionBuilderRepository;

        public SelectDestination(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onDestinationSelected = events.OnDestinationSelected;
            missionBuilderRepository = repositories.MissionBuilderRepository;
        }

        public void Do(MapCoordinates destination)
        {
            missionBuilderRepository.UpdateMapCoordinates(destination)
                .Subscribe(onDestinationSelected.OnNext);
        }
    }
}