﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone
{
    public class OpenMap
    {
        readonly IObserver<IEnumerable<Destination>> onMapOpened;
        readonly MapRepository mapRepository;

        public OpenMap(GameplayEvents events, GameplayRepositories repositories)
        {
            onMapOpened = events.OnMapOpened;
            mapRepository = repositories.MapRepository;
        }

        public void Do()
        {
            mapRepository.RetrieveAllDestinations()
                .Subscribe(onMapOpened.OnNext);
        }
    }
}