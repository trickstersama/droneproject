﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext

{
    public class SelectDroneStatusTab
    {
        readonly IObserver<IEnumerable<Drone>> onDroneStatusTabSelected;

        readonly DronesRepository dronesRepository;

        public SelectDroneStatusTab(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onDroneStatusTabSelected = events.OnDroneStatusTabSelected;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do()
        {
            dronesRepository.RetrieveAllBusyDrones()
                .Subscribe(onDroneStatusTabSelected.OnNext);
        }
    }
}