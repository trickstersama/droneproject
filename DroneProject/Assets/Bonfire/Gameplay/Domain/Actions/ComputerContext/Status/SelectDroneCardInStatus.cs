﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Status

{
    public class SelectDroneCardInStatus
    {
        readonly IObserver<Drone> onDroneInStatusSelected;

        public SelectDroneCardInStatus(GameplayEvents events) => onDroneInStatusSelected = events.OnDroneInStatusSelected;

        public void Do(Drone drone) => 
            onDroneInStatusSelected.OnNext(drone);
    }
}