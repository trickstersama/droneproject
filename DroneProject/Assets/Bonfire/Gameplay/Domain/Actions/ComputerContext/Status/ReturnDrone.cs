﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.ComputerContext.Status
{
    public class ReturnDrone
    {
        readonly DronesRepository dronesRepository;
        readonly IObserver<Drone> onDroneReturning;

        public ReturnDrone(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            dronesRepository = repositories.DronesRepository;
            onDroneReturning = events.OnDroneReturning;
        }

        public void Do(Drone drone)
        {
            dronesRepository.SetDroneToIdle(drone)
                .Subscribe(onDroneReturning.OnNext);
        }
    }
}