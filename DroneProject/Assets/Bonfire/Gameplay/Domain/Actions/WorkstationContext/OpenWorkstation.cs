﻿using System;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext
{
    public class OpenWorkstation
    {
        readonly IObserver<Unit> onOpenWorkstation;

        public OpenWorkstation(GameplayEvents events) => 
            onOpenWorkstation = events.OnOpenWorkstation;

        public void Do() => 
            onOpenWorkstation.OnNext(Unit.Default);
    }
}