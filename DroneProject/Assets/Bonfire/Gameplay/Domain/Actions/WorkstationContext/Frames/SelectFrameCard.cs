﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Frames
{
    public class SelectFrameCard
    {
        readonly IObserver<Frame> onFrameCardSelected;

        public SelectFrameCard(GameplayEvents events)
        {
            onFrameCardSelected = events.OnFrameCardSelected;
        }

        public void Do(Frame frame)
        {
            onFrameCardSelected.OnNext(frame);
        }
    }
}