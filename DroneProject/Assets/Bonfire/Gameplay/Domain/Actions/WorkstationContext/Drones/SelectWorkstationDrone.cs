﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones
{
    public class SelectWorkstationDrone
    {
        readonly IObserver<Unit> OnRepairNeeded;
        readonly ISubject<Unit> onRechargeNeeded;
        
        readonly DronesRepository dronesRepository;
        readonly DroneService droneService;

        public SelectWorkstationDrone(
            GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            OnRepairNeeded = events.OnRepairNeeded;
            onRechargeNeeded = events.OnRechargeNeeded;
            
            dronesRepository = repositories.DronesRepository;
            droneService = services.DroneService;
        }

        public void Do(Drone drone)
        {
            dronesRepository.SelectDroneForWorkshop(drone);
            
            if (droneService.IsDroneDischarged(drone)) 
                onRechargeNeeded.OnNext(Unit.Default);
            if (droneService.IsDroneDamaged(drone)) 
                OnRepairNeeded.OnNext(Unit.Default);
            
        }
    }
}