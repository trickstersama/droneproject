﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones
{
    public class RepairDrone
    {
        readonly IObserver<Drone> onDroneRepaired;
        readonly DronesRepository dronesRepository;
        readonly DroneService droneService;

        public RepairDrone(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onDroneRepaired = events.OnDroneRepaired;
            dronesRepository = repositories.DronesRepository;
            droneService = services.DroneService;
        }


        public void Do()
        {
            dronesRepository.RetrieveDroneInWorkshop()
                .Select(droneService.RepairDrone)
                .Do(drone => dronesRepository.UpdateDrone(drone))
                .Subscribe(onDroneRepaired.OnNext);
        }
    }
}