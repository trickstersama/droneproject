﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones
{
    public class RechargeDrone
    {
        readonly IObserver<Drone> onDroneRecharged;
        readonly DronesRepository dronesRepository;
        readonly DroneService droneService;

        public RechargeDrone(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onDroneRecharged = events.OnDroneRecharged;
            dronesRepository = repositories.DronesRepository;
            droneService = services.DroneService;
        }

        public void Do()
        {
            dronesRepository.RetrieveDroneInWorkshop()
                .Select(droneService.RechargeDrone)
                .Do(drone => dronesRepository.UpdateDrone(drone))
                .Subscribe(drone => onDroneRecharged.OnNext(drone));
        }
    }
}