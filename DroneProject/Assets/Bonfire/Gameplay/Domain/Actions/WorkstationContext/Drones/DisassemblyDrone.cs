﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones
{
    public class DisassemblyDrone
    {
        readonly IObserver<Unit> onDroneDisassembled;
        readonly DroneService droneService;
        readonly DronesRepository dronesRepository;
        readonly ComponentsRepository componentsRepository;

        public DisassemblyDrone(
            GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            onDroneDisassembled = events.OnDroneDisassembled;
            droneService = services.DroneService;
            dronesRepository = repositories.DronesRepository;
            componentsRepository = repositories.ComponentsRepository;
        }

        public void Do()
        {
            dronesRepository.RetrieveDroneInWorkshop()
                .Do(GetAndSaveModules)
                .Do(GetAndSaveFrame)
                .Do(dronesRepository.Remove)
                .Subscribe(_ => onDroneDisassembled.OnNext(Unit.Default));
        }

        void GetAndSaveFrame(Drone drone)
        {
            var frame = droneService.GetFrameFromDrone(drone);
            componentsRepository.SaveFrame(frame);
        }

        void GetAndSaveModules(Drone drone)
        {
            var modules = droneService.GetModulesFromDrone(drone);
            componentsRepository.SaveModules(modules);
        }
    }
}