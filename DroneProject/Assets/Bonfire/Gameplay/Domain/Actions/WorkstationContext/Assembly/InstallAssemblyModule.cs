﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class InstallAssemblyModule
    {
        readonly IObserver<DroneModifiers> onAssemblyModuleInstalled;
        readonly IObserver<Frame> onAssemblyModuleSlotsUpdated;
        readonly DroneInProgressRepository droneInProgressRepository;

        public InstallAssemblyModule(GameplayEvents events, GameplayRepositories repositories)
        {
            droneInProgressRepository = repositories.DroneInProgressRepository;

            onAssemblyModuleInstalled = events.OnAssemblyModuleInstalled;
            onAssemblyModuleSlotsUpdated = events.OnAssemblyModuleSlotsUpdated;
        }

        public void Do(Module module)
        {
            if (droneInProgressRepository.SlotsAvailableFor(module) > 0)
            {
                droneInProgressRepository.ConsumeSlot(module)
                    .Do(onAssemblyModuleSlotsUpdated.OnNext)
                    .Select(_ => module)
                    .SelectMany(droneInProgressRepository.AddModuleToProgress(module))
                    .Subscribe(onAssemblyModuleInstalled.OnNext);
            }
        }
    }
}