﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class SelectFrame
    {
        readonly IObserver<Frame> onFrameSelected;
        readonly DroneInProgressRepository droneInProgressRepository;

        public SelectFrame(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onFrameSelected = events.OnFrameSelected;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do(Frame frame)
        {
            droneInProgressRepository.ClearFrame();
            droneInProgressRepository.UpdateFrame(frame);
            onFrameSelected.OnNext(frame);
        }
    }
}