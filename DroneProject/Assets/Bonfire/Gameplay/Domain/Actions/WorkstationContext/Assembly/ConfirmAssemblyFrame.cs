﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class ConfirmAssemblyFrame
    {
        readonly IObserver<Frame> onAssemblyFrameConfirmed;
        readonly DroneInProgressRepository droneInProgressRepository;

        public ConfirmAssemblyFrame(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            droneInProgressRepository = repositories.DroneInProgressRepository;
            onAssemblyFrameConfirmed = events.OnAssemblyFrameConfirmed;
        }

        public void Do() => 
            onAssemblyFrameConfirmed.OnNext(droneInProgressRepository.GetFrame());
    }
}