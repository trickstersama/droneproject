﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class NameDrone
    {
        readonly IObserver<Unit> onDroneNamed;
        readonly IObserver<Unit> onDroneNameInvalid;
        
        readonly DroneAssemblerService droneAssemblerService;
        readonly DroneInProgressRepository droneInProgressRepository;

        public NameDrone(GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            onDroneNamed = events.OnDroneNamed;
            onDroneNameInvalid = events.OnDroneNameInvalid;
            droneInProgressRepository = repositories.DroneInProgressRepository;
            droneAssemblerService = services.DroneAssemblerService;
        }

        public void Do(string name)
        {
            if (droneAssemblerService.NameIsValid(name))
            {
                droneInProgressRepository.SetName(name);
                onDroneNamed.OnNext(Unit.Default);
            }
            else
            {
                onDroneNameInvalid.OnNext(Unit.Default);
            }
        }
    }
}