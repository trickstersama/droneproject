﻿using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class UninstallAssemblyModule
    {
        readonly ISubject<Unit> onAssemblyModuleUninstalled;
        readonly DroneInProgressRepository droneInProgressRepository;
        readonly ISubject<Frame> onAssemblyModuleSlotsUpdated;

        public UninstallAssemblyModule(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onAssemblyModuleUninstalled = events.OnAssemblyModuleUninstalled;
            onAssemblyModuleSlotsUpdated = events.OnAssemblyModuleSlotsUpdated;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do(Module module)
        {
            Observable.ReturnUnit()
                .Select(_ => droneInProgressRepository.ReleaseSlot(module))
                .Do(frame => onAssemblyModuleSlotsUpdated.OnNext(frame))
                .Do(_ => onAssemblyModuleUninstalled.OnNext(Unit.Default))
                .SelectMany(_ => droneInProgressRepository.RemoveModuleFromProgress(module))
                .Subscribe();
        }
    }
}