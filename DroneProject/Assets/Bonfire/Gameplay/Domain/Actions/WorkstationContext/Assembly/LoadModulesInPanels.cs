﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly
{
    public class LoadModulesInPanels
    {
        readonly IObserver<IEnumerable<Module>> onModulesInPanelsLoaded;
        readonly ComponentsRepository componentsRepository;
        readonly DroneInProgressRepository droneInProgressRepository;

        public LoadModulesInPanels(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onModulesInPanelsLoaded = events.OnModulesInPanelsLoaded;
            
            componentsRepository = repositories.ComponentsRepository;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do() =>
            componentsRepository.GetAllModules()
                .SelectMany(droneInProgressRepository.LoadModulesForAssembly)
                .Subscribe(onModulesInPanelsLoaded.OnNext);
    }
}