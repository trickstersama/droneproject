﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules
{
    public class SelectModuleTypeToDisplay
    {
        readonly IObserver<IEnumerable<Module>> onModuleTypeToDisplaySelected;
        readonly ComponentsRepository componentsRepository;

        public SelectModuleTypeToDisplay(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onModuleTypeToDisplaySelected = events.OnModuleTypeToDisplaySelected;
            componentsRepository = repositories.ComponentsRepository;
        }

        public void Do(ModuleType type)
        {
            componentsRepository.GetModulesOfType(type)
                .Subscribe(onModuleTypeToDisplaySelected.OnNext);
        }
    }
}