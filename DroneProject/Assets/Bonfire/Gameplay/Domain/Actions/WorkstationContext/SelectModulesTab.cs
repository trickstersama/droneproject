﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext
{
    public class SelectModulesTab
    {
        readonly IObserver<IEnumerable<Module>> onModulesTabSelected;
        readonly ComponentsRepository componentsRepository;

        public SelectModulesTab(
            GameplayEvents events, 
            GameplayRepositories repositories
        )
        {
            onModulesTabSelected = events.OnModulesTabOpened;
            componentsRepository = repositories.ComponentsRepository;
        }

        public void Do() =>
            componentsRepository.GetAllModules()
                .Subscribe(onModulesTabSelected.OnNext);
    }
}