﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.WorkstationContext
{
    public class SelectAssemblyTab
    {
        readonly IObserver<IEnumerable<Frame>> onAssemblyTabSelected;
        readonly ComponentsRepository componentsRepository;
        readonly DroneInProgressRepository droneInProgressRepository;

        public SelectAssemblyTab(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onAssemblyTabSelected = events.OnAssemblyTabSelected;
            
            componentsRepository = repositories.ComponentsRepository;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do() =>
            componentsRepository.GetFrames()
                .Do(_ => droneInProgressRepository.ClearAll())
                .Subscribe(onAssemblyTabSelected.OnNext);
    }
}