﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.BalconyContext
{
    public class OpenBalcony
    {
        readonly IObserver<IEnumerable<MissionResults>> onBalconyOpened;
        readonly MissionsRepository missionsRepository;

        public OpenBalcony(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onBalconyOpened = events.OnBalconyOpened;
            missionsRepository = repositories.MissionsRepository;
        }

        public void Do() =>
            Observable.Return(missionsRepository.GetFinishedMissions())
                .Subscribe(_ => onBalconyOpened.OnNext(Enumerable.Empty<MissionResults>()));
    }
}