using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.HouseBatteryContext
{
    public class UpgradeHouseBattery
    {
        readonly IObserver<Unit> onHouseBatteryUpgraded;
        readonly HouseResourcesRepository houseResourcesRepository;

        public UpgradeHouseBattery(
            HouseGameplayEvents houseEvents, 
            GameplayRepositories repositories
        ) {
            onHouseBatteryUpgraded = houseEvents.OnHouseBatteryUpgraded;
            houseResourcesRepository = repositories.HouseResourcesRepository;
        }

        public void Do(HouseBattery houseBattery)
        {
            houseResourcesRepository.AddHouseBattery(houseBattery);
            onHouseBatteryUpgraded.OnNext(Unit.Default);
        }
    }
}