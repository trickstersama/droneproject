using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Actions.HouseBatteryContext
{
    public class OpenHouseBattery
    {
        readonly HouseResourcesRepository houseResourcesRepository;
        readonly IObserver<HouseEnergyStatus> onHouseBatteryOpened;

        public OpenHouseBattery(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            houseResourcesRepository = repositories.HouseResourcesRepository;
            onHouseBatteryOpened = events.OnHouseBatteryOpened;
        }

        public void Do()
        {
            CreateEnergyStats()
                .Subscribe(onHouseBatteryOpened.OnNext);
        }

        IObservable<HouseEnergyStatus> CreateEnergyStats() =>
            Observable.Return(new HouseEnergyStatus(
                productionPerSecond: houseResourcesRepository.GetEnergyProductionPerSecond(),
                consumptionPerSecond: houseResourcesRepository.GetEnergyConsumptionPerSecond(),
                capacity: houseResourcesRepository.GetTotalStorage()));
    }
}