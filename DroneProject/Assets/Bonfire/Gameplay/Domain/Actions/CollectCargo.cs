﻿using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Actions
{
    public class CollectCargo
    {
        readonly HouseItemsRepository houseItemsRepository;
        public CollectCargo(
            GameplayRepositories repositories
        ) =>
            houseItemsRepository = repositories.HouseItemsRepository;

        public void Do() => houseItemsRepository.AddItems();
    }
}