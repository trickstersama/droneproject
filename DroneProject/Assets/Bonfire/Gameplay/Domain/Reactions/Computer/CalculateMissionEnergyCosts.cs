using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Computer
{
    public class CalculateMissionEnergyCosts
    {
        readonly MissionService missionService;
        readonly MissionBuilderRepository missionBuilderRepository;
        readonly IObserver<MissionOrders> onEnergyCostsCalculated;
        readonly DronesRepository dronesRepository;

        public CalculateMissionEnergyCosts(
            GameplayEvents events,
            GameplayServices services, 
            GameplayRepositories repositories
        ) {
            onEnergyCostsCalculated = events.OnEnergyCostsCalculated;
            missionService = services.MissionService;
            missionBuilderRepository = repositories.MissionBuilderRepository;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do() =>
            missionBuilderRepository.GetMissionOrders()
                .SelectMany(CalculateCost)
                .SelectMany(missionBuilderRepository.AddEnergyCost)
                .Subscribe(onEnergyCostsCalculated.OnNext);


        IObservable<MissionEnergyCost> CalculateCost(MissionOrders missionOrders)
        {
            var drone = dronesRepository.GetDroneByName(missionOrders.droneName);
            var energyCost = missionService.CalculateEnergyCosts(missionOrders, drone);
            return Observable.Return(energyCost);
        }
    }
}