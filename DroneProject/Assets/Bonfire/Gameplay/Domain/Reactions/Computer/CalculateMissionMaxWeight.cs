﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Computer
{
    public class CalculateMissionMaxWeight
    {
        readonly IObserver<MissionOrders> onMissionMaxWeightAllowed;
        readonly MissionBuilderRepository missionBuilderRepository;
        readonly MissionService missionService;
        readonly DronesRepository dronesRepository;

        public CalculateMissionMaxWeight(
            GameplayEvents events, 
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onMissionMaxWeightAllowed = events.OnMissionMaxWeightAllowed;
            missionBuilderRepository = repositories.MissionBuilderRepository;
            dronesRepository = repositories.DronesRepository;
            missionService = services.MissionService;
        }

        public void Do() =>
            missionBuilderRepository.GetMissionOrders()
                .SelectMany(CalculateMaxExtraWeight)
                .SelectMany(missionBuilderRepository.AddMaxWeightAllowed)
                .Subscribe(onMissionMaxWeightAllowed.OnNext);

        IObservable<float> CalculateMaxExtraWeight(MissionOrders missionOrders)
        {
            var drone = dronesRepository.GetDroneByName(missionOrders.droneName);
            return Observable.Return(missionService.CalculateMaxExtraWeightAllowed(drone));
        }
    }
}