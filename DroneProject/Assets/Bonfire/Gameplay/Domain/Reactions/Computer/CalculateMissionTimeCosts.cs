using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Computer
{
    public class CalculateMissionTimeCosts
    {
        readonly IObserver<MissionOrders> onMissionTimeCostsCalculated;
        readonly MissionService missionService;
        readonly MissionBuilderRepository missionBuilderRepository;
        readonly ModifiersRepository modifiersRepository;
        readonly DronesRepository dronesRepository;

        public CalculateMissionTimeCosts(
            GameplayEvents events,
            GameplayServices services, 
            GameplayRepositories repositories
        ) {
            onMissionTimeCostsCalculated = events.OnMissionTimeCostsCalculated;
            missionService = services.MissionService;
            missionBuilderRepository = repositories.MissionBuilderRepository;
            modifiersRepository = repositories.ModifiersRepository;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do() =>
            missionBuilderRepository.GetMissionOrders()
                .SelectMany(CalculateTime)
                .SelectMany(missionBuilderRepository.AddTimeCost)
                .Subscribe(onMissionTimeCostsCalculated.OnNext);

        IObservable<MissionTimeCost> CalculateTime(MissionOrders missionOrders)
        {
            var drone = dronesRepository.GetDroneByName(missionOrders.droneName);
            var missionTimeCostsModifiers = modifiersRepository.GetTimeCostModifiers();
            var timeCost = missionService.CalculateTimeCost(missionOrders, missionTimeCostsModifiers, drone);
            return Observable.Return(timeCost);
        }
    }
}