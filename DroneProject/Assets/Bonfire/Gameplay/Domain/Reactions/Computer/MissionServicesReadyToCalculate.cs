﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Computer
{
    public class MissionServicesReadyToCalculate
    {
        readonly IObserver<Unit> onMissionServicesReadyToCalculate;
        readonly MissionBuilderRepository missionBuilderRepository;

        public MissionServicesReadyToCalculate(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onMissionServicesReadyToCalculate = events.OnMissionServicesReadyToCalculate;
            missionBuilderRepository = repositories.MissionBuilderRepository;
        }
        
        public void Do() =>
            missionBuilderRepository.GetMissionOrders()
                .Where(_ => missionBuilderRepository.IsMissionReadyToCalculate())
                .Subscribe(_ => onMissionServicesReadyToCalculate.OnNext(Unit.Default));
    }
}