﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.House
{
    public class ProduceEnergy
    {
        readonly IObserver<float> onEnergyProduced;
        readonly HouseResourcesRepository houseResourcesRepository;

        public ProduceEnergy(
            HouseGameplayEvents houseEvents,
            GameplayRepositories repositories
        ) {
            onEnergyProduced = houseEvents.OnEnergyProduced;
            houseResourcesRepository = repositories.HouseResourcesRepository;
        }

        public void Do(float timeScale) =>
            Observable.Return(houseResourcesRepository.GetEnergyProductionPerSecond())
                .Select(time => time * timeScale)
                .Subscribe(onEnergyProduced.OnNext);
    }
}