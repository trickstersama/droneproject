﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.House
{
    public class DistributeEnergy
    {
        readonly IObserver<float> onEnergyOverheadProduced;
        readonly HouseResourcesRepository houseResourcesRepository;
        readonly IObserver<Unit> onInsufficientEnergyIsProduced;

        public DistributeEnergy(
            HouseGameplayEvents houseEvents, 
            GameplayRepositories repositories
        ) {
            onEnergyOverheadProduced = houseEvents.OnEnergyOverheadProduced;
            onInsufficientEnergyIsProduced = houseEvents.OnInsufficientEnergyIsProduced;
            houseResourcesRepository = repositories.HouseResourcesRepository;
        }

        public void Do(float energyProduced)
        {
            Observable.Return(houseResourcesRepository.GetEnergyConsumptionPerSecond())
                .Select(consumption => energyProduced - consumption)
                .Do(SendOnInsufficientEnergyIsProduced)
                .Where(energy => energy > 0)
                .Subscribe(onEnergyOverheadProduced.OnNext);
        }


        void SendOnInsufficientEnergyIsProduced(float energy)
        {
            if (energy < 0 ) onInsufficientEnergyIsProduced.OnNext(Unit.Default); 
        }
    }
}