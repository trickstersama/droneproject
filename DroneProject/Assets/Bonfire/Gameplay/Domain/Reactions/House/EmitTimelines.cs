﻿using System;
using Bonfire.Gameplay.Domain.Wrappers;

namespace Bonfire.Gameplay.Domain.Reactions.House
{
    public class EmitTimelines
    {
        readonly IObserver<float> onEnergyTimelineEmitted;
        readonly IObserver<float> onMissionsTimelineEmmited;

        public EmitTimelines(
            HouseGameplayEvents houseEvents,
            GameplayEvents gameplayEvents
        ) {
            onEnergyTimelineEmitted = houseEvents.OnEnergyTimelineEmitted;
            onMissionsTimelineEmmited = gameplayEvents.OnMissionsTimelineEmitted;
        }

        public void Do(float timelineInterval)
        {
            onEnergyTimelineEmitted.OnNext(timelineInterval);
            onMissionsTimelineEmmited.OnNext(timelineInterval);
        }
    }
}