﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.House
{
    public class StoreEnergy
    {
        readonly IObserver<float> onEnergyStored;
        readonly IObserver<Unit> onEnergyStorageFull;
        readonly HouseResourcesRepository houseResourcesRepository;

        public StoreEnergy(
            HouseGameplayEvents houseEvents,
            GameplayRepositories repositories
        ) {
            onEnergyStored = houseEvents.OnEnergyStored;
            onEnergyStorageFull = houseEvents.OnEnergyStorageFull;
            
            houseResourcesRepository = repositories.HouseResourcesRepository;
        }

        public void Do(float energyToStore)
        {
            if (houseResourcesRepository.IsEnergyStorageFull())
                onEnergyStorageFull.OnNext(Unit.Default);
            else
                houseResourcesRepository.StoreEnergy(energyToStore)
                    .Subscribe(onEnergyStored.OnNext);
        }
    }
}