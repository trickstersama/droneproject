﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class RemoveFrameFromInventory
    {
        readonly IObserver<IEnumerable<Frame>> onFrameFromInventoryRemoved;
        readonly ComponentsRepository componentsRepository;

        public RemoveFrameFromInventory(GameplayEvents events, GameplayRepositories repositories)
        {
            onFrameFromInventoryRemoved = events.OnFrameFromInventoryRemoved;
            componentsRepository = repositories.ComponentsRepository;
        }

        public void Do(Frame frame)
        {
            componentsRepository.RemoveFrame(frame)
                .Subscribe(onFrameFromInventoryRemoved.OnNext);
        }
    }
}