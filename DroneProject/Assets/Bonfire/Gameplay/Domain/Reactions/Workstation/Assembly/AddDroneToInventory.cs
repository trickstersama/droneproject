﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class AddDroneToInventory
    {
        readonly IObserver<IEnumerable<Drone>> onDroneAddedToInventory;
        readonly DronesRepository dronesRepository;

        public AddDroneToInventory(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onDroneAddedToInventory = events.OnDroneAddedToInventory;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do(Drone drone)
        {
            dronesRepository.AddNewDrone(drone)
                .Subscribe(onDroneAddedToInventory.OnNext);
        }
    }
}