﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class LoadFramePanel
    {
        readonly IObserver<IEnumerable<Frame>> onFramePanelLoaded;
        readonly ComponentsRepository componentsRepository;
        readonly DroneInProgressRepository droneInProgressRepository;

        public LoadFramePanel(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onFramePanelLoaded = events.OnFramePanelLoaded;
            
            componentsRepository = repositories.ComponentsRepository;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do() =>
            componentsRepository.GetFrames()
                .SelectMany(droneInProgressRepository.LoadFramesForAssembly)
                .Subscribe(onFramePanelLoaded.OnNext);
    }
}