﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class SelectFramesTab
    {
        readonly IObserver<IEnumerable<Frame>> onFramesTabSelected;
        readonly ComponentsRepository componentsRepository;

        public SelectFramesTab(GameplayEvents events, GameplayRepositories repositories)
        {
            onFramesTabSelected = events.OnFramesTabSelected;
            componentsRepository = repositories.ComponentsRepository;
        }

        public void Do() =>
            componentsRepository.GetFrames()
                .Subscribe(onFramesTabSelected.OnNext);
    }
}