﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class RemoveModulesFromInventory
    {
        readonly ComponentsRepository componentsRepository;
        readonly IObserver<IEnumerable<Module>> onModulesInInventoryRemoved;

        public RemoveModulesFromInventory(GameplayEvents events, GameplayRepositories repositories)
        {
            componentsRepository = repositories.ComponentsRepository;
            onModulesInInventoryRemoved = events.OnModulesInInventoryRemoved;
        }

        public void Do(IEnumerable<Module> modules)
        {
            componentsRepository.RemoveModules(modules)
                .Subscribe(onModulesInInventoryRemoved.OnNext);
        }
    }
}