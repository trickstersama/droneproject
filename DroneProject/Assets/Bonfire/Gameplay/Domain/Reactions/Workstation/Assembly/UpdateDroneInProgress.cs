﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class UpdateDroneInProgress
    {
        readonly IObserver<DroneModifiers> onDroneInProgressUpdated;
        readonly DroneInProgressRepository droneInProgressRepository;

        public UpdateDroneInProgress(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onDroneInProgressUpdated = events.OnDroneInProgressUpdated;
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do()
        {
            droneInProgressRepository.GetDroneModifiers()
                .Subscribe(onDroneInProgressUpdated.OnNext);
        }
    }
}