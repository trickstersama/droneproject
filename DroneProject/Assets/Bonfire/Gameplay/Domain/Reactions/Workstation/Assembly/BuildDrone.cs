﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly
{
    public class BuildDrone
    {
        readonly IObserver<Drone> onDroneBuilt;
        
        readonly DroneAssemblerService droneAssemblerService;
        readonly DroneInProgressRepository droneInProgressRepository;

        public BuildDrone(
            GameplayEvents events, 
            GameplayServices services, 
            GameplayRepositories repositories
        ) {
            onDroneBuilt = events.OnDroneBuilt;
            droneAssemblerService = services.DroneAssemblerService;
            
            droneInProgressRepository = repositories.DroneInProgressRepository;
        }

        public void Do()
        {
            droneAssemblerService.CreateEmptyDrone(droneInProgressRepository.GetFrame())
                .SelectMany(AddModifiers)
                .SelectMany(AddModules)
                .SelectMany(AddName)
                .Subscribe(onDroneBuilt.OnNext);
        }

        IObservable<Drone> AddName(Drone drone) => 
            droneAssemblerService.AddName(drone, droneInProgressRepository.GetName());

        IObservable<Drone> AddModifiers(Drone drone) =>
            from modifiers in droneInProgressRepository.GetDroneModifiers()
            from newDrone in droneAssemblerService.AddModifiers(drone, modifiers)
            select newDrone;
        
        IObservable<Drone> AddModules(Drone drone) =>
            from modules in droneInProgressRepository.GetModules()
            from newDrone in droneAssemblerService.AddModules(drone, modules)
            select newDrone;
    }
}