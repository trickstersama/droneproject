﻿using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions
{
    public class DroneEncounter
    {
        readonly ISubject<Encounter> onEncounterStart;
        readonly EncounterDeciderService encounterDecider;
        
        public DroneEncounter(
            GameplayEvents events,
            GameplayServices services
        )
        {
            onEncounterStart = events.OnEncounterStart;
            encounterDecider = services.EncounterDeciderService;
        }

        public void Do()
        {
            encounterDecider.DetermineEncounterType()
                .Subscribe(onEncounterStart.OnNext);
        }
    }
}