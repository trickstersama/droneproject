﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class StartMissionTarget
    {
        readonly IObserver<Unit> onMissionTargetStarted;
        readonly DronesRepository dronesRepository;

        public StartMissionTarget(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onMissionTargetStarted = events.OnMissionTargetStarted;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do(Mission mission) =>
            GetDrone(mission)
                .Select(UpdateDroneToOnTarget)
                .Subscribe(_ => onMissionTargetStarted.OnNext(Unit.Default));

        Drone UpdateDroneToOnTarget(Drone drone)
        {
            var updatedDrone = drone.ModifyThis(withStatus: DroneStatus.OnTarget);
            dronesRepository.UpdateDrone(updatedDrone);
            return updatedDrone;
        }

        IObservable<Drone> GetDrone(Mission mission) => 
            Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName));
    }
}