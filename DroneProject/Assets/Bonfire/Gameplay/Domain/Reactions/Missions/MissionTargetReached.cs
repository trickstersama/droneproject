﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class MissionTargetReached
    {
        readonly IObserver<Mission> onMissionTargetReached;
        readonly DronesRepository dronesRepository;
        readonly MissionService missionService;


        public MissionTargetReached(
            GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            onMissionTargetReached = events.OnMissionTargetReached;
            dronesRepository = repositories.DronesRepository;
            missionService = services.MissionService;
        }

        public void Do(Mission mission) =>
            Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName))
                .Where(drone => drone.status == DroneStatus.Traveling)
                .Where(_ => EnoughTimeHasPassed(mission))
                .Subscribe(_ => onMissionTargetReached.OnNext(mission));

        bool EnoughTimeHasPassed(Mission mission) => 
            missionService.TimeNeededToTarget(mission) < mission.timeOnMission.timePassed;
    }
}