﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class MissionReturnsHome
    {
        readonly IObserver<Mission> onMissionReturnedHome;
        readonly DronesRepository dronesRepository;
        readonly MissionService missionService;

        public MissionReturnsHome(
            GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            onMissionReturnedHome = events.OnMissionReturnedHome;
            dronesRepository = repositories.DronesRepository;
            missionService = services.MissionService;
        }

        public void Do(Mission mission)
        {
            GetDrone(mission)
                .Where(drone => drone.status == DroneStatus.Returning)
                .Where(_ => EnoughTimeHasPassed(mission))
                .Subscribe(_ => onMissionReturnedHome.OnNext(mission));
        }

        IObservable<Drone> GetDrone(Mission mission) => 
            Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName));

        bool EnoughTimeHasPassed(Mission mission) => 
            missionService.TimeNeededToReturn(mission) - mission.timeOnMission.timePassed < 0;

    }
}