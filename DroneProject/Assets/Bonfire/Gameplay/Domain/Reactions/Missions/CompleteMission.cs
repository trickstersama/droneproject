﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class CompleteMission
    {
        readonly IObserver<Unit> onMissionCompleted;
        readonly DronesRepository dronesRepository;

        public CompleteMission(
            GameplayEvents events, 
            GameplayRepositories repositories
        ) {
            onMissionCompleted = events.OnMissionCompleted;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do(Mission mission)
        {
            GetDrone(mission)
                .Select(drone => drone.ModifyThis(withStatus: DroneStatus.InBalcony))
                .Do(drone => dronesRepository.UpdateDrone(drone))
                .Subscribe(_ => onMissionCompleted.OnNext(Unit.Default));
        }

        IObservable<Drone> GetDrone(Mission mission)
        {
            return Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName));
        }
    }
}