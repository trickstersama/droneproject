﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class MissionTargetCompleted
    {
        readonly IObserver<Mission> onMissionTargetCompleted;
        readonly DronesRepository dronesRepository;
        readonly MissionService missionService;

        public MissionTargetCompleted(
            GameplayEvents events,
            GameplayRepositories repositories,
            GameplayServices services
        ) {
            onMissionTargetCompleted = events.OnMissionTargetCompleted;
            dronesRepository = repositories.DronesRepository;
            missionService = services.MissionService;
        }

        public void Do(Mission mission) =>
            GetDrone(mission)
                .Where(drone => drone.status == DroneStatus.OnTarget)
                .Where(_ => EnoughTimeHasPassed(mission))
                .Subscribe(_ => onMissionTargetCompleted.OnNext(mission));

        bool EnoughTimeHasPassed(Mission mission) => 
            missionService.TimeNeededToCompleteTarget(mission) < mission.timeOnMission.timePassed;

        IObservable<Drone> GetDrone(Mission mission) => 
            Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName));
    }
}