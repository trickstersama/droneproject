﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class UpdateMissionsTime
    {
        readonly IObserver<IEnumerable<Mission>> onMissionsTimeUpdated;
        readonly MissionsRepository missionsRepository;
        readonly MissionService missionService;

        public UpdateMissionsTime(
            GameplayEvents events,
            GameplayRepositories repositories, 
            GameplayServices services
        ) {
            onMissionsTimeUpdated = events.OnMissionsTimeUpdated;
            missionsRepository = repositories.MissionsRepository;
            missionService = services.MissionService;
        }

        public void Do(float timePassed)
        {
            missionsRepository.GetAllMissions()
                .Select(missions => UpdateTimes(missions, timePassed))
                .Do(missionsRepository.UpdateMissions)
                .Do(missions => onMissionsTimeUpdated.OnNext(missions))
                .Subscribe();
        }

        IEnumerable<Mission> UpdateTimes(IEnumerable<Mission> missions, float timePassed) => 
            missions.Select(mission => missionService.UpdateTime(mission, timePassed)).ToList(); //to list para evitar lazyness
    }
}