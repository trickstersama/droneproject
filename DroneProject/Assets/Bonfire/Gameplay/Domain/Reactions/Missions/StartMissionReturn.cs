﻿using System;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Domain.Reactions.Missions
{
    public class StartMissionReturn
    {
        readonly IObserver<Unit> onMissionReturnStarted;
        readonly DronesRepository dronesRepository;

        public StartMissionReturn(
            GameplayEvents events,
            GameplayRepositories repositories
        ) {
            onMissionReturnStarted = events.OnMissionReturnStarted;
            dronesRepository = repositories.DronesRepository;
        }

        public void Do(Mission mission)
        {
            GetDrone(mission)
                .Select(UpdateDroneToOnTarget)
                .Subscribe(_ => onMissionReturnStarted.OnNext(Unit.Default));
        }
        
        Drone UpdateDroneToOnTarget(Drone drone)
        {
            var updatedDrone = drone.ModifyThis(withStatus: DroneStatus.Returning);
            dronesRepository.UpdateDrone(updatedDrone);
            return updatedDrone;
        }

        IObservable<Drone> GetDrone(Mission mission) => 
            Observable.Return(dronesRepository.GetDroneByName(mission.missionOrders.droneName));

    }
}