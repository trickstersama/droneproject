﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface EncounterDeciderService
    {
        IObservable<Encounter> DetermineEncounterType();
    }
}