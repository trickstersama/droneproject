﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface DroneAssemblerService
    {
        IObservable<Drone> CreateEmptyDrone(Frame frame);
        IObservable<Drone> AddModifiers(Drone drone, DroneModifiers modifiers);
        IObservable<Drone> AddModules(Drone drone, IEnumerable<Module> modules);
        IObservable<Drone> AddName(Drone drone, string name);
        bool NameIsValid(string name);
    }
}