﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface DroneService
    {
        bool IsDroneDischarged(Drone drone);
        bool IsDroneDamaged(Drone drone);
        Drone RechargeDrone(Drone drone);
        Drone RepairDrone(Drone drone);
        IEnumerable<Module> GetModulesFromDrone(Drone drone);
        Frame GetFrameFromDrone(Drone drone);
    }
}