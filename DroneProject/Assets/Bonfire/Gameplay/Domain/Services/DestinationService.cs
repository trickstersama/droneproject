﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface DestinationService
    {
        Destination CreateDefaultDestination();
        IObservable<Destination> AddMissionType(Destination destination, MissionType type);
        IObservable<Destination> AddCoordinates(Destination destination, MapCoordinates mapCoordinates);
        IObservable<Destination> AddName(Destination destination, string name);
        bool IsValid(Destination destination);
    }
}