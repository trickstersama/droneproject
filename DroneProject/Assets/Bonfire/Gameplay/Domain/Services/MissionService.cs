﻿using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface MissionService
    {
        float HorizontalAcceleration(Drone drone, float occupiedStorage = 0);
        float TerminalVelocity(Drone drone, float occupiedStorage = 0);
        float AirDragForceByVelocity(Drone drone, float velocity);
        float FlightTimeToTarget(Drone drone, MapCoordinates mapCoordinates, float occupiedStorage = 0);
        float LiftForce(Drone drone, float occupiedStorage = 0);
        float EfficientThrust(Drone drone, float occupiedStorage = 0);
        float TravelJoulesWithNoLoss(Drone drone, MapCoordinates mapCoordinates, float occupiedStorage = 0);
        MissionEnergyCost CalculateEnergyCosts(MissionOrders missionOrders, Drone drone);
        MissionTimeCost CalculateTimeCost(
            MissionOrders orders, 
            MissionsModifiers missionsesModifiers,
            Drone drone);
        float CalculateMaxExtraWeightAllowed(Drone drone);
        Mission CreateMission(MissionOrders missionOrders);
        Mission UpdateTime(Mission mission, float timePassed);
        float TimeNeededToReturn(Mission mission);
        float TimeNeededToTarget(Mission mission);
        float TimeNeededToCompleteTarget(Mission mission);
    }
}