﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Domain.Services
{
    public interface FrameSlotsAmountService
    {
        IObservable<int> GetAmountByModuleType(Frame frame, ModuleType moduleType);
    }
}