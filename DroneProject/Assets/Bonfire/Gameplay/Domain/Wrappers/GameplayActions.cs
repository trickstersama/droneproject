﻿using Bonfire.Gameplay.Domain.Actions;
using Bonfire.Gameplay.Domain.Actions.BalconyContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Status;
using Bonfire.Gameplay.Domain.Actions.HouseBatteryContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Frames;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules;
using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct GameplayActions
    {
        public readonly SendDrone SendDrone;
        public readonly CollectCargo CollectCargo;
        public readonly OpenComputer OpenComputer;
        public readonly SelectMissionDrone SelectMissionDrone;
        public readonly SelectSendDroneTab SelectSendDroneTab;
        public readonly SelectDroneStatusTab SelectDroneStatusTab;
        public readonly SelectDroneCardInStatus SelectDroneCardInStatus;
        public readonly ReturnDrone ReturnDrone;
        public readonly OpenWorkstation OpenWorkstation;
        public readonly SelectModulesTab SelectModulesTab;
        public readonly SelectAssemblyTab SelectAssemblyTab;
        public readonly SelectFrame SelectFrame;
        public readonly ConfirmAssemblyFrame ConfirmAssemblyFrame;
        public readonly InstallAssemblyModule InstallAssemblyModule;
        public readonly SelectModuleTypeToDisplay SelectModuleTypeToDisplay;
        public readonly SelectFramesTab SelectFramesTab;
        public readonly SelectFrameCard SelectFrameCard;
        public readonly SelectMissionType SelectMissionType;
        public readonly SelectMapTab SelectMapTab;
        public readonly SelectNewPin SelectNewPin;
        public readonly SelectNewPinCoordinates SelectNewPinCoordinates;
        public readonly ChoosePinName ChoosePinName;
        public readonly ChoosePinType ChoosePinType;
        public readonly SaveNewPin SaveNewPin;
        public readonly SelectPinnedLocationsSection SelectPinnedLocationsSection;
        public readonly NameDrone NameDrone;
        public readonly OpenMap OpenMap;
        public readonly SelectDestination SelectDestination;
        public readonly SelectWorkstationDronesTab SelectWorkstationDronesTab;
        public readonly SelectWorkstationDrone SelectWorkstationDrone;
        public readonly RechargeDrone RechargeDrone;
        public readonly RepairDrone RepairDrone;
        public readonly DisassemblyDrone DisassemblyDrone;
        public readonly SelectPinnedLocation SelectPinnedLocation;
        public readonly MissionServicesReadyToCalculate MissionServicesReadyToCalculate;
        public readonly OpenHouseBattery OpenHouseBattery;
        public readonly OpenBalcony OpenBalcony;
        public readonly UninstallAssemblyModule UninstallAssemblyModule;

        public GameplayActions(
            SendDrone sendDrone,
            CollectCargo collectCargo,
            OpenComputer openComputer,
            SelectMissionDrone selectMissionDrone, 
            SelectSendDroneTab selectSendDroneTab,
            SelectDroneStatusTab selectDroneStatusTab, 
            SelectDroneCardInStatus selectDroneCardInStatus,
            ReturnDrone returnDrone, 
            OpenWorkstation openWorkstation,
            SelectModulesTab selectModulesTab, 
            SelectAssemblyTab selectAssemblyTab, 
            SelectFrame selectFrame, 
            ConfirmAssemblyFrame confirmAssemblyFrame, 
            InstallAssemblyModule installAssemblyModule, 
            SelectModuleTypeToDisplay selectModuleTypeToDisplay, 
            SelectFramesTab selectFramesTab, 
            SelectFrameCard selectFrameCard, 
            SelectMissionType selectMissionType, 
            SelectMapTab selectMapTab, 
            SelectNewPin selectNewPin, 
            SelectNewPinCoordinates selectNewPinCoordinates,
            ChoosePinName choosePinName,
            ChoosePinType choosePinType, 
            SaveNewPin saveNewPin,
            SelectPinnedLocationsSection selectPinnedLocationsSection, 
            NameDrone nameDrone, 
            OpenMap openMap,
            SelectDestination selectDestination,
            SelectWorkstationDronesTab selectWorkstationDronesTab, 
            SelectWorkstationDrone selectWorkstationDrone, 
            RechargeDrone rechargeDrone, 
            RepairDrone repairDrone, 
            DisassemblyDrone disassemblyDrone,
            SelectPinnedLocation selectPinnedLocation, 
            MissionServicesReadyToCalculate missionServicesReadyToCalculate,
            OpenHouseBattery openHouseBattery, 
            OpenBalcony openBalcony,
            UninstallAssemblyModule uninstallAssemblyModule
        ) {
            SendDrone = sendDrone;
            CollectCargo = collectCargo;
            OpenComputer = openComputer;
            SelectMissionDrone = selectMissionDrone;
            SelectSendDroneTab = selectSendDroneTab;
            SelectDroneStatusTab = selectDroneStatusTab;
            SelectDroneCardInStatus = selectDroneCardInStatus;
            ReturnDrone = returnDrone;
            OpenWorkstation = openWorkstation;
            SelectModulesTab = selectModulesTab;
            SelectAssemblyTab = selectAssemblyTab;
            SelectFrame = selectFrame;
            ConfirmAssemblyFrame = confirmAssemblyFrame;
            InstallAssemblyModule = installAssemblyModule;
            SelectModuleTypeToDisplay = selectModuleTypeToDisplay;
            SelectFramesTab = selectFramesTab;
            SelectFrameCard = selectFrameCard;
            SelectMissionType = selectMissionType;
            SelectMapTab = selectMapTab;
            SelectNewPin = selectNewPin;
            SelectNewPinCoordinates = selectNewPinCoordinates;
            ChoosePinName = choosePinName;
            ChoosePinType = choosePinType;
            SaveNewPin = saveNewPin;
            SelectPinnedLocationsSection = selectPinnedLocationsSection;
            NameDrone = nameDrone;
            OpenMap = openMap;
            SelectDestination = selectDestination;
            SelectWorkstationDronesTab = selectWorkstationDronesTab;
            SelectWorkstationDrone = selectWorkstationDrone;
            RechargeDrone = rechargeDrone;
            RepairDrone = repairDrone;
            DisassemblyDrone = disassemblyDrone;
            SelectPinnedLocation = selectPinnedLocation;
            MissionServicesReadyToCalculate = missionServicesReadyToCalculate;
            OpenHouseBattery = openHouseBattery;
            OpenBalcony = openBalcony;
            UninstallAssemblyModule = uninstallAssemblyModule;
        }
    }
}