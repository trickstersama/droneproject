﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct GameplayEvents
    {
        public readonly ISubject<Unit> OnDroneSent;
        public readonly ISubject<Drone> OnDroneReturning;
        public readonly ISubject<Encounter> OnEncounterStart;
        public readonly ISubject<Module> OnModuleRepaired;
        public readonly ISubject<IEnumerable<Drone>> OnSendDroneTabSelected;
        public readonly ISubject<MissionOrders> OnMissionDroneSelected;
        public readonly ISubject<Unit> OnOpenComputer;
        public readonly ISubject<IEnumerable<Drone>> OnDroneStatusTabSelected;
        public readonly ISubject<Drone> OnDroneInStatusSelected;
        public readonly ISubject<Unit> OnOpenWorkstation;
        public readonly ISubject<IEnumerable<Frame>> OnFramePanelLoaded;
        public readonly ISubject<IEnumerable<Module>> OnModulesTabOpened;
        public readonly ISubject<IEnumerable<Frame>> OnAssemblyTabSelected;
        public readonly ISubject<Frame> OnFrameSelected;
        public readonly ISubject<IEnumerable<Module>> OnModulesInPanelsLoaded;
        public readonly ISubject<Frame> OnAssemblyFrameConfirmed;
        public readonly ISubject<DroneModifiers> OnAssemblyModuleInstalled;
        public readonly ISubject<DroneModifiers> OnDroneInProgressUpdated;
        public readonly ISubject<Frame> OnAssemblyModuleSlotsUpdated;
        public readonly ISubject<Drone> OnDroneBuilt;
        public readonly ISubject<IEnumerable<Module>> OnModulesInInventoryRemoved;
        public readonly ISubject<IEnumerable<Frame>> OnFrameFromInventoryRemoved;
        public readonly ISubject<IEnumerable<Drone>> OnDroneAddedToInventory;
        public readonly ISubject<IEnumerable<Module>> OnModuleTypeToDisplaySelected;
        public readonly ISubject<IEnumerable<Frame>> OnFramesTabSelected;
        public readonly ISubject<Frame> OnFrameCardSelected;
        public readonly ISubject<MissionOrders> OnMissionTypeSelected;
        public readonly ISubject<MissionOrders> OnDestinationSelected;
        public readonly ISubject<IEnumerable<Drone>> OnWorkstationDronesTabSelected;
        public readonly ISubject<IEnumerable<Destination>> OnMapTabSelected;
        public readonly ISubject<Unit> OnNewPinSelected;
        public readonly ISubject<IEnumerable<Mission>> OnMissionsTimeUpdated;
        public readonly ISubject<Destination> OnNewPinCoordinatesSelected;
        public readonly ISubject<Destination> OnPinNameChosen;
        public readonly ISubject<Destination> OnPinTypeChosen;
        public readonly ISubject<Unit> OnNewPinSaved;
        public readonly ISubject<IEnumerable<Destination>> OnPinnedLocationsSectionSelected;
        public readonly ISubject<Unit> OnDroneNamed;
        public readonly ISubject<Unit> OnDroneNameInvalid;
        public readonly ISubject<IEnumerable<Destination>> OnMapOpened;
        public readonly ISubject<Unit> OnRepairNeeded;
        public readonly ISubject<Drone> OnDroneRecharged;
        public readonly ISubject<Unit> OnRechargeNeeded;
        public readonly ISubject<Drone> OnDroneRepaired;
        public readonly ISubject<Unit> OnDroneDisassembled;
        public readonly ISubject<Destination> OnPinnedLocationSelected;
        public readonly ISubject<Unit> OnMissionServicesReadyToCalculate;
        public readonly ISubject<MissionOrders> OnEnergyCostsCalculated;
        public readonly ISubject<MissionOrders> OnMissionTimeCostsCalculated;
        public readonly ISubject<MissionOrders> OnMissionMaxWeightAllowed;
        public readonly ISubject<HouseEnergyStatus> OnHouseBatteryOpened;
        public readonly ISubject<Unit> OnDronesInMissionUpdated;
        public readonly ISubject<float> OnMissionsTimelineEmitted;
        public readonly ISubject<Mission> OnMissionTargetReached;
        public readonly ISubject<Mission> OnMissionTargetCompleted;
        public readonly ISubject<Unit> OnMissionTargetStarted;
        public readonly ISubject<Unit> OnMissionReturnStarted;
        public readonly ISubject<Mission> OnMissionReturnedHome;
        public readonly ISubject<Unit> OnMissionCompleted;
        public readonly ISubject<IEnumerable<MissionResults>> OnBalconyOpened;
        public readonly ISubject<Unit> OnAssemblyModuleUninstalled;
        public readonly ISubject<Unit> OnNewPinReadyToSave;
        public readonly ISubject<Unit> onNewPinNotReadyToSave;


        public GameplayEvents(
            ISubject<Unit> onDroneSent,
            ISubject<Drone> onDroneReturning,
            ISubject<Encounter> onEncounterStart,
            ISubject<Module> onModuleRepaired,
            ISubject<IEnumerable<Drone>> onSendDroneTabSelected,
            ISubject<MissionOrders> onMissionDroneSelected,
            ISubject<Unit> onOpenComputer,
            ISubject<IEnumerable<Drone>> onDroneStatusTabSelected,
            ISubject<Drone> onDroneInStatusSelected,
            ISubject<Unit> onOpenWorkstation,
            ISubject<IEnumerable<Frame>> onFramePanelLoaded,
            ISubject<IEnumerable<Module>> onModulesTabOpened,
            ISubject<IEnumerable<Frame>> onAssemblyTabSelected,
            ISubject<Frame> onFrameSelected,
            ISubject<IEnumerable<Module>> onModulesInPanelsLoaded,
            ISubject<Frame> onAssemblyFrameConfirmed,
            ISubject<DroneModifiers> onAssemblyModuleInstalled,
            ISubject<DroneModifiers> onDroneInProgressUpdated,
            ISubject<Frame> onAssemblyModuleSlotsUpdated,
            ISubject<Drone> onDroneBuilt,
            ISubject<IEnumerable<Module>> onModulesInInventoryRemoved,
            ISubject<IEnumerable<Frame>> onFrameFromInventoryRemoved,
            ISubject<IEnumerable<Drone>> onDroneAddedToInventory,
            ISubject<IEnumerable<Module>> onModuleTypeToDisplaySelected, 
            ISubject<Frame> onFrameCardSelected,
            ISubject<IEnumerable<Frame>> onFramesTabSelected, 
            ISubject<MissionOrders> onMissionTypeSelected,
            ISubject<MissionOrders> onDestinationSelected,
            ISubject<IEnumerable<Drone>> onWorkstationDronesTabSelected,
            ISubject<IEnumerable<Destination>> onMapTabSelected,
            ISubject<Unit> onNewPinSelected,
            ISubject<IEnumerable<Mission>> onMissionsTimeUpdated,
            ISubject<Destination> onNewPinCoordinatesSelected,
            ISubject<Destination> onPinNameChosen,
            ISubject<Destination> onPinTypeChosen,
            ISubject<Unit> onNewPinSaved,
            ISubject<IEnumerable<Destination>> onPinnedLocationsSectionSelected,
            ISubject<Unit> onDroneNamed,
            ISubject<Unit> onDroneNameInvalid,
            ISubject<IEnumerable<Destination>> onMapOpened,
            ISubject<Unit> onRepairNeeded,
            ISubject<Drone> onDroneRecharged,
            ISubject<Unit> onRechargeNeeded,
            ISubject<Drone> onDroneRepaired,
            ISubject<Unit> onDroneDisassembled,
            ISubject<Destination> onPinnedLocationSelected,
            ISubject<Unit> onMissionServicesReadyToCalculate,
            ISubject<MissionOrders> onEnergyCostsCalculated,
            ISubject<MissionOrders> onMissionTimeCostsCalculated,
            ISubject<MissionOrders> onMissionMaxWeightAllowed,
            ISubject<HouseEnergyStatus> onHouseBatteryOpened,
            ISubject<Unit> onDronesInMissionUpdated,
            ISubject<float> onMissionsTimelineEmitted,
            ISubject<Mission> onMissionTargetReached,
            ISubject<Mission> onMissionTargetCompleted,
            ISubject<Unit> onMissionTargetStarted,
            ISubject<Unit> onMissionReturnStarted,
            ISubject<Mission> onMissionReturnedHome,
            ISubject<Unit> onMissionCompleted,
            ISubject<IEnumerable<MissionResults>> onBalconyOpened,
            ISubject<Unit> onAssemblyModuleUninstalled,
            ISubject<Unit> onNewPinReadyToSave,
            ISubject<Unit> onNewPinNotReadyToSave
        ) {
            OnDroneSent = onDroneSent;
            OnDroneReturning = onDroneReturning;
            OnEncounterStart = onEncounterStart;
            OnModuleRepaired = onModuleRepaired;
            OnSendDroneTabSelected = onSendDroneTabSelected;
            OnMissionDroneSelected = onMissionDroneSelected;
            OnOpenComputer = onOpenComputer;
            OnDroneStatusTabSelected = onDroneStatusTabSelected;
            OnDroneInStatusSelected = onDroneInStatusSelected;
            OnOpenWorkstation = onOpenWorkstation;
            OnFramePanelLoaded = onFramePanelLoaded;
            OnModulesTabOpened = onModulesTabOpened;
            OnAssemblyTabSelected = onAssemblyTabSelected;
            OnFrameSelected = onFrameSelected;
            OnModulesInPanelsLoaded = onModulesInPanelsLoaded;
            OnAssemblyFrameConfirmed = onAssemblyFrameConfirmed;
            OnAssemblyModuleInstalled = onAssemblyModuleInstalled;
            OnDroneInProgressUpdated = onDroneInProgressUpdated;
            OnAssemblyModuleSlotsUpdated = onAssemblyModuleSlotsUpdated;
            OnDroneBuilt = onDroneBuilt;
            OnModulesInInventoryRemoved = onModulesInInventoryRemoved;
            OnFrameFromInventoryRemoved = onFrameFromInventoryRemoved;
            OnDroneAddedToInventory = onDroneAddedToInventory;
            OnModuleTypeToDisplaySelected = onModuleTypeToDisplaySelected;
            OnFrameCardSelected = onFrameCardSelected;
            OnFramesTabSelected = onFramesTabSelected;
            OnMissionTypeSelected = onMissionTypeSelected;
            OnDestinationSelected = onDestinationSelected;
            OnWorkstationDronesTabSelected = onWorkstationDronesTabSelected;
            OnMapTabSelected = onMapTabSelected;
            OnNewPinSelected = onNewPinSelected;
            OnMissionsTimeUpdated = onMissionsTimeUpdated;
            OnNewPinCoordinatesSelected = onNewPinCoordinatesSelected;
            OnPinNameChosen = onPinNameChosen;
            OnPinTypeChosen = onPinTypeChosen;
            OnNewPinSaved = onNewPinSaved;
            OnPinnedLocationsSectionSelected = onPinnedLocationsSectionSelected;
            OnDroneNamed = onDroneNamed;
            OnDroneNameInvalid = onDroneNameInvalid;
            OnMapOpened = onMapOpened;
            OnRepairNeeded = onRepairNeeded;
            OnDroneRecharged = onDroneRecharged;
            OnRechargeNeeded = onRechargeNeeded;
            OnDroneRepaired = onDroneRepaired;
            OnDroneDisassembled = onDroneDisassembled;
            OnPinnedLocationSelected = onPinnedLocationSelected;
            OnMissionServicesReadyToCalculate = onMissionServicesReadyToCalculate;
            OnEnergyCostsCalculated = onEnergyCostsCalculated;
            OnMissionTimeCostsCalculated = onMissionTimeCostsCalculated;
            OnMissionMaxWeightAllowed = onMissionMaxWeightAllowed;
            OnHouseBatteryOpened = onHouseBatteryOpened;
            OnDronesInMissionUpdated = onDronesInMissionUpdated;
            OnMissionsTimelineEmitted = onMissionsTimelineEmitted;
            OnMissionTargetReached = onMissionTargetReached;
            OnMissionTargetCompleted = onMissionTargetCompleted;
            OnMissionTargetStarted = onMissionTargetStarted;
            OnMissionReturnStarted = onMissionReturnStarted;
            OnMissionReturnedHome = onMissionReturnedHome;
            OnMissionCompleted = onMissionCompleted;
            OnBalconyOpened = onBalconyOpened;
            OnAssemblyModuleUninstalled = onAssemblyModuleUninstalled;
            OnNewPinReadyToSave = onNewPinReadyToSave;
            this.onNewPinNotReadyToSave = onNewPinNotReadyToSave;
        }
    }
}

