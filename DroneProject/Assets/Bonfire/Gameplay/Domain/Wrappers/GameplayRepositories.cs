﻿using Bonfire.Gameplay.Domain.Repositories;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct GameplayRepositories
    {
        public readonly DronesRepository DronesRepository;
        public readonly HouseItemsRepository HouseItemsRepository;
        public readonly ComponentsRepository ComponentsRepository;
        public readonly DroneInProgressRepository DroneInProgressRepository;
        public readonly MissionBuilderRepository MissionBuilderRepository;
        public readonly MapRepository MapRepository;
        public readonly PinInProgressRepository PinInProgressRepository;
        public readonly HouseResourcesRepository HouseResourcesRepository;
        public readonly ModifiersRepository ModifiersRepository;
        public readonly MissionsRepository MissionsRepository;

        public GameplayRepositories(
            DronesRepository dronesRepository, 
            HouseItemsRepository houseItemsRepository, 
            ComponentsRepository componentsRepository, 
            DroneInProgressRepository droneInProgressRepository,
            MissionBuilderRepository missionBuilderRepository,
            MapRepository mapRepository, 
            PinInProgressRepository pinInProgressRepository, 
            HouseResourcesRepository houseResourcesRepository, 
            ModifiersRepository modifiersRepository, 
            MissionsRepository missionsRepository
        ) {
            DronesRepository = dronesRepository;
            HouseItemsRepository = houseItemsRepository;
            ComponentsRepository = componentsRepository;
            DroneInProgressRepository = droneInProgressRepository;
            MissionBuilderRepository = missionBuilderRepository;
            MapRepository = mapRepository;
            PinInProgressRepository = pinInProgressRepository;
            HouseResourcesRepository = houseResourcesRepository;
            ModifiersRepository = modifiersRepository;
            MissionsRepository = missionsRepository;
        }
    }

}