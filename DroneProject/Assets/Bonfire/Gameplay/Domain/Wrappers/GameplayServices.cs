﻿using Bonfire.Gameplay.Domain.Services;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct GameplayServices
    {
        public readonly EncounterDeciderService EncounterDeciderService;
        public readonly FrameSlotsAmountService FrameSlotsAmountService;
        public readonly DroneAssemblerService DroneAssemblerService;
        public readonly DestinationService DestinationService;
        public readonly DroneService DroneService;
        public readonly MissionService MissionService;

        public GameplayServices(EncounterDeciderService encounterDeciderService, FrameSlotsAmountService frameSlotsAmountService, DestinationService destinationService, DroneAssemblerService droneAssemblerService, DroneService droneService, MissionService missionService)
        {
            EncounterDeciderService = encounterDeciderService;
            FrameSlotsAmountService = frameSlotsAmountService;
            DestinationService = destinationService;
            DroneAssemblerService = droneAssemblerService;
            DroneService = droneService;
            MissionService = missionService;
        }
    }
}