﻿using UniRx;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct HouseGameplayEvents
    {
        public ISubject<float> OnEnergyProduced { get; set; }
        public ISubject<float> OnEnergyTimelineEmitted { get; set; }
        public ISubject<float> OnEnergyOverheadProduced { get; set; }
        public ISubject<Unit> OnInsufficientEnergyIsProduced { get; set; }
        public ISubject<float> OnEnergyStored { get; set; }
        public ISubject<Unit> OnEnergyStorageFull { get; set; }
        public ISubject<Unit> OnHouseBatteryUpgraded { get; set; }
        
        public HouseGameplayEvents(
            ISubject<float> onEnergyProduced,
            ISubject<float> onEnergyTimelineEmitted,
            ISubject<float> onEnergyOverheadProduced,
            ISubject<Unit> onInsufficientEnergyIsProduced,
            ISubject<float> onEnergyStored,
            ISubject<Unit> onEnergyStorageFull,
            ISubject<Unit> onHouseBatteryUpgraded
            
        ) {
            OnEnergyProduced = onEnergyProduced;
            OnEnergyTimelineEmitted = onEnergyTimelineEmitted;
            OnEnergyOverheadProduced = onEnergyOverheadProduced;
            OnInsufficientEnergyIsProduced = onInsufficientEnergyIsProduced;
            OnEnergyStored = onEnergyStored;
            OnEnergyStorageFull = onEnergyStorageFull;
            OnHouseBatteryUpgraded = onHouseBatteryUpgraded;
        }

    }
}