﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.Reactions.House;
using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;

namespace Bonfire.Gameplay.Domain.Wrappers
{
    public struct GameplayReactions
    {
        public readonly LoadModulesInPanels LoadModulesInPanels;
        public readonly UpdateDroneInProgress UpdateDroneInProgress;
        public readonly RemoveModulesFromInventory RemoveModulesFromInventory;
        public readonly RemoveFrameFromInventory RemoveFrameFromInventory;
        public readonly AddDroneToInventory AddDroneToInventory;
        public readonly BuildDrone BuildDrone;
        public readonly ProduceEnergy ProduceEnergy;
        public readonly EmitTimelines EmitTimelines;
        public readonly DistributeEnergy DistributeEnergy;
        public readonly StoreEnergy StoreEnergy;
        public readonly MissionServicesReadyToCalculate MissionServicesReadyToCalculate;
        public readonly CalculateMissionTimeCosts CalculateMissionTimeCosts;
        public readonly CalculateMissionEnergyCosts CalculateMissionEnergyCosts;
        public readonly CalculateMissionMaxWeight CalculateMissionMaxWeight;
        public readonly UpdateMissionsTime UpdateMissionsTime;
        public readonly MissionTargetReached MissionTargetReached;
        public readonly StartMissionTarget StartMissionTarget;
        public readonly MissionTargetCompleted MissionTargetCompleted;
        public readonly StartMissionReturn StartMissionReturn;
        public readonly MissionReturnsHome MissionReturnsHome;
        public readonly CompleteMission CompleteMission;
        public readonly NewPinReadyToSave NewPinReadyToSave;

        public GameplayReactions(LoadModulesInPanels loadModulesInPanels,
            UpdateDroneInProgress updateDroneInProgress,
            RemoveModulesFromInventory removeModulesFromInventory,
            RemoveFrameFromInventory removeFrameFromInventory,
            AddDroneToInventory addDroneToInventory,
            BuildDrone buildDrone,
            ProduceEnergy produceEnergy,
            EmitTimelines emitTimelines,
            DistributeEnergy distributeEnergy,
            StoreEnergy storeEnergy,
            MissionServicesReadyToCalculate missionServicesReadyToCalculate,
            CalculateMissionTimeCosts calculateMissionTimeCosts,
            CalculateMissionEnergyCosts calculateMissionEnergyCosts,
            CalculateMissionMaxWeight calculateMissionMaxWeight,
            UpdateMissionsTime updateMissionsTime,
            MissionTargetReached missionTargetReached,
            StartMissionTarget startMissionTarget,
            MissionTargetCompleted missionTargetCompleted,
            StartMissionReturn startMissionReturn,
            MissionReturnsHome missionReturnsHome,
            CompleteMission completeMission, 
            NewPinReadyToSave newPinReadyToSave
        ) {
            LoadModulesInPanels = loadModulesInPanels;
            UpdateDroneInProgress = updateDroneInProgress;
            RemoveModulesFromInventory = removeModulesFromInventory;
            RemoveFrameFromInventory = removeFrameFromInventory;
            AddDroneToInventory = addDroneToInventory;
            BuildDrone = buildDrone;
            ProduceEnergy = produceEnergy;
            EmitTimelines = emitTimelines;
            DistributeEnergy = distributeEnergy;
            StoreEnergy = storeEnergy;
            MissionServicesReadyToCalculate = missionServicesReadyToCalculate;
            CalculateMissionTimeCosts = calculateMissionTimeCosts;
            CalculateMissionEnergyCosts = calculateMissionEnergyCosts;
            CalculateMissionMaxWeight = calculateMissionMaxWeight;
            UpdateMissionsTime = updateMissionsTime;
            MissionTargetReached = missionTargetReached;
            StartMissionTarget = startMissionTarget;
            MissionTargetCompleted = missionTargetCompleted;
            StartMissionReturn = startMissionReturn;
            MissionReturnsHome = missionReturnsHome;
            CompleteMission = completeMission;
            NewPinReadyToSave = newPinReadyToSave;
        }
    }
}