﻿using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.House;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;

namespace Bonfire.Gameplay.Delivery.Views
{
    public struct CanvasViews
    {
        public readonly ContextDirector ContextDirector;
        public readonly BalconyView BalconyView;
        public readonly HouseBatteryView HouseBatteryView;
        public readonly WorkstationView WorkstationView;
        public readonly PopUpsView PopUpsView;
        public readonly ComputerView ComputerView;

        public CanvasViews(
            ContextDirector contextDirector,
            BalconyView balconyView,
            HouseBatteryView houseBatteryView,
            WorkstationView workstationView, 
            PopUpsView popUpsView,
            ComputerView computerView
        ) {
            ContextDirector = contextDirector;
            BalconyView = balconyView;
            HouseBatteryView = houseBatteryView;
            WorkstationView = workstationView;
            PopUpsView = popUpsView;
            ComputerView = computerView;
        }
    }
}