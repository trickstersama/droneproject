using System;
using UniRx;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Views
{
    public class TimelineView : MonoBehaviour
    {
    
        public readonly ISubject<float> UnityTimeline = new Subject<float>();
        int timeLineInterval = 1;

        void Start() => 
            StartTimeline();

        void StartTimeline() =>
            Observable.Interval(TimeSpan.FromSeconds(timeLineInterval))
                .Subscribe(_ => UnityTimeline.OnNext(timeLineInterval));
    }
}
