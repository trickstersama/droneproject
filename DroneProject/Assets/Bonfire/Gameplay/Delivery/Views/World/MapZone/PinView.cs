using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Views.World.MapZone
{
    public class PinView : MonoBehaviour
    {
        [SerializeField] Sprite sprite;
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] GameObject highlight;
    
        Destination destination;
    
        public void Initialize(Destination destination)
        {
            this.destination = destination;
            transform.localPosition = GetLocalPosition(destination.mapCoordinates);
            spriteRenderer.sprite = sprite;
        }

        public Destination GetDestination() => destination;

        static Vector3 GetLocalPosition(MapCoordinates mapCoordinates) =>
            new Vector3(mapCoordinates.Y * 2, mapCoordinates.X * 2, 1);

        public void Highlight() => 
            highlight.SetActive(true);

        public void DisableHighlight() => 
            highlight.SetActive(false);
    }
}
