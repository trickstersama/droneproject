using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Views.World.MapZone
{
    public class MapWorldView : MonoBehaviour
    {
        [SerializeField] Camera mapCamera;
        [SerializeField] PinView pinPrefab;
        [SerializeField] GameObject pinsContainer;
        [SerializeField] GameObject scopePrefab;

        [Header("Camera settings")] 
        [SerializeField] float minFOV;
        [SerializeField] float maxFOV;
        [SerializeField] float zoomSensitivity;
        [SerializeField] public float defaultFOV;

        public readonly ISubject<MapCoordinates> OnCoordinatesSelected =
            new Subject<MapCoordinates>();

        public readonly ISubject<MapCoordinates> OnPinSelected =
            new Subject<MapCoordinates>();

        readonly List<PinView> allPins = new List<PinView>();

        PinView pinInProgress;
        float actualFov;
        GameObject locationScope;
        Vector3 positionOverMap = new Vector3(0, 0, -1);


        void Start()
        {
            pinInProgress = Instantiate(pinPrefab, positionOverMap, Quaternion.identity, pinsContainer.transform);
            HidePinInProgress();
            locationScope = Instantiate(scopePrefab, positionOverMap, Quaternion.identity, pinsContainer.transform);
            HideLocationScope();
        }

        public void OnZoomUsed(ZoomType zoomType)
        {
            actualFov = mapCamera.orthographicSize;
            actualFov += zoomType == ZoomType.ZoomIn ? zoomSensitivity : -zoomSensitivity;
            actualFov = Mathf.Clamp(actualFov, minFOV, maxFOV);
            mapCamera.orthographicSize = actualFov;
        }
        
        public void Default()
        {
            HidePinInProgress();
            DeleteAllPins();
            CenterFOV();
            CenterMap();
            HideLocationScope();
        }

        void HidePinInProgress() => 
            pinInProgress.gameObject.SetActive(false);
        
        public void HidePins()
        {
            pinInProgress.gameObject.SetActive(false);
            allPins.ForEach(pin => pin.gameObject.SetActive(false));
        }

        void DeleteAllPins()
        {
            allPins.ForEach(pin => Destroy(pin.gameObject));
            allPins.Clear();
        }

        void CenterFOV() => 
            mapCamera.orthographicSize = defaultFOV;

        void CenterMap()
        {
            var position = mapCamera.transform;
            position.localPosition = new Vector3(0, 0, position.localPosition.z);
        }

        void ReadClick(Vector2 mousePositionByPercent)
        {
            var pinPosition = MapPinsPositionByPercentage(mousePositionByPercent);
            OnCoordinatesSelected.OnNext(pinPosition.ToMapCoordinates());
            locationScope.transform.localPosition = new Vector3(pinPosition.x, pinPosition.y, positionOverMap.z);
        }

        Vector3 MapPinsPositionByPercentage(Vector2 mousePositionByPercent) =>
            CalculateWorldUnitsByPercent(mousePositionByPercent) 
            + mapCamera.transform.localPosition 
            - new Vector3(mapCamera.orthographicSize, -mapCamera.orthographicSize, 0);

        //------------


        public void DragMapBy(Vector3 deltaPercent)
        {
            var xxx = CalculateWorldUnitsByPercent(deltaPercent);
            mapCamera.transform.position += xxx ;
        }

        public void SetPinPositionByPercent(Vector2 mouseHoverPositionByPercent)
        {
            var cameraOffset = mapCamera.transform.localPosition;
            var pinTransform = pinInProgress.transform;
            pinTransform.localPosition = new Vector3(cameraOffset.x, cameraOffset.y, pinTransform.localPosition.z) + CalculateWorldUnitsByPercent(mouseHoverPositionByPercent);
        }
        


        public void SpawnPinAt(Destination destination)
        {
            var newPin = Instantiate(pinPrefab, Vector3.zero, Quaternion.identity, pinsContainer.transform);
            newPin.Initialize(destination);
            allPins.Add(newPin);
        }
        
        public void SpawnPinAt(IEnumerable<Destination> destinations)
        {
            DeleteAllPins();
            foreach (var destination in destinations)
            {
                var newPin = Instantiate(pinPrefab, Vector3.zero, Quaternion.identity, pinsContainer.transform);
                newPin.Initialize(destination);
                allPins.Add(newPin);
            }
        }

        public void ShowPinInProgress() => 
            pinInProgress.gameObject.SetActive(true);

        public void SendPinOrLocation(Vector2 clickPositionByPercent)
        {
            var positionInScreen = CalculateWorldUnitsByPercent(clickPositionByPercent);
            var ra2 = new Ray(positionInScreen + mapCamera.transform.position, Vector3.forward);
            if (Physics.Raycast(ra2, out var hit, 50f))
            {
                var objectHit = hit.transform;
                var pinView = objectHit.GetComponent<PinView>();
                OnPinSelected.OnNext(pinView.GetDestination().mapCoordinates);
                pinView.Highlight();
                HideLocationScope();
            }
            else
            {
                ReadClick(clickPositionByPercent);
                ShowLocationScope();
            }
        }

        void ShowLocationScope() => 
            locationScope.SetActive(true);

        void HideLocationScope() => 
            locationScope.SetActive(false);

        public void DisablePinsHighlight() => 
            allPins.ForEach(pin => pin.DisableHighlight());
        
        Vector3 CalculateWorldUnitsByPercent(Vector3 deltaPercent)
        {
            var orthographicSize = mapCamera.orthographicSize;
            var worldPosition = new Vector3(
                orthographicSize * 2 * deltaPercent.x / 100,
                -orthographicSize * 2 * deltaPercent.y / 100,
                0) ;
            return worldPosition;
        }

        public void ZoomTo(MapCoordinates destinationMapCoordinates)
        {
            var cameraTransform = mapCamera.transform;
            cameraTransform.localPosition = new Vector3(
                    destinationMapCoordinates.Y * 2,
                    destinationMapCoordinates.X * 2,
                    cameraTransform.localPosition.z
                );
        }
    }
}
