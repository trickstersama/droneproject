﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Views.World
{
    public class WorldGameplayView : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField] PolygonCollider2D computerButton;
        [SerializeField] PolygonCollider2D workstationButton;
        [SerializeField] BoxCollider2D balconyButton;
        [SerializeField] BoxCollider2D houseBatteryButton;

        public IObservable<Unit> OnActivateComputer => computerButton.OnMouseDownAsObservable();
        public IObservable<Unit> OnActivateWorkstation => workstationButton.OnMouseDownAsObservable();
        public IObservable<Unit> OnActivateBalcony => balconyButton.OnMouseDownAsObservable();
        public IObservable<Unit> OnActivateHouseBattery => houseBatteryButton.OnMouseDownAsObservable();
        
    }
}