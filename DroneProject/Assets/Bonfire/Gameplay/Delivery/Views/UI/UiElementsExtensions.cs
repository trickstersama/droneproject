﻿using System;
using System.Linq;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI

{
    public static class UiElementsExtensions
    {
        public static IObservable<Unit> OnClickAsObservable(this Button button)
        {
            var xxx = new Subject<Unit>();
            button.clickable.clicked += delegate {  xxx.OnNext(Unit.Default); };
            return xxx;
        }
        public static VisualElement FindSelectedVisualElement(this ListView listView)
        {
            var template = listView.Q(className: "unity-collection-view__item--selected") as TemplateContainer;
            return template?.Children().First();

        }
    }
}