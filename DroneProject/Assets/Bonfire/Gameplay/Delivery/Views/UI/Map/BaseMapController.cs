﻿using Bonfire.Gameplay.Delivery.Views.World.MapZone;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Map
{
    public class BaseMapController
    {
        protected MapWorldView mapWorldView;
        protected IMGUIContainer map;
        protected VisualElement contextRoot;
        
        Vector2 mapIMGSize;
        Vector2 initialDragPosition;

        readonly ISubject<ZoomType> OnWheelUsed = new Subject<ZoomType>();

        protected BaseMapController(MapWorldView mapWorldView)
        {
            this.mapWorldView = mapWorldView;
        }

        protected virtual void DoSubscriptions()
        {
            contextRoot.RegisterCallback<GeometryChangedEvent>(UIStarted);
            map.RegisterCallback<MouseDownEvent>(OnMouseDown);
            map.RegisterCallback<MouseUpEvent>(OnMouseUp);
            map.RegisterCallback<WheelEvent>(OnWheelEvent);

            OnWheelUsed.Subscribe(mapWorldView.OnZoomUsed);
        }
        
        protected virtual void FindElements(VisualElement root)
        {
            contextRoot = root.Q<VisualElement>("MapWindow");
            map = contextRoot.Q<IMGUIContainer>("Map");
        }
        void UIStarted(GeometryChangedEvent evt)
        {
            mapIMGSize = new Vector2(map.resolvedStyle.width, map.resolvedStyle.height);
        }

        void OnWheelEvent(WheelEvent evt) => 
            OnWheelUsed.OnNext(evt.delta.y <= 0 ? ZoomType.ZoomIn : ZoomType.ZoomOut);

        void OnMouseDown(MouseDownEvent evt)
        {
            initialDragPosition = evt.localMousePosition;
        }

        void OnMouseUp(MouseUpEvent evt)
        {
            var offsetInPercent = PercentageByPosition(initialDragPosition - evt.localMousePosition);
            if (offsetInPercent.magnitude <= .1f)
                mapWorldView.SendPinOrLocation(PercentageByPosition(evt.localMousePosition));
            else
                mapWorldView.DragMapBy(new Vector3(offsetInPercent.x, offsetInPercent.y, 0));
        }

        Vector2 PercentageByPosition(Vector2 position) => 
            new(
                position.x /mapIMGSize.x  * 100, 
                position.y /mapIMGSize.y  * 100
            );


    }
}