using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.World.MapZone;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Map
{
    public sealed class SendDroneMapController : BaseMapController
    {
        public readonly ISubject<MapCoordinates> OnMapCoordinatesSelected = new Subject<MapCoordinates>();

        public SendDroneMapController(VisualElement root, MapWorldView mapWorldView) : base(mapWorldView)
        {
            FindElements(root);
            DoSubscriptions();
        }

        protected override void DoSubscriptions()
        {
            mapWorldView.OnPinSelected
                .Subscribe(value => OnMapCoordinatesSelected.OnNext(value)); //TODO quitar destination y poner coordinates
            mapWorldView.OnCoordinatesSelected
                .Subscribe(value => OnMapCoordinatesSelected.OnNext(value)); //TODO quitar destination y poner coordinates
        }

        public void CloseContext() => 
            contextRoot.style.display = DisplayStyle.None;
        public void OpenContext(IEnumerable<Destination> destinations)
        {
            PopulateDestinations(destinations);
            contextRoot.style.display = DisplayStyle.Flex;
        }

        void PopulateDestinations(IEnumerable<Destination> destinations) => 
            mapWorldView.SpawnPinAt(destinations);

        public void Default()
        {
            mapWorldView.Default();
        }
    }
}