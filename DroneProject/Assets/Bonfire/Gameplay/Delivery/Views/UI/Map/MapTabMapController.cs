﻿using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.World.MapZone;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Map
{
    public sealed class MapTabMapController : BaseMapController
    {
        public readonly ISubject<MapCoordinates> OnMapCoordinatesSelected = new Subject<MapCoordinates>();

        public MapTabMapController(VisualElement root, MapWorldView mapWorldView) : base(mapWorldView)
        {
            FindElements(root);
            DoSubscriptions();
        }

        protected override void DoSubscriptions()
        {
            base.DoSubscriptions();
            mapWorldView.OnCoordinatesSelected
                .Subscribe(value =>
                {
                    OnMapCoordinatesSelected.OnNext(value);
                    Debug.Log(value);
                }); 

        }

        protected override void FindElements(VisualElement root)
        {
            base.FindElements(root);
            
        }
        
        public void CloseContext()
        {
            Default();
            contextRoot.style.display = DisplayStyle.None;
        }

        public void OpenForNewPin()
        {
            Default();
            contextRoot.style.display = DisplayStyle.Flex;
        }
        
        public void OpenForPins(IEnumerable<Destination> destinations)
        {
            Default();
            mapWorldView.SpawnPinAt(destinations);
            contextRoot.style.display = DisplayStyle.Flex;
        }
        
        void Default()
        {
            mapWorldView.Default();
        }
    }
}