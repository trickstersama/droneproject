﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Tabs
{
    public class DronesTabController
    {
        readonly List<Drone> drones = new();
        VisualElement tabRoot;
        Button disassemblyButton;
        Button repairButton;
        Button rechargeButton;
        ListView list;
        Label droneStatusText;

        public readonly ISubject<Unit> OnDisassemblyButtonPressed = new Subject<Unit>();
        public readonly ISubject<Drone> OnItemSelected = new Subject<Drone>();
        public readonly ISubject<Unit> OnRechargeButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnRepairButtonPressed = new Subject<Unit>();

        public DronesTabController(VisualElement root, VisualTreeAsset droneItem)
        {
            FindElements(root);
            SetListItems(droneItem);
            list.onSelectionChange += OnDroneItemSelected;
            DoSubscriptions();
        }
        
        public void CloseTab() => tabRoot.style.display = DisplayStyle.None;
        public void OpenTab() => tabRoot.style.display = DisplayStyle.Flex;

        public void UpdateDrones(IEnumerable<Drone> updatedDrones)
        {
            drones.Clear();
            drones.AddRange(updatedDrones.OrderBy(drone => drone.droneName));
            list.Rebuild();
            ResetActionButtons();
            droneStatusText.text = string.Empty;
        }
        
        public void UpdateSelectedDrone(Drone updatedDrone)
        {
            var index = list.selectedIndex;
            drones[index] = updatedDrone;
            list.selectedIndex = index;
        }
        public void ActivateRepairButton() => repairButton.SetEnabled(true);

        public void ActivateRechargeButton() => rechargeButton.SetEnabled(true);
        
        void OnDroneItemSelected(IEnumerable<object> _)
        {
            ResetActionButtons();
            if (list.selectedIndex >= 0)
                ADroneIsSelected();
            else
                droneStatusText.text = string.Empty;
        }

        void ADroneIsSelected()
        {
            var drone = drones[list.selectedIndex];
            OnItemSelected.OnNext(drone);
            disassemblyButton.SetEnabled(true);
            PopulateDescription(drone);
        }

        void PopulateDescription(Drone drone) =>
            droneStatusText.text = $"Drone name: {drone.droneName}\n" +
                                   $"Charge: {drone.charge} / 100% \n" +
                                   $"Damage received: {drone.damageReceived}\n" +
                                   $"Frame model: {drone.frame.name} \n" +
                                   $"Modifiers: {drone.modifiers}";

        void ResetActionButtons()
        {
            rechargeButton.SetEnabled(false);
            repairButton.SetEnabled(false);
            disassemblyButton.SetEnabled(false);
        }

        void SetListItems(VisualTreeAsset droneItem)
        {
            list.makeItem = () => MakeItem(droneItem);
            list.itemsSource = drones;
            list.bindItem = (item, index) => 
                (item.userData as DroneItemController)?.SetCharacterData(drones[index]);
        }

        static VisualElement MakeItem(VisualTreeAsset droneItem)
        {
            var item = droneItem.Instantiate();
            var itemController = new DroneItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }

        void FindElements(VisualElement root)
        {
            tabRoot = root.Q<VisualElement>("DronesTabUI");
            list = root.Q<ListView>("DroneList");
            droneStatusText = root.Q<Label>("DroneStatus");
            
            disassemblyButton = root.Q<Button>("Disassembly");
            repairButton = root.Q<Button>("Repair");
            rechargeButton = root.Q<Button>("Recharge");
        }

        void DoSubscriptions()
        {
            rechargeButton.OnClickAsObservable()
                .Subscribe(_ => OnRechargeButtonPressed.OnNext(Unit.Default));
            repairButton.OnClickAsObservable()
                .Subscribe(_ => OnRepairButtonPressed.OnNext(Unit.Default));
            disassemblyButton.OnClickAsObservable()
                .Subscribe(_ => OnDisassemblyButtonPressed.OnNext(Unit.Default));
        }
    }
}