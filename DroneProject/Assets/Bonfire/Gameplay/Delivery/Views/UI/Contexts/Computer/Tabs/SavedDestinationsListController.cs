﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer.Tabs
{
    public class SavedDestinationsListController
    {
        readonly ListView list;
        List<Destination> destinations = new List<Destination>();

        public SavedDestinationsListController(ListView listView, VisualTreeAsset listItem)
        {
            list = listView;
            InitializeList(listItem);
        }

        public void UpdateDestinations(IEnumerable<Destination> newDestinations)
        {
            destinations.Clear();
            destinations.AddRange(newDestinations);
            list.Rebuild();
        }
        void InitializeList(VisualTreeAsset assemblyItem)
        {
            list.makeItem = () => MakeItem(assemblyItem);
            list.itemsSource = destinations;
            list.onSelectionChange += OnSelectModuleItem;
            BindItems();
            list.selectionType = SelectionType.Single;
        }
        
        VisualElement MakeItem(VisualTreeAsset assemblyItem)
        {
            var item = assemblyItem.Instantiate();
            var itemController = new SavedDestinationItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }
        
        void OnSelectModuleItem(IEnumerable<object> _)
        {
            if (list.selectedIndex >= 0)
            {
                //OnDestinationItemSelected.OnNext(modules[destinations.selectedIndex]);
                //destination selected
            }
            else
            {
                //nothing selected
            }
        }
        void BindItems()
        {
            list.bindItem = (item, index) =>
                (item.userData as SavedDestinationItemController)?.SetCharacterData(destinations[index]);
        }

    }
}