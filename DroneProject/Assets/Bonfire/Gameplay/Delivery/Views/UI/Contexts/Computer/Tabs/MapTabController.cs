﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine.UIElements;
using static System.String;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer.Tabs
{
    public class MapTabController
    {
        const int MinPinNameCharacters = 3;
        const int MaxPinNameCharacters = 15;

        readonly VisualElement tabRoot;
        Button newPinButton;
        Button pinsButton;
        VisualElement startingViews;
        VisualElement newPinViews;
        VisualElement pinsViews;
        VisualElement droneArt;
        TextField pinNameInput;
        Label destinationOverview;
        Label coordinatesOverview;
        Label nameFeedback;

        Button scoutButton;
        Button lootButton;
        Button battleButton;
        Button saveNewPinButton;
        Button clearButton;

        ListView savedLocationsList;
        SavedDestinationsListController savedDestinationListController;

        public readonly ISubject<Unit> OnNewPinButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnPinsButtonPressed = new Subject<Unit>();
        public readonly ISubject<MissionType> OnNewPinTypePressed = new Subject<MissionType>();
        public readonly ISubject<string> OnNewPinNameEntered = new Subject<string>();
        public readonly ISubject<Unit> OnSaveNewPinButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnClearPinInProgressButtonPressed = new Subject<Unit>();

        public MapTabController(VisualElement root, VisualTreeAsset destinationItem)
        {
            tabRoot = root.Q<TemplateContainer>("MapTab");
            FindElements();
            DoSubscriptions();
            SetupList(destinationItem);
        }

        void SetupList(VisualTreeAsset destinationItem)
        {
            savedDestinationListController = new SavedDestinationsListController(savedLocationsList, destinationItem);
        }

        void DoSubscriptions()
        {
            newPinButton.OnClickAsObservable()
                .Subscribe(_ => OnNewPinButtonPressed.OnNext(Unit.Default));
            pinsButton.OnClickAsObservable()
                .Subscribe(_ => OnPinsButtonPressed.OnNext(Unit.Default));
            saveNewPinButton.OnClickAsObservable()
                .Subscribe(_ => OnSaveNewPinButtonPressed.OnNext(Unit.Default));
            clearButton.OnClickAsObservable()
                .Subscribe(_ => OnClearPinInProgressButtonPressed.OnNext(Unit.Default));
                
            scoutButton.OnClickAsObservable()
                .Subscribe(_ => OnNewPinTypePressed.OnNext(MissionType.Scout));
            lootButton.OnClickAsObservable()
                .Subscribe(_ => OnNewPinTypePressed.OnNext(MissionType.Loot));
            battleButton.OnClickAsObservable()
                .Subscribe(_ => OnNewPinTypePressed.OnNext(MissionType.Battle));
            
            pinNameInput.RegisterValueChangedCallback(HandleInputValue);
        }
        
        public void UpdateDestinationOverview(Destination destination) => 
            destinationOverview.text = destination.ToFormattedString();

        public void NewPinIsReadyToSave() => 
            saveNewPinButton.pickingMode = PickingMode.Position;

        public void NewPinIsNotReadyToSave() => 
            saveNewPinButton.pickingMode = PickingMode.Ignore;
        
        void HandleInputValue(ChangeEvent<string> evt)
        {
            if (evt.newValue.Length < MinPinNameCharacters)
            {
                OnNewPinNameEntered.OnNext(evt.newValue);
                nameFeedback.text = "Name not long enough";
            }
            else
            {
                if (evt.newValue.Length > MaxPinNameCharacters)
                    HandleNameInputTooLong(evt);
                else
                {
                    OnNewPinNameEntered.OnNext(evt.newValue);
                    nameFeedback.text = Empty;
                }
            }
        }

        void HandleNameInputTooLong(ChangeEvent<string> evt)
        {
            pinNameInput.value = evt.previousValue;
            nameFeedback.text = "Name too long";
        }

        void FindElements()
        {
            newPinButton = tabRoot.Q<Button>("NewPin");
            pinsButton = tabRoot.Q<Button>("Pins");
            saveNewPinButton = tabRoot.Q<Button>("Save");
            clearButton = tabRoot.Q<Button>("Clear");
            
            startingViews = tabRoot.Q<VisualElement>("StartingViews");
            newPinViews = tabRoot.Q<VisualElement>("NewPinViews");
            pinsViews = tabRoot.Q<VisualElement>("PinsViews");
            droneArt = tabRoot.Q<VisualElement>("DroneArtWrapper");
            
            scoutButton = tabRoot.Q<Button>("Scout");
            lootButton = tabRoot.Q<Button>("Loot");
            battleButton = tabRoot.Q<Button>("Battle");
            pinNameInput = tabRoot.Q<TextField>("PinName");
            destinationOverview = tabRoot.Q<Label>("DestinationOverview");
            coordinatesOverview = tabRoot.Q<Label>("CoordinatesOverview");
            nameFeedback = tabRoot.Q<Label>("NameFeedback");
            
            savedLocationsList = tabRoot.Q<ListView>("SavedLocations");
        }

        public void CloseTab() => 
            tabRoot.style.display = DisplayStyle.None;

        public void OpenTab()
        {
            Default();
            tabRoot.style.display = DisplayStyle.Flex;
        }

        public void OpenNewPin()
        {
            CloseViews();
            DefaultFields();
            newPinViews.style.display = DisplayStyle.Flex;
        }

        void DefaultFields()
        {
            destinationOverview.text = Empty;
            coordinatesOverview.text = Empty;
            nameFeedback.text = Empty;
            pinNameInput.value = Empty; 
        }

        void Default()
        {
            saveNewPinButton.pickingMode = PickingMode.Ignore;
            CloseViews();
            startingViews.style.display = DisplayStyle.Flex;
            droneArt.style.display = DisplayStyle.Flex;
        }

        public void OpenPins(IEnumerable<Destination> destinations)
        {
            CloseViews();
            pinsViews.style.display = DisplayStyle.Flex;
            pinsButton.style.display = DisplayStyle.Flex;
            savedDestinationListController.UpdateDestinations(destinations);
        }

        void CloseViews()
        {
            droneArt.style.display = DisplayStyle.None;
            startingViews.style.display = DisplayStyle.None;
            newPinViews.style.display = DisplayStyle.None;
            pinsViews.style.display = DisplayStyle.None;
        }
    }
}