using Bonfire.Gameplay.Domain.ValueObjects.House;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.House
{
    public class HouseBatteryView : MonoBehaviour
    {
        HouseBatteryController HouseBatteryController;

        public ISubject<Unit> onCloseButtonPressed => HouseBatteryController.onCloseButtonPressed;
        
        VisualElement root;
        public void CloseContext() => root.visible = false;
        public void OpenContext() => root.visible = true;

        public void SetEnergyAvailable(float energy) => 
            HouseBatteryController.SetEnergyAvailable(energy);

        public void UpdateStaticValues(HouseEnergyStatus energyStatus) => 
            HouseBatteryController.UpdateStaticValues(energyStatus);

        void OnEnable()
        {
            var uiDocument = GetComponent<UIDocument>();
            
            HouseBatteryController = new HouseBatteryController();
            root = uiDocument.rootVisualElement;
            HouseBatteryController.Initialize(root);
        }
    }
}
