﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.ValueObjects;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class FramePanelController 
    {
        readonly VisualElement panelRoot;
        readonly ISubject<string> onPanelOpenedMessage;
        ListView list;
        Label typeText;

        List<Frame> frames = new();

        public readonly ISubject<Frame> OnFrameItemSelected = new Subject<Frame>();
        public readonly ISubject<Unit> OnFrameConfirmed = new Subject<Unit>();
        public readonly ISubject<Unit> OnNothingSelected = new Subject<Unit>();

        public FramePanelController(
            VisualElement panelRoot, 
            VisualTreeAsset assemblyItem,
            ISubject<string> onPanelOpenedMessage
        ) {
            this.panelRoot = panelRoot;
            this.onPanelOpenedMessage = onPanelOpenedMessage;
            FindElements(panelRoot);
            InitializeList(assemblyItem);
            typeText.style.visibility = Visibility.Hidden;
        }

        void InitializeList(VisualTreeAsset assemblyItem)
        {
            list.makeItem = () => MakeItem(assemblyItem);
            list.itemsSource = frames;
            list.onSelectionChange += OnSelectFrameItem;
            list.bindItem = (item, index) =>
                (item.userData as AssemblyFrameItemController)?.SetCharacterData(frames[index]);
        }

        public void ConfirmSelection()
        {
            var selectedItem = list.FindSelectedVisualElement();
            selectedItem.AddToClassList(UiTags.ClassNameWhenSelected);
            list.selectionType = SelectionType.None;
            
            Debug.Log($"last selected + {list.selectedIndex}");
            OnFrameConfirmed.OnNext(Unit.Default);
        }
        
        void FindElements(VisualElement panelRoot)
        {
            list = panelRoot.Q<ListView>("List");
            typeText = panelRoot.Q<Label>("Type");
        }


        public void UpdateFrames(IEnumerable<Frame> updatedFrames)
        {
            frames.Clear();
            frames.AddRange(updatedFrames.OrderBy(frame => frame.name));
            list.Rebuild();
        }
        
        VisualElement MakeItem(VisualTreeAsset assemblyItem)
        {
            var item = assemblyItem.Instantiate();
            var itemController = new AssemblyFrameItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }

        void OnSelectFrameItem(IEnumerable<object> _)
        {
            if (list.selectedIndex >= 0)
            {
                var selectedFrame = frames[list.selectedIndex];
                OnFrameItemSelected.OnNext(selectedFrame);
            }
            else
            {
                OnNothingSelected.OnNext(Unit.Default);
            }
        }

        public void Show()
        {
            onPanelOpenedMessage.OnNext("-Slots amount-");
            panelRoot.style.display = DisplayStyle.Flex;
        }

        public void Hide() => panelRoot.style.display = DisplayStyle.None;

        public void Reset()
        {
            list.selectionType = SelectionType.Single;
        }
    }


}