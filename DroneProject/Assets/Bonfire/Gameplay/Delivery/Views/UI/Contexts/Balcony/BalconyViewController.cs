using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony
{
    public class BalconyViewController
    {
        ListView droneList;
        VisualElement root;

        public void InitializeCharacterList(VisualElement root, VisualTreeAsset droneBalconyItem)
        {
            this.root = root;
            FindUIElements(root);
        }

        void FindUIElements(VisualElement root) => 
            droneList = root.Q<ListView>();

        public void Hide() => 
            root.style.display = DisplayStyle.None;

        public void Show() => 
            root.style.display = DisplayStyle.Flex;
    }
}
