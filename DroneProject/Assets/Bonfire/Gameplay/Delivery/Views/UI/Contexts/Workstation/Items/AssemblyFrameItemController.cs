﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class AssemblyFrameItemController
    {
        Label name;
        public void SetVisualElement(VisualElement item) => name = item.Q<Label>("Name");
        public void SetCharacterData(Frame frame) => name.text = frame.name;
    }
}