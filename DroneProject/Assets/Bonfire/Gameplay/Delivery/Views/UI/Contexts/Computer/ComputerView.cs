using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer.Tabs;
using Bonfire.Gameplay.Delivery.Views.UI.Map;
using Bonfire.Gameplay.Delivery.Views.World.MapZone;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer
{
    public class ComputerView : MonoBehaviour
    {
        [SerializeField] VisualTreeAsset droneCard;
        [SerializeField] MapWorldView mapWorldView;
        [SerializeField] VisualTreeAsset destinationItem;
        ComputerViewController mainController;
        SendDroneTabController sendDroneTabController;
        SendDroneMapController sendDroneMapController;
        MapTabController mapTabController;
        MapTabMapController mapTabMapController;

        //-----         Tabs Buttons
        public IObservable<Unit> onCloseButtonPressed => mainController.OnCloseButtonPressed;
        public IObservable<Unit> OnSendDroneTabButtonPressed => mainController.OnSendDroneTabButtonPressed;
        public IObservable<Unit> OnEncountersTabButtonPressed => mainController.OnEncountersTabButtonPressed;
        public IObservable<Unit> OnStatusTabButtonPressed => mainController.OnStatusTabButtonPressed;
        public IObservable<Unit> OnMapTabButtonPressed => mainController.OnMapTabButtonPressed;
        
        //-------- send drone tab
        public IObservable<Drone> OnCardCardSelected => sendDroneTabController.OnDroneCardSelected;
        public IObservable<Unit> OnSelectDestinationButtonPressed =>
            sendDroneTabController.OnSelectDestinationButtonPressed;
        public IObservable<MapCoordinates> OnSendDroneMapCoordinatesSelected => 
            sendDroneMapController.OnMapCoordinatesSelected;
        public IObservable<MissionType> OnMissionTypeButtonPressed =>
            sendDroneTabController.OnMissionTypeButtonPressed;
        public IObservable<Unit> OnDeployButtonPressed => 
            sendDroneTabController.OnDeployButtonPressed;
        
        //-------- map Tab

        public IObservable<Unit> OnNewPinButtonPressed => mapTabController.OnNewPinButtonPressed;
        public IObservable<Unit> OnPinsButtonPressed => mapTabController.OnPinsButtonPressed;
        public IObservable<MapCoordinates> OnNewPinCoordinatesSelected => mapTabMapController.OnMapCoordinatesSelected;
        public IObservable<MissionType> OnNewPinTypePressed => mapTabController.OnNewPinTypePressed;
        public IObservable<string> OnNewPinNameEntered => mapTabController.OnNewPinNameEntered;
        public IObservable<Unit> OnSaveNewPinButtonPressed => mapTabController.OnSaveNewPinButtonPressed;
        public IObservable<Unit> OnClearPinInProgressButtonPressed => mapTabController.OnClearPinInProgressButtonPressed;

        public void UpdateDestinationOverview(Destination destination) => 
            mapTabController.UpdateDestinationOverview(destination);


        //-------


        void OnEnable()
        {
            var root = GetComponent<UIDocument>().rootVisualElement;
            mainController = new ComputerViewController(root);
            
            sendDroneTabController = new SendDroneTabController(root, droneCard);
            sendDroneMapController = new SendDroneMapController(root, mapWorldView);
            
            mapTabController = new MapTabController(root, destinationItem);
            mapTabMapController = new MapTabMapController(root, mapWorldView);
        }
        
        public void OpenContext() => 
            mainController.Show();

        public void CloseContext() => 
            mainController.Hide();
        public void UpdateMissionOverview(MissionOrders missionOrders) =>
            sendDroneTabController.UpdateMissionOverview(missionOrders);
        public void OpenMapUI(IEnumerable<Destination> destinations) => 
            sendDroneMapController.OpenContext(destinations);

        public void OpenSendDroneSection(IEnumerable<Drone> drones)
        {
            CloseAllTabs();
            sendDroneMapController.Default();
            sendDroneTabController.OpenTab();
            sendDroneTabController.Initialize(drones);
        }

        public void OpenMapTab()
        {
            CloseAllTabs();
            mapTabController.OpenTab();
        }
        public void SendDroneCardSelected() => 
            sendDroneTabController.CardSelected();

        public void CoordinatesSelected() => 
            sendDroneTabController.CoordinatesSelected();

        public void CloseAllTabs()
        {
            sendDroneMapController.CloseContext();
            sendDroneTabController.CloseTab();
            mapTabController.CloseTab();
            mapTabMapController.CloseContext();
        }
        
        public void EnableDeploy() => 
            sendDroneTabController.EnableDeploy();

        public void OpenNewPin()
        {
            mapTabMapController.OpenForNewPin();
            mapTabController.OpenNewPin();
        }

        public void OpenPins(IEnumerable<Destination> coordinates)
        {
            mapTabMapController.OpenForPins(coordinates);
            mapTabController.OpenPins(coordinates);
        }

        public void NewPinIsReadyToSave() => mapTabController.NewPinIsReadyToSave();

        public void NewPinIsNotReadyToSave() => mapTabController.NewPinIsNotReadyToSave();
    }
}