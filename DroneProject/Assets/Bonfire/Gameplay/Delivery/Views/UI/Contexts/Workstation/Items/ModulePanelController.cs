﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class ModulePanelController
    {
        readonly VisualElement panelRoot;
        ListView list;
        Label typeText;

        int slotsRemaining;
        List<Module> modules = new();
        readonly List<int> selectedIndexes = new();
        
        public readonly ModuleType moduleType;
        readonly IObserver<string> onPanelOpenedMessage;
        public readonly ISubject<Module> OnModuleItemSelected = new Subject<Module>();
        public readonly ISubject<Module> OnModuleItemInstalled = new Subject<Module>();
        public readonly ISubject<bool> OnModuleSelectedAlreadyInstalled = new Subject<bool>();
        public readonly ISubject<bool> OnInstallEnabled = new Subject<bool>();
        public readonly ISubject<Unit> OnNothingSelected = new Subject<Unit>();
        public readonly ISubject<Module> OnUninstallModule = new Subject<Module>();
        
        Module module;

        public ModulePanelController(
            VisualElement panelRoot,
            ModuleType moduleType,
            VisualTreeAsset assemblyItem, 
            IObserver<string> onPanelOpenedMessage
        ) {
            this.panelRoot = panelRoot;
            this.moduleType = moduleType;
            this.onPanelOpenedMessage = onPanelOpenedMessage;
            FindElements(panelRoot);
            InitializeList(assemblyItem);
            typeText.text = this.moduleType.ToString();
        }

        void InitializeList(VisualTreeAsset assemblyItem)
        {
            list.makeItem = () => MakeItem(assemblyItem);
            list.itemsSource = modules;
            list.onSelectionChange += OnSelectModuleItem;
            BindItems();
            list.selectionType = SelectionType.Single;
        }

        public void Clear()
        {
            modules.Clear();
            slotsRemaining = 0;
            list.Rebuild();
            selectedIndexes.Clear();
        }
        
        void FindElements(VisualElement panelRoot)
        {
            list = panelRoot.Q<ListView>("List");
            typeText = panelRoot.Q<Label>("Type");
        }

        public void UpdateModules(IEnumerable<Module> updatedFrames)
        {
            modules.Clear();
            modules.AddRange(updatedFrames.OrderBy(frame => frame.name));
            list.Rebuild();
        }

        void BindItems()
        {
            list.bindItem = (item, index) =>
            {
                module = modules[index];
                var itemController  = item.userData as AssemblyModuleItemController;
                itemController?.SetCharacterData(module);
                if (selectedIndexes.Contains(index))
                {
                    itemController?.Highlight();
                }
            };
        }

        VisualElement MakeItem(VisualTreeAsset assemblyItem)
        {
            var item = assemblyItem.Instantiate();
            var itemController = new AssemblyModuleItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }


        void OnSelectModuleItem(IEnumerable<object> _)
        {
            if (list.selectedIndex >= 0)
            {
                OnModuleItemSelected.OnNext(modules[list.selectedIndex]);
                OnInstallEnabled.OnNext(slotsRemaining > 0);
                OnModuleSelectedAlreadyInstalled.OnNext(selectedIndexes.Contains(list.selectedIndex));
            }
            else
            {
                OnNothingSelected.OnNext(Unit.Default);
            }
        }

        public void SetSlotsAmount(Frame frame) => 
            slotsRemaining = frame.GetSlotsAmountByModuleType(moduleType);

        public void Hide() => 
            panelRoot.style.display = DisplayStyle.None;

        public void Show()
        {
            onPanelOpenedMessage.OnNext($"Slots remaining: {slotsRemaining}");
            panelRoot.style.display = DisplayStyle.Flex;
            list.selectedIndex = -1;
        }

        public void ConfirmSelection()
        {
            var selectedModule = modules[list.selectedIndex];
            selectedIndexes.Add(list.selectedIndex);
            OnModuleItemInstalled.OnNext(selectedModule);
            list.Rebuild();
        }

        public void UninstallSelectedModule()
        {
            var selectedModule = modules[list.selectedIndex];
            selectedIndexes.Remove(list.selectedIndex);
            OnUninstallModule.OnNext(selectedModule);
            list.Rebuild();
        }
    }
}