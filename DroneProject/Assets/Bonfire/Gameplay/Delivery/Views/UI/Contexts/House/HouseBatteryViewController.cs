using Bonfire.Gameplay.Domain.ValueObjects.House;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.House
{
    public class HouseBatteryController
    {
        Label EnergyAvailableText;
        Label ProductionText;
        Label ConsumptionText;
        Label CapacityText;
        Button closeButton;

        public readonly ISubject<Unit> onCloseButtonPressed = new Subject<Unit>();
        public void Initialize(VisualElement root)
        {
            FindUiElements(root);
            DoSubscriptions();
        }

        void DoSubscriptions()
        {
            closeButton.OnClickAsObservable()
                .Subscribe(_ => onCloseButtonPressed.OnNext(Unit.Default));
        }

        void FindUiElements(VisualElement root)
        {
            EnergyAvailableText = root.Q<Label>("EnergyAvailable");
            ProductionText = root.Q<Label>("Production");
            ConsumptionText = root.Q<Label>("Consumption");
            CapacityText = root.Q<Label>("Capacity");
            closeButton = root.Q<Button>("Close");
        }

        public void UpdateStaticValues(HouseEnergyStatus energyStatus)
        {

            ProductionText.text= $"00{energyStatus.productionPerSecond:0.0}";
            ConsumptionText.text = $"00{energyStatus.consumptionPerSecond:0.0}";
            CapacityText.text = $"00{energyStatus.capacity:0.0}";
        }

        public void SetEnergyAvailable(float energy)
        {
            EnergyAvailableText.text = $"00{energy:0.0}";
        }
    }
}