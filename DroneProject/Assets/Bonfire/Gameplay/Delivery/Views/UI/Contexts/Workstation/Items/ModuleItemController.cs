﻿using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class ModuleItemController
    {
        Label name;

        public void SetVisualElement(VisualElement item) => 
            name = item.Q<Label>();
        public void SetItemData(Module module) => 
            name.text = module.name;
    }
}