﻿using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps
{
    public class PopUpsController 
    {
        readonly VisualElement root;

        public PopUpsController(VisualElement root)
        {
            this.root = root;
        }

        public void OpenContext() => root.style.display = DisplayStyle.Flex;
        public void CloseContext() => root.style.display = DisplayStyle.None;
    }
}