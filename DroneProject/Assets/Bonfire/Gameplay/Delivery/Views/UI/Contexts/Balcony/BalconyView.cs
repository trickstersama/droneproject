using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony
{
    public class BalconyView : MonoBehaviour
    {
        [SerializeField]
        VisualTreeAsset DroneBalconyItem;

        public ISubject<Unit> onCloseButtonPressed = new Subject<Unit>();
        BalconyViewController controller;

        void OnEnable()
        {
            var uiDocument = GetComponent<UIDocument>();

            controller = new BalconyViewController();
            controller.InitializeCharacterList(uiDocument.rootVisualElement, DroneBalconyItem);
        }

        public void CloseContext()
        {
            controller.Hide();
        }

        public void OpenContext()
        {
            controller.Show();
        }
    }
}
