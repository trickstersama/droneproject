﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class DroneItemController
    {
        Label description;

        public void SetVisualElement(VisualElement visualElement) => 
            description = visualElement.Q<Label>("DroneDescription");

        public void SetCharacterData(Drone drone) => 
            description.text = drone.droneName;
    }
}