﻿using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer
{
    public class ComputerViewController
    {
        VisualElement root;
        Button closeContextButton;
        Button sendDroneTabButton;
        Button encountersTabButton;
        Button statusTabButton;
        Button mapTabButton;

        public readonly ISubject<Unit> OnCloseButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnSendDroneTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnEncountersTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnStatusTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnMapTabButtonPressed = new Subject<Unit>();

        public ComputerViewController(VisualElement root)
        {
            this.root = root;
            FindElements();
            DoSubscriptions();
        }

        void DoSubscriptions()
        {
            closeContextButton.OnClickAsObservable()
                .Subscribe(_ => OnCloseButtonPressed.OnNext(Unit.Default));

            sendDroneTabButton.OnClickAsObservable()
                .Subscribe(_ => OnSendDroneTabButtonPressed.OnNext(Unit.Default));
            
            encountersTabButton.OnClickAsObservable()
                .Subscribe(_ => OnEncountersTabButtonPressed.OnNext(Unit.Default));
            
            statusTabButton.OnClickAsObservable()
                .Subscribe(_ => OnStatusTabButtonPressed.OnNext(Unit.Default));
            
            mapTabButton.OnClickAsObservable()
                .Subscribe(_ => OnMapTabButtonPressed.OnNext(Unit.Default));
        }

        void FindElements()
        {
            closeContextButton = root.Q<Button>("Close");
            sendDroneTabButton = root.Q<Button>("SendDrone");
            encountersTabButton = root.Q<Button>("Encounters");
            statusTabButton = root.Q<Button>("Status");
            mapTabButton = root.Q<Button>("Map");
        }

        public void Show() => root.style.display = DisplayStyle.Flex;
        public void Hide() => root.style.display = DisplayStyle.None;

    }
}