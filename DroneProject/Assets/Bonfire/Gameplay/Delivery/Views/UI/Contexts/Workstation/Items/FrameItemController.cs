﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class FrameItemController
    {
        Label name;
        public void SetVisualElement(VisualElement item) => 
            name = item.Q<Label>("Name");
        
        public void SetItemData(Frame module) => 
            name.text = module.name;
    }
}