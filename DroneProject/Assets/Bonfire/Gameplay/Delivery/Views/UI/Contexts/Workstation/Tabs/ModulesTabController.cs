﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Tabs
{
    public class ModulesTabController
    {
        VisualElement tabRoot;
        ListView list;
        Label description;
        Label modulesAmount;

        List<Module> modules = new();
        
        Button batteryButton;
        Button sensorButton;
        Button storageButton;
        Button propulsionButton;
        Button CPUButton;
        Button armorButton;
        Button weaponButton;

        public readonly Subject<ModuleType> OnModuleTypeButtonPressed = new();
        public void CloseTab() => tabRoot.style.display = DisplayStyle.None;
        public void OpenTab() => tabRoot.style.display = DisplayStyle.Flex;
        
        public ModulesTabController(VisualElement root, VisualTreeAsset moduleItem)
        {
            FindElements(root);
            SetupList(moduleItem);
            DoSubscriptions();
            DefaultDescription();
        }

        public void UpdateModuleItems(IEnumerable<Module> modulesOfType)
        {
            modules.Clear();
            modules.AddRange(modulesOfType.OrderBy(module => module.name));
            list.Rebuild();
            DefaultDescription();
        }

        public void InitializeModules(IEnumerable<Module> updatedModules)
        {
            DefaultDescription();
            modulesAmount.text = $"{updatedModules.Count()} modules found";
            modules.Clear();
        }

        void DefaultDescription()
        {
            description.text = "Select a module";
        }

        void OnModuleItemSelected(IEnumerable<object> _)
        {
            if (list.selectedIndex >= 0)
                AModuleIsSelected();
            else
                description.text = string.Empty;
        }

        void AModuleIsSelected()
        {
            var module = modules[list.selectedIndex];
            SetDescriptionText(module);
        }

        void DoSubscriptions()
        {
            batteryButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Battery));
            sensorButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Sensor));
            storageButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Storage));
            propulsionButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Propulsion));
            CPUButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.CPU));
            armorButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Armor));
            weaponButton.OnClickAsObservable().Subscribe(_ => OnModuleTypeButtonPressed.OnNext(ModuleType.Weapon));
        }

        void SetupList(VisualTreeAsset moduleItem)
        {
            list.makeItem = () => MakeItem(moduleItem);
            list.itemsSource = modules;
            list.onSelectionChange += OnModuleItemSelected;
            list.bindItem = (item, index) => 
                (item.userData as ModuleItemController)?.SetItemData(modules[index]);
        }

        VisualElement MakeItem(VisualTreeAsset moduleItem)
        {
            var item = moduleItem.Instantiate();
            var itemController = new ModuleItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }
        
        void SetDescriptionText(Module module) =>
            description.text = $"Module of type : {module.moduleType} \n" +
                               $"Model: {module.name} \n" +
                               $"Stats: {module.GetFormattedStats()} \n" +
                               $"Description: {module.description}";


        void FindElements(VisualElement root)
        {
            tabRoot = root.Q<VisualElement>("ModulesTabUI");
            list = root.Q<ListView>("ModulesList");
            description = root.Q<Label>("Description");
            modulesAmount = root.Q<Label>("ModulesAmount");
            
            batteryButton = root.Q<Button>("Battery");
            sensorButton = root.Q<Button>("Sensor");
            storageButton = root.Q<Button>("Storage");
            propulsionButton = root.Q<Button>("Propulsion");
            CPUButton = root.Q<Button>("CPU");
            armorButton = root.Q<Button>("Armor");
            weaponButton = root.Q<Button>("Weapon");
        }
    }
}