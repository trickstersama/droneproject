﻿using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer.Tabs
{
    public class SendDroneTabController
    {
        VisualElement tabRoot;
        Label droneSpecs;
        Button selectDestinationButton;
        Label missionOverview;
        Button scoutButton;
        Button lootButton;
        Button battleButton;
        Button deployButton;

        ListView dronesList;
        List<Drone> drones = new();

        public void OpenTab()
        {
            SetDefault();
            tabRoot.style.display = DisplayStyle.Flex;
        }

        public void CloseTab() => tabRoot.style.display = DisplayStyle.None;

        public readonly ISubject<Drone> OnDroneCardSelected = new Subject<Drone>();
        public readonly ISubject<Unit> OnNothingSelected = new Subject<Unit>();
        public readonly ISubject<Unit> OnSelectDestinationButtonPressed = new Subject<Unit>();
        public readonly ISubject<MissionType> OnMissionTypeButtonPressed = new Subject<MissionType>();
        public readonly ISubject<Unit> OnDeployButtonPressed = new Subject<Unit>();


        public SendDroneTabController(VisualElement root, VisualTreeAsset droneItem)
        {
            tabRoot = root.Q<TemplateContainer>("SendDroneTab");
            FindElements();
            CreateList(droneItem);
            DoSubscriptions();
        }

        void SetDefault()
        {
            droneSpecs.text = "Pick a drone";
            missionOverview.text = string.Empty;
            DisableSelectDestination();
            DisableMissionTypeButtons();
            deployButton.pickingMode = PickingMode.Ignore;
        }

        void DoSubscriptions()
        {
            OnDroneCardSelected
                .Subscribe(UpdateSpecs);

            selectDestinationButton.OnClickAsObservable()
                .Subscribe(OnSelectDestinationButtonPressed.OnNext);

            lootButton.OnClickAsObservable()
                .Select(_ => MissionType.Loot)
                .Subscribe(OnMissionTypeButtonPressed.OnNext);
            
            battleButton.OnClickAsObservable()
                .Select(_ => MissionType.Battle)
                .Subscribe(OnMissionTypeButtonPressed.OnNext);
            
            scoutButton.OnClickAsObservable()
                .Select(_ => MissionType.Scout)
                .Subscribe(OnMissionTypeButtonPressed.OnNext);

            deployButton.OnClickAsObservable()
                .Subscribe(_ => OnDeployButtonPressed.OnNext(Unit.Default));
        }

        void UpdateSpecs(Drone drone) => 
            droneSpecs.text = drone.droneName;

        public void Initialize(IEnumerable<Drone> drones) => 
            UpdateDrones(drones);

        void FindElements()
        {
            droneSpecs = tabRoot.Q<Label>("DroneSpecs");
            dronesList = tabRoot.Q<ListView>("Drones");
            selectDestinationButton = tabRoot.Q<Button>("SelectDestination");
            missionOverview = tabRoot.Q<Label>("Overview");
            scoutButton = tabRoot.Q<Button>("Scout");
            lootButton = tabRoot.Q<Button>("Loot");
            battleButton = tabRoot.Q<Button>("Battle");
            deployButton = tabRoot.Q<Button>("Deploy");
        }
        void CreateList(VisualTreeAsset visualTreeAsset)
        {
            dronesList.makeItem = () => MakeItem(visualTreeAsset);
            dronesList.itemsSource = drones;
            dronesList.fixedItemHeight = 90;
            dronesList.onSelectionChange += OnSelectDroneItem;
            dronesList.bindItem = (item, index) =>
                (item.userData as DroneItemController)?.SetCharacterData(drones[index]);
        }

        VisualElement MakeItem(VisualTreeAsset droneItem)
        {
            var item = droneItem.Instantiate();
            var itemController = new DroneItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }
        
        void OnSelectDroneItem(IEnumerable<object> _)
        {
            if (dronesList.selectedIndex >= 0)
            {
                var selectedDrone = drones[dronesList.selectedIndex];
                OnDroneCardSelected.OnNext(selectedDrone);
            }
            else
                OnNothingSelected.OnNext(Unit.Default);
        }

        void UpdateDrones(IEnumerable<Drone> updatedDrones)
        {
            drones.Clear();
            drones.AddRange(updatedDrones);
            dronesList.Rebuild();
        }

        public void CardSelected() => 
            EnableSelectDestination();
        
        public void CoordinatesSelected() => 
            EnableMissionTypeButtons();

        void EnableSelectDestination() => 
            selectDestinationButton.pickingMode = PickingMode.Position;

        void DisableSelectDestination() => 
            selectDestinationButton.pickingMode = PickingMode.Ignore;

        public void UpdateMissionOverview(MissionOrders missionOrders) => 
            missionOverview.text = missionOrders.ToString();


        void EnableMissionTypeButtons()
        {
            scoutButton.pickingMode = PickingMode.Position;
            lootButton.pickingMode = PickingMode.Position;
            battleButton.pickingMode = PickingMode.Position;
        }
        
        void DisableMissionTypeButtons()
        {
            scoutButton.pickingMode = PickingMode.Ignore;
            lootButton.pickingMode = PickingMode.Ignore;
            battleButton.pickingMode = PickingMode.Ignore;
        }

        public void EnableDeploy() => 
            deployButton.pickingMode = PickingMode.Position;
    }
}