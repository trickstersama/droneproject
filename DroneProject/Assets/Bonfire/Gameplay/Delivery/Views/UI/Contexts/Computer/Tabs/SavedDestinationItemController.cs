﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer.Tabs
{
    public class SavedDestinationItemController
    {
        Label nameText;

        public void SetVisualElement(VisualElement item)
        {
            nameText = item.Q<Label>("Name");
        }

        public void SetCharacterData(Destination destination)
        {
            nameText.text = destination.ToFormattedString();
        }
    }
}