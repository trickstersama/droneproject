﻿using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps
{
    public class DronePopUpController
    {
        readonly VisualElement root;
        Button claimButton;
        Button nameDroneButton;
        TextField textField;
        Label warningText;

        public readonly ISubject<string> OnNameDroneButtonPressed = new Subject<string>();
        public readonly ISubject<Unit> OnDroneClaimButtonPressed = new Subject<Unit>();

        public DronePopUpController(VisualElement root)
        {
            this.root = root.Q<VisualElement>("DronePopUpUI");
            FindElements();
            DoSubscriptions();
            Default();
        }
        /*
         
        unavailable to do right now with UI elements in this version
        nameInput.onValidateInput += (_, __, addedChar) => InputUtilities.Validate(addedChar);

        BuildDroneButton.OnClickAsObservable()
            .Select(_ => nameInput.text)
            .Subscribe(OnBuildDronePressed.OnNext);
        */
        
        void Default()
        {
            nameDroneButton.style.display = DisplayStyle.Flex;
            claimButton.style.display = DisplayStyle.None;
            textField.style.display = DisplayStyle.Flex;
            warningText.text = string.Empty;
        }

        void DoSubscriptions()
        {
            nameDroneButton.OnClickAsObservable()
                .Select(_ => textField.value)
                .Subscribe(OnNameDroneButtonPressed.OnNext);

            claimButton.OnClickAsObservable()
                .Subscribe(OnDroneClaimButtonPressed.OnNext);
        }

        void FindElements()
        {
            claimButton = root.Q<Button>("Claim");
            nameDroneButton = root.Q<Button>("NameDrone");
            textField = root.Q<TextField>("Input");
            warningText = root.Q<Label>("Warning");
        }

        public void Show() => root.style.display = DisplayStyle.Flex;

        public void Hide() => root.style.display = DisplayStyle.None;

        public void ShowInvalidNameWarning() => warningText.text = "Name invalid";

        public void ShowClaimButton()
        {
            textField.style.display = DisplayStyle.None;
            nameDroneButton.style.display = DisplayStyle.None;
            claimButton.style.display = DisplayStyle.Flex;
        }
    }
}