﻿using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items
{
    public class AssemblyModuleItemController
    {
        Label name;
        VisualElement root;
        public void SetVisualElement(VisualElement item)
        {
            root = item;
            name = root.Q<Label>("Name");
            root.focusable = true;
        }

        public void SetCharacterData(Module frame) => name.text = frame.name;

        public void Highlight() => root.AddToClassList(UiTags.ClassNameWhenSelected);
    }
}