﻿using UniRx;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation
{
    public class WorkstationViewController
    {
        VisualElement root;
        Button dronesTabButton;
        Button modulesTabButton;
        Button framesTabButton;     
        Button assemblyTabButton;

        Button closeContextButton;

        public readonly ISubject<Unit> OnDronesTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnModulesTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnFramesTabButtonPressed = new Subject<Unit>();
        public readonly ISubject<Unit> OnAssemblyTabButtonPressed = new Subject<Unit>();

        public readonly ISubject<Unit> OnCloseButtonPressed = new Subject<Unit>();

        public void Initialize(VisualElement root)
        {
            this.root = root;
            FindElements();
            MakeSubscriptions();
        }

        void MakeSubscriptions()
        {
            dronesTabButton.OnClickAsObservable()
                .Subscribe(_ => OnDronesTabButtonPressed.OnNext(Unit.Default));
            modulesTabButton.OnClickAsObservable()
                .Subscribe(_ => OnModulesTabButtonPressed.OnNext(Unit.Default));
            framesTabButton.OnClickAsObservable()
                .Subscribe(_ => OnFramesTabButtonPressed.OnNext(Unit.Default));     
            assemblyTabButton.OnClickAsObservable()
                .Subscribe(_ => OnAssemblyTabButtonPressed.OnNext(Unit.Default));
            closeContextButton.OnClickAsObservable()
                .Subscribe(_ => OnCloseButtonPressed.OnNext(Unit.Default));
        }

        void FindElements()
        {
            dronesTabButton = root.Q<Button>("Drones");
            modulesTabButton = root.Q<Button>("Modules");
            framesTabButton = root.Q<Button>("Frames");
            assemblyTabButton = root.Q<Button>("Assembly");
            closeContextButton = root.Q<Button>("Close");
        }

        public void Show() => root.style.display = DisplayStyle.Flex;

        public void Hide() => root.style.display = DisplayStyle.None;
    }
}