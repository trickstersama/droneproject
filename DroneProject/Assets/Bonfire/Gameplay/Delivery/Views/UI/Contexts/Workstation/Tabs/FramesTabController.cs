﻿using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items;
using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Tabs
{
    public class FramesTabController
    {
        readonly List<Frame> frames = new();
        
        VisualElement tabRoot;
        ListView list;
        Label description;

        public FramesTabController(VisualElement root, VisualTreeAsset frameItem)
        {
            FindElements(root);
            SetUpList(frameItem);
        }

        public void CloseTab() => tabRoot.style.display = DisplayStyle.None;
        public void OpenTab() => tabRoot.style.display = DisplayStyle.Flex;
        public void InitializeFrames(IEnumerable<Frame> updatedFrames)
        {
            frames.Clear();
            frames.AddRange(updatedFrames);
            list.Rebuild();
            DefaultDescription();
        }
        
        void SetUpList(VisualTreeAsset frameItem)
        {
            list.makeItem = () => MakeItem(frameItem);
            list.itemsSource = frames;
            list.onSelectionChange += OnModuleItemSelected;
            list.bindItem = (item, index) => 
                (item.userData as FrameItemController)?.SetItemData(frames[index]);
        }

        VisualElement MakeItem(VisualTreeAsset moduleItem)
        {
            var item = moduleItem.Instantiate();
            var itemController = new FrameItemController();
            item.userData = itemController;
            itemController.SetVisualElement(item);
            return item;
        }
        
        void FindElements(VisualElement root)
        {
            tabRoot = root.Q<VisualElement>("FramesTabUI");
            list = tabRoot.Q<ListView>("FramesList");
            description = tabRoot.Q<Label>("FrameDescription");
        }
        void OnModuleItemSelected(IEnumerable<object> _)
        {
            if (list.selectedIndex >= 0)
                AFrameIsSelected();
            else
                description.text = string.Empty;
        }
        void AFrameIsSelected()
        {
            var module = frames[list.selectedIndex];
            SetDescriptionText(module);
        }

        void SetDescriptionText(Frame frame) => description.text = frame.ToString();

        void DefaultDescription() => description.text = "Select a frame";
    }
}