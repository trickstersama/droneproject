using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Tabs;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation
{
    public class WorkstationView : MonoBehaviour
    {
        [SerializeField] VisualTreeAsset DroneItem;
        [SerializeField] VisualTreeAsset ModuleItem;
        [SerializeField] VisualTreeAsset FrameItem;
        [SerializeField] VisualTreeAsset AssemblyItem;
        WorkstationViewController mainController;
        DronesTabController dronesTabController;
        ModulesTabController modulesTabController;
        FramesTabController framesTabController;
        AssemblyTabController assemblyTabController;

        public IObservable<Unit> onCloseButtonPressed => mainController.OnCloseButtonPressed;
        public IObservable<Unit> OnDronesTabButtonPressed => mainController.OnDronesTabButtonPressed;
        public IObservable<Unit> OnModulesTabButtonPressed => mainController.OnModulesTabButtonPressed;
        public IObservable<Unit> OnFramesTabButtonPressed => mainController.OnFramesTabButtonPressed;
        public IObservable<Unit> OnAssemblyTabButtonPressed => mainController.OnAssemblyTabButtonPressed;
        
        public IObservable<Drone> OnItemSelected => dronesTabController.OnItemSelected;
        public IObservable<Unit> OnRechargeButtonPressed => dronesTabController.OnRechargeButtonPressed;
        public IObservable<Unit> OnRepairButtonPressed => dronesTabController.OnRepairButtonPressed;
        public IObservable<Unit> OnDisassemblyButtonPressed => dronesTabController.OnDisassemblyButtonPressed;
        
        public IObservable<ModuleType> OnModuleTypeButtonPressed => modulesTabController.OnModuleTypeButtonPressed;
        public IObservable<Frame> OnFrameItemSelected => assemblyTabController.OnFrameItemSelected;
        public IObservable<Unit> OnConfirmFrame => assemblyTabController.OnConfirmFrame;
        public IObservable<Unit> OnClearFrameSelection => assemblyTabController.OnClearFrameSelection;
        public IObservable<Module> OnInstallAssemblyModule => assemblyTabController.OnModuleItemInstalled;
        public IObservable<Unit> OnBuildDronePressed => assemblyTabController.OnBuildDronePressed;
        public IObservable<Module> OnUninstallModulePressed => assemblyTabController.OnUninstallModulePressed;


        void OnEnable()
        {
            var root = GetComponent<UIDocument>().rootVisualElement;
            mainController = new WorkstationViewController();
            dronesTabController = new DronesTabController(root: root, droneItem: DroneItem);
            modulesTabController = new ModulesTabController(root: root, moduleItem: ModuleItem);
            framesTabController = new FramesTabController(root: root, frameItem: FrameItem);
            assemblyTabController = new AssemblyTabController(root: root, assemblyItem: AssemblyItem);
            
            mainController.Initialize(root);
            
        }

        public void SelectDronesTab(IEnumerable<Drone> drones)
        {
            CloseAllTabs();
            dronesTabController.OpenTab();
            dronesTabController.UpdateDrones(drones);
        }

        public void SelectModulesTab(IEnumerable<Module> modules)
        {
            CloseAllTabs();
            modulesTabController.OpenTab();
            modulesTabController.InitializeModules(modules);
        }
        
        public void SelectFramesTab(IEnumerable<Frame> updatedFrames)
        {
            CloseAllTabs();
            framesTabController.OpenTab();
            framesTabController.InitializeFrames(updatedFrames);
        }
        
        public void SelectAssemblyTab(IEnumerable<Frame> updatedFrames)
        {
            CloseAllTabs();
            assemblyTabController.OpenTab();
            assemblyTabController.InitializeFrames(updatedFrames);
        }

        public void OpenContext() => 
            mainController.Show();

        public void CloseContext() => 
            mainController.Hide();

        public void ActivateRepair() => 
            dronesTabController.ActivateRepairButton();

        public void ActivateRecharge() => 
            dronesTabController.ActivateRechargeButton();

        public void UpdateModuleItems(IEnumerable<Module> modules) => modulesTabController.UpdateModuleItems(modules);

        public void UpdateSelectedDrone(Drone selectedDrone) => dronesTabController.UpdateSelectedDrone(selectedDrone);

        public void UpdateDroneStats(DroneModifiers droneModifiers) => assemblyTabController.UpdateDroneStats(droneModifiers);

        public void SetPanelsSlotsAmount(Frame frame) => assemblyTabController.SetPanelsSlotsAmount(frame);
        public void UpdateAssemblyModules(IEnumerable<Module> modules) => assemblyTabController.UpdateModules(modules);

        void CloseAllTabs()
        {
            dronesTabController.CloseTab();
            modulesTabController.CloseTab();
            framesTabController.CloseTab();
            assemblyTabController.CloseTab();
        }

        public void ShowStats(Frame frame)
        {
            assemblyTabController.ShowStats(frame);
        }

        public void RefreshModulePanelSelection()
        {
            assemblyTabController.RefreshModulePanelSelection();
        }

    }
}
