using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps
{
    public class PopUpsView : MonoBehaviour
    {
        PopUpsController mainController;
        DronePopUpController dronePopUpController;
        
        public ISubject<string> OnNameDronePressed => dronePopUpController.OnNameDroneButtonPressed;
        public ISubject<Unit> OnDroneClaimButtonPressed => dronePopUpController.OnDroneClaimButtonPressed;

        void OnEnable()
        {
            var root = GetComponent<UIDocument>().rootVisualElement;
            mainController = new PopUpsController(root);
            dronePopUpController = new DronePopUpController(root);
            mainController.CloseContext();
        }

        public void ShowDronePopUp()
        {
            mainController.OpenContext();
            dronePopUpController.Show();
        }

        public void ShowInvalidNameWarning()
        {
            dronePopUpController.ShowInvalidNameWarning();
        }

        public void ShowClaimButton()
        {
            dronePopUpController.ShowClaimButton();
        }

        public void ClosePopUps()
        {
            mainController.CloseContext();
            dronePopUpController.Hide();
        }
    }
}
