﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Items;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using UniRx;
using UnityEngine.UIElements;
using static Bonfire.Gameplay.Domain.ValueObjects.EnumeratesUtilities;

namespace Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation.Tabs
{
    public class AssemblyTabController
    {
        VisualElement tabRoot;
        Label finalStats;
        Label individualSpecs;
        Label slotsAmount;
        Button resetButton;
        Button selectButton;
        Button uninstallButton;
        Button buildButton;
        
        //ModuleButtons
        VisualElement weaponPanel;
        VisualElement propulsionPanel;
        VisualElement batteryPanel;
        VisualElement sensorPanel;
        VisualElement storagePanel;
        VisualElement CPUPanel;
        VisualElement armorPanel;

        FramePanelController framePanelController;
        ModulePanelController panelSelected;
        List<ModulePanelController> modulePanelControllers;
        List<Button> modulePanelButtons;

        public readonly ISubject<Unit> OnClearFrameSelection = new Subject<Unit>();
        public readonly ISubject<Module> OnModuleItemInstalled = new Subject<Module>();
        public readonly ISubject<Unit> OnBuildDronePressed = new Subject<Unit>();
        public readonly ISubject<Module> OnUninstallModulePressed = new Subject<Module>(); 
        readonly ISubject<string> OnPanelOpenedMessage = new Subject<string>();

        public ISubject<Frame> OnFrameItemSelected => framePanelController.OnFrameItemSelected;

        public ISubject<Unit> OnConfirmFrame => framePanelController.OnFrameConfirmed;

        public AssemblyTabController(VisualElement root, VisualTreeAsset assemblyItem)
        {
            FindElements(root);
            CreateLists(assemblyItem);
            CreateSubscriptions();
        }

        void FindElements(VisualElement root)
        {            
            tabRoot = root.Q<VisualElement>("AssemblyTabUI");
            finalStats = tabRoot.Q<Label>("FinalStats");
            individualSpecs = tabRoot.Q<Label>("IndividualSpecs");
            slotsAmount = tabRoot.Q<Label>("SlotsAmount");
            resetButton = tabRoot.Q<Button>("Reset");
            selectButton = tabRoot.Q<Button>("Select");
            uninstallButton = tabRoot.Q<Button>("Uninstall");
            buildButton = tabRoot.Q<Button>("Build");
        }

        void CreateLists(VisualTreeAsset assemblyItem)
        {
            FindLists();
            CreateFramePanelController(assemblyItem);
            CreateModulePanelControllers(assemblyItem);
        }

        void CreateSubscriptions()
        {
            framePanelController.OnFrameItemSelected
                .Do(_ => EnableSelectButton())
                .Subscribe();
            
            framePanelController.OnNothingSelected
                .Do(_ => DisableSelectButton())
                .Subscribe();
            
            resetButton.OnClickAsObservable()
                .Do(_ => DefaultPanels())
                .Subscribe(OnClearFrameSelection.OnNext);

            selectButton.OnClickAsObservable()
                .Do(_ => CleanSpecs())
                .Subscribe(_ => ConfirmSelection());
            
            modulePanelControllers.ForEach(controller => controller.OnModuleItemSelected
                .Do(_ => EnableSelectButton())
                .Subscribe(ShowStats));    
            
            modulePanelControllers.ForEach(controller => controller.OnModuleItemInstalled
                .Do(ShowStats)
                .Subscribe(module => OnModuleItemInstalled.OnNext(module)));

            modulePanelControllers.ForEach(controller => controller.OnInstallEnabled
                .Subscribe(enabled => selectButton.pickingMode = enabled ? PickingMode.Position : PickingMode.Ignore));

            modulePanelControllers.ForEach(controller => controller.OnModuleSelectedAlreadyInstalled
                .Subscribe(HandleSelectButton));
            
            modulePanelControllers.ForEach(controller => controller.OnNothingSelected
                .Subscribe(_ => DisableSelectButton()));
            
            modulePanelControllers.ForEach(controller => controller.OnUninstallModule
                .Subscribe(module => OnUninstallModulePressed.OnNext(module)));

            OnPanelOpenedMessage
                .Subscribe(amount => slotsAmount.text = amount.ToString());

            buildButton.OnClickAsObservable()
                .Subscribe(OnBuildDronePressed.OnNext);

            uninstallButton.OnClickAsObservable()
                .Subscribe(_ => panelSelected.UninstallSelectedModule());
            
            SetupButtons();
        }

        void DisableSelectButton() => 
            selectButton.pickingMode = PickingMode.Ignore;

        void EnableSelectButton() => 
            selectButton.pickingMode = PickingMode.Position;

        public void RefreshModulePanelSelection() => 
            ShowModulePanel(panelSelected.moduleType);
        public void CloseTab() =>
            tabRoot.style.display = DisplayStyle.None;

        public void OpenTab()
        {
            tabRoot.style.display = DisplayStyle.Flex;
            
            DefaultActionButtonsForFrame();
            CleanSpecs();
            finalStats.text = "Build a drone";
            DefaultPanels();
            ShowFramePanel();
        }

        void DefaultActionButtonsForFrame()
        {
            modulePanelButtons.ForEach(button => button.pickingMode = PickingMode.Ignore);
            ShowSelectButton();
            buildButton.pickingMode = PickingMode.Ignore;
            
            selectButton.pickingMode = PickingMode.Ignore;
            selectButton.text = "Select";
        }

        void DefaultPanels()
        {
            framePanelController.Reset();
            slotsAmount.text = string.Empty;
            modulePanelControllers.ForEach(controller => controller.Clear());
            panelSelected = null;
        }

        void ActivateModuleButtons() =>
            modulePanelButtons.ForEach(button => button.pickingMode = PickingMode.Position);

        void ShowFramePanel()
        {
            HideAllLists();
            framePanelController.Show();
        }

        void HideAllLists()
        {
            modulePanelControllers.ForEach(controller => controller.Hide());
            framePanelController.Hide();
        }

        void HandleSelectButton(bool alreadyInstalled)
        {
            if (alreadyInstalled)
                ShowUninstallButton();
            else
                ShowSelectButton();
        }

        void ShowSelectButton()
        {
            uninstallButton.style.display = DisplayStyle.None;
            selectButton.style.display = DisplayStyle.Flex;
        }

        void ShowUninstallButton()
        {
            uninstallButton.style.display = DisplayStyle.Flex;
            selectButton.style.display = DisplayStyle.None;
        }

        void ConfirmSelection()
        {
            if (panelSelected != null)
                panelSelected.ConfirmSelection();
            else
            {
                framePanelController.ConfirmSelection();
                ActivateModuleButtons();
                SelectFirstModuleList();
                buildButton.pickingMode = PickingMode.Position;
            }
        }

        void SelectFirstModuleList()
        {
            panelSelected = modulePanelControllers[0];
            ShowModulePanel(panelSelected.moduleType);
        }

        public void InitializeFrames(IEnumerable<Frame> updatedFrames) => 
            framePanelController.UpdateFrames(updatedFrames);

         public void UpdateDroneStats(DroneModifiers droneModifiers) =>
            finalStats.text = 
                $"Stats; stealth: {droneModifiers.stealth} \n" +
                $"damage: {droneModifiers.damage} \n" +
                $"weight: {droneModifiers.weight}";

        public void SetPanelsSlotsAmount(Frame frame) => 
            modulePanelControllers.ForEach(controller => controller.SetSlotsAmount(frame));

        public void UpdateModules(IEnumerable<Module> modules) => 
            modulePanelControllers.ForEach(controller => controller.UpdateModules(GetModulesFor(modules, controller.moduleType)));

        public void ShowStats(Frame frame) => 
            individualSpecs.text = frame.GetModifiers().ToString();

        static IEnumerable<Module> GetModulesFor(IEnumerable<Module> modules, ModuleType moduleType) => 
            modules.Where(module => module.moduleType == moduleType);

        void SetupButtons()
        {
            modulePanelButtons = new List<Button>();
            
            foreach (var (name, type) in ModuleTypeByStringSingular())
            {
                var module = tabRoot.Q<Button>(name);
                modulePanelButtons.Add(module);
                module.OnClickAsObservable()
                    .Do(_ => CleanSpecs())
                    .Subscribe(_ => ShowModulePanel(type));
            }
        }

        void ShowModulePanel(ModuleType moduleType)
        {
            DefaultActionButtonsForModules();
            HideAllLists();
            AssignSelectedPanel(moduleType);
        }

        void AssignSelectedPanel(ModuleType moduleType)
        {
            var panelController = modulePanelControllers.Find(module => module.moduleType == moduleType);
            panelController.Show();
            panelSelected = panelController;
        }

        void DefaultActionButtonsForModules()
        {
            ShowSelectButton();
            selectButton.text = "Install";
        }

        void CreateFramePanelController(VisualTreeAsset assemblyItem)
        {
            var framePanel = tabRoot.Q<VisualElement>("FrameList");
            framePanelController = new FramePanelController(framePanel, assemblyItem, OnPanelOpenedMessage);
        }

        void CreateModulePanelControllers(VisualTreeAsset assemblyItem) =>
            modulePanelControllers = new List<ModulePanelController>
            {
                new(weaponPanel, ModuleType.Weapon, assemblyItem, OnPanelOpenedMessage),
                new(propulsionPanel, ModuleType.Propulsion, assemblyItem, OnPanelOpenedMessage),
                new(batteryPanel, ModuleType.Battery, assemblyItem, OnPanelOpenedMessage),
                new(sensorPanel, ModuleType.Sensor, assemblyItem, OnPanelOpenedMessage),
                new(storagePanel, ModuleType.Storage, assemblyItem, OnPanelOpenedMessage),
                new(CPUPanel, ModuleType.CPU, assemblyItem, OnPanelOpenedMessage),
                new(armorPanel, ModuleType.Armor, assemblyItem, OnPanelOpenedMessage)
            };

        void FindLists()
        {
            weaponPanel = tabRoot.Q<VisualElement>("WeaponList");
            propulsionPanel = tabRoot.Q<VisualElement>("PropulsionList");
            batteryPanel = tabRoot.Q<VisualElement>("BatteryList");
            sensorPanel = tabRoot.Q<VisualElement>("SensorList");
            storagePanel = tabRoot.Q<VisualElement>("StorageList");
            CPUPanel = tabRoot.Q<VisualElement>("CPUList");
            armorPanel = tabRoot.Q<VisualElement>("ArmorList");
        }

        void CleanSpecs() => 
            individualSpecs.text = string.Empty;

        void ShowStats(Module module) => 
            individualSpecs.text = module.GetFormattedStats();
    }
}