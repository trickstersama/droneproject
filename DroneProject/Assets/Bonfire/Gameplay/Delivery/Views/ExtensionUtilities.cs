﻿using Bonfire.Gameplay.Domain.ValueObjects;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Views
{
    public static class ExtensionUtilities
    {
        public static MapCoordinates ToMapCoordinates(this Vector3 vector) => 
            new(vector.x, vector.y);
    }
}