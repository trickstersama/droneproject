﻿namespace Bonfire.Gameplay.Delivery.Views
{
    public class ContextDirector 
    {
        bool aContextIsOpen;
        public bool AContextIsOpen() => aContextIsOpen;
        public void ContextOpened() => aContextIsOpen = true;
        public void ContextClosed() => aContextIsOpen = false;
    }
}