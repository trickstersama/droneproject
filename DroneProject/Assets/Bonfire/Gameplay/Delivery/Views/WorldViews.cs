﻿using Bonfire.Gameplay.Delivery.Views.World;

namespace Bonfire.Gameplay.Delivery.Views
{
    public struct WorldViews
    {
        public WorldGameplayView WorldGameplayView { get; set; }
        public TimelineView TimelineView { get; set; }
    }
}