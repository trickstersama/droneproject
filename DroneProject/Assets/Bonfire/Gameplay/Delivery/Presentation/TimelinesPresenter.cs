﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Domain.Reactions.House;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation

{
    public class TimelinesPresenter
    {
        readonly EmitTimelines emitTimelines;
        readonly ProduceEnergy produceEnergy;
        
        readonly IObservable<float> onEnergyTimelineEmitted;

        readonly TimelineView timelineView;

        public TimelinesPresenter(
            GameplayReactions reactions, 
            WorldViews worldViews,
            HouseGameplayEvents houseEvents
        ) {
            emitTimelines = reactions.EmitTimelines;
            produceEnergy = reactions.ProduceEnergy;

            onEnergyTimelineEmitted = houseEvents.OnEnergyTimelineEmitted;
            
            timelineView = worldViews.TimelineView;
        }

        public IEnumerable<IDisposable> Disposables => 
            new[]
            {
                ASecondInGameHasPassed,
                OnEnergyTimelineEmitted,
                OnMissionsTimelineEmitted
            };
        
        IDisposable ASecondInGameHasPassed =>
            timelineView.UnityTimeline
                .Subscribe(interval => emitTimelines.Do(interval));
        
        IDisposable OnEnergyTimelineEmitted =>
            onEnergyTimelineEmitted
                .Subscribe(produceEnergy.Do); 
        
        IDisposable OnMissionsTimelineEmitted =>
            onEnergyTimelineEmitted
                .Subscribe(produceEnergy.Do);

    }
}