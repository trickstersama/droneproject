﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.House;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;
using Bonfire.Gameplay.Delivery.Views.World;
using Bonfire.Gameplay.Domain.Actions.BalconyContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.HouseBatteryContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation
{
    public class ContextsPresenter
    {
        readonly IObservable<HouseEnergyStatus> onHouseBatteryOpened;

        readonly WorldGameplayView worldGameplayView;
        readonly WorkstationView workstationView;
        readonly BalconyView balconyView;
        readonly HouseBatteryView houseBatteryView;
        readonly ComputerView computerView;

        readonly OpenComputer openComputer;
        readonly OpenWorkstation openWorkstation;
        readonly OpenHouseBattery openHouseBattery;
        readonly OpenBalcony openBalcony;
        ContextDirector contextDirector;

        public ContextsPresenter(
            CanvasViews canvasViews,
            WorldViews worldViews,
            GameplayActions actions,
            GameplayEvents events
        ) {
            worldGameplayView = worldViews.WorldGameplayView;

            workstationView = canvasViews.WorkstationView;
            balconyView = canvasViews.BalconyView;
            computerView = canvasViews.ComputerView;
            houseBatteryView = canvasViews.HouseBatteryView;
            contextDirector = canvasViews.ContextDirector;
    
            openComputer = actions.OpenComputer;
            openWorkstation = actions.OpenWorkstation;
            openHouseBattery = actions.OpenHouseBattery;
            openBalcony = actions.OpenBalcony;

            onHouseBatteryOpened = events.OnHouseBatteryOpened;
            
            CloseEverything();
        }
        
        public void DoSubscriptions(CompositeDisposable disposables, IEnumerable<IDisposable> externalDisposables) => 
            PrepareForDisposition(disposables, Disposables().Concat(externalDisposables));
        static void PrepareForDisposition(CompositeDisposable disposables, IEnumerable<IDisposable> subscriptions)
        {
            foreach (var subscription in subscriptions) subscription.AddTo(disposables);
        }


        IEnumerable<IDisposable> Disposables() => 
            new[]
            {
                OpenWorkstation,
                OpenBalcony,
                OpenComputerContext,
                OpenHouseBattery,
                CloseWorkstation,
                CloseComputerContext,
                CloseBalconyContext,
                CloseHouseBatteryContext,
                OnHouseBatteryOpened
            };
        
        IDisposable OpenComputerContext =>
            worldGameplayView.OnActivateComputer
                .Where(_ => !contextDirector.AContextIsOpen())
                .Do(_ => contextDirector.ContextOpened())
                .Do(_ => computerView.OpenContext())
                .Subscribe(_ => openComputer.Do());


        IDisposable OpenWorkstation =>
            worldGameplayView.OnActivateWorkstation
                .Where(_ => !contextDirector.AContextIsOpen())
                .Do(_ => contextDirector.ContextOpened())
                .Do(_ => workstationView.OpenContext())
                .Subscribe(_ => openWorkstation.Do());

        IDisposable OpenBalcony =>
            worldGameplayView.OnActivateBalcony
                .Where(_ => !contextDirector.AContextIsOpen())
                .Do(_ => contextDirector.ContextOpened())
                .Do(_ => balconyView.OpenContext())
                .Subscribe(_ => openBalcony.Do());

        IDisposable OpenHouseBattery =>
            worldGameplayView.OnActivateHouseBattery
                .Where(_ => !contextDirector.AContextIsOpen())
                .Do(_ => contextDirector.ContextOpened())
                .Do(_ => houseBatteryView.OpenContext())
                .Subscribe(_ => openHouseBattery.Do());
        /// 


        IDisposable CloseWorkstation =>
            workstationView.onCloseButtonPressed
                .Do(_ => contextDirector.ContextClosed())
                .Subscribe(_ => workstationView.CloseContext()); 
        
        IDisposable CloseComputerContext =>
            computerView.onCloseButtonPressed
                .Do(_ => contextDirector.ContextClosed())
                .Subscribe(_ => computerView.CloseContext()); 
        
        IDisposable CloseBalconyContext =>
            balconyView.onCloseButtonPressed
                .Do(_ => contextDirector.ContextClosed())
                .Subscribe(_ => balconyView.CloseContext());

        IDisposable CloseHouseBatteryContext =>
            houseBatteryView.onCloseButtonPressed
                .Do(_ => contextDirector.ContextClosed())
                .Subscribe(_ => houseBatteryView.CloseContext());

        /// <summary>
        /// to extract
        /// </summary>

        IDisposable OnHouseBatteryOpened =>
            onHouseBatteryOpened
                .Do(houseBatteryView.UpdateStaticValues)
                .Subscribe();
        
        void CloseEverything()
        {
            workstationView.CloseContext();
            balconyView.CloseContext();
            houseBatteryView.CloseContext();
            computerView.CloseContext();
        }
    }
}