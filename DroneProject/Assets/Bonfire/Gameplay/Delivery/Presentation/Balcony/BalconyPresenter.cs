﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Balcony

{
    public class BalconyPresenter
    {
        readonly ISubject<IEnumerable<MissionResults>> onBalconyOpened;
        readonly BalconyView balconyView;

        public BalconyPresenter(
            GameplayEvents events,
            CanvasViews canvasViews
        ) {
            onBalconyOpened = events.OnBalconyOpened;
            balconyView = canvasViews.BalconyView;
        }

        public IEnumerable<IDisposable> Disposables => 
            new []
            {
                OnBalconyOpened
            };

        IDisposable OnBalconyOpened =>
            onBalconyOpened.Subscribe(/*balconyView.Initialize*/);
    }
}