﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Missions

{
    public class MissionsPresenter
    {
        readonly IObservable<float> onMissionsTimelineEmitted;
        readonly IObservable<IEnumerable<Mission>> onMissionsTimeUpdated;
        readonly IObservable<Mission> onMissionTargetReached;
        readonly IObservable<Mission> onMissionTargetCompleted;
        readonly IObservable<Mission> onMissionReturnedHome;
        
        readonly UpdateMissionsTime updateMissionsTime;
        readonly MissionTargetCompleted missionTargetCompleted;
        readonly MissionTargetReached missionTargetReached;
        readonly StartMissionTarget startMissionTarget;
        readonly MissionReturnsHome missionReturnsHome;
        readonly StartMissionReturn startMissionReturn;
        readonly CompleteMission completeMission;

        public MissionsPresenter(
        GameplayEvents events,
        GameplayReactions reactions
    ) {
        onMissionsTimelineEmitted = events.OnMissionsTimelineEmitted;
        onMissionsTimeUpdated = events.OnMissionsTimeUpdated;
        onMissionTargetReached = events.OnMissionTargetReached;
        onMissionTargetCompleted = events.OnMissionTargetCompleted;
        onMissionReturnedHome = events.OnMissionReturnedHome;
        
        updateMissionsTime = reactions.UpdateMissionsTime;
        missionTargetReached = reactions.MissionTargetReached;
        startMissionTarget = reactions.StartMissionTarget;
        missionTargetCompleted = reactions.MissionTargetCompleted;
        missionReturnsHome = reactions.MissionReturnsHome;
        startMissionReturn = reactions.StartMissionReturn;
        completeMission = reactions.CompleteMission;
    }

        public IEnumerable<IDisposable> Disposables => 
            new[]
            {
                OnMissionsTimelineEmitted,
                OnMissionsTimeUpdated,
                OnMissionTargetReached,
                OnMissionTargetCompleted,
                OnMissionReturnedHome
            };
        
        IDisposable OnMissionsTimelineEmitted =>
            onMissionsTimelineEmitted
                .Do(timePassed => updateMissionsTime.Do(timePassed))
                .Subscribe();

        IDisposable OnMissionsTimeUpdated =>
            onMissionsTimeUpdated
                .Do(missions => UnityEngine.Debug.Log($"There is {missions.Count()} missions right now"))
                .SelectMany(missions => missions)
                .Do(mission => missionTargetReached.Do(mission))
                .Do(mission => missionTargetCompleted.Do(mission))
                .Do(mission => missionReturnsHome.Do(mission))
                .Subscribe();

        IDisposable OnMissionTargetReached =>
            onMissionTargetReached
                .Do(startMissionTarget.Do)
                .Subscribe();

        IDisposable OnMissionTargetCompleted =>
            onMissionTargetCompleted
                .Do(startMissionReturn.Do)
                .Subscribe();

        IDisposable OnMissionReturnedHome =>
            onMissionReturnedHome
                .Do(completeMission.Do)
                .Subscribe();
    }
}