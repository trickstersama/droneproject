﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.House;
using Bonfire.Gameplay.Domain.Reactions.House;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.House
{
    public class HouseEnergyPresenter
    {
        readonly DistributeEnergy distributeEnergy;
        readonly StoreEnergy storeEnergy;
        
        readonly IObservable<float> onEnergyProduced;
        readonly IObservable<Unit> onInsufficientEnergyIsProduced;
        readonly IObservable<float> onEnergyOverheadProduced;
        readonly IObservable<float> onEnergyStored;
        readonly IObservable<Unit> onEnergyStorageFull;

        readonly HouseBatteryView houseBatteryView;


        public HouseEnergyPresenter(
            GameplayReactions reactions,
            HouseGameplayEvents houseEvents,
            CanvasViews canvasViews
        ) {
            distributeEnergy = reactions.DistributeEnergy;
            storeEnergy = reactions.StoreEnergy;
            
            onEnergyProduced = houseEvents.OnEnergyProduced;
            onInsufficientEnergyIsProduced = houseEvents.OnInsufficientEnergyIsProduced;
            onEnergyOverheadProduced = houseEvents.OnEnergyOverheadProduced;
            onEnergyStored = houseEvents.OnEnergyStored;
            onEnergyStorageFull = houseEvents.OnEnergyStorageFull;

            houseBatteryView = canvasViews.HouseBatteryView;
        }

        public IEnumerable<IDisposable> Disposables => 
            new[]
            {
                OnEnergyProduced,
                OnInsufficientEnergyIsProduced,
                OnEnergyOverheadProduced,
                OnEnergyStored,
                OnEnergyStorageFull
            };

        IDisposable OnEnergyProduced =>
            onEnergyProduced
                .Subscribe(distributeEnergy.Do);

        IDisposable OnInsufficientEnergyIsProduced =>
            onInsufficientEnergyIsProduced
                .Subscribe(_ => UnityEngine.Debug.Log("Insufficient energy Produced"));

        IDisposable OnEnergyOverheadProduced =>
            onEnergyOverheadProduced
                .Subscribe(storeEnergy.Do);

        IDisposable OnEnergyStored =>
            onEnergyStored
                .Do(energy => houseBatteryView.SetEnergyAvailable(energy))
                .Subscribe();

        IDisposable OnEnergyStorageFull =>
            onEnergyStorageFull.Subscribe();
        
    }
}
