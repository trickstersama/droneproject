﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Status;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;
using UnityEngine;

namespace Bonfire.Gameplay.Delivery.Presentation.Computer
{
    public class ComputerPresenter
    {
        readonly SelectDroneStatusTab selectDroneStatusTab;
        readonly SelectSendDroneTab selectSendDroneTab;
        readonly SelectDroneCardInStatus selectDroneCardInStatus;
        readonly SelectMapTab selectMapTab;
        readonly ReturnDrone returnDrone;

        readonly ComputerView computerView;
        
        readonly IObservable<IEnumerable<Drone>> onDroneStatusTabSelected;
        readonly IObservable<Unit> onOpenComputer;
        readonly IObservable<Drone> onDroneInStatusSelected;
        readonly IObservable<Drone> onDroneReturning;

        public ComputerPresenter(
            CanvasViews canvasViews,
            GameplayActions actions,
            GameplayEvents events
        ) {
            selectDroneStatusTab = actions.SelectDroneStatusTab;
            selectSendDroneTab = actions.SelectSendDroneTab;
            selectDroneCardInStatus = actions.SelectDroneCardInStatus;
            selectMapTab = actions.SelectMapTab;
            returnDrone = actions.ReturnDrone;

            computerView = canvasViews.ComputerView;

            onDroneStatusTabSelected = events.OnDroneStatusTabSelected;
            onOpenComputer = events.OnOpenComputer;
            onDroneInStatusSelected = events.OnDroneInStatusSelected;
            onDroneReturning = events.OnDroneReturning;
        }
        
        public IEnumerable<IDisposable> Disposables => 
            new[]
            {
                OnComputerOpen,
                OnSendDroneTabButtonPressed,
                OnEncountersTabButtonPressed,
                OnMapTabButtonPressed,
                OnStatusTabButtonPressed,
                StatusTabSelected,
                /*
                SelectDroneCardInStatus,
                OnDroneCardInStatusSelected,
                OnReturnButtonPressed,
                OnDroneReturning
                */
            };


        IDisposable OnComputerOpen =>
            onOpenComputer
                .Subscribe(_ => selectSendDroneTab.Do());
        
        //activate tabs
        
        IDisposable OnSendDroneTabButtonPressed =>
            computerView.OnSendDroneTabButtonPressed
                .Subscribe( _ => selectSendDroneTab.Do());
            
        IDisposable OnEncountersTabButtonPressed => //WIP
            computerView.OnEncountersTabButtonPressed
                .Subscribe(_ => Debug.Log("WIP tab"));
        
        IDisposable OnStatusTabButtonPressed =>
            computerView.OnStatusTabButtonPressed
                .Subscribe(_ => selectDroneStatusTab.Do());
        
        IDisposable OnMapTabButtonPressed =>
            computerView.OnMapTabButtonPressed
                .Subscribe(_ => selectMapTab.Do());

        // tab actions
        //later move to its own presenter

        
        IDisposable StatusTabSelected =>
            onDroneStatusTabSelected
                .Subscribe(_ => Debug.Log("WIP tab"));

        //drone status (TODO mover a otro presenter)
        /*
        IDisposable SelectDroneCardInStatus =>
            droneStatusView.OnCardSelected
                .Subscribe(selectDroneCardInStatus.Do);

        IDisposable OnDroneCardInStatusSelected =>
            onDroneInStatusSelected
                .Do(droneStatusView.SelectCard)
                .Subscribe(_ => droneStatusView.ShowReturnButton());

        IDisposable OnReturnButtonPressed =>
            droneStatusView.OnReturnButtonPressed
                .Subscribe(drone => returnDrone.Do(drone));

        IDisposable OnDroneReturning =>
            onDroneReturning
                .Subscribe(_ => selectDroneStatusTab.Do());
*/
    }
}