﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Computer

{
    public class MapPresenter
    {
        readonly SelectNewPin selectNewPin;
        readonly SelectNewPinCoordinates selectNewPinCoordinates;
        readonly ChoosePinName choosePinName;
        readonly ChoosePinType choosePinType;
        readonly SaveNewPin saveNewPin;
        readonly SelectPinnedLocationsSection selectPinnedLocationsSection;
        readonly SelectMapTab selectMapTab;
        readonly SelectPinnedLocation selectPinnedLocation;

        readonly IObservable<IEnumerable<Destination>> onMapTabSelected;
        readonly IObservable<Unit> onNewPinSelected;
        readonly IObservable<Destination> onNewPinCoordinatesSelected;
        readonly IObservable<Destination> onPinNameChosen;
        readonly IObservable<Destination> onPinTypeChosen;
        readonly IObservable<Unit> onNewPinSaved;
        readonly IObservable<IEnumerable<Destination>> onPinnedLocationsSectionSelected;
        readonly IObservable<Destination> onPinnedLocationSelected;
        readonly IObservable<Unit> onNewPinReadyToSave;
        readonly IObservable<Unit> onNewPinNotReadyToSave;

        readonly ComputerView computerView;
        readonly NewPinReadyToSave newPinReadyToSave;

        public MapPresenter(
            CanvasViews canvasViews,
            GameplayActions actions,
            GameplayEvents events,
            GameplayReactions reactions
        ) {
            selectNewPin = actions.SelectNewPin;
            selectNewPinCoordinates = actions.SelectNewPinCoordinates;
            choosePinName = actions.ChoosePinName;
            choosePinType = actions.ChoosePinType;
            saveNewPin = actions.SaveNewPin;
            selectPinnedLocationsSection = actions.SelectPinnedLocationsSection;
            selectMapTab = actions.SelectMapTab;
            selectPinnedLocation = actions.SelectPinnedLocation;

            newPinReadyToSave = reactions.NewPinReadyToSave;
            
            onMapTabSelected = events.OnMapTabSelected;
            onNewPinSelected = events.OnNewPinSelected;
            onNewPinCoordinatesSelected = events.OnNewPinCoordinatesSelected;
            onPinNameChosen = events.OnPinNameChosen;
            onPinTypeChosen = events.OnPinTypeChosen;
            onNewPinSaved = events.OnNewPinSaved;
            onPinnedLocationsSectionSelected = events.OnPinnedLocationsSectionSelected;
            onPinnedLocationSelected = events.OnPinnedLocationSelected;
            onNewPinReadyToSave = events.OnNewPinReadyToSave;
            onNewPinNotReadyToSave = events.onNewPinNotReadyToSave;

            computerView = canvasViews.ComputerView;
        }
        
        public IEnumerable<IDisposable> Disposables => 
            new[]
            {
                MapTabSelected,
                OnNewPinButtonPressed,
                OnNewPinSelected,
                OnCoordinatesSelected,
                OnNewPinCoordinatesSelected,
                OnPressPinType,
                OnPinTypeChosen,
                OnNameEntered,
                OnPinNameChosen,
                OnSaveNewPinButtonPressed,
                OnNewPinSaved,
                OnNewPinReadyToSave,
                OnNewPinNotReadyToSave,
                OnClearPinInProgressButtonPressed,

                OnPinsButtonPressed,
                OnPinnedLocationsSelected,
                
                // OnPinnedLocationItemSelected,
                // OnPinnedLocationSelected
            };

        IDisposable MapTabSelected =>
            onMapTabSelected
                .Subscribe(_ => computerView.OpenMapTab());

        //--- new pin
        IDisposable OnNewPinButtonPressed =>
            computerView.OnNewPinButtonPressed
                .Subscribe(_ => selectNewPin.Do());
        
        IDisposable OnNewPinSelected =>
            onNewPinSelected
                .Subscribe(_ => computerView.OpenNewPin());
        
         IDisposable OnCoordinatesSelected =>
             computerView.OnNewPinCoordinatesSelected
                 .Subscribe(selectNewPinCoordinates.Do);
         
         IDisposable OnNewPinCoordinatesSelected =>
             onNewPinCoordinatesSelected
                 .Do(newPinReadyToSave.Do)
                 .Subscribe(computerView.UpdateDestinationOverview);
        
         IDisposable OnPressPinType =>
             computerView.OnNewPinTypePressed
                 .Subscribe(choosePinType.Do);
         
         IDisposable OnPinTypeChosen =>
             onPinTypeChosen
                 .Do(newPinReadyToSave.Do)
                 .Subscribe(computerView.UpdateDestinationOverview);

         IDisposable OnNameEntered =>
             computerView.OnNewPinNameEntered
                 .Subscribe(choosePinName.Do);
         
         IDisposable OnPinNameChosen =>
             onPinNameChosen
                 .Do(newPinReadyToSave.Do)
                 .Subscribe(computerView.UpdateDestinationOverview);
         
         IDisposable OnSaveNewPinButtonPressed =>
             computerView.OnSaveNewPinButtonPressed
                 .Subscribe(_ => saveNewPin.Do());
         
         IDisposable OnNewPinSaved =>
             onNewPinSaved
                 .Subscribe(_ => selectMapTab.Do());

         IDisposable OnNewPinReadyToSave =>
             onNewPinReadyToSave
                 .Subscribe(_ => computerView.NewPinIsReadyToSave());    
         
         IDisposable OnNewPinNotReadyToSave =>
             onNewPinNotReadyToSave
                 .Subscribe(_ => computerView.NewPinIsNotReadyToSave());

         IDisposable OnClearPinInProgressButtonPressed =>
             computerView.OnClearPinInProgressButtonPressed
                 .Subscribe();
             
        //--- pins
        
        IDisposable OnPinsButtonPressed =>
            computerView.OnPinsButtonPressed
                .Subscribe(_ => selectPinnedLocationsSection.Do());
        
        IDisposable OnPinnedLocationsSelected =>
            onPinnedLocationsSectionSelected
                .Subscribe(computerView.OpenPins);
        
        /// /////

        // IDisposable OnPinnedLocationItemSelected =>
        //     mapSectionView.OnPinnedLocationItemSelected
        //         .Subscribe(selectPinnedLocation.Do);
        //
        // IDisposable OnPinnedLocationSelected =>
        //     onPinnedLocationSelected
        //         //.Do(destination => mapViewOld.ZoomTo(destination))
        //         .Subscribe(destination => mapSectionView.UpdateCoordinatesOverview(destination.mapCoordinates));
    }
}