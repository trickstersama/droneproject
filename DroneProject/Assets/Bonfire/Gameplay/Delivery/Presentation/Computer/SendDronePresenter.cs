﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Computer

{
    public class SendDronePresenter
    {
        readonly SendDrone sendDrone;
        readonly SelectMissionDrone selectMissionDrone;
        readonly OpenMap openMap;
        readonly SelectDestination selectDestination;
        readonly SelectMissionType selectMissionType;
        readonly SelectSendDroneTab selectSendDroneTab;

        readonly ComputerView computerView;

        readonly IObservable<IEnumerable<Drone>> onSendDroneTabSelected;
        readonly IObservable<IEnumerable<Destination>> onMapOpened;
        readonly IObservable<MissionOrders> onMissionDroneSelected;
        readonly IObservable<MissionOrders> onDestinationSelected;
        readonly IObservable<MissionOrders> onMissionTypeSelected;
        readonly IObservable<Unit> onDroneSent;
        readonly IObservable<Unit> onMissionServicesReadyToCalculate;
        readonly IObservable<MissionOrders> onMissionTimeCostsCalculated;
        readonly IObservable<MissionOrders> onMissionEnergyCostsCalculated;
        readonly IObservable<MissionOrders> onMissionMaxWeightCalculated;

        readonly MissionServicesReadyToCalculate missionServicesReadyToCalculate;
        readonly CalculateMissionEnergyCosts calculateMissionEnergyCosts;
        readonly CalculateMissionTimeCosts calculateMissionTimeCosts;
        readonly CalculateMissionMaxWeight calculateMissionMaxWeight;


        public SendDronePresenter(
            CanvasViews canvasViews,
            GameplayActions actions,
            GameplayEvents events,
            GameplayReactions reactions
        )
        {
            sendDrone = actions.SendDrone;
            selectMissionDrone = actions.SelectMissionDrone;
            openMap = actions.OpenMap;
            selectDestination = actions.SelectDestination;
            selectMissionType = actions.SelectMissionType;
            selectSendDroneTab = actions.SelectSendDroneTab;

            computerView = canvasViews.ComputerView;

            onSendDroneTabSelected = events.OnSendDroneTabSelected;
            onMapOpened = events.OnMapOpened;
            onMissionDroneSelected = events.OnMissionDroneSelected;
            onDestinationSelected = events.OnDestinationSelected;
            onMissionTypeSelected = events.OnMissionTypeSelected;
            onDroneSent = events.OnDroneSent;
            onMissionServicesReadyToCalculate = events.OnMissionServicesReadyToCalculate;
            onMissionTimeCostsCalculated = events.OnMissionTimeCostsCalculated;
            onMissionEnergyCostsCalculated = events.OnEnergyCostsCalculated;
            onMissionMaxWeightCalculated = events.OnMissionMaxWeightAllowed;

            missionServicesReadyToCalculate = reactions.MissionServicesReadyToCalculate;
            calculateMissionTimeCosts = reactions.CalculateMissionTimeCosts;
            calculateMissionEnergyCosts = reactions.CalculateMissionEnergyCosts;
            calculateMissionMaxWeight = reactions.CalculateMissionMaxWeight;
        }

        public IEnumerable<IDisposable> Disposables =>
            new[]
            {
                OnSendDroneTabSelected,
                OnSendDroneCardSelected,
                OnMissionDroneSelected,
                OnSelectDestinationButtonPressed,
                OnMapOpened,
                OnSendDroneMapCoordinatesSelected,
                OnMissionDestinationSelected,
                OnMissionServicesReadyToCalculate,
                OnMissionTimeCostsCalculated,
                OnMissionEnergyCostsCalculated,
                OnMissionServicesReady,
                OnMissionTypeButtonPressed,
                OnMissionTypeSelected,
                OnDeployButtonPressed,
                OnDroneSent
            };

        IDisposable OnSendDroneTabSelected =>
            onSendDroneTabSelected
                .Do(_ => CloseAllTabs())
                .Subscribe(computerView.OpenSendDroneSection);

        IDisposable OnSendDroneCardSelected =>
            computerView.OnCardCardSelected
                .Do(_ => missionServicesReadyToCalculate.Do())
                .Subscribe(selectMissionDrone.Do);

        IDisposable OnMissionDroneSelected =>
            onMissionDroneSelected
                .Do(_ => computerView.SendDroneCardSelected())
                .Subscribe(_ => missionServicesReadyToCalculate.Do());

        IDisposable OnSelectDestinationButtonPressed =>
            computerView.OnSelectDestinationButtonPressed
                .Subscribe(_ => openMap.Do());

        IDisposable OnMapOpened =>
            onMapOpened
                .Subscribe(computerView.OpenMapUI);

        IDisposable OnSendDroneMapCoordinatesSelected =>
            computerView.OnSendDroneMapCoordinatesSelected
                .Subscribe(selectDestination.Do);

        IDisposable OnMissionDestinationSelected =>
            onDestinationSelected
                .Do(_ => computerView.CoordinatesSelected())
                .Subscribe(_ => missionServicesReadyToCalculate.Do());

        IDisposable OnMissionServicesReadyToCalculate =>
            onMissionServicesReadyToCalculate
                .Subscribe(_ => calculateMissionTimeCosts.Do());

        IDisposable OnMissionTimeCostsCalculated =>
            onMissionTimeCostsCalculated
                .Subscribe(_ => calculateMissionEnergyCosts.Do());

        IDisposable OnMissionEnergyCostsCalculated =>
            onMissionEnergyCostsCalculated
                .Subscribe(_ => calculateMissionMaxWeight.Do());

        IDisposable OnMissionServicesReady =>
            onMissionMaxWeightCalculated
                .Do(_ => computerView.EnableDeploy())
                .Subscribe(computerView.UpdateMissionOverview);
        
        IDisposable OnMissionTypeButtonPressed =>
            computerView.OnMissionTypeButtonPressed
                .Subscribe(selectMissionType.Do);
        
        IDisposable OnMissionTypeSelected =>
            onMissionTypeSelected
                .Subscribe(_ => missionServicesReadyToCalculate.Do());
        IDisposable OnDeployButtonPressed =>
            computerView.OnDeployButtonPressed
                .Subscribe(_ => sendDrone.Do());
        
        IDisposable OnDroneSent =>
            onDroneSent
                .Subscribe(_ => selectSendDroneTab.Do());


        void CloseAllTabs()
        {
            computerView.CloseAllTabs();
        }
    }
}