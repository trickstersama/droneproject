﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Workstation

{
    public class WorkstationDronesPresenter
    {
        
        readonly IObservable<IEnumerable<Drone>> onWorkstationDronesTabSelected;
        readonly IObservable<Unit> onRepairNeeded;
        readonly IObservable<Unit> onRechargeNeeded;
        readonly IObservable<Drone> onDroneRecharged;
        readonly IObservable<Drone> onDroneRepaired;
        readonly IObservable<Unit> onDroneDisassembled;
        
        readonly SelectWorkstationDrone selectWorkstationDrone;
        readonly RechargeDrone rechargeDrone;
        readonly RepairDrone repairDrone;
        readonly DisassemblyDrone disassemblyDrone;

        readonly WorkstationView workstationView;
        readonly SelectWorkstationDronesTab selectWorkstationDronesTab;

        public WorkstationDronesPresenter(
            GameplayActions actions,
            GameplayEvents events,
            CanvasViews views
        ) {
            onWorkstationDronesTabSelected = events.OnWorkstationDronesTabSelected;
            onRepairNeeded = events.OnRepairNeeded;
            onRechargeNeeded = events.OnRechargeNeeded;
            onDroneRecharged = events.OnDroneRecharged;
            onDroneRepaired = events.OnDroneRepaired;
            onDroneDisassembled = events.OnDroneDisassembled;
            
            selectWorkstationDrone = actions.SelectWorkstationDrone;
            selectWorkstationDronesTab = actions.SelectWorkstationDronesTab;
            rechargeDrone = actions.RechargeDrone;
            repairDrone = actions.RepairDrone;
            disassemblyDrone = actions.DisassemblyDrone;
            
            workstationView = views.WorkstationView;

        }

        public IEnumerable<IDisposable> Disposables =>
            new[]
            {
                
                OnWorkstationDronesTabSelected,
                OnDroneCardSelected,
                OnRepairNeeded,
                OnRechargeNeeded,
                OnRechargeButtonPressed,
                OnRepairButtonPressed,
                OnDisassembleButtonPressed,
                OnDroneDisassembled,
                OnDroneRecharged,
                OnDroneRepaired,
            };

        IDisposable OnWorkstationDronesTabSelected =>
            onWorkstationDronesTabSelected
            .Subscribe(workstationView.SelectDronesTab);

        IDisposable OnDroneCardSelected =>
            workstationView.OnItemSelected
                .Subscribe(selectWorkstationDrone.Do);

        IDisposable OnRepairNeeded =>
            onRepairNeeded
                .Subscribe(_ => workstationView.ActivateRepair());
        
        IDisposable OnRechargeNeeded =>
            onRechargeNeeded
                .Subscribe(_ => workstationView.ActivateRecharge());

        IDisposable OnRechargeButtonPressed =>
            workstationView.OnRechargeButtonPressed
                .Subscribe(_ => rechargeDrone.Do());
        IDisposable OnRepairButtonPressed =>
            workstationView.OnRepairButtonPressed
                .Subscribe(_ => repairDrone.Do());
        
        IDisposable OnDisassembleButtonPressed =>
            workstationView.OnDisassemblyButtonPressed
                .Subscribe(_ => disassemblyDrone.Do());
        
        IDisposable OnDroneDisassembled =>
            onDroneDisassembled
                .Subscribe(_ => selectWorkstationDronesTab.Do());

        IDisposable OnDroneRecharged =>
            onDroneRecharged
                .Subscribe(workstationView.UpdateSelectedDrone);
        
        IDisposable OnDroneRepaired =>
            onDroneRepaired
                .Subscribe(workstationView.UpdateSelectedDrone);
    }
}