﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Workstation

{
    public class WorkstationPresenter
    {
        readonly IObservable<Unit> onOpenWorkstation;
        readonly IObservable<IEnumerable<Module>> onModuleTypeToDisplaySelected;
        readonly IObservable<IEnumerable<Frame>> onFrameTabSelected;
        readonly IObservable<IEnumerable<Module>> onModulesTabOpened;


        readonly SelectModulesTab selectModulesTab;
        readonly SelectWorkstationDronesTab selectWorkstationDronesTab;

        readonly SelectModuleTypeToDisplay selectModuleTypeToDisplay;
        readonly SelectFramesTab selectFramesTab;
        readonly WorkstationView workstationView;


        public WorkstationPresenter(
            GameplayEvents events,
            GameplayActions actions,
            CanvasViews canvasViews
        ) {
            onOpenWorkstation = events.OnOpenWorkstation;
            onModuleTypeToDisplaySelected = events.OnModuleTypeToDisplaySelected;
            onFrameTabSelected = events.OnFramesTabSelected;
            onModulesTabOpened = events.OnModulesTabOpened;

            selectModulesTab = actions.SelectModulesTab;
            selectModuleTypeToDisplay = actions.SelectModuleTypeToDisplay;
            selectFramesTab = actions.SelectFramesTab;
            selectWorkstationDronesTab = actions.SelectWorkstationDronesTab;

            workstationView = canvasViews.WorkstationView;
        }

        //default
        
        public IEnumerable<IDisposable> Disposables =>
            new[]
            {
                OnWorkstationOpened,
                
                OnModulesTabButtonPressed,
                OnFramesTabButtonPressed,
                OnDronesTabButtonPressed,
                //falta selectModulesTab
                
                OnModulesTabOpened,
                OnModuleTypeButtonPressed,
                OnModuleTypeToDisplaySelected,
                
                OnFrameTabOpened,
            };

        IDisposable OnWorkstationOpened =>
            onOpenWorkstation
                .Subscribe(_ => selectWorkstationDronesTab.Do());
        
        IDisposable OnModulesTabButtonPressed =>
            workstationView.OnModulesTabButtonPressed
                .Subscribe(_ => selectModulesTab.Do());
        
        IDisposable OnDronesTabButtonPressed =>
            workstationView.OnDronesTabButtonPressed
                .Subscribe(_ => selectWorkstationDronesTab.Do());
        
        IDisposable OnFramesTabButtonPressed =>
            workstationView.OnFramesTabButtonPressed
                .Subscribe(_ => selectFramesTab.Do());
        
        //----------------------Modules section

        IDisposable OnModulesTabOpened =>
            onModulesTabOpened.Subscribe(workstationView.SelectModulesTab);
        
        IDisposable OnModuleTypeButtonPressed =>
            workstationView.OnModuleTypeButtonPressed
                .Subscribe(selectModuleTypeToDisplay.Do);
        
        IDisposable OnModuleTypeToDisplaySelected =>
            onModuleTypeToDisplaySelected
                .Subscribe(workstationView.UpdateModuleItems);
        
        
        //Frames section
        IDisposable OnFrameTabOpened =>
            onFrameTabSelected
                .Subscribe(workstationView.SelectFramesTab);

    }
}