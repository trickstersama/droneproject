﻿using System;
using System.Collections.Generic;
using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Delivery.Presentation.Workstation

{
    public class AssemblyPresenter
    {
        readonly WorkstationView workstationView;
        readonly PopUpsView popUpsView;

        readonly IObservable<IEnumerable<Frame>> onAssemblyTabSelected;
        readonly IObservable<Frame> onFrameSelected;
        readonly IObservable<Frame> onAssemblyFrameConfirmed;
        readonly IObservable<IEnumerable<Module>> onModulesInPanelsLoaded;
        readonly IObservable<Frame> onAssemblyModuleSlotsUpdated;
        readonly IObservable<DroneModifiers> onAssemblyModuleInstalled;
        readonly IObservable<DroneModifiers> onDroneInProgressUpdated;
        readonly IObservable<Drone> onDroneBuilt;
        readonly IObservable<Unit> onDroneNamed;
        readonly IObservable<Unit> onDroneNameInvalid;
        readonly IObservable<Unit> onAssemblyModuleUninstalled;

        //actions
        readonly SelectAssemblyTab selectAssemblyTab;
        readonly SelectFrame selectFrame;
        readonly ConfirmAssemblyFrame confirmAssemblyFrame;
        readonly InstallAssemblyModule installAssemblyModule;
        readonly NameDrone nameDrone;
        readonly UninstallAssemblyModule uninstallAssemblyModule;


        //reactions
        readonly UpdateDroneInProgress updateDroneInProgress;
        readonly LoadModulesInPanels loadModulesInPanels;
        readonly RemoveModulesFromInventory removeModulesFromInventory;
        readonly RemoveFrameFromInventory removeFrameFromInventory;
        readonly AddDroneToInventory addDroneToInventory;
        readonly BuildDrone buildDrone;

        public AssemblyPresenter(
            GameplayEvents events,
            GameplayReactions reactions, 
            GameplayActions actions,
            CanvasViews canvasViews
        ) {
            workstationView = canvasViews.WorkstationView;

            selectAssemblyTab = actions.SelectAssemblyTab;
            selectFrame = actions.SelectFrame;
            confirmAssemblyFrame = actions.ConfirmAssemblyFrame;
            installAssemblyModule = actions.InstallAssemblyModule;
            nameDrone = actions.NameDrone;
            uninstallAssemblyModule = actions.UninstallAssemblyModule;
            
            updateDroneInProgress = reactions.UpdateDroneInProgress;
            loadModulesInPanels = reactions.LoadModulesInPanels;
            removeModulesFromInventory = reactions.RemoveModulesFromInventory;
            removeFrameFromInventory = reactions.RemoveFrameFromInventory;
            addDroneToInventory = reactions.AddDroneToInventory;
            buildDrone = reactions.BuildDrone;

            onAssemblyTabSelected = events.OnAssemblyTabSelected;
            onFrameSelected = events.OnFrameSelected;
            onAssemblyFrameConfirmed = events.OnAssemblyFrameConfirmed;
            onModulesInPanelsLoaded = events.OnModulesInPanelsLoaded;
            onAssemblyModuleSlotsUpdated = events.OnAssemblyModuleSlotsUpdated;
            onAssemblyModuleInstalled = events.OnAssemblyModuleInstalled;
            onDroneInProgressUpdated = events.OnDroneInProgressUpdated;
            onDroneBuilt = events.OnDroneBuilt;
            onDroneNamed = events.OnDroneNamed;
            onDroneNameInvalid = events.OnDroneNameInvalid;
            onAssemblyModuleUninstalled = events.OnAssemblyModuleUninstalled;

            popUpsView = canvasViews.PopUpsView;
        }
        
        public IEnumerable<IDisposable> Disposables =>
            new[]
            {
                SelectAssemblyTab,
                AssemblyTabSelected,
                OnFrameItemSelected,
                OnFrameSelected,
                OnFrameConfirm,
                OnAssemblyFrameConfirmed,
                OnClearFrameSelection,
                OnModulesInPanelsLoaded,
                OnInstallAssemblyModule,
                OnAssemblyModuleSlotsUpdated,
                OnAssemblyModuleSelected,
                OnDroneInProgressUpdated,
                OnBuildDroneButtonPressed,
                OnDroneBuilt,
                OnNameDronePressed,
                OnDroneNamed,
                OnDroneNameInvalid,
                OnDroneClaimButtonPressed,
                OnUninstallModulePressed,
                OnAssemblyModuleUninstalled
            };
        
        IDisposable SelectAssemblyTab =>
            workstationView.OnAssemblyTabButtonPressed
                .Subscribe( _ => selectAssemblyTab.Do());

        IDisposable AssemblyTabSelected =>
            onAssemblyTabSelected
                .Subscribe(workstationView.SelectAssemblyTab);

        IDisposable OnFrameItemSelected =>
            workstationView.OnFrameItemSelected
                .Subscribe(selectFrame.Do);
        
        IDisposable OnFrameSelected =>
            onFrameSelected
                .Subscribe(workstationView.ShowStats);

        IDisposable OnFrameConfirm => 
            workstationView.OnConfirmFrame
                .Subscribe(_ => confirmAssemblyFrame.Do());
        
        IDisposable OnAssemblyFrameConfirmed =>
            onAssemblyFrameConfirmed
                .Do(workstationView.SetPanelsSlotsAmount)
                .Do(_ => updateDroneInProgress.Do())
                .Subscribe(_ => loadModulesInPanels.Do());

        IDisposable OnDroneInProgressUpdated =>
            onDroneInProgressUpdated
                .Subscribe(workstationView.UpdateDroneStats);

        IDisposable OnModulesInPanelsLoaded =>
            onModulesInPanelsLoaded
                .Subscribe(workstationView.UpdateAssemblyModules);

        IDisposable OnClearFrameSelection =>
            workstationView.OnClearFrameSelection
                .Subscribe(_ => selectAssemblyTab.Do());

        IDisposable OnInstallAssemblyModule =>
            workstationView.OnInstallAssemblyModule
                .Subscribe(installAssemblyModule.Do);

        IDisposable OnAssemblyModuleSlotsUpdated =>
            onAssemblyModuleSlotsUpdated
                .Subscribe(workstationView.SetPanelsSlotsAmount);
        
        IDisposable OnAssemblyModuleSelected =>
            onAssemblyModuleInstalled
                .Do(_ => workstationView.RefreshModulePanelSelection())
                .Subscribe(_ => updateDroneInProgress.Do());
        
        IDisposable OnBuildDroneButtonPressed =>
            workstationView.OnBuildDronePressed
                .Subscribe(_ => popUpsView.ShowDronePopUp());

        IDisposable OnUninstallModulePressed =>
            workstationView.OnUninstallModulePressed
                .Subscribe(uninstallAssemblyModule.Do);
        
         
        IDisposable OnAssemblyModuleUninstalled =>
            onAssemblyModuleUninstalled
                .Do(_ => workstationView.RefreshModulePanelSelection())
                .Subscribe(_ => updateDroneInProgress.Do());
        /*
         * deprecated behaviour


    IDisposable OnModulePanelSelectionCleared =>
            onModulePanelSelectionCleared
                .Subscribe(assemblySectionView.UpdateDroneStats);
                
                
        IDisposable OnPanelSlotsAmountReset =>
            onPanelSlotsAmountReset
                .Subscribe(assemblySectionView.SetPanelsSlotsAmount);


*/    

        IDisposable OnNameDronePressed =>
            popUpsView.OnNameDronePressed
                .Subscribe(nameDrone.Do);

        IDisposable OnDroneNameInvalid =>
            onDroneNameInvalid
                .Subscribe(_ => popUpsView.ShowInvalidNameWarning());

        IDisposable OnDroneNamed =>
            onDroneNamed
                .Subscribe(_ => popUpsView.ShowClaimButton());  
        
        IDisposable OnDroneClaimButtonPressed =>
            popUpsView.OnDroneClaimButtonPressed
                .Subscribe(_ => buildDrone.Do());

        IDisposable OnDroneBuilt =>
            onDroneBuilt
                .Do(_ => popUpsView.ClosePopUps())
                .Do(drone => removeModulesFromInventory.Do(drone.modules))
                .Do(drone => removeFrameFromInventory.Do(drone.frame))
                .Do(addDroneToInventory.Do)
                .Subscribe(_ => selectAssemblyTab.Do());
    }
}