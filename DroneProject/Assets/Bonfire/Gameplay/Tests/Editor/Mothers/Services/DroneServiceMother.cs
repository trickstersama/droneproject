﻿using System.Linq;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Services
{
    public static class DroneServiceMother
    {
        public static DroneService ADroneService(bool withDroneFullyCharged = true) 
        {
            var droneService = Substitute.For<DroneService>();
            droneService.IsDroneDischarged(Arg.Any<Drone>())
                .Returns(withDroneFullyCharged);
            droneService.RechargeDrone(Arg.Any<Drone>())
                .Returns(new Drone());
            droneService.GetModulesFromDrone(Arg.Any<Drone>())
                .Returns(Enumerable.Empty<Module>());
            droneService.GetFrameFromDrone(Arg.Any<Drone>())
                .Returns(new Frame());
            return droneService;
        }

        public static DroneService ALocalDroneService() => 
            new LocalDroneService();
    }
}