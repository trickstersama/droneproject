﻿using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Services;
using NSubstitute;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Services
{
    public static class FrameSlotsAmountServiceMother{
        public static FrameSlotsAmountService AFrameSlotsAmountService()
        {
            var service = Substitute.For<FrameSlotsAmountService>();
            return service;
        }

        public static FrameSlotsAmountService ALocalFrameSlotsAmountService() => 
            new LocalFrameSlotsAmountService();
    }
}