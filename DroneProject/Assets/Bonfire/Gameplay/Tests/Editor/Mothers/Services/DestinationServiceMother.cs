﻿using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Services
{
    public static class DestinationServiceMother
    {
        public static DestinationService ADestinationService(bool? withValidDestination = null)
        {
            var service = Substitute.For<DestinationService>();
            service.CreateDefaultDestination().Returns(new Destination());
            service.AddMissionType( Arg.Any<Destination>(),Arg.Any<MissionType>())
                .Returns(Observable.Return(new Destination()));
            service.AddCoordinates( Arg.Any<Destination>(),Arg.Any<MapCoordinates>())
                .Returns(Observable.Return(new Destination()));
            service.AddName( Arg.Any<Destination>(),Arg.Any<string>())
                .Returns(Observable.Return(new Destination()));
            service.IsValid( Arg.Any<Destination>())
                .Returns(withValidDestination ?? true);

            return service;
        }
        
    }
}