﻿using Bonfire.Gameplay.Domain.Configuration;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Services
{
    public static class MissionServiceMother
    {
        public static MissionService AMissionService(float withTotalAcceleration = 0f)
        {
            var service = Substitute.For<MissionService>();
            service.HorizontalAcceleration(Arg.Any<Drone>())
                .Returns(withTotalAcceleration);
            service.CalculateEnergyCosts(Arg.Any<MissionOrders>(), Arg.Any<Drone>())
                .Returns(new MissionEnergyCost()); 
            service.CalculateTimeCost(Arg.Any<MissionOrders>(), Arg.Any<MissionsModifiers>(), Arg.Any<Drone>())
                .Returns(new MissionTimeCost());
            service.UpdateTime(Arg.Any<Mission>(), Arg.Any<float>())
                .Returns(new Mission());
            service.TimeNeededToReturn(Arg.Any<Mission>())
                .Returns(1f);
            service.TimeNeededToTarget(Arg.Any<Mission>())
                .Returns(1f);
            return service;
        }

        public static MissionService ALocalMissionService(MissionConfiguration? missionConfiguration = null)
        {
            return new LocalMissionService( 
                missionConfiguration ??
                new MissionConfiguration(5,5,5)
            );
        }
    }
}