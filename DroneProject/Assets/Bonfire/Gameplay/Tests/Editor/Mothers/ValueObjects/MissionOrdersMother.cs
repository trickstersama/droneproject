﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionOrdersMother
    {
        public static MissionOrders AMissionOrders(
            MissionType? withMissionType = null,
            MapCoordinates? withMapCoordinates = null,
            string withDroneName = null,
            MissionEnergyCost? withMissionEnergyCost = null,
            MissionTimeCost? withTimeCost = null,
            float? withMaxWeightAllowed = null
        ) {
            return new MissionOrders(
                withMissionType ?? MissionType.Battle, 
                withMapCoordinates ?? new MapCoordinates(),
                withDroneName ?? String.Empty, 
                withMissionEnergyCost ?? new MissionEnergyCost(),
                withTimeCost ?? new MissionTimeCost(),
                withMaxWeightAllowed ?? 0f
            );
        }
    }
}