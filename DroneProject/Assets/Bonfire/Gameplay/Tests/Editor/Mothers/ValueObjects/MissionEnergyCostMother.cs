﻿using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionEnergyCostMother
    {
        public static MissionEnergyCost AMissionEnergyCost(
            float withEnergyToTarget  = 0,
            float TBD = 0,
            float withEnergyToReturnWithMaxLoad = 0
        ) =>
            new MissionEnergyCost(
                withEnergyToTarget,
                TBD,
                withEnergyToReturnWithMaxLoad
            );
    }
}