﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class DroneMother
    {
        public static Drone ADrone(
            float withCharge = 100, 
            string withName = null,
            DroneStatus? withStatus = null,
            Frame? withFrame = null,
            DroneModifiers? withModifiers = null,
            IEnumerable<Module> withModules = null,
            int withDamageReceived = 0
        ) =>
            new Drone
            {
                droneName = withName ?? string.Empty,
                status = withStatus ?? DroneStatus.Idle,
                frame = withFrame ?? AFrame(),
                modifiers = withModifiers ?? ADroneModifiers(),
                modules = withModules?.ToList() ?? new List<Module>(),
                charge = withCharge,
                damageReceived = withDamageReceived
            };

        public static Drone AFullyArmedDrone(string withName = null)
        {
            return ADrone(
                withName: withName,
                withStatus: DroneStatus.Busy, 
                withFrame: AFrame(
                    withDescription: "For Testing", 
                    withStealth: 23, 
                    withArmorSlotsAmount: 3,
                    withWeaponSlotsAmount: 3
                ),
                withModifiers: ADroneModifiers(
                    withDamage: 23,
                    withHardness: 24,
                    withWeight: 90
                ), 
                withModules: new List<Module>
                {
                    AnArmorModule(withName: "Testing1"), 
                    AWeaponModule(withName: "Testing2")
                });
        }

        public static Drone ADroneWithModifiersFromParts(
            float withCharge = 100, 
            string withName = null,
            DroneStatus withStatus = DroneStatus.Idle,
            Frame? withFrame = null,
            IEnumerable<Module> withModules = null,
            int withDamageReceived = 0
        ) {
            var frameModifiers = withFrame?.GetModifiers();

            var modifiers = (withModules ?? Enumerable.Empty<Module>())
                .Select(module => module.GetDroneModifiers())
                .Aggregate(frameModifiers, (previous, next) => previous + next);
            
            return new Drone
            {
                charge =  withCharge,
                droneName = withName ?? string.Empty,
                status =  withStatus,
                frame = withFrame ?? new Frame(),
                modules = withModules?.ToList() ?? new List<Module>(),
                modifiers =  modifiers ?? new DroneModifiers(),
                damageReceived =  withDamageReceived
            };
        }
    }
}