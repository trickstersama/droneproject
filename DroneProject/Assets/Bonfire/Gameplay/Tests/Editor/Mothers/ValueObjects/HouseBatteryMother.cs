﻿using Bonfire.Gameplay.Domain.ValueObjects.House;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class HouseBatteryMother
    {
        public static HouseBattery AHouseBattery(
            string withDescription = null,
            string withName = null,
            float withStorageSize = 0
        ) =>
            new HouseBattery
            {
                description = withDescription ?? string.Empty,
                name = withName ?? string.Empty,
                storageSize = withStorageSize
            };
    }
}