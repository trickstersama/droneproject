﻿using Bonfire.TestSupport.Editor;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class EventMother
    {
        public static ISubject<T> AnEvent<T>() => new TestEvent<T>();
    }
}