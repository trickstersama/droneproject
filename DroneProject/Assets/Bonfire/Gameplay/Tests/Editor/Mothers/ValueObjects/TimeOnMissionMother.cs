﻿using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class TimeOnMissionMother
    {
        public static TimeOnMission ATimeOnMission(float? timePassed = null) => 
            new TimeOnMission(timePassed ?? 0);
    }
}