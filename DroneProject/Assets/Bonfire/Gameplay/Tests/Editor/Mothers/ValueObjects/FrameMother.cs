﻿using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class FrameMother
    {
        public static Frame AFrame(
            string withDescription = null,
            string withName = null,
            string frameModel = null,
            
            int withWeaponSlotsAmount = 0,
            int withBatterySlotsAmount = 0,
            int withSensorSlotsAmount = 0,
            int withPropulsionSlotsAmount = 0,
            int withCpuSlotsAmount = 0,
            int withArmorSlotsAmount = 0,
            int withStorageSlotsAmount = 0,
            
            int withHardness = 0,
            int withWeight = 0,
            int withStealth = 0,
            int withHitPoints = 0,
            float withAerodynamics = 0,
            float withCrossSectionArea = 0
        ) =>
            new Frame(
                description: withDescription,
                name: withName,
                frameModel: frameModel,
                
                
                weaponSlotsAmount: withWeaponSlotsAmount,
                batterySlotsAmount: withBatterySlotsAmount,
                sensorSlotsAmount: withSensorSlotsAmount,
                propulsionSlotsAmount: withPropulsionSlotsAmount,
                cpuSlotsAmount: withCpuSlotsAmount,
                armorSlotsAmount: withArmorSlotsAmount,
                storageUnitSlotsAmount: withStorageSlotsAmount,
                
                hardness: withHardness,
                weight: withWeight,
                stealth: withStealth,
                hitPoints: withHitPoints,
                aerodynamics: withAerodynamics,
                crossSectionArea: withCrossSectionArea
                
            );

        public static Frame AFullFrame() =>
            new Frame(
                name: "Full Frame",
                description: "Frame with all fields with data",
                frameModel: "Testing frame",
                
                
                weaponSlotsAmount: 1,
                batterySlotsAmount: 2,
                sensorSlotsAmount: 3,
                propulsionSlotsAmount: 1,
                cpuSlotsAmount: 2,
                armorSlotsAmount: 3,
                storageUnitSlotsAmount: 4,
                
                hardness: 14,
                weight: 14,
                stealth: 14,
                hitPoints: 14,
                aerodynamics: 100,
                crossSectionArea: 0.1f
            );
    }
}