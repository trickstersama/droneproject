﻿using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class ModuleMother
    {
        public static Module AWeaponModule(
            string withName = null,
            string withDescription = null,
            int withDamage = 0,
            WeaponType withWeaponType = WeaponType.Ballistic,
            int withWeight = 0
        ) =>
            new WeaponModule
            {
                name = withName ?? "Weapon",
                description = withDescription ?? "Weapon Module",
                moduleType = ModuleType.Weapon,
                damage = withDamage,
                weaponType = withWeaponType,
                weight = withWeight
            };

        public static Module AnArmorModule(
            string withName = null,
            string withDescription = null,
            int withWeight = 0,
            int withHardness = 0,
            int withStealth = 0,
            int withHitPoints = 0
        ) =>
            new ArmorModule
            {
                name = withName ?? "Armor",
                moduleType = ModuleType.Armor,
                description = withDescription ?? "Armor module",
                weight = withWeight,
                hardness = withHardness,
                stealth = withStealth,
                hitPoints = withHitPoints
            };

        public static Module ASensorModule(
            string withName = null,
            string withDescription = null,
            int withDamage = 0,
            int withWeight = 0,
            int withStealth = 0
        ) =>
            new SensorModule
            {
                name = withName ?? "Sensor",
                description = withDescription ?? "Sensor module",
                moduleType = ModuleType.Sensor,
                damage = withDamage,
                weight = withWeight,
                stealth = withStealth
            };

        public static Module ABatteryModule(
            string withName = null,
            string withDescription = null,
            int withWeight = 0,
            int withPowerCapacity = 0
        ) =>
            new BatteryModule
            {
                name = withName ?? "Battery",
                description = withDescription ?? "Battery module",
                moduleType = ModuleType.Battery,
                weight = withWeight,
                powerCapacity = withPowerCapacity
            };

        public static Module AStorageModule(
            string withName = null,
            string withDescription = null,
            int withReservedWeight = 0,
            StorageType withStorageType = StorageType.Solid
        ) => 
            new StorageModule
            {
                name = withName ?? "Storage",
                description = withDescription ?? "Storage module",
                moduleType = ModuleType.Storage,
                reservedWeight = withReservedWeight,
                storageType = withStorageType
            };
        
        public static Module APropulsionModule(
            string withName = null,
            string withDescription = null,
            int withThrust = 0,
            int withStealth = 0,
            int withConsumption = 0,
            PropulsionModule.EngineTechnology withEngineTech = null
        ) => 
            new PropulsionModule
            {
                name = withName ?? "Propulsion",
                description = withDescription ?? "Propulsion module",
                thrust = withThrust,
                stealth = withStealth,
                consumption = withConsumption,
                moduleType = ModuleType.Propulsion,
                engineTechnology = withEngineTech ?? new PropulsionModule.Multirotor()
            };

        public static Module ACPUModule(
            string withName = null,
            string withDescription = null,
            int withDamage = 0,
            int withStealth = 0,
            int withConsumption = 0
        ) =>
            new CPUModule
            {
                name = withName ?? "CPU",
                description = withDescription ?? "CPU module",
                moduleType = ModuleType.CPU,
                stealth = withStealth,
                damage = withDamage,
                consumption = withConsumption
            };

    }
}