﻿using Bonfire.Gameplay.Domain.ValueObjects.House;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class EnergyGeneratorMother
    {
        public static SolarGenerator ASolarEnergyGenerator(float withEnergyPerSecond = 0) 
            => new SolarGenerator
            {
                EnergyPerSecond = withEnergyPerSecond
            };
        public static WindGenerator AWindEnergyGenerator(float withEnergyPerSecond = 0) 
            => new WindGenerator
            {
                EnergyPerSecond = withEnergyPerSecond
            };
        public static EnergyReactor AReactorEnergyGenerator(float withEnergyPerSecond = 0) 
            => new EnergyReactor
            {
                EnergyPerSecond = withEnergyPerSecond
            };
    }


    
}