﻿using System;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionResultsMother
    {
        public static MissionResults AMissionResult(
            string withDroneName = null,
            float? withDroneDamage = null,
            MissionType? withMissionType = null,
            TimeOnMission? withTimeOnMission = null
        ) {
            return new MissionResults(
                droneName: withDroneName ?? String.Empty,
                damageReceived: withDroneDamage ?? 0,
                missionType: withMissionType ?? MissionType.Debug,
                timeOnMission: withTimeOnMission ?? ATimeOnMission()
            );
        }
    }
}