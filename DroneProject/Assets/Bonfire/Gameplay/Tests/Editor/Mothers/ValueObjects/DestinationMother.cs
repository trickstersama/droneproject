﻿using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class DestinationMother
    {
        public static Destination ADestination(
            MapCoordinates? withMapCoordinates = null,
            string withName = null,
            MissionType? withMissionType = null
        ) {
            return new Destination
            {
                name = withName ?? "",
                mapCoordinates = withMapCoordinates ?? new MapCoordinates(1, 1),
                missionType = withMissionType ?? MissionType.Battle
            };
        }
    }
}