using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionTimeCostModifiersMother
    {
        public static MissionsModifiers AMissionTimeCostModifiers(
            float withMissionMultiplier = 1,
            float withTravelMultiplier = 1
        ) =>
            new MissionsModifiers(
                travelMultiplier: withTravelMultiplier,
                missionMultiplier: withMissionMultiplier
            );
    }
}