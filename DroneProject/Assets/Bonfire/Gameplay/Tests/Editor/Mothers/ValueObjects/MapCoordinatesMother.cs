﻿using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MapCoordinatesMother
    {
        public static MapCoordinates AMapCoordinates(int withX = 0, int withY = 0) => 
            new MapCoordinates(x: withX, y: withY);
    }
}