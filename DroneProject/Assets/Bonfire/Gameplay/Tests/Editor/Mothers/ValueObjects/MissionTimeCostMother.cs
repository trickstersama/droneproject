﻿using Bonfire.Gameplay.Domain.ValueObjects.Mission;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionTimeCostMother
    {
        public static MissionTimeCost AMissionTimeCost(
            float withTimeToTarget = 0,
            float withTimeOnTarget = 0,
            float withTimeToBaseWithMaxWeight = 0,
            float withToBaseWithMaxAllowed = 0
        ) {
            return new MissionTimeCost(
                toTarget: withTimeToTarget,
                onTarget: withTimeOnTarget,
                toBaseWithMaxWeight: withTimeToBaseWithMaxWeight,
                toBaseWithMaxAllowed: withToBaseWithMaxAllowed
            );
        }
    }
}