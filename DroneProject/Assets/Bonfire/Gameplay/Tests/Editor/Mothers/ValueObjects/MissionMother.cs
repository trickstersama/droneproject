﻿using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using static Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Balcony.MissionProgressionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects
{
    public static class MissionMother
    {
        public static Mission AMission(
            MissionOrders? withMissionOrders = null, 
            TimeOnMission? withTimeOnMission = null,
            MissionProgression? witMissionProgression = null
        ) =>
            new Mission(
                withMissionOrders ?? AMissionOrders(),
                withTimeOnMission ?? ATimeOnMission(),
                witMissionProgression ?? AMissionProgression()
            );
    }
}