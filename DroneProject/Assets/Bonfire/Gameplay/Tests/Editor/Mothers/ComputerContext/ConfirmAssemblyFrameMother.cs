﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class ConfirmAssemblyFrameMother
    {
        public static ConfirmAssemblyFrame AConfirmAssemblyFrame(
            GameplayEvents? withEvents = null,
            GameplayRepositories? withRepositories = null
        ) {
            return new ConfirmAssemblyFrame(
                withEvents ?? SomeEvents(),
                withRepositories ?? SomeRepositories()
            );
        }
    }
}