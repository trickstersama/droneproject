﻿using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class MissionBuilderRepositoryMother
    {
        public static MissionBuilderRepository AMissionBuilderRepository(
            bool withMissionReadyToCalculate = true
        ) {
            var repository = Substitute.For<MissionBuilderRepository>();
            repository.UpdateMissionType(Arg.Any<MissionType>()).Returns(Observable.Return(new MissionOrders()));
            repository.UpdateMapCoordinates(Arg.Any<MapCoordinates>()).Returns(Observable.Return(new MissionOrders()));
            repository.UpdateDrone(Arg.Any<Drone>()).Returns(Observable.Return(new MissionOrders()));
            repository.IsMissionReadyToCalculate().Returns(withMissionReadyToCalculate);
            repository.AddEnergyCost(Arg.Any<MissionEnergyCost>()).Returns(Observable.Return(new MissionOrders()));
            repository.GetMissionOrders().Returns(Observable.Return(new MissionOrders()));
            repository.AddTimeCost(Arg.Any<MissionTimeCost>()).Returns(Observable.Return(new MissionOrders()));
            repository.AddMaxWeightAllowed(Arg.Any<float>()).Returns(Observable.Return(new MissionOrders()));
            repository.GetDrone().Returns(ADrone());
            return repository;
        }

        public static MissionBuilderRepository AnInMemoryMissionBuilderRepository(
            MissionOrders? withMissionOrders = null,
            Drone? withDrone = null,
            bool withMissionReadyToCalculate = false
        ) {
            return new InMemoryMissionBuilderRepository(
                withMissionOrders ?? new MissionOrders(),
                withDrone,
                withMissionReadyToCalculate
            );
        }
    }
}