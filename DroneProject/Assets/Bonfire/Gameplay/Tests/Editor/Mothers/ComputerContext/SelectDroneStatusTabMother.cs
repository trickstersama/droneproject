﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class SelectDroneStatusTabMother
    {
        public static SelectDroneStatusTab ASelectDroneStatusTab(
            GameplayEvents? withGameplayEvents = null,
            GameplayRepositories? withGameplayRepositories = null
        ) =>
            new SelectDroneStatusTab(
                withGameplayEvents ?? SomeEvents(), 
                withGameplayRepositories ?? SomeRepositories()
            );
    }
}