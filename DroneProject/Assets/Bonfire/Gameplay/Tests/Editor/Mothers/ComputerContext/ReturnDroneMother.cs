﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Status;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class ReturnDroneMother
    {
        public static ReturnDrone AReturnDrone(
            GameplayRepositories? withRepositories = null, 
            GameplayEvents? withEvents = null
        ) =>
            new ReturnDrone(
                events: withEvents ?? SomeEvents(),
                repositories: withRepositories ?? SomeRepositories()
            );
    }
}

