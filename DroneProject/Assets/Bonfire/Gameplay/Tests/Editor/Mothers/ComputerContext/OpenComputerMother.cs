﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class OpenComputerMother{
        
        public static OpenComputer AOpenComputer(GameplayEvents? withGameplayEvents = null) => 
            new OpenComputer(events: withGameplayEvents ?? SomeEvents());
    }
}