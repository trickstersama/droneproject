﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext
{
    public static class SendDroneMother
    {
        public static SendDrone ASendDrone(
            GameplayEvents? withEvents = null, 
            GameplayRepositories? withRepositories = null,
            GameplayServices? withServices = null
        ){
            return new SendDrone(
                events: withEvents ?? SomeEvents(),
                repositories: withRepositories ?? SomeRepositories(),
                services: withServices ?? SomeServices()
            );
        }
    }
}

