﻿using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers
{
    public static class EncounterDeciderMother
    {
        public static EncounterDeciderService AnEncounterDecider(
            Encounter withEncounter = null,
            Drone? withDrone = null
        ) {
            
            var encounter = withEncounter ?? new Encounter();
            
            var encounterDecider = Substitute.For<EncounterDeciderService>();
            var encounterObservable = Observable.Return(encounter);
            encounterDecider.DetermineEncounterType()
                .Returns(encounterObservable);
            return encounterDecider;
        }
    }
}