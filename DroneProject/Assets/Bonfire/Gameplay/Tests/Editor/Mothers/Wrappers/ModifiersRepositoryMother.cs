using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostModifiersMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers
{
    public static class ModifiersRepositoryMother
    {
        public static ModifiersRepository AModifiersRepository(MissionsModifiers? withMissionTimeCostModifiers = null)
        {
            var repository = Substitute.For<ModifiersRepository>();
            repository.GetTimeCostModifiers().Returns(withMissionTimeCostModifiers ?? AMissionTimeCostModifiers());
            return repository;
        }
    }
}