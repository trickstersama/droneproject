﻿using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.MapRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseItemsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.MissionsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.ModifiersRepositoryMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers
{
    public static class GameplayRepositoriesMother
    {
        public static GameplayRepositories SomeRepositories(
            DronesRepository withDronesRepository = null,
            HouseItemsRepository withHouseItemsRepository = null,
            ComponentsRepository withComponentsRepository = null,
            DroneInProgressRepository withDroneInProgressRepository = null,
            MissionBuilderRepository withMissionBuilderRepository = null,
            MapRepository withMapRepository = null,
            PinInProgressRepository withPinInProgressRepository = null,
            HouseResourcesRepository withHouseResourcesRepository = null,
            ModifiersRepository withModifiersRepository = null,
            MissionsRepository withMissionsRepository = null
        ) =>
            new GameplayRepositories
            (
                dronesRepository: withDronesRepository ?? ADronesRepository(),
                houseItemsRepository: withHouseItemsRepository ?? AHouseItemsRepository(),
                componentsRepository: withComponentsRepository ?? AComponentsRepository(),
                droneInProgressRepository: withDroneInProgressRepository ?? ADroneInProgressRepository(),
                missionBuilderRepository: withMissionBuilderRepository ?? AMissionBuilderRepository(),
                mapRepository: withMapRepository ?? AMapRepository(),
                pinInProgressRepository: withPinInProgressRepository ?? APinInProgressRepository(),
                houseResourcesRepository: withHouseResourcesRepository ?? AHouseResourcesRepository(),
                modifiersRepository: withModifiersRepository ?? AModifiersRepository(),
                missionsRepository: withMissionsRepository ?? AMissionsRepository()
            );
    }
}

