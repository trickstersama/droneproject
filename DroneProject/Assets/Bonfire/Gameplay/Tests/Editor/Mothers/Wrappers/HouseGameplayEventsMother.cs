﻿using Bonfire.Gameplay.Domain.Wrappers;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers
{
    public static class HouseGameplayEventsMother
    {
        public static HouseGameplayEvents SomeHouseEvents(
            ISubject<float> withEnergyTimelineEmitted = null,
            ISubject<float> withEnergyProduced = null,
            ISubject<float> withEnergyOverheadProduced = null,
            ISubject<Unit> withInsufficientEnergyIsProduced = null,
            ISubject<float> withEnergyStored = null,
            ISubject<Unit> withEnergyStorageFull = null,
            ISubject<Unit> withHouseBatteryUpgraded = null
        ) {
            return new HouseGameplayEvents(
                onEnergyProduced: withEnergyProduced ?? new Subject<float>(),
                onEnergyTimelineEmitted: withEnergyTimelineEmitted ?? new Subject<float>(),
                onEnergyOverheadProduced: withEnergyOverheadProduced ?? new Subject<float>(),
                onInsufficientEnergyIsProduced: withInsufficientEnergyIsProduced ?? new Subject<Unit>(),
                onEnergyStored: withEnergyStored ?? new Subject<float>(),
                onEnergyStorageFull: withEnergyStorageFull ?? new Subject<Unit>(),
                onHouseBatteryUpgraded: withHouseBatteryUpgraded ?? new Subject<Unit>()
            );
        }
    }
}