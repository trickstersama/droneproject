﻿using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DestinationServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.DroneAssemblerServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.EncounterDeciderMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DroneServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.FrameSlotsAmountServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers
{
    public static class GameplayServicesMother
    {
        public static GameplayServices SomeServices(
            EncounterDeciderService withDroneEncounter = null,
            FrameSlotsAmountService withFrameSlotsAmount = null,
            DroneAssemblerService withDroneAssembler = null,
            DestinationService withDestinationService = null,
            DroneService withDroneService = null,
            MissionService withMissionService = null
        ) =>
            new GameplayServices
            (
                encounterDeciderService: withDroneEncounter ?? AnEncounterDecider(),
                frameSlotsAmountService: withFrameSlotsAmount ?? ALocalFrameSlotsAmountService(),
                droneAssemblerService: withDroneAssembler ?? ADroneAssemblerService(),
                destinationService: withDestinationService ?? ADestinationService(),
                droneService: withDroneService ?? ADroneService(),
                missionService: withMissionService ?? AMissionService()
            );
    }
}