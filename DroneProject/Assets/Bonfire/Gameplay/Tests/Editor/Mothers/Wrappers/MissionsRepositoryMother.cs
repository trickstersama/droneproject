using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers
{
    public static class MissionsRepositoryMother
    {
        public static MissionsRepository AMissionsRepository(IEnumerable<Mission> withMissions = null)
        {
            var missionsRepository = Substitute.For<MissionsRepository>();
            missionsRepository.GetAllMissions()
                .Returns(Observable.Return(withMissions ?? new []{AMission()}));
            return missionsRepository;
        }

        public static MissionsRepository AnInMemoryMissionsRepository(Mission[] withMissions = null) => 
            new InMemoryMissionsRepository(withMissions: withMissions ?? new []{ AMission()});
    }
}