﻿using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class BuildDroneMother
    {
        public static BuildDrone ABuildDrone
        (
            GameplayEvents? events = null,
            GameplayServices? services = null,
            GameplayRepositories? repositories = null
            
        ) {
            return new BuildDrone(
                events: events ?? SomeEvents(),
                services: services ?? SomeServices(),
                repositories: repositories ?? SomeRepositories()
            );
        }
    }
}