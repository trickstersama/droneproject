﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Services;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class DroneAssemblerServiceMother
    {
        public static DroneAssemblerService ADroneAssemblerService()
        {
            var service = Substitute.For<DroneAssemblerService>();
            service.CreateEmptyDrone(Arg.Any<Frame>()).Returns(Observable.Return(new Drone()));
            service.AddModifiers(Arg.Any<Drone>(),Arg.Any<DroneModifiers>()).Returns(Observable.Return(new Drone()));
            service.AddModules(Arg.Any<Drone>(),Arg.Any<IEnumerable<Module>>()).Returns(Observable.Return(new Drone()));
            service.AddName(Arg.Any<Drone>(),Arg.Any<string>()).Returns(Observable.Return(new Drone()));
            service.NameIsValid(Arg.Any<string>()).Returns(true);
            return service;
        }

        public static DroneAssemblerService ALocalDroneAssemblerService()
        {
            return new LocalDroneAssemblerService();
        }
    }
}