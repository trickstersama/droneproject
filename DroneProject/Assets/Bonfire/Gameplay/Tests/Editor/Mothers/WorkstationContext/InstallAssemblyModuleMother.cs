﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class InstallAssemblyModuleMother
    {
        public static InstallAssemblyModule AInstallAssemblyModule(
            GameplayEvents? events = null,
            GameplayRepositories? repositories = null
        ) =>
            new(
                events: events ?? SomeEvents(),
                repositories: repositories ?? SomeRepositories());

    }
}