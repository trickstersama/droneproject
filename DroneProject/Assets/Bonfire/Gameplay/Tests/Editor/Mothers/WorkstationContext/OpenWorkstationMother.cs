﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Wrappers;
using Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class OpenWorkstationMother
    {
        public static OpenWorkstation AOpenWorkstation(
            GameplayEvents? withEvents = null,
            GameplayRepositories? withRepositories = null
        ) =>
            new OpenWorkstation(
                events: withEvents ?? GameplayEventsMother.SomeEvents()
            );
    }
}