﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class SelectAssemblyTabMother
    {
        public static SelectAssemblyTab ASelectAssemblyTab(
            GameplayEvents? withEvents = null,
            GameplayRepositories? withRepositories = null
        ) =>
            new SelectAssemblyTab(
                withEvents ?? SomeEvents(),
                withRepositories ?? SomeRepositories()
            );
    }
}