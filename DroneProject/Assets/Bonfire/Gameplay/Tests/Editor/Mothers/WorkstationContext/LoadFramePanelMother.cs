﻿using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class LoadFramePanelMother
    {
        public static LoadFramePanel ALoadFramePanel(
            GameplayEvents? withEvents = null,
            GameplayRepositories? withRepositories = null
        ) =>
            new LoadFramePanel(
                withEvents ?? SomeEvents(),
                withRepositories ?? SomeRepositories()
            );
    }
}