﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext
{
    public static class SelectFrameMother
    {
        public static SelectFrame ASelectFrame(
            GameplayEvents? withEvents = null, 
            GameplayRepositories? withRepositories = null
        ) =>
            new(
                withEvents ?? SomeEvents(),
                withRepositories ?? SomeRepositories()
            );
    }
}