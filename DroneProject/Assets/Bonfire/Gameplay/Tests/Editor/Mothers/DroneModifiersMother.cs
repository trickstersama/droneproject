﻿using Bonfire.Gameplay.Domain.ValueObjects;

namespace Bonfire.Gameplay.Tests.Editor.Mothers
{
    public static class DroneModifiersMother
    {
        public static DroneModifiers ADroneModifiers(
            int withHardness = 0,
            float withWeight = 0,
            int withDamage = 0,
            int withHitPoints = 0,
            float withThrust = 0,
            WeaponType withWeaponType = WeaponType.Laser,
            float withAerodynamics = 0,
            float withCrossSectionArea = 0,
            float withReservedWeight = 0
        ) {
            return new DroneModifiers(
                hardness: withHardness,
                weight: withWeight,
                damage: withDamage,
                hitPoints: withHitPoints,
                weaponType: withWeaponType,
                thrust: withThrust,
                aerodynamics: withAerodynamics,
                crossSectionArea: withCrossSectionArea,
                reservedWeight: withReservedWeight
            );
        }
    }
}