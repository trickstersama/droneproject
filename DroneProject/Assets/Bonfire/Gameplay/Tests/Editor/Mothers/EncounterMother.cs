﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;

namespace Bonfire.Gameplay.Tests.Editor.Mothers
{
    public static class EncounterMother
    {
        public static Encounter AEncounter()
        {
            var encounter = Substitute.For<Encounter>();
            encounter.Type.Returns("default");
            return encounter;
        }
    }
}