﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class HouseItemsRepositoryMother
    {
        public static HouseItemsRepository AHouseItemsRepository(
            Module withModule = null
        ) {
            var itemsList = new List<Item> {new Item(), new Item()};
            var itemsRepo = Substitute.For<HouseItemsRepository>();
            itemsRepo.GetItems()
                .Returns(Observable.Return(itemsList));
            return itemsRepo;
        }

        public static HouseItemsRepository AnInMemoryHouseItemsRepository()
        {
            var inMemoryHouseItemsRepository = new InMemoryHouseItemsRepository();
            return inMemoryHouseItemsRepository;
        }
    }
}