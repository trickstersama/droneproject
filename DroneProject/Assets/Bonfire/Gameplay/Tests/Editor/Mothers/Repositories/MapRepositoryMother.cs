﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class MapRepositoryMother
    {
        public static MapRepository AMapRepository()
        {
            var repository = Substitute.For<MapRepository>();
            repository.RetrieveAllDestinations()
                .Returns(Observable.Return(Enumerable.Empty<Destination>()));
            return repository;
        }

        public static MapRepository AnInMemoryMapRepository(
            IEnumerable<Destination> withDestinations = null)
        {
            return new InMemoryMapRepository(withDestinations);
        }
    }
}