﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class DronesRepositoryMother
    {
        public static DronesRepository ADronesRepository(
            IEnumerable<Drone> withIdleDrones = null,
            IEnumerable<Drone> withBusyDrones = null,
            Drone? withDroneByName = null
        ) {
            var repository = Substitute.For<DronesRepository>();
            
            var busyDrones = withBusyDrones ?? Enumerable.Empty<Drone>();
            var idleDrones = withIdleDrones ?? Enumerable.Empty<Drone>();
            
            repository.RetrieveAllBusyDrones()
                .Returns(Observable.Return(busyDrones));
            repository.RetrieveAllIdleDrones()
                .Returns(Observable.Return(idleDrones));
            repository.SetDroneToBusy(Arg.Any<Drone>())
                .Returns(arg  => Observable.Return((Drone)arg[0]));
            repository.SetDroneToIdle(Arg.Any<Drone>())
                .Returns(arg  => Observable.Return((Drone)arg[0]));
            repository.AddNewDrone(Arg.Any<Drone>())
                .Returns(Observable.Return(Enumerable.Empty<Drone>()));
            repository.RetrieveDroneInWorkshop()
                .Returns(Observable.Return(new Drone()));
            repository.UpdateDrone(Arg.Any<Drone>())
                .Returns(Observable.Return(new Drone()));
            repository.SetDroneToTraveling(Arg.Any<Drone>())
                .Returns(Observable.Return(new Drone()));
            repository.GetDroneByName(Arg.Any<string>())
                .Returns(withDroneByName ?? new Drone());
            repository.GetDronesInBalcony()
                .Returns(Enumerable.Empty<Drone>());
            
            return repository;
        }
        public static DronesRepository AnInMemoryDronesRepository(
            IEnumerable<Drone> withDrones = null,
            Drone? withDroneInWorkshop = null
        ) =>
            new InMemoryDronesRepository(
                withDrones: withDrones,
                withDroneInWorkshop: withDroneInWorkshop
            );
    }
}