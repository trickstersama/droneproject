﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class HouseResourcesRepositoryMother
    {
        public static HouseResourcesRepository AHouseResourcesRepository(
            float withEnergyConsumption = 0,
            bool withStorageFull = false
        ) {
            var repository = Substitute.For<HouseResourcesRepository>();
            repository.GetEnergyProductionPerSecond().Returns(1f);
            repository.GetEnergyConsumptionPerSecond().Returns(withEnergyConsumption);
            repository.IsEnergyStorageFull().Returns(withStorageFull);
            repository.StoreEnergy(Arg.Any<float>()).Returns(Observable.Return(1f));
            return repository;
        }

        public static HouseResourcesRepository AnInMemoryHouseResourcesRepository(
            IEnumerable<SolarGenerator> withSolarGenerators = null,
            IEnumerable<WindGenerator> withWindGenerators = null,
            IEnumerable<EnergyReactor> withEnergyReactors = null,
            IEnumerable<HouseBattery> withHouseBatteries = null,
            float withEnergyConsumption = 0,
            float withEnergyStored = 0
        ) {
            return new InMemoryHouseResourcesRepository(
                withSolarGenerators: withSolarGenerators,
                withWindGenerators: withWindGenerators,
                withEnergyReactors: withEnergyReactors,
                withEnergyConsumption: withEnergyConsumption,
                withHouseBatteries: withHouseBatteries,
                withEnergyStored: withEnergyStored
            );
        }
    }
}