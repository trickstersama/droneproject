﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using UniRx;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class ComponentsRepositoryMother
    {
        public static ComponentsRepository AComponentsRepository(
            IEnumerable<Module> withModules = null,
            IEnumerable<Frame> withFrames = null,
            IEnumerable<Module> withModuleToRemove = null
        ) {
            var componentRepository = Substitute.For<ComponentsRepository>();
            componentRepository.GetAllModules()
                .Returns(Observable.Return(withModules ?? Enumerable.Empty<Module>()));
            componentRepository.GetFrames().Returns(Observable.Return(withFrames ?? Enumerable.Empty<Frame>()));
            componentRepository.RemoveModules(Arg.Any<IEnumerable<Module>>())
                .Returns(Observable.Return(withModuleToRemove ?? Enumerable.Empty<Module>()));
            componentRepository.RemoveFrame(Arg.Any<Frame>())
                .Returns(Observable.Return(withFrames ?? Enumerable.Empty<Frame>()));
            componentRepository.GetModulesOfType(Arg.Any<ModuleType>())
                .Returns(Observable.Return(withModules ?? Enumerable.Empty<Module>()));
            return componentRepository;
        }

        public static ComponentsRepository AnInMemoryComponentRepository(
            IEnumerable<Module> withModules = null,
            IEnumerable<Frame> withFrames = null
        ) {
            return new InMemoryComponentRepository(
                withModules ?? Enumerable.Empty<Module>(),
                withFrames ?? Enumerable.Empty<Frame>()
            );
        }
    }
}