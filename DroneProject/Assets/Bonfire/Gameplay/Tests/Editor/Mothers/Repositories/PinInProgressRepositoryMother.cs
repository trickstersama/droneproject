﻿using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DestinationMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class PinInProgressRepositoryMother
    {
        public static PinInProgressRepository APinInProgressRepository()
        {
            var repository = Substitute.For<PinInProgressRepository>();
            repository.GetDestination().Returns(new Destination());
            return repository;
        }
        public static PinInProgressRepository AnInMemoryPinInProgressRepository(Destination? withDestination = null) => 
            new InMemoryPinInProgressRepository(withDestination: withDestination ?? ADestination());
    }
}