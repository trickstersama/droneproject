﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers.Repositories
{
    public static class DroneInProgressRepositoryMother
    {
        public static DroneInProgressRepository ADroneInProgressRepository(Frame? withFrame = null,int withSlotsAvailable = 1)
        {
            var assemblyDroneRepository = Substitute.For<DroneInProgressRepository>();
            assemblyDroneRepository.AddModuleToProgress(Arg.Any<Module>())
                .Returns(Observable.Return(ADroneModifiers()));
            assemblyDroneRepository.SlotsAvailableFor(Arg.Any<Module>())
                .Returns(withSlotsAvailable);
            assemblyDroneRepository.ConsumeSlot(Arg.Any<Module>())
                .Returns(Observable.Return(AFrame()));
            assemblyDroneRepository.ClearModulesOfType(Arg.Any<ModuleType>())
                .Returns(Observable.Return(ADroneModifiers()));
            assemblyDroneRepository.GetDroneModifiers()
                .Returns(Observable.Return(ADroneModifiers()));
            assemblyDroneRepository.GetActualFrameState()
                .Returns(Observable.Return(AFrame()));
            assemblyDroneRepository.LoadModulesForAssembly(Arg.Any<IEnumerable<Module>>())
                .Returns(Observable.Return(Enumerable.Empty<Module>()));
            assemblyDroneRepository.LoadFramesForAssembly(Arg.Any<IEnumerable<Frame>>())
                .Returns(Observable.Return(Enumerable.Empty<Frame>()));
            assemblyDroneRepository.GetFrame()
                .Returns(withFrame ?? AFrame());
            assemblyDroneRepository.GetModules()
                .Returns(Observable.Return(Enumerable.Empty<Module>()));
            assemblyDroneRepository.GetName()
                .Returns(String.Empty);
            assemblyDroneRepository.ReleaseSlot(Arg.Any<Module>())
                .Returns(withFrame ?? AFrame());    
            assemblyDroneRepository.RemoveModuleFromProgress(Arg.Any<Module>())
                .Returns(Observable.Return(ADroneModifiers()));
            
            return assemblyDroneRepository;
        }

        public static DroneInProgressRepository InMemoryDroneInProgressRepository(
            Frame? withActualFrameState = null,
            Frame? withOriginalFrame = null,
            IEnumerable<Module> withModules = null
        ) {
            return new InMemoryDroneInProgressRepository(
                actualFrameState: withActualFrameState ?? AFrame(),
                originalFrame: withOriginalFrame ?? AFrame(),
                modulesInUse: withModules 
            );
        }
    }
}