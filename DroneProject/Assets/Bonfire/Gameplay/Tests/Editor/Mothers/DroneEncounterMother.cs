﻿using Bonfire.Gameplay.Domain.Reactions;
using Bonfire.Gameplay.Domain.Wrappers;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;

namespace Bonfire.Gameplay.Tests.Editor.Mothers
{
    public static class DroneEncounterMother
    {
        public static DroneEncounter ADroneEncounter(
            GameplayEvents? withGameplayEvents = null,
            GameplayServices? withGameplayServices = null
        ) =>
            new DroneEncounter(
                withGameplayEvents ?? SomeEvents(),
                withGameplayServices ?? SomeServices()
            );
    }
}