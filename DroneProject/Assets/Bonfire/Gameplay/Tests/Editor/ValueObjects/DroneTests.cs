﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;

namespace Bonfire.Gameplay.Tests.Editor.ValueObjects
{
    [TestFixture]
    public class DroneTests
    {
        [Test]
        public void ModifyDroneCharge()
        {
            //given
            var oldCharge = 0;
            var newCharge = 100;
            
            var droneModifiers = ADroneModifiers(withDamage: 92, withHitPoints: 100, withHardness: 2);
            var droneToModify = ADrone(
                withModifiers: droneModifiers,
                withCharge: oldCharge,
                withName: "carlos"
                );

            var expectedDrone = ADrone(
                withModifiers: droneModifiers,
                withCharge: newCharge,
                withName: "carlos"
                );
            
            //when
            var modifiedDrone = droneToModify.ModifyThis(withCharge: newCharge);
            
            //then

            Assert.AreEqual(expectedDrone, modifiedDrone);
        }
        
        [Test]
        public void ModifyDroneModifiers()
        {
            //given
            var oldModifiers = ADroneModifiers(
                withHitPoints: 100,
                withHardness: 1,
                withDamage: 9239
            );
            
            var newModifiers = ADroneModifiers(
                withWeaponType: WeaponType.Ballistic,
                withWeight: 1239
            );
            
            var droneToModify = ADrone(
                withModifiers: oldModifiers,
                withCharge: 100,
                withName: "carlos",
                withDamageReceived: 2
            );

            var expectedDrone = ADrone(
                withModifiers: newModifiers,
                withCharge: 100,
                withName: "carlos",
                withDamageReceived: 2
            );
            
            //when
            var modifiedDrone = droneToModify.ModifyThis(withModifiers: newModifiers);
            
            //then

            Assert.AreEqual(expectedDrone, modifiedDrone);
        }
        
        [Test]
        public void Modify3Components()
        {
            //given
            var oldDamage = 0;
            var oldCharge = 0;
            var oldFrame = AFullFrame();
            
            var newDamage = 100;
            var newCharge = 100;
            var newFrame = AFrame();
            
            var droneModifiers = ADroneModifiers(withDamage: 92, withHitPoints: 100, withHardness: 2);
            var droneToModify = ADrone(
                withModifiers: droneModifiers,
                withCharge: oldCharge,
                withName: "carlos",
                withDamageReceived: oldDamage,
                withFrame: oldFrame
            );

            var expectedDrone = ADrone(
                withModifiers: droneModifiers,
                withCharge: newCharge,
                withName: "carlos",
                withDamageReceived: newDamage,
                withFrame: newFrame
            );
            
            //when
            var modifiedDrone = droneToModify.ModifyThis(
                withCharge: newCharge,
                withFrame: newFrame,
                withDamageReceived: newDamage
            );
            
            //then

            Assert.AreEqual(expectedDrone, modifiedDrone);
        }
        
        [Test]
        public void AddDroneModifiersForTravel()
        {
            //given
            var aerodynamics = 100;
            var crossSectionArea = 0.1f;
            
            var oldModifiers = ADroneModifiers(
                withDamage: 92, 
                withHitPoints: 100, 
                withHardness: 2
            );
            
            var newModifiers = ADroneModifiers(
                withDamage: 92, 
                withHitPoints: 100, 
                withHardness: 2,
                withAerodynamics: aerodynamics,
                withCrossSectionArea: crossSectionArea
            );
            
            
            var droneToModify = ADrone(
                withModifiers: oldModifiers,
                withCharge: 100,
                withName: "carlos"
            );

            var expectedDrone = ADrone(
                withModifiers: newModifiers,
                withCharge: 100,
                withName: "carlos"
            );
            
            //when
            var modifiedDrone = droneToModify.ModifyThis(withModifiers: newModifiers);
            
            //then

            Assert.AreEqual(expectedDrone, modifiedDrone);
        }

    }
}