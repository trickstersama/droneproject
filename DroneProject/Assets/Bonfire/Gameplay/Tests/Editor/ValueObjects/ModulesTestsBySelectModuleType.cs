﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.ValueObjects
{
    [TestFixture]
    public class ModulesTestsBySelectModuleType
    {
        List<Module> modulesInRepository;

        [SetUp]
        public void SetUp()
        {
            modulesInRepository = new List<Module>
            {
                AWeaponModule(),
                AnArmorModule(),
                ASensorModule(),
                ABatteryModule(),
                AStorageModule(),
                APropulsionModule()
            };
        }

        [Test]
        public void ReturnAllWeapons()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = AWeaponModule(
                withName: "AWeaponWithFullStats",
                withDamage: 10,
                withWeaponType: WeaponType.Ballistic,
                withWeight: 10
            );
            
            modulesInRepository.Add(customModule);
            
            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                AWeaponModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Weapon))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }

        [Test]
        public void ReturnAllArmors()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = AnArmorModule(
                withName: "AnArmorWithFullStats",
                withWeight: 11,
                withHardness: 11,
                withStealth: 11,
                withHitPoints: 11
            );
            
            modulesInRepository.Add(customModule);
            
            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                AnArmorModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Armor))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
        
        [Test]
        public void ReturnAllSensors()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = ASensorModule(
                withName: "AnArmorWithFullStats",
                withWeight: 11,
                withDamage: 11,
                withStealth: 11
            );
            
            modulesInRepository.Add(customModule);

            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                ASensorModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Sensor))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
        
        [Test]
        public void ReturnAllBatteries()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = ABatteryModule(
                withName: "AnArmorWithFullStats",
                withDescription: "Battery Module",
                withWeight: 11,
                withPowerCapacity: 11
            );

            modulesInRepository.Add(customModule);

            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                ABatteryModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Battery))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
        
        [Test]
        public void ReturnAllStorage()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = AStorageModule(
                withName: "AnArmorWithFullStats",
                withDescription: "Battery Module",
                withReservedWeight: 12,
                withStorageType: StorageType.Living
            );

            modulesInRepository.Add(customModule);

            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                AStorageModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Storage))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
        [Test]
        public void ReturnAllPropulsion()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();
            var customModule = APropulsionModule(
                withName: "APropulsionWithStats",
                withDescription: "Propulsion",
                withThrust: 1000,
                withStealth: -10,
                withConsumption: 100
            );

            modulesInRepository.Add(customModule);

            var expectedCustomModule = customModule;
            var expectedModules = new List<Module>
            {
                APropulsionModule(),
                expectedCustomModule
            };

            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(ModuleType.Propulsion))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
    }
}