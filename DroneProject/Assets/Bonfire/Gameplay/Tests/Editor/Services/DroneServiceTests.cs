﻿using System;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MapCoordinatesMother;

namespace Bonfire.Gameplay.Tests.Editor.Services
{
    [TestFixture]
    public class MissionServiceTests
    {

        [Test]
        public void CalculateHorizontalAcceleration()
        {

            var missionService = ALocalMissionService();
            //Given
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5f,
                withThrust: 100
            ));

            var expectedForce = 10.2f;

            //When
            var acceleration = missionService.HorizontalAcceleration(drone);

            //Then

            Assert.IsTrue(Math.Abs(expectedForce - acceleration) < 1f);

        }

        [Test]
        public void CalculateTerminalVelocity()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 1,
                withThrust: 9.8f,
                withAerodynamics: 0.01f,
                withCrossSectionArea: 0.04f
            ));

            var expectedVelocity = 0;

            //When
            var terminalVelocity = missionService.TerminalVelocity(drone);

            //Then
            Assert.IsTrue(Math.Abs(terminalVelocity - expectedVelocity) < 1f);
        }

        [Test]
        public void CalculateTerminalVelocity2()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 99,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f
            ));

            //10m/s
            var expectedVelocity = 63.8;

            //When
            var terminalVelocity = missionService.TerminalVelocity(drone);

            //Then
            Assert.IsTrue(Math.Abs(terminalVelocity - expectedVelocity) < 1f);
        }

        [Test]
        public void CalculateAirDragForceByVelocity_WithTerminalVelocity()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 99,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f
            ));

            // expectedVelocity = 63.8;
            var expectedDragForce = 49.8f;
            var velocity = missionService.TerminalVelocity(drone);

            //When
            var terminalVelocity = missionService.AirDragForceByVelocity(drone, velocity);

            //Then
            Assert.IsTrue(Math.Abs(terminalVelocity - expectedDragForce) < 1f);
        }

        [Test]
        public void CalculateFlightTimeToTarget()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 99,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f
            ));

            // expectedVelocity = 63.8;
            var expectedTimeToTarget = 70;
            var mapCoordinates = AMapCoordinates(4, 2);

            //When
            var timeToTarget = missionService.FlightTimeToTarget(drone, mapCoordinates);

            //Then
            Assert.IsTrue(Math.Abs(timeToTarget - expectedTimeToTarget) < 1f);
        }

        [Test]
        public void CalculateReturnFlightTime()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 99,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f,
                withReservedWeight: 5
            ));

            // expectedVelocity = 63.8;
            var expectedTimeToTarget = 494.9;
            var mapCoordinates = AMapCoordinates(4, 2);
            var occupiedStorage = 5f;

            //When
            var timeToTarget = missionService.FlightTimeToTarget(drone, mapCoordinates, occupiedStorage);

            //Then
            Assert.IsTrue(Math.Abs(timeToTarget - expectedTimeToTarget) < 1f);
        }

        [Test]
        public void CalculateLiftForceWithoutCargo()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 999,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f,
                withReservedWeight: 5
            ));

            // expectedVelocity = 63.8;
            var expectedForce = 49;

            //When
            var force = missionService.LiftForce(drone);

            //Then
            Assert.IsTrue(Math.Abs(force - expectedForce) < 1f);
        }

        [Test]
        public void CalculateLiftForceWithCargo()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 50,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f,
                withReservedWeight: 5
            ));

            var expectedForce = 98f;
            var occupiedStorage = 5f;

            //When
            var force = missionService.LiftForce(drone, occupiedStorage);

            //Then
            Assert.IsTrue(Math.Abs(force - expectedForce) < 1f);
        }

        [Test]
        public void CalculateEfficientThrust()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 5,
                withThrust: 99,
                withAerodynamics: 0.5f,
                withCrossSectionArea: 0.04f,
                withReservedWeight: 5
            ));

            var expectedForce = 1f;
            var occupiedStorage = 5f;

            //When
            var force = missionService.EfficientThrust(drone, occupiedStorage);

            //Then
            Assert.IsTrue(Math.Abs(force - expectedForce) < 1f);
        }

        [Test]
        public void CalculateTravelJoulesWithNoLoss()
        {
            //Given

            var missionService = ALocalMissionService();
            var drone = ADrone(withModifiers: ADroneModifiers(
                withWeight: 1,
                withThrust: 50,
                withAerodynamics: 0.15f,
                withCrossSectionArea: 0.04f,
                withReservedWeight: 5
            ));
            var mapCoordinates = AMapCoordinates(4, 2);
            var reservedWeight = 1;
            var expectedWork = 4917;

            //When
            var joules = missionService.TravelJoulesWithNoLoss(drone, mapCoordinates, reservedWeight);

            //Then
            Assert.IsTrue(Math.Abs(joules - expectedWork) < 1f);
        }
    }
}