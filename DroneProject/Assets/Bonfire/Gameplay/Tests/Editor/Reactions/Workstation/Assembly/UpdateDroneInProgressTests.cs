﻿using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class UpdateDroneInProgressTests
    {
        [Test]
        public void SendOnDroneInProgressUpdated()
        {
            var onDroneInProgressUpdated = new Subject<DroneModifiers>();
            var droneInProgressRepository = InMemoryDroneInProgressRepository();

            Given(new UpdateDroneInProgress(
                    events: SomeEvents(withDroneInProgressUpdated: onDroneInProgressUpdated), 
                    repositories: SomeRepositories(withDroneInProgressRepository:droneInProgressRepository) 
                ))
                .When(action => action.Do())
                .Then(_ => onDroneInProgressUpdated, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallUpdateDroneInProgress()
        {
            var droneInProgressRepository = ADroneInProgressRepository();
            var onDroneInProgressUpdated = new Subject<DroneModifiers>();

            Given(new UpdateDroneInProgress(
                    events: SomeEvents(withDroneInProgressUpdated: onDroneInProgressUpdated),
                    repositories: SomeRepositories(withDroneInProgressRepository:droneInProgressRepository) 
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).GetDroneModifiers())
                .Run();
            
        }

        [Test] 
        public void AddWeaponProgressToInAssemblyDrone()
        {
            var onDroneInProgressUpdated = new Subject<DroneModifiers>();
    
            var frame = AFrame(withHardness: 99, withWeight: 12);
            var armorModule = AnArmorModule(withHardness: 20, withWeight: 1);
            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withOriginalFrame: frame,
                withModules: new []{armorModule}
            );

            var expectedProgress = frame.GetModifiers() + armorModule.GetDroneModifiers();

            Given(new UpdateDroneInProgress(
                    events: SomeEvents(withDroneInProgressUpdated: onDroneInProgressUpdated), 
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository) 
                ))
                .When(action => action.Do())
                .Then(_ => onDroneInProgressUpdated, it => it.Receives(expectedProgress))
                .Run();
        }
    }
}