﻿using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.DroneAssemblerServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.BuildDroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class BuildDroneTests
    {
        [Test]
        public void SendOnDroneBuilt()
        {
            var onDroneBuilt = new Subject<Drone>();

            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallCreateEmptyDroneInService()
        {
            var droneAssemblerService = ADroneAssemblerService();
            
            Given(ABuildDrone(
                    services: SomeServices(withDroneAssembler: droneAssemblerService)
                ))
                .When(action => action.Do())
                .Then(() => droneAssemblerService.Received(1).CreateEmptyDrone(Arg.Any<Frame>()))
                .Run();
        }

        [Test]
        public void CallGetFrameFromDroneInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();
            
            Given(ABuildDrone(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).GetFrame())
                .Run();
        }
        
        [Test]
        public void CallGetNameFromDroneInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();
            
            Given(ABuildDrone(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).GetName())
                .Run();
        }
        
        [Test]
        public void CallAddNameInDroneAssemblerService()
        {
            var droneAssemblerService = ADroneAssemblerService();

            Given(ABuildDrone(
                    services: SomeServices(withDroneAssembler: droneAssemblerService)
                ))
                .When(action => action.Do())
                .Then(() => droneAssemblerService.Received(1).AddName(Arg.Any<Drone>(), Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void ReturnNewDrone()
        {
            var onDroneBuilt = new Subject<Drone>();
            var droneInProgressRepository = InMemoryDroneInProgressRepository();
            var expectedDrone = ADrone();
            
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }



        [Test]
        public void CallGetModifiersFromDroneInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(ABuildDrone(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).GetDroneModifiers())
                .Run();
        }
        
        [Test]
        public void CallAddModifiersInService()
        {
            var droneAssemblerService = ADroneAssemblerService();
            
            Given(ABuildDrone(
                    services: SomeServices(withDroneAssembler: droneAssemblerService)
                ))
                .When(action => action.Do())
                .Then(() => droneAssemblerService.Received(1).AddModifiers(Arg.Any<Drone>(),Arg.Any<DroneModifiers>()))
                .Run();
        }

        [Test]
        public void CreatedDroneIsIdle()
        {

            var onDroneBuilt = new Subject<Drone>();
            var droneInProgressRepository = InMemoryDroneInProgressRepository();
            var expectedDrone = ADrone(withStatus: DroneStatus.Idle);
        
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }

        [Test]
        public void CallGetModulesFromDroneInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(ABuildDrone(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).GetModules())
                .Run();
        }
        
                
        [Test]
        public void CallAddModulesInService()
        {
            var droneAssemblerService = ADroneAssemblerService();
            
            Given(ABuildDrone(
                    services: SomeServices(withDroneAssembler: droneAssemblerService)
                ))
                .When(action => action.Do())
                .Then(() => droneAssemblerService.Received(1).AddModules(Arg.Any<Drone>(),Arg.Any<IEnumerable<Module>>()))
                .Run();
        }
        [Test] 
        public void ReturnNewDroneWithOnlyFrame()
        {
            var onDroneBuilt = new Subject<Drone>();
            var frameInAssembly = AFrame(withHardness: 10, withArmorSlotsAmount:1, withName: "caca", withWeight: 10);
            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withOriginalFrame: frameInAssembly
            );
            var expectedDrone = ADroneWithModifiersFromParts(withFrame: frameInAssembly);
            
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }

        [Test] 
        public void ReturnNewDroneWithModifiers()
        {
            var onDroneBuilt = new Subject<Drone>();
            var frameInAssembly = AFrame(withHardness: 10, withArmorSlotsAmount:1, withName: "caca", withWeight: 10);
            var armorModule = AnArmorModule(withHardness: 123, withName: "Radamantis");
            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withOriginalFrame: frameInAssembly,
                withModules: new []{armorModule}
            );
            var expectedDrone = ADroneWithModifiersFromParts(
                withFrame: frameInAssembly, 
                withModules: new []{armorModule}
                );
            
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }

        
        [Test] 
        public void ReturnNewDroneWithModules()
        {
            var onDroneBuilt = new Subject<Drone>();
            var frameInAssembly = AFrame(withHardness: 10, withArmorSlotsAmount:1, withName: "caca", withWeight: 10);
            //modules
            var modulesList = new[]
            {
                AnArmorModule(withHardness: 123, withName: "Radamantis", withHitPoints: 22, withWeight: 1),
                AWeaponModule(withDamage: 2, withName: "Shiet", withWeight: 1),
                AStorageModule(withReservedWeight: 5, withStorageType: StorageType.Corrosive),
                ABatteryModule(withWeight: 1, withPowerCapacity: 10000),
                ACPUModule(withDamage: 20, withConsumption: 10, withStealth: 58),
                ASensorModule(withDamage: 20, withWeight: 1, withStealth: 58),
                APropulsionModule(withThrust: 99, withConsumption: 10, withStealth: 58)
            };

            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withOriginalFrame: frameInAssembly,
                withModules: modulesList
            );

            var expectedModifiers = modulesList
                .Select(module => module.GetDroneModifiers())
                .Aggregate(ADroneModifiers(), (module, nextModule) => module + nextModule);
            
            var expectedDrone = ADrone(
                withFrame: frameInAssembly, 
                withModules: modulesList,
                withModifiers:  expectedModifiers + frameInAssembly.GetModifiers()
            );
            
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }

        [Test] 
        public void ReturnFullyAssembledDrone()
        {
            var onDroneBuilt = new Subject<Drone>();
            
            var frameInAssembly = AFrame(withHardness: 10, withArmorSlotsAmount:1,withBatterySlotsAmount: 4, withName: "caca", withWeight: 10);
            var modules = new[]
            {
                AnArmorModule(withHardness: 123, withName: "Radamantis"),
                AnArmorModule(withHardness: 2, withName: "Shiet", withWeight: 239),
                AWeaponModule(withDamage: 20, withWeight: 100, withWeaponType: WeaponType.Plasma),
                AWeaponModule(withDamage: 3, withWeight: 4, withWeaponType: WeaponType.Ballistic),
            };

            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withOriginalFrame: frameInAssembly,
                withModules: modules);
            
            var expectedDrone = ADroneWithModifiersFromParts(
                withFrame: frameInAssembly, 
                withModules: modules
            );
            
            Given(ABuildDrone(
                    events: SomeEvents(withDroneBuilt: onDroneBuilt),
                    services: SomeServices(withDroneAssembler: ALocalDroneAssemblerService()),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onDroneBuilt, it => it.Receives(expectedDrone))
                .Run();
        }
    }
}