﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.LoadModulesInPanelMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class LoadModulesInPanelTests
    {
        [Test]
        public void SendOnModulesInPanelLoaded()
        {
            var OnModulesInPanelsLoaded = new Subject<IEnumerable<Module>>();

            Given(ALoadModulesInPanel(withEvents: SomeEvents(withModulesInPanelsLoaded: OnModulesInPanelsLoaded)))
                .When(action => action.Do())
                .Then(_ => OnModulesInPanelsLoaded, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallGetAllModules()
        {
            var componentsRepository = AComponentsRepository();

            Given(ALoadModulesInPanel(
                    withRepositories: SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do())
                .Then(() => componentsRepository.Received(1).GetAllModules())
                .Run();
        }

        [Test]
        public void CallLoadAvailableModules()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(ALoadModulesInPanel(
                    withRepositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).LoadModulesForAssembly(Arg.Any<IEnumerable<Module>>()))
                .Run();
        }
        
        
        [Test]
        public void LoadExpectedModules()
        {
            var onModuleSelected = new Subject<IEnumerable<Module>>();
            var modules = new[] {AWeaponModule()};
            var componentsRepository = AnInMemoryComponentRepository(withModules: modules);
            var droneInProgressRepository = InMemoryDroneInProgressRepository();
            var expectedModules = modules;

            Given(ALoadModulesInPanel(
                    withRepositories: SomeRepositories(
                        withComponentsRepository: componentsRepository,
                        withDroneInProgressRepository: droneInProgressRepository),
                    withEvents: SomeEvents(withModulesInPanelsLoaded: onModuleSelected)
                ))
                .When(action => action.Do())
                .Then(_ => onModuleSelected, it => it.Receives(expectedModules))
                .Run();
        }
    }
}