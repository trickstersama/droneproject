﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class AddDroneToInventoryTests
    {
        [Test]
        public void SendOnDroneAddedToInventory()
        {
            var onDroneAddedToInventory = new Subject<IEnumerable<Drone>>();
            
            Given(new AddDroneToInventory(
                    SomeEvents(withDroneAddedToInventory: onDroneAddedToInventory),
                    SomeRepositories()
                ))
                .When(action => action.Do(new Drone()))
                .Then(_ => onDroneAddedToInventory, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallAddNewDrone()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new AddDroneToInventory(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(new Drone()))
                .Then(() => dronesRepository.Received(1).AddNewDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void ReturnAllIdleDronesPlusNewOne()
        {
            var onDroneAddedToInventory = new Subject<IEnumerable<Drone>>();
            
            var busyDrone = ADrone(withName: "drone1", withStatus: DroneStatus.Busy);
            var idleDrone = ADrone(withName: "drone2", withStatus: DroneStatus.Idle);
            var recentlyCraftedDrone = AFullyArmedDrone(withName: "recentlyCraftedDrone").ModifyThis(withStatus: DroneStatus.Idle);
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] {idleDrone, busyDrone});

            var expectedDrones = new[] {idleDrone, recentlyCraftedDrone};
            
            Given(new AddDroneToInventory(
                    SomeEvents(withDroneAddedToInventory: onDroneAddedToInventory),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(recentlyCraftedDrone))
                .Then(_ => onDroneAddedToInventory, it => it.Receives(expectedDrones))
                .Run();
        }
        
    }
}