﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class RemoveFrameFromInventoryTests
    {
        [Test]
        public void SendOnModulesInInventoryRemoved()
        {
            var onFrameFromInventoryRemoved = new Subject<IEnumerable<Frame>>();

            Given(new RemoveFrameFromInventory(
                    events: SomeEvents(withFrameFromInventoryRemoved: onFrameFromInventoryRemoved),
                    repositories: SomeRepositories()
                ))
                .When(action => action.Do(new Frame()))
                .Then(_ => onFrameFromInventoryRemoved, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallRemoveFrameInInventory()
        {
            var componentsRepository = AComponentsRepository();

            Given(new RemoveFrameFromInventory(
                    events: SomeEvents(),
                    repositories: SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do(new Frame()))
                .Then(() => componentsRepository.Received(1).RemoveFrame(Arg.Any<Frame>()))
                .Run();
        }
    }
}