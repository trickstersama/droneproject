﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;
using Arg = NSubstitute.Arg;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly

{
    [TestFixture]
    public class RemoveModulesFromInventoryTests
    {
        [Test]
        public void SendOnModulesInInventoryRemoved()
        {
            var OnModulesInInventoryRemoved = new Subject<IEnumerable<Module>>();

            Given(new RemoveModulesFromInventory(
                    events: SomeEvents(withModulesInInventoryRemoved: OnModulesInInventoryRemoved),
                    repositories: SomeRepositories()
                ))
                .When(action => action.Do(new List<Module>()))
                .Then(_ => OnModulesInInventoryRemoved, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallRemoveModulesInComponentRepository()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(new RemoveModulesFromInventory(
                    events: SomeEvents(),
                    repositories: SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do(new List<Module>()))
                .Then(() => componentsRepository.Received(1).RemoveModules(Arg.Any<IEnumerable<Module>>()))
                .Run();
        }
        
                
        [Test]
        public void UseAllModulesFromInventory()
        {
            var modules = new List<Module>
            {
                AnArmorModule(withName: "Carlos", withWeight: 1, withHardness: 1),
                AnArmorModule(withName: "Juan", withWeight: 2, withHardness: 2), 
                AWeaponModule(withName: "Diptongo", withWeight: 3, withDamage: 3), 
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4)
            };
            
            var componentsRepository = AnInMemoryComponentRepository(withModules: modules);
            var OnModulesInInventoryRemoved = new Subject<IEnumerable<Module>>();
            var modulesToRemove = modules;

            Given(new RemoveModulesFromInventory(
                    events: SomeEvents(withModulesInInventoryRemoved: OnModulesInInventoryRemoved),
                    repositories: SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do(modulesToRemove))
                .Then(_ => OnModulesInInventoryRemoved, it => it.Receives(new List<Module>()))
                .Run();
        }
        
        [Test]
        public void UseSomeModulesFromInventory()
        {
            var modulesInInventory = new List<Module>
            {
                AnArmorModule(withName: "Carlos", withWeight: 1, withHardness: 1),
                AnArmorModule(withName: "Juan", withWeight: 2, withHardness: 2), 
                AWeaponModule(withName: "Diptongo", withWeight: 3, withDamage: 3), 
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4),
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4),
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4)
            };
            
            var modulesToRemove = new List<Module>
            {
                AnArmorModule(withName: "Carlos", withWeight: 1, withHardness: 1),
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4)
            };
            
            var expectedModules = new List<Module>
            {
                AnArmorModule(withName: "Juan", withWeight: 2, withHardness: 2), 
                AWeaponModule(withName: "Diptongo", withWeight: 3, withDamage: 3), 
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4),
                AWeaponModule(withName: "Messi", withWeight: 4, withDamage: 4)
            }; 
            
            var componentsRepository = AnInMemoryComponentRepository(withModules: modulesInInventory);
            var OnModulesInInventoryRemoved = new Subject<IEnumerable<Module>>();
            
            

            Given(new RemoveModulesFromInventory(
                    events: SomeEvents(withModulesInInventoryRemoved: OnModulesInInventoryRemoved),
                    repositories: SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do(modulesToRemove))
                .Then(_ => OnModulesInInventoryRemoved, it => it.Receives(expectedModules))
                .Run();
        }
        
    }
}