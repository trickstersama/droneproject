﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.LoadFramePanelMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Workstation.Assembly
{
    [TestFixture]
    public class LoadFramePanelTests
    {
        [Test]
        public void SendOnFramePanelLoaded()
        {
            var OnFramePanelLoaded = new Subject<IEnumerable<Frame>>();
            
            Given(ALoadFramePanel(withEvents: SomeEvents(withFramePanelLoaded: OnFramePanelLoaded)))
                .When(action => action.Do())
                .Then(_ => OnFramePanelLoaded, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallGetFrames()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(ALoadFramePanel(withRepositories: SomeRepositories(withComponentsRepository: componentsRepository)))
                .When(action => action.Do())
                .Then(() => componentsRepository.Received(1).GetFrames())
                .Run();
        }
        
        [Test]
        public void CallLoadFramesForAssembly()
        {
            var droneInProgressRepository = ADroneInProgressRepository();
            
            Given(ALoadFramePanel(withRepositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).LoadFramesForAssembly(Arg.Any<IEnumerable<Frame>>()))
                .Run();
        }

        [Test]
        public void LoadAllAvailableFramesOnRepository()
        {
            var inMemoryFrames = new[] {AFrame(withName: "#1"), AFrame(withName: "#2")};
            var componentsRepository = AnInMemoryComponentRepository(withFrames: inMemoryFrames);
            var OnFramePanelLoaded = new Subject<IEnumerable<Frame>>();

            var expectedFrames = inMemoryFrames;
            
            Given(ALoadFramePanel(
                    withEvents: SomeEvents(withFramePanelLoaded: OnFramePanelLoaded),
                    withRepositories: SomeRepositories(
                        withComponentsRepository: componentsRepository, 
                        withDroneInProgressRepository: InMemoryDroneInProgressRepository())
                ))
                .When(action => action.Do())
                .Then(_ => OnFramePanelLoaded, it => it.Receives(expectedFrames))
                .Run();
        }
    }
}