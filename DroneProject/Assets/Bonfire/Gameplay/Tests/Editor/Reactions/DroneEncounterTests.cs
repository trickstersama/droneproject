﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneEncounterMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.EncounterDeciderMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EventMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions
{
    public class DroneEncounterTests
    {
        [Test]
        public void DroneEncounterCallsOnEncounterStart()
        {

            var onEncounterStart = AnEvent<Encounter>();

            Given(ADroneEncounter(
                    withGameplayEvents: SomeEvents(withEncounterStart: onEncounterStart))
                    )
                .When(action => action.Do())
                .Then(_ => onEncounterStart, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void ConsumeEncounterServiceBasedOnDrone()
        {
            var encounterDecider = AnEncounterDecider();

            Given(ADroneEncounter(
                    withGameplayServices: SomeServices(withDroneEncounter: encounterDecider)
                ))
                .When(action => action.Do())
                .Then(() => encounterDecider.Received(1).DetermineEncounterType())
                .Run();
        }


    }
}