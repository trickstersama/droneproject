using Bonfire.Gameplay.Domain.Configuration;
using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.Repositories;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using Bonfire.Gameplay.Tests.Editor.Mothers.Repositories;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MapCoordinatesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.ModifiersRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostModifiersMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Computer
{
    [TestFixture]
    public class CalculateMissionTimeCostsTests
    {
        [Test]
        public void SendOnMissionTimeCostsCalculated()
        {
            
            var onMissionTimeCostsCalculated = new Subject<MissionOrders>();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(withMissionTimeCostsCalculated: onMissionTimeCostsCalculated),
                    SomeServices(),
                    SomeRepositories()
                ))
                .When(reaction => reaction.Do())
                .Then(_ => onMissionTimeCostsCalculated, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallCalculateTimeCost_FromMissionService()
        {

            var missionService = AMissionService();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(),
                    SomeServices(withMissionService: missionService),
                    SomeRepositories()
                ))
                .When(reaction => reaction.Do())
                .Then( () => missionService.Received(1).CalculateTimeCost(Arg.Any<MissionOrders>(), Arg.Any<MissionsModifiers>(), Arg.Any<Drone>()))
                .Run();
        }
        
        
        [Test]
        public void CallAddTimeCost_FromMissionBuilderRepository()
        {

            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(),
                    SomeServices(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(reaction => reaction.Do())
                .Then( () => missionBuilderRepository.Received(1).AddTimeCost(Arg.Any<MissionTimeCost>()))
                .Run();
        }
        
        [Test]
        public void CallGetMissionOrders_FromMissionBuilderRepository()
        {

            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(),
                    SomeServices(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(reaction => reaction.Do())
                .Then( () => missionBuilderRepository.Received(1).AddTimeCost(Arg.Any<MissionTimeCost>()))
                .Run();
        }
        
        [Test]
        public void CallGetTimeCostModifiers_FromModifiersRepository()
        {

            var modifiersRepository = Substitute.For<ModifiersRepository>();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(),
                    SomeServices(),
                    SomeRepositories(withModifiersRepository: modifiersRepository)
                ))
                .When(reaction => reaction.Do())
                .Then( () => modifiersRepository.Received(1).GetTimeCostModifiers())
                .Run();
        }
        
        [Test]
        public void CallGetDroneByName_FromDronesRepository()
        {

            var dronesRepository = ADronesRepository();
            
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(),
                    SomeServices(),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(reaction => reaction.Do())
                .Then( () => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void SendExpectedTimeCost()
        {
            var modifiers = ADroneModifiers(
                withAerodynamics: 0.1f,
                withCrossSectionArea: 0.04f,
                withWeight: 1, 
                withThrust: 35, 
                withReservedWeight: 2
            );
            var droneName = "Drone Name";
            var drone = ADrone(withCharge: 100, withModifiers: modifiers, withName: droneName);
            var mapCoordinates = AMapCoordinates(withX: 2, withY: 4);

            var missionTimeCostModifiers =
                AMissionTimeCostModifiers(
                    withMissionMultiplier: 2f, withTravelMultiplier: 10f
                );
            
            var missionOrders = AMissionOrders(
                withDroneName: droneName,
                withMapCoordinates: mapCoordinates,
                withMissionType: MissionType.Battle
            );

            var onMissionTimeCostsCalculated = new Subject<MissionOrders>();

            var missionBuilderRepository = AnInMemoryMissionBuilderRepository(withMissionOrders: missionOrders);
            var modifiersRepository = AModifiersRepository(withMissionTimeCostModifiers: missionTimeCostModifiers);
            var expectedTimeCost = AMissionTimeCost(
                withTimeOnTarget: 5f * missionTimeCostModifiers.missionMultiplier,
                withTimeToTarget: 44.09f * missionTimeCostModifiers.travelMultiplier,
                withToBaseWithMaxAllowed: 1f * missionTimeCostModifiers.travelMultiplier,
                withTimeToBaseWithMaxWeight: 93.541f *missionTimeCostModifiers.travelMultiplier
            );

            var missionConfiguration = new MissionConfiguration(5, 5, 5);
            var expectedMissionOrders = AMissionOrders(
                withDroneName: droneName,
                withMapCoordinates: mapCoordinates,
                withMissionType: MissionType.Battle,
                withTimeCost: expectedTimeCost
            );

            var dronesRepository = DronesRepositoryMother.AnInMemoryDronesRepository(withDrones: new[] { drone });
            Given(new CalculateMissionTimeCosts(
                    SomeEvents(withMissionTimeCostsCalculated: onMissionTimeCostsCalculated),
                    SomeServices(withMissionService: ALocalMissionService(missionConfiguration)),
                    SomeRepositories(
                        withMissionBuilderRepository: missionBuilderRepository,
                        withModifiersRepository: modifiersRepository,
                        withDronesRepository: dronesRepository)
                ))
                .When(reaction => reaction.Do())
                .Then(_ => onMissionTimeCostsCalculated, it => it.Receives(expectedMissionOrders))
                .Run();
        }
    }
}