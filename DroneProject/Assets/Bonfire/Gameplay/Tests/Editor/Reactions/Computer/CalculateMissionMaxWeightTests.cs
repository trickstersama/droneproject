﻿using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Computer
{
    [TestFixture]
    public class CalculateMissionMaxWeightTests
    {
        [Test]
        public void SendOnMissionMaxWeightCalculated()
        {
            var onMissionMaxWeightAllowed = new Subject<MissionOrders>();
            
            Given(new CalculateMissionMaxWeight(
                    SomeEvents(withMissionMaxWeightAllowed: onMissionMaxWeightAllowed),
                    SomeRepositories(),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(_ => onMissionMaxWeightAllowed, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetMissionOrders_FromMissionBuilderRepository()
        {
            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new CalculateMissionMaxWeight(
                    SomeEvents(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).GetMissionOrders())
                .Run();
        }
        
        [Test]
        public void CallAddMaxWeightAllowed_FromMissionBuilderRepository()
        {
            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new CalculateMissionMaxWeight(
                    SomeEvents(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).GetMissionOrders())
                .Run();
        }
        [Test]
        public void CallGetDroneByName_FromDroneRepository()
        {
            var droneRepository = ADronesRepository();
            
            Given(new CalculateMissionMaxWeight(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: droneRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => droneRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }

        [Test]
        public void SendExpectedMaxWeight()
        {
            var onMissionMaxWeightAllowed = new Subject<MissionOrders>();
            
            var modifiers = ADroneModifiers(
                withAerodynamics: 0.1f,
                withCrossSectionArea: 0.04f,
                withWeight: 1, 
                withThrust: 100, 
                withReservedWeight: 2
            );
            
            var drone = ADrone(withCharge: 100, withModifiers: modifiers);

            var oldMissionOrders = AMissionOrders(withDroneName: drone.droneName);

            var missionBuilderRepository = AnInMemoryMissionBuilderRepository(
                withMissionOrders: oldMissionOrders,
                withMissionReadyToCalculate: true
            );
            var droneRepository = AnInMemoryDronesRepository(withDrones: new []{drone});
            
            var expected = AMissionOrders(
                withDroneName: drone.droneName,
                withMaxWeightAllowed: 4.6f
            );
            
            Given(new CalculateMissionMaxWeight(
                    SomeEvents(withMissionMaxWeightAllowed: onMissionMaxWeightAllowed),
                    SomeRepositories(
                        withMissionBuilderRepository: missionBuilderRepository,
                        withDronesRepository: droneRepository
                    ),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do())
                .Then(_ => onMissionMaxWeightAllowed, it => it.Receives(expected))
                .Run();
            
        }
    }
}