﻿using Bonfire.Gameplay.Domain.Reactions.Computer;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Computer
{
    [TestFixture]
    public class MissionServicesReadyToCalculateTests
    {
        [Test] 
        public void SendOnMissionServicesCalculated()
        {
            var onMissionServicesReadyToCalculate = new Subject<Unit>();
            
            Given(new MissionServicesReadyToCalculate(
                    SomeEvents(withMissionServicesReadyToCalculate: onMissionServicesReadyToCalculate),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onMissionServicesReadyToCalculate, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnMissionServicesCalculated_WhenIsNotReady()
        {
            var onMissionServicesReadyToCalculate = new Subject<Unit>();
            var missionBuilderRepository = AMissionBuilderRepository(withMissionReadyToCalculate: false);

            
            Given(new MissionServicesReadyToCalculate(
                    SomeEvents(withMissionServicesReadyToCalculate: onMissionServicesReadyToCalculate),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onMissionServicesReadyToCalculate, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void CallIsMissionReadyToCalculate()
        {
            var missionBuilderRepository = AMissionBuilderRepository();

            Given(new MissionServicesReadyToCalculate(
                    SomeEvents(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).IsMissionReadyToCalculate())
                .Run();
        }
        
        [Test]
        public void CallGetMissionOrders()
        {
            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new MissionServicesReadyToCalculate(
                    SomeEvents(),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).GetMissionOrders())
                .Run();
        }
    }
}