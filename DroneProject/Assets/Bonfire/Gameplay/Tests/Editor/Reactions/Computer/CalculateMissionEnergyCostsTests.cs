using Bonfire.Gameplay.Domain.Reactions.Computer;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MapCoordinatesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionEnergyCostMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Computer
{
    public class CalculateMissionEnergyCostsTests
    {
        [Test]
        public void SendOnEnergyCostsCalculated()
        {
            var onEnergyCostsCalculated = new Subject<MissionOrders>();
            
            Given(new CalculateMissionEnergyCosts(
                    SomeEvents(withEnergyCostsCalculated: onEnergyCostsCalculated),
                    SomeServices(),
                    SomeRepositories()
                ))
                .When(reaction => reaction.Do())
                .Then(_ => onEnergyCostsCalculated, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
         public void CallCalculateEnergyCosts_FromMissionService()
         {
             var missionService = AMissionService();
             
             Given(new CalculateMissionEnergyCosts(
                     SomeEvents(),
                     SomeServices(withMissionService: missionService),
                     SomeRepositories()
                 ))
                 .When(reaction => reaction.Do())
                 .Then(() => missionService.Received(1).CalculateEnergyCosts(Arg.Any<MissionOrders>(), Arg.Any<Drone>()))
                 .Run();
         }
         
         [Test]
         public void CallAddEnergyCost_FromRepository()
         {
             var missionBuilderRepository = AMissionBuilderRepository();
             
             Given(new CalculateMissionEnergyCosts(
                     SomeEvents(),
                     SomeServices(),
                     SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                 ))
                 .When(reaction => reaction.Do())
                 .Then(() => missionBuilderRepository.Received(1).AddEnergyCost(Arg.Any<MissionEnergyCost>()))
                 .Run();
         }
         
         [Test]
         public void CallGetMissionOrders_FromRepository()
         {
             var missionBuilderRepository = AMissionBuilderRepository();
             
             Given(new CalculateMissionEnergyCosts(
                     SomeEvents(),
                     SomeServices(),
                     SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                 ))
                 .When(reaction => reaction.Do())
                 .Then(() => missionBuilderRepository.Received(1).AddEnergyCost(Arg.Any<MissionEnergyCost>()))
                 .Run();
         }
         
         [Test]
         public void CallGetDroneByName_FromDronesRepository()
         {
             var dronesRepository = ADronesRepository();
             
             Given(new CalculateMissionEnergyCosts(
                     SomeEvents(),
                     SomeServices(),
                     SomeRepositories(withDronesRepository: dronesRepository)
                 ))
                 .When(reaction => reaction.Do())
                 .Then(() => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                 .Run();
         }

         
         [Test]
         public void CalculateEnergyCost()
         {
             var modifiers = ADroneModifiers(
                 withAerodynamics: 0.1f,
                 withCrossSectionArea: 0.04f,
                 withWeight: 1, 
                 withThrust: 35, 
                 withReservedWeight: 2
             );

             var droneName = "DroneName";
             var drone = ADrone(withCharge: 100, withModifiers: modifiers, withName: droneName);
             var mapCoordinates = AMapCoordinates(withX: 2, withY: 4);

             var oldOrders = AMissionOrders(
                 withDroneName: droneName,
                 withMapCoordinates: mapCoordinates,
                 withMissionType: MissionType.Battle
             );

             var expectedEnergyCost = AMissionEnergyCost(
                 withEnergyToTarget: 1543.3f,
                 withEnergyToReturnWithMaxLoad: 9821.8f,
                 TBD: 0
             );
             
             var expectedOrders = AMissionOrders(
                 withDroneName: droneName,
                 withMapCoordinates: mapCoordinates,
                 withMissionType: MissionType.Battle,
                 withMissionEnergyCost: expectedEnergyCost 
             );

             var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] { drone });
             var onEnergyCostsCalculated = new Subject<MissionOrders>();
             var missionBuilderRepository = AnInMemoryMissionBuilderRepository(withMissionOrders: oldOrders);

             
             Given(new CalculateMissionEnergyCosts(
                     SomeEvents(withEnergyCostsCalculated: onEnergyCostsCalculated),
                     SomeServices(withMissionService: ALocalMissionService()),
                     SomeRepositories(
                         withMissionBuilderRepository: missionBuilderRepository,
                         withDronesRepository: dronesRepository)
                 ))
                 .When(reaction => reaction.Do())
                 .Then(_ => onEnergyCostsCalculated, it => it.Receives(expectedOrders))
                 .Run();
         }
    }
}