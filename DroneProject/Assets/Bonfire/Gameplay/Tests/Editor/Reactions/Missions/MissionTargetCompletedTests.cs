﻿using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions
{
    [TestFixture]
    public class MissionTargetCompletedTests
    {
        [Test]
        public void SendOnMissionTargetCompleted_WhenHasEnoughTimeAndCorrectStatus()
        {
            var droneName = "PenisMcBalls";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.OnTarget);
            var onMissionTargetCompleted = new Subject<Mission>();
            var mission = AMission(
                withTimeOnMission: ATimeOnMission(200f),
                withMissionOrders: AMissionOrders(
                    withDroneName: droneName,
                    withTimeCost: AMissionTimeCost(
                        withTimeOnTarget: 100f,
                        withTimeToTarget: 99f)
                )
            );

            var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] {drone});
            
            Given(new MissionTargetCompleted(
                    SomeEvents(withMissionTargetCompleted: onMissionTargetCompleted),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetCompleted, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnMissionTargetCompleted_WhenHasNotEnoughTime()
        {
            var droneName = "PenisMcBalls";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.OnTarget);
            var onMissionTargetCompleted = new Subject<Mission>();
            var mission = AMission(
                withTimeOnMission: ATimeOnMission(198f),
                withMissionOrders: AMissionOrders(
                    withDroneName: droneName,
                    withTimeCost: AMissionTimeCost(
                        withTimeOnTarget: 100f,
                        withTimeToTarget: 99f)
                )
            );

            var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] {drone});
            
            Given(new MissionTargetCompleted(
                    SomeEvents(withMissionTargetCompleted: onMissionTargetCompleted),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetCompleted, it => it.ReceivesNothing())
                .Run();
        }
        [Test]
        public void DoNotSendOnMissionTargetCompleted_WhenHasNotCorrectStatus()
        {
            var droneName = "PenisMcBalls";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Traveling);
            var onMissionTargetCompleted = new Subject<Mission>();
            var mission = AMission(
                withTimeOnMission: ATimeOnMission(200f),
                withMissionOrders: AMissionOrders(
                    withDroneName: droneName,
                    withTimeCost: AMissionTimeCost(
                        withTimeOnTarget: 100f,
                        withTimeToTarget: 99f)
                )
            );

            var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] {drone});
            
            Given(new MissionTargetCompleted(
                    SomeEvents(withMissionTargetCompleted: onMissionTargetCompleted),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetCompleted, it => it.ReceivesNothing())
                .Run();
        }

        [Test]
        public void CallGetDroneByNameFrom_DronesRepository()
        {
            var onMissionTargetCompleted = new Subject<Mission>();
            var dronesRepository = ADronesRepository();
            
            Given(new MissionTargetCompleted(
                    SomeEvents(withMissionTargetCompleted: onMissionTargetCompleted),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(AMission()))
                .Then(() => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void CallTimeNeededToCompleteTarget_fromMissionsService()
        {
            var onMissionTargetCompleted = new Subject<Mission>();
            var missionService = AMissionService();
            var drone = ADrone(withName: "Penis", withStatus: DroneStatus.OnTarget);
            var dronesRepository = ADronesRepository(withDroneByName: drone);
            
            Given(new MissionTargetCompleted(
                    SomeEvents(withMissionTargetCompleted: onMissionTargetCompleted),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do(AMission()))
                .Then(() => missionService.Received(1).TimeNeededToCompleteTarget(Arg.Any<Mission>()))
                .Run();
        }
    }
}