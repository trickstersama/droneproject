﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.MissionsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions
{
    [TestFixture]
    public class UpdateMissionsTimeTests
    {
        [Test]
        public void SendOnMissionsInProgressUpdated()
        {
            var onMissionTimeUpdated = new Subject<IEnumerable<Mission>>();
            
            Given(new UpdateMissionsTime(
                    SomeEvents(withMissionTimeUpdated: onMissionTimeUpdated),
                    SomeRepositories(),
                    SomeServices()
                ))
                .When(action => action.Do(1f))
                .Then(_ => onMissionTimeUpdated, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetAllMissions_FromMissionsRepository()
        {
            var missionsRepository = AMissionsRepository();
            
            Given(new UpdateMissionsTime(
                    SomeEvents(),
                    SomeRepositories(withMissionsRepository: missionsRepository),
                    SomeServices()
                ))
                .When(action => action.Do(1f))
                .Then(() => missionsRepository.Received(1).GetAllMissions())
                .Run();
        }
        
        [Test]
        public void CallUpdateTime_OnMissions()
        {
            var missionService = AMissionService();
            
            Given(new UpdateMissionsTime(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do(1f))
                .Then(() => missionService.Received(1).UpdateTime(Arg.Any<Mission>(), Arg.Any<float>()))
                .Run();
        }
        
        [Test]
        public void CallUpdateMissions_OnMissionsRepository()
        {
            var missionsRepository = AMissionsRepository(withMissions: new[] {new Mission()});
            Given(new UpdateMissionsTime(
                    SomeEvents(),
                    SomeRepositories(withMissionsRepository: missionsRepository),
                    SomeServices()
                ))
                .When(action => action.Do(1f))
                .Then(() => missionsRepository.Received(1).UpdateMissions(Arg.Any<IEnumerable<Mission>>()))
                .Run();
        }
        
        [Test]
        public void Emit2MissionsUpdated()
        {
            var timePassed = 1f;
            var firstMission = AMission(withTimeOnMission: ATimeOnMission(10f));
            var secondMission = AMission(withMissionOrders: AMissionOrders(withDroneName: "pepe"));
            
            var onMissionTimeUpdated = new Subject<IEnumerable<Mission>>();
            var missionsRepository = AnInMemoryMissionsRepository(withMissions: new[] {firstMission, secondMission});
            var missionService = ALocalMissionService();

            var expectedMissions = new[] {
                AMission(withTimeOnMission: ATimeOnMission(timePassed + 10f)),
                AMission(
                    withMissionOrders: secondMission.missionOrders,
                    withTimeOnMission: ATimeOnMission(timePassed))
            };
            
            Given(new UpdateMissionsTime(
                    SomeEvents(withMissionTimeUpdated: onMissionTimeUpdated),
                    SomeRepositories(withMissionsRepository: missionsRepository),
                    SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do(timePassed))
                .Then(_ => onMissionTimeUpdated, it => it.Receives(expectedMissions))
                .Run();
        }
    }
}