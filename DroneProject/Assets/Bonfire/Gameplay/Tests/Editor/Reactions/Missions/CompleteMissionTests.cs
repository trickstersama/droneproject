﻿using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions

{
    [TestFixture]
    public class CompleteMissionTests
    {
        [Test]
        public void SendOnDroneArrivedBalcony()
        {
            var onMissionCompleted = new Subject<Unit>();
            
            Given(new CompleteMission(
                    SomeEvents(withMissionCompleted: onMissionCompleted),
                    SomeRepositories()
                ))
                .When(action => action.Do(AMission()))
                .Then(_ => onMissionCompleted,it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetDroneByName_FromDronesRepository()
        {
            var dronesRepository = ADronesRepository(); 
            
            Given(new CompleteMission(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(AMission()))
                .Then(() => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void UpdateDroneInRepository()
        {
            var droneName = "Carlos";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Returning);
            var mission = AMission(withMissionOrders: AMissionOrders(withDroneName: droneName));
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});
            var expectedDrone = drone.ModifyThis(withStatus: DroneStatus.InBalcony);


            Given(new CompleteMission(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(mission))
                .Then(() => Assert.AreEqual(expectedDrone, dronesRepository.GetDroneByName(droneName)))
                .Run();
        }
    }
}