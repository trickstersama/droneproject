﻿using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions
{
    [TestFixture]
    public class MissionTargetReachedTests
    {

        [Test]
        public void CallGetDroneByName_FromDronesRepository()
        {
            var droneRepository = ADronesRepository();
            
            Given(new MissionTargetReached(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: droneRepository),
                    SomeServices()
                ))
                .When(action => action.Do(AMission()))
                .Then( () => droneRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void CallTimeNeededToTarget_fromMissionsService()
        {
            var missionService = AMissionService();
            var droneName = "DroneName";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Traveling);
            var dronesRepository = ADronesRepository(withDroneByName: drone);

            Given(new MissionTargetReached(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do(AMission()))
                .Then( () => missionService.Received(1).TimeNeededToTarget(Arg.Any<Mission>()))
                .Run();
        }
        
        

        [Test]
        public void DoNotSend_WhenDroneIsNotInTravelState()
        {
            var onMissionTargetReached = new Subject<Mission>();
            var droneName = "DroneName";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.OnTarget);
            
            var mission = AMission(withMissionOrders: AMissionOrders(withDroneName: droneName));
            
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});

            Given(new MissionTargetReached(
                    SomeEvents(withMissionTargetReached: onMissionTargetReached),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetReached, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void DoNotSend_WhenEnoughTimeHasNotPassed()
        {
            var timeToTarget = 100;
            var onMissionTargetReached = new Subject<Mission>();
            var droneName = "DroneName";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Traveling);

            var mission = AMission(
                withMissionOrders: AMissionOrders(
                    withTimeCost: AMissionTimeCost(withTimeToTarget: timeToTarget),
                    withDroneName: droneName),
                withTimeOnMission: ATimeOnMission(99)
            );
            
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});

            Given(new MissionTargetReached(
                    SomeEvents(withMissionTargetReached: onMissionTargetReached),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetReached, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void Send_WhenTimeHasPassedAndDroneIsTraveling()
        {
            var timeToTarget = 98;
            var onMissionTargetReached = new Subject<Mission>();
            var droneName = "DroneName";
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Traveling);

            var mission = AMission(
                withMissionOrders: AMissionOrders(
                    withTimeCost: AMissionTimeCost(withTimeToTarget: timeToTarget),
                    withDroneName: droneName),
                withTimeOnMission: ATimeOnMission(99)
            );
            
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});

            Given(new MissionTargetReached(
                    SomeEvents(withMissionTargetReached: onMissionTargetReached),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: ALocalMissionService())
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionTargetReached, it => it.ReceivesSomething())
                .Run();
        }
    }
}