﻿using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionTimeCostMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions
{
    [TestFixture]
    public class MissionReturnsHomeTests
    {
        [Test]
        public void SendOnMissionReturnedHomeWhen_IsReturningAndHasEnoughTime()
        {
            var onMissionReturnedHome = new Subject<Mission>();
            var name = "pepe";
            var drone = ADrone(withName: name, withStatus: DroneStatus.Returning);
            var missionOrders = AMissionOrders(withTimeCost: AMissionTimeCost(
                    withTimeOnTarget: 10,
                    withTimeToTarget: 10,
                    withToBaseWithMaxAllowed: 10,
                    withTimeToBaseWithMaxWeight: 10
                ),
                withDroneName: name
            );
            var mission = AMission(withMissionOrders: missionOrders, withTimeOnMission: ATimeOnMission(31));
            var dronesRepository = ADronesRepository(withDroneByName: drone);

            Given(new MissionReturnsHome(
                    SomeEvents(withMissionReturnedHome: onMissionReturnedHome),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionReturnedHome, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetDroneByName_FromDronesRepository()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new MissionReturnsHome(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(AMission()))
                .Then(() => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }

        [Test]
        public void CallTimeNeededToReturn_fromMissionsService()
        {
            var missionService = AMissionService();
            var drone = ADrone(withName: "pepe", withStatus: DroneStatus.Returning);
            var dronesRepository = ADronesRepository(withDroneByName: drone);
            
            Given(new MissionReturnsHome(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do(AMission()))
                .Then(() => missionService.Received(1).TimeNeededToReturn(Arg.Any<Mission>()))
                .Run();
        }
        
        [Test]
        public void DoNotSendOnMissionReturnedHomeWhen_IsNotReturningAndHasEnoughTime()
        {
            var onMissionReturnedHome = new Subject<Mission>();
            var name = "pepe";
            var drone = ADrone(withName: name, withStatus: DroneStatus.Idle);
            var missionOrders = AMissionOrders(withTimeCost: AMissionTimeCost(
                    withTimeOnTarget: 10,
                    withTimeToTarget: 10,
                    withToBaseWithMaxAllowed: 10,
                    withTimeToBaseWithMaxWeight: 10
                ),
                withDroneName: name
            );
            var mission = AMission(withMissionOrders: missionOrders, withTimeOnMission: ATimeOnMission(31));
            var dronesRepository = ADronesRepository(withDroneByName: drone);

            Given(new MissionReturnsHome(
                    SomeEvents(withMissionReturnedHome: onMissionReturnedHome),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionReturnedHome, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnMissionReturnedHomeWhen_IsReturningAndHasNotEnoughTime()
        {
            var onMissionReturnedHome = new Subject<Mission>();
            var name = "pepe";
            var drone = ADrone(withName: name, withStatus: DroneStatus.Idle);
            var missionOrders = AMissionOrders(withTimeCost: AMissionTimeCost(
                    withTimeOnTarget: 10,
                    withTimeToTarget: 10,
                    withToBaseWithMaxAllowed: 10,
                    withTimeToBaseWithMaxWeight: 10
                ),
                withDroneName: name
            );
            var mission = AMission(withMissionOrders: missionOrders, withTimeOnMission: ATimeOnMission(29));
            var dronesRepository = ADronesRepository(withDroneByName: drone);

            Given(new MissionReturnsHome(
                    SomeEvents(withMissionReturnedHome: onMissionReturnedHome),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(mission))
                .Then(_ => onMissionReturnedHome, it => it.ReceivesNothing())
                .Run();
        }
    }
}