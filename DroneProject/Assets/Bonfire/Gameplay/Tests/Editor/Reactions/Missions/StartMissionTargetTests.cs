﻿using Bonfire.Gameplay.Domain.Reactions.Missions;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.Missions
{
    [TestFixture]
    public class StartMissionTargetTests
    {
        [Test]
        public void SendOnMissionStarted()
        {
            var onMissionTargetStarted = new Subject<Unit>();
            
            Given(new StartMissionTarget(
                    SomeEvents(withMissionTargetStarted: onMissionTargetStarted),
                    SomeRepositories()
                ))
                .When(action => action.Do(AMission()))
                .Then(_ => onMissionTargetStarted, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetDroneFrom_DronesRepository()
        {
            var onMissionTargetStarted = new Subject<Unit>();
            var dronesRepository = ADronesRepository();
            
            Given(new StartMissionTarget(
                    SomeEvents(withMissionTargetStarted: onMissionTargetStarted),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(AMission()))
                .Then(() => dronesRepository.Received(1).GetDroneByName(Arg.Any<string>()))
                .Run();
        }
        
        
        [Test]
        public void UpdateDroneStatusToOnTarget()
        {
            var droneName = "Carlos";
            var onMissionTargetStarted = new Subject<Unit>();
            var drone = ADrone(withName: droneName, withStatus: DroneStatus.Traveling);
            var mission = AMission(withMissionOrders: AMissionOrders(withDroneName: droneName));
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});
            var expectedDrone = drone.ModifyThis(withStatus: DroneStatus.OnTarget);
            
            Given(new StartMissionTarget(
                    SomeEvents(withMissionTargetStarted: onMissionTargetStarted),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do(mission))
                .Then(() => Assert.AreEqual(expectedDrone, dronesRepository.GetDroneByName(droneName)))
                .Run();
        }
    }
}