using System;
using Bonfire.Gameplay.Domain.Wrappers;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.House
{
    [TestFixture]
    public class UpdateDronesInMissionTests
    {
        [Test]
        public void SendOnDronesInMissionUpdated()
        {
            var onDronesInMissionUpdated = new Subject<Unit>();
            
            Given(new UpdateDronesInMission(
                    events: SomeEvents(withDronesInMissionUpdated: onDronesInMissionUpdated)
                ))
                .When(action => action.Do())
                .Then(_ =>onDronesInMissionUpdated,  it => it.ReceivesSomething())
                .Run();
        }
    }

    public class UpdateDronesInMission
    {
        readonly IObserver<Unit> onDronesInMissionUpdated;

        public UpdateDronesInMission(GameplayEvents events)
        {
            onDronesInMissionUpdated = events.OnDronesInMissionUpdated;
        }

        public void Do()
        {
            onDronesInMissionUpdated.OnNext(Unit.Default);
        }
    }
}