﻿using Bonfire.Gameplay.Domain.Reactions.House;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EnergyGeneratorMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.HouseGameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.HouseBatteryMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.House
{
    [TestFixture]
    public class StoreEnergyTests
    {
        [Test]
        public void SendOnEnergyStoredWhenStorageIsNotFull()
        {
            var onEnergyStored = new Subject<float>();
            var houseResourcesRepository = AHouseResourcesRepository(withStorageFull: false);
            
            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStored: onEnergyStored),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyStored, it => it.ReceivesSomething())
                .Run();
        }
        [Test]
        public void DoNotSendOnEnergyStoredWhenStorageIsFull()
        {
            var onEnergyStored = new Subject<float>();
            var houseResourcesRepository = AHouseResourcesRepository(withStorageFull: true);
            
            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStored: onEnergyStored),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyStored, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void CallGetEnergyStorageStatusFromResourcesRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();

            Given(new StoreEnergy(
                    SomeHouseEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(() => houseResourcesRepository.Received(1).IsEnergyStorageFull())
                .Run();
        }
        
        [Test]
        public void CallStoreEnergyIfStorageIsNotFull()
        {
            var houseResourcesRepository = AHouseResourcesRepository(withStorageFull: false);

            Given(new StoreEnergy(
                    SomeHouseEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(() => houseResourcesRepository.Received(1).StoreEnergy(Arg.Any<float>()))
                .Run();
        }
        
        [Test]
        public void SendOnEnergyStorageFull()
        {
            var houseResourcesRepository = AHouseResourcesRepository(withStorageFull: true);
            var onEnergyStorageFull = new Subject<Unit>();

            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStorageFull: onEnergyStorageFull),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyStorageFull, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendSendOnEnergyStorageFull_WhenStorageIsNotFull()
        {
            var houseResourcesRepository = AHouseResourcesRepository(withStorageFull: false);
            var onEnergyStorageFull = new Subject<Unit>();

            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStorageFull: onEnergyStorageFull),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyStorageFull, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void AggregateToRepositoryWhenStorageIsEmpty()
        {
            var overHeadAmount = 50f;
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(
                withEnergyStored: 0,
                withSolarGenerators:new [] { ASolarEnergyGenerator(withEnergyPerSecond: overHeadAmount)},
                withHouseBatteries: new[]{AHouseBattery(withStorageSize: 1000f)}
            );
            var onEnergyStored = new Subject<float>();

            var expected = overHeadAmount;
            
            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStored: onEnergyStored),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(overHeadAmount))
                .Then(_ => onEnergyStored, it => it.Receives(expected))
                .Run();
        }
        
        [Test]
        public void SendStorageIsFull()
        {
            var overHeadAmount = 50f;
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(
                withEnergyStored: 1000,
                withHouseBatteries: new []{AHouseBattery(withStorageSize: 1000f)}
            );
            var onEnergyStorageFull = new Subject<Unit>();

            Given(new StoreEnergy(
                    SomeHouseEvents(withEnergyStorageFull: onEnergyStorageFull),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(overHeadAmount))
                .Then(_ => onEnergyStorageFull, it => it.ReceivesSomething())
                .Run();
        }
    }
}