﻿using Bonfire.Gameplay.Domain.Reactions.House;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.HouseGameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.House
{
    [TestFixture]
    public class EmitTimelinesTests
    {
        [Test]
        public void SendOnEnergyTimelineEmitted()
        {
            var onEnergyTimelineEmitted = new Subject<float>();
            
            Given(new EmitTimelines(
                    SomeHouseEvents(withEnergyTimelineEmitted: onEnergyTimelineEmitted),
                    SomeEvents()))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyTimelineEmitted, it => it.ReceivesSomething())
                .Run();
        }
        [Test]
        public void SendOnMissionsTimelineEmitted()
        {
            var onMissionsTimelineEmitted = new Subject<float>();
            
            Given(new EmitTimelines(
                    SomeHouseEvents(),
                    SomeEvents(withMissionsTimelineEmitted: onMissionsTimelineEmitted)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onMissionsTimelineEmitted, it => it.ReceivesSomething())
                .Run();
        }
    }
}