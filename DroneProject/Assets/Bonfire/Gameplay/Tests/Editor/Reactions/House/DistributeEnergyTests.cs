﻿using Bonfire.Gameplay.Domain.Reactions.House;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.HouseGameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.House
{
    [TestFixture]
    public class DistributeEnergyTests
    {
        [Test]
        public void SendOnEnergyOverheadProduced_WhenExistsOverhead()
        {
            var onEnergyOverheadProduced = new Subject<float>();
            var houseResourcesRepository = AHouseResourcesRepository(withEnergyConsumption: 0);
            var energyProduction = 1f;

            Given(new DistributeEnergy(
                    SomeHouseEvents(withEnergyOverheadProduced: onEnergyOverheadProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(energyProduction))
                .Then(_ => onEnergyOverheadProduced, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetEnergyConsumptionFromRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new DistributeEnergy(
                    SomeHouseEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(() => houseResourcesRepository.Received(1).GetEnergyConsumptionPerSecond())
                .Run();
        }

        
        [Test]
        public void SendOnInsufficientEnergyIsProduced_WhenConsumptionIsGreaterThanProduction()
        {
            var OnInsufficientEnergyIsProduced = new Subject<Unit>();
            var houseResourcesRepository = AHouseResourcesRepository(withEnergyConsumption: 10);
            var energyProduction = 1f;
            
            Given(new DistributeEnergy(
                    SomeHouseEvents(withInsufficientEnergyIsProduced: OnInsufficientEnergyIsProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(energyProduction))
                .Then(_ => OnInsufficientEnergyIsProduced, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnInsufficientEnergyIsProduced_WhenExistsOverhead()
        {
            var OnInsufficientEnergyIsProduced = new Subject<Unit>();
            var houseResourcesRepository = AHouseResourcesRepository(withEnergyConsumption: 10);
            var energyProduction = 100f;
            
            Given(new DistributeEnergy(
                    SomeHouseEvents(withInsufficientEnergyIsProduced: OnInsufficientEnergyIsProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(energyProduction))
                .Then(_ => OnInsufficientEnergyIsProduced, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void SendExpectedPositiveDifferenceInEnergy()
        {
            var energyConsumption = 10f;
            var onEnergyOverheadProduced = new Subject<float>();
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(withEnergyConsumption: energyConsumption);
            var energyProduction = 100f;
            var expected = energyProduction - energyConsumption;
            
            Given(new DistributeEnergy(
                    SomeHouseEvents(withEnergyOverheadProduced: onEnergyOverheadProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(energyProduction))
                .Then(_ => onEnergyOverheadProduced, it => it.Receives(expected))
                .Run();
        }
    }
}