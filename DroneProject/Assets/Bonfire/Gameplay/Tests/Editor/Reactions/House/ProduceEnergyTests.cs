﻿using Bonfire.Gameplay.Domain.Reactions.House;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.HouseGameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EnergyGeneratorMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Reactions.House

{
    public class ProduceEnergyTests
    {
        [Test]
        public void SendOnEnergyProduced()
        {
            var onEnergyProduced = new Subject<float>();
            
            Given(new ProduceEnergy(
                    SomeHouseEvents(withEnergyProduced: onEnergyProduced),
                    SomeRepositories()
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyProduced, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetEnergyProductionPerSecond()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new ProduceEnergy(
                    SomeHouseEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(() => houseResourcesRepository.Received(1).GetEnergyProductionPerSecond())
                .Run();
        }

        [Test]
        public void ReturnRightAmountOfEnergyPerSecond()
        {
            var onEnergyProduced = new Subject<float>();
            var energyPerSecond = 5f;
            var solarGenerators = new[] {ASolarEnergyGenerator(withEnergyPerSecond: energyPerSecond)};
            var windGenerators = new[] {AWindEnergyGenerator(withEnergyPerSecond:energyPerSecond)};
            var energyReactors = new[] {AReactorEnergyGenerator(withEnergyPerSecond: energyPerSecond)};

            
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(
                withSolarGenerators: solarGenerators,
                withWindGenerators: windGenerators,
                withEnergyReactors: energyReactors
            );

            var expectedAmount = energyPerSecond * 3;
            
            Given(new ProduceEnergy(
                    SomeHouseEvents(withEnergyProduced: onEnergyProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(1f))
                .Then(_ => onEnergyProduced, it => it.Receives(expectedAmount))
                .Run();
        }
        
        [Test]
        public void ReturnRightAmountOfEnergyPerSecondWithHalfMultiplier()
        {
            var timeScale = .5f;
            var energyPerSecond = 5f;
            
            var onEnergyProduced = new Subject<float>();
            var solarGenerators = new[] {ASolarEnergyGenerator(withEnergyPerSecond: energyPerSecond)};
            var windGenerators = new[] {AWindEnergyGenerator(withEnergyPerSecond:energyPerSecond)};
            var energyReactors = new[] {AReactorEnergyGenerator(withEnergyPerSecond: energyPerSecond)};

            
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(
                withSolarGenerators: solarGenerators,
                withWindGenerators: windGenerators,
                withEnergyReactors: energyReactors
            );

            var expectedAmount = energyPerSecond * 3 * timeScale;
            
            Given(new ProduceEnergy(
                    SomeHouseEvents(withEnergyProduced: onEnergyProduced),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(timeScale))
                .Then(_ => onEnergyProduced, it => it.Receives(expectedAmount))
                .Run();
        }
    }
}