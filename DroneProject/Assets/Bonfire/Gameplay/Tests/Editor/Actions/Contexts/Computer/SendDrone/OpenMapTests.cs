﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DestinationMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.MapRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.SendDrone
{
    [TestFixture]
    public class OpenMapTests
    {
        [Test]
        public void SendOnMapOpened()
        {
            var onMapOpened = new Subject<IEnumerable<Destination>>();
            
            Given(new OpenMap(
                    SomeEvents(withMapOpened: onMapOpened),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onMapOpened, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetDestinationsFromRepository()
        {
            var onMapOpened = new Subject<IEnumerable<Destination>>();
            var repository = AMapRepository();
            
            Given(new OpenMap(
                    SomeEvents(withMapOpened: onMapOpened),
                    SomeRepositories(withMapRepository: repository)
                    ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).RetrieveAllDestinations())
                .Run();
        }
        
        [Test]
        public void SendLoadedDestinations()
        {
            var destinations = new[]
            {
                ADestination(),
                ADestination(withName: "carlos", withMapCoordinates: MapCoordinatesMother.AMapCoordinates())
            };
            var onMapOpened = new Subject<IEnumerable<Destination>>();
            var repository = new InMemoryMapRepository(withDestinations: destinations);

            var expectedDestinations = new[]
            {
                ADestination(),
                ADestination(withName: "carlos", withMapCoordinates: MapCoordinatesMother.AMapCoordinates())
            };
            
            Given(new OpenMap(
                    SomeEvents(withMapOpened: onMapOpened),
                    SomeRepositories(withMapRepository: repository)
                ))
                .When(action => action.Do())
                .Then(_ => onMapOpened, it => it.Receives(expectedDestinations))
                .Run();
        }
    }
}