﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MapCoordinatesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.SendDrone
{
    [TestFixture]
    public class SelectMissionDroneTests
    {
        [Test]
        public void SendOnSendDroneCardSelected()
        {
            var OnMissionDroneSelected = new Subject<MissionOrders>();

            Given(new SelectMissionDrone(
                    SomeEvents(withMissionDroneSelected: OnMissionDroneSelected),
                    SomeRepositories(withMissionBuilderRepository: AMissionBuilderRepository())
                ))
                .When(action => action.Do(ADrone()))
                .Then(_ => OnMissionDroneSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallUpdateDroneOnRepository()
        {
            var OnMissionDroneSelected = new Subject<MissionOrders>();
            var missionBuilderRepository = AMissionBuilderRepository();
        
            Given(new SelectMissionDrone(
                    SomeEvents(withMissionDroneSelected: OnMissionDroneSelected),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do(new Drone()))
                .Then(() => missionBuilderRepository.Received(1).UpdateDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void ReturnUpdatedMissionOrdersWithNewDrone()
        {
            var oldMissionOrders = AMissionOrders(withMapCoordinates: AMapCoordinates(5, 5));
            var OnMissionDroneSelected = new Subject<MissionOrders>();
            var selectedDrone = ADrone(withName: "Rigoberto", withStatus: DroneStatus.Busy);
            var expectedMissionOrders = AMissionOrders(withMapCoordinates: AMapCoordinates(5,5), withDroneName: selectedDrone.droneName);

            Given(new SelectMissionDrone(
                    SomeEvents(withMissionDroneSelected: OnMissionDroneSelected),
                    SomeRepositories(withMissionBuilderRepository: new InMemoryMissionBuilderRepository(withMissionOrders: oldMissionOrders))
                ))
                .When(action => action.Do(selectedDrone))
                .Then(_ => OnMissionDroneSelected, it => it.Receives(expectedMissionOrders))
                .Run();
        }
    }
}