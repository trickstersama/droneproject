﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.DroneAssemblerServiceMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Assembly
{
    [TestFixture]
    public class NameDroneTests
    {
        [Test]
        public void SendOnDroneNamed()
        {
            var onDroneNamed = new Subject<Unit>();

            Given(new NameDrone(
                    SomeEvents(withDroneNamed: onDroneNamed),
                    SomeRepositories(withDroneInProgressRepository: ADroneInProgressRepository()),
                    SomeServices()
                ))                
                .When(action => action.Do(string.Empty))
                .Then(_ => onDroneNamed, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallSetNameOnDroneInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(new NameDrone(
                    SomeEvents(withDroneNamed: new Subject<Unit>()),
                    SomeRepositories(withDroneInProgressRepository: droneInProgressRepository),
                    SomeServices()
                ))
                .When(action => action.Do(string.Empty))
                .Then(() => droneInProgressRepository.Received(1).SetName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void CallValidateNameInDroneAssemblerService()
        {
            var droneAssemblerService = ADroneAssemblerService();

            Given(new NameDrone(
                    SomeEvents(withDroneNamed: new Subject<Unit>()),
                    SomeRepositories(withDroneInProgressRepository: ADroneInProgressRepository()),
                    SomeServices(withDroneAssembler: droneAssemblerService)
                ))
                .When(action => action.Do(string.Empty))
                .Then(() => droneAssemblerService.Received(1).NameIsValid(Arg.Any<string>()))
                .Run();
        }

        
        
        [Test]
        public void SetCorrectlyNameInDroneInProgress()
        {
            var inputDroneName = "DroneName";
            var expectedName = "DroneName";
            var onDroneNamed = new Subject<Unit>();
            var droneInProgressRepository = InMemoryDroneInProgressRepository();

            Given(new NameDrone(
                    SomeEvents(withDroneNamed: onDroneNamed),
                    SomeRepositories(withDroneInProgressRepository: droneInProgressRepository),
                    SomeServices()
                ))
                .When(action => action.Do(inputDroneName))
                .Then(() => Assert.AreEqual(expectedName, droneInProgressRepository.GetName()))
                .Run();
        }
        
        [Test]
        public void SendOnDroneNameInvalidIfNameTooShort()
        {
            var inputDroneName = "DIK";
            var onDroneNameInvalid = new Subject<Unit>();
            var droneInProgressRepository = InMemoryDroneInProgressRepository();
            var droneAssemblerService = ALocalDroneAssemblerService();

            Given(new NameDrone(
                    SomeEvents(withDroneNameInvalid: onDroneNameInvalid),
                    SomeRepositories(withDroneInProgressRepository: droneInProgressRepository),
                    SomeServices(withDroneAssembler: droneAssemblerService)
                ))                
                .When(action => action.Do(inputDroneName))
                .Then(_ => onDroneNameInvalid, it => it.ReceivesSomething())
                .Run();
        }

        
    }
}