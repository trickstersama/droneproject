﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.SelectFrameMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Assembly
{
    [TestFixture]
    public class SelectFrameTests
    {
        [Test]
        public void SendOnFrameSelected()
        {
            var onFrameSelected = new Subject<Frame>();
            var frame = AFrame();
            
            Given(ASelectFrame(SomeEvents(withFrameSelected: onFrameSelected)))
                .When(action => action.Do(frame))
                .Then(_ => onFrameSelected, it => it.ReceivesSomething())
                .Run();
        }
        
                
        [Test]
        public void CallUpdateFrameOnAssemblyDroneRepository()
        {
            var assemblyDroneRepository = ADroneInProgressRepository();
            
            Given(ASelectFrame(withRepositories: SomeRepositories(withDroneInProgressRepository: assemblyDroneRepository)))
                .When(action => action.Do(AFrame()))
                .Then(() => assemblyDroneRepository.Received(1).UpdateFrame(Arg.Any<Frame>()))
                .Run();
        }
        
        [Test]
        public void CallClearModifiers()
        {
            var assemblyDroneRepository = ADroneInProgressRepository();
            
            Given(ASelectFrame(withRepositories: SomeRepositories(withDroneInProgressRepository: assemblyDroneRepository)))
                .When(action => action.Do(AFrame()))
                .Then(() => assemblyDroneRepository.Received(1).ClearFrame())
                .Run();
        }

        [Test]
        public void SendRightFrame()
        {
            var onFrameSelected = new Subject<Frame>();
            var assemblyDroneRepository =
                InMemoryDroneInProgressRepository(withActualFrameState: AFrame(withName: "Old Frame"));
            var newFrame = AFrame(withName: "New Frame");

            Given(ASelectFrame(SomeEvents(withFrameSelected: onFrameSelected),
                    withRepositories: SomeRepositories(withDroneInProgressRepository: assemblyDroneRepository)
                ))
                .When(action => action.Do(newFrame))
                .Then(_ => onFrameSelected, it => it.Receives(newFrame))
                .Run();
        }
    }
}