﻿using Bonfire.Gameplay.Domain.Actions;
using NSubstitute;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseItemsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Balcony
{
    [TestFixture]
    public class CollectCargoTests
    {
        
        [Test]
        public void CollectCargoOnDroneReturns()
        {
            var itemsRepository = AHouseItemsRepository();
            
            Given(new CollectCargo(SomeRepositories(withHouseItemsRepository: itemsRepository)))
                .When(action => action.Do())
                .Then(()=> itemsRepository.Received(1).AddItems())
                .Run();
        }
    }
}