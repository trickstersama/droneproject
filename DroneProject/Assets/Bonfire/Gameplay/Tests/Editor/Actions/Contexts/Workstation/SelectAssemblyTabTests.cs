﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.SelectAssemblyTabMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation
{
    [TestFixture]
    public class SelectAssemblyTabTests
    {
        [Test]
        public void SendOnAssemblyTabSelected()
        {
            var onAssemblyTabSelected = new Subject<IEnumerable<Frame>>();

            Given(ASelectAssemblyTab(withEvents: SomeEvents(withAssemblyTabSelected: onAssemblyTabSelected)))
                .When(action => action.Do())
                .Then(_ => onAssemblyTabSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallLoadAvailableFrames()
        {
            var componentRepository = AComponentsRepository();

            Given(ASelectAssemblyTab(withRepositories: SomeRepositories(withComponentsRepository: componentRepository)))
                .When(action => action.Do())
                .Then(() => componentRepository.Received(1).GetFrames())
                .Run();
        }

        [Test]
        public void ReturnDefaultDroneFromRepository()
        {
            var onAssemblyTabSelected = new Subject<IEnumerable<Frame>>();
            var defaultFrames = new[] {AFrame()};
            var componentRepository = AnInMemoryComponentRepository(withFrames: defaultFrames);
            var expectedFrames = defaultFrames;
            Given(ASelectAssemblyTab(
                    withEvents: SomeEvents(withAssemblyTabSelected: onAssemblyTabSelected),
                    withRepositories: SomeRepositories(withComponentsRepository: componentRepository)))
                .When(action => action.Do())
                .Then(_ => onAssemblyTabSelected, it => it.Receives(expectedFrames))
                .Run();
        }
        
        [Test]
        public void CallClearAllInProgressRepository()
        {
            var droneInProgressRepository = ADroneInProgressRepository();
            
            Given(ASelectAssemblyTab(
                    withRepositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneInProgressRepository.Received(1).ClearAll())
                .Run();
        }
    }
}