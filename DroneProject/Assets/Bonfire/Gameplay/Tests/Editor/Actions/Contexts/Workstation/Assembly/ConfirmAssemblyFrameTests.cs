﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.ConfirmAssemblyFrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Assembly
{
    [TestFixture]
    public class ConfirmAssemblyFrameTests
    {
        [Test]
        public void SendOnAssemblyFrameConfirmed()
        {
            var onAssemblyFrameConfirmed = new Subject<Frame>();
            
            Given(AConfirmAssemblyFrame(SomeEvents(withAssemblyFrameConfirmed: onAssemblyFrameConfirmed)))
                .When(action => action.Do())
                .Then(_ => onAssemblyFrameConfirmed, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetFrameFromInProgressRepository()
        {

            var repository = ADroneInProgressRepository();
            Given(AConfirmAssemblyFrame(withRepositories: SomeRepositories(withDroneInProgressRepository: repository)))
                .When(action => action.Do())
                .Then(() => repository.Received(1).GetFrame())
                .Run();
        }
        
        [Test]
        public void ReturnStoredFrame()
        {            
            var frame = AFrame(withName: "test Frame");
            
            var onAssemblyFrameConfirmed = new Subject<Frame>();
            var repository = ADroneInProgressRepository(withFrame: frame);
            
            Given(AConfirmAssemblyFrame(
                    withEvents: SomeEvents(withAssemblyFrameConfirmed: onAssemblyFrameConfirmed),
                    withRepositories: SomeRepositories(withDroneInProgressRepository: repository)
                    )
                )
                .When(action => action.Do())
                .Then(_ => onAssemblyFrameConfirmed, it => it.Receives(frame))
                .Run();
        }
    }
}