using Bonfire.Gameplay.Domain.Actions.HouseBatteryContext;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.HouseBatteryContext
{
    [TestFixture]
    public class OpenHouseBatteryTests
    {
        [Test]
        public void SendOnHouseBatteryOpened()
        {
            var onHouseBatteryOpened = new Subject<HouseEnergyStatus>();
            
            Given(new OpenHouseBattery(
                    SomeEvents(withHouseBatteryOpened: onHouseBatteryOpened),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onHouseBatteryOpened, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetEnergyProduction_FromHouseRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new OpenHouseBattery(
                    SomeEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do())
                .Then(() => houseResourcesRepository.Received(1).GetEnergyProductionPerSecond())
                .Run();
        }
        [Test]
        public void CallGetEnergyConsumption_FromHouseRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new OpenHouseBattery(
                    SomeEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do())
                .Then(() => houseResourcesRepository.Received(1).GetEnergyConsumptionPerSecond())
                .Run();
        }
        [Test]
        public void CallGetCapacity_FromHouseRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new OpenHouseBattery(
                    SomeEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do())
                .Then(() => houseResourcesRepository.Received(1).GetTotalStorage())
                .Run();
        }
    }
}