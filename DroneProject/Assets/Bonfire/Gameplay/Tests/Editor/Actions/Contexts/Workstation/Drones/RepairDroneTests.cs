﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DroneServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Drones
{
    [TestFixture]
    public class RepairDroneTests
    {
        [Test]
        public void SendOnDroneRepaired()
        {
            var onDroneRepaired = new Subject<Drone>();
            
            Given(new RepairDrone(
                    SomeEvents(withDroneRepaired: onDroneRepaired),
                    SomeRepositories(),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(_ => onDroneRepaired, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallRetrieveDroneInWorkshopFromRepository()
        {
            var droneRepository = ADronesRepository();
            
            Given(new RepairDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: droneRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => droneRepository.Received(1).RetrieveDroneInWorkshop())
                .Run();
        }
        
        [Test]
        public void CallRepairDroneFromService()
        {
            var droneService = ADroneService();
            
            Given(new RepairDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do())
                .Then(() => droneService.Received(1).RepairDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallUpdateDroneFromRepository()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new RepairDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).UpdateDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void ReturnRepairedDrone()
        {
            var onDroneRepaired = new Subject<Drone>();
            var damagedDrone = ADrone(
                withCharge: 0,
                withFrame: AFullFrame(),
                withName: "TheRock",
                withDamageReceived: 99
            );

            var dronesRepository = AnInMemoryDronesRepository(withDroneInWorkshop: damagedDrone);
            
            var expectedDrone = ADrone(
                withCharge: 0,
                withFrame: AFullFrame(),
                withName: "TheRock",
                withDamageReceived: 0
            );
            
            
            Given(new RepairDrone(
                    SomeEvents(withDroneRepaired: onDroneRepaired),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do())
                .Then(_ => onDroneRepaired, it => it.Receives(expectedDrone))
                .Run();
        }
    }
}