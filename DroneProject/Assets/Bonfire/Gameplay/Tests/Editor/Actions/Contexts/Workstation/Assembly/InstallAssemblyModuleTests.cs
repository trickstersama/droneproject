﻿using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.InstallAssemblyModuleMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Assembly
{
    [TestFixture]
    public class InstallAssemblyModuleTests
    {
        [Test]
        public void SendOnAssemblyModuleInstalledWhenASlotIsAvailable()
        {
            var onAssemblyModuleInstalled = new Subject<DroneModifiers>();
            
            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleInstalled: onAssemblyModuleInstalled),
                    repositories: SomeRepositories(withDroneInProgressRepository: ADroneInProgressRepository(withSlotsAvailable: 1))
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleInstalled, it => it.ReceivesSomething())
                .Run();
        }  
        [Test]
        public void SendOnAssemblyModuleSlotConsumedWhenASlotIsAvailable()
        {
            var onAssemblyModuleSlotConsumed = new Subject<Frame>();
            var baseFrame = AFrame(withArmorSlotsAmount: 2);
            var droneInProgressRepository = InMemoryDroneInProgressRepository(withActualFrameState: baseFrame);
            var expectedFrame = AFrame(withArmorSlotsAmount: baseFrame.armorSlotsAmount - 1);
            
            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleSlotsUpdated: onAssemblyModuleSlotConsumed),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleSlotConsumed, it => it.Receives(expectedFrame))
                .Run();
        }
        
        [Test]
        public void CallConsumeSlotIfItHas()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(AInstallAssemblyModule(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(() => droneInProgressRepository.Received(1).ConsumeSlot(Arg.Any<Module>()))
                .Run();
        }
        
        [Test]
        public void CallAddModuleToProgressWhenSlotIsAvailable()
        {
            var droneInProgressRepository = ADroneInProgressRepository();

            Given(AInstallAssemblyModule(
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(() => droneInProgressRepository.Received(1).AddModuleToProgress(Arg.Any<Module>()))
                .Run();
        }
        
        [Test]
        public void CallHasSlotsAvailableFor()
        {
            var droneInProgressRepository = ADroneInProgressRepository(withSlotsAvailable: 1);
            
            Given(AInstallAssemblyModule(repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)))
                .When(action => action.Do(AnArmorModule()))
                .Then(() => droneInProgressRepository.Received(1).SlotsAvailableFor(Arg.Any<Module>()))
                .Run();
        }
        
        [Test]
        public void DoNotSendOnAssemblyModuleInstalledWhenItHasNoSlotsLeft()
        {
            var onAssemblyModuleInstalled = new Subject<DroneModifiers>();
            var droneInProgressRepository = ADroneInProgressRepository(withSlotsAvailable: 0);
            
            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleInstalled: onAssemblyModuleInstalled),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                    ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleInstalled, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnAssemblyModuleSlotConsumedWhenItHasNoSlotsLeft()
        {
            var onAssemblyModuleSlotConsumed = new Subject<Frame>();
            var droneInProgressRepository = ADroneInProgressRepository(withSlotsAvailable: 0);
            
            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleSlotsUpdated: onAssemblyModuleSlotConsumed),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleSlotConsumed, it => it.ReceivesNothing())
                .Run();
        }
        [Test]
        public void SendCorrectArmorModifiers()
        {
            var onAssemblyModuleInstalled = new Subject<DroneModifiers>();
            
            var frame = AFrame(withArmorSlotsAmount: 1, withWeight: 200);
            var weaponModule = AWeaponModule(withDamage: 100);
            var droneInProgressRepository = InMemoryDroneInProgressRepository(
                withActualFrameState: frame, 
                withOriginalFrame: frame,
                withModules: new []{weaponModule}
                ); 
            
            var armorModule = AnArmorModule(withHardness: 10, withWeight: 10);
            
            var expectedDroneModifiers = weaponModule.GetDroneModifiers() 
                                         + frame.GetModifiers() 
                                         + armorModule.GetDroneModifiers();
            
            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleInstalled: onAssemblyModuleInstalled),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(armorModule))
                .Then(_ => onAssemblyModuleInstalled, it => it.Receives(expectedDroneModifiers))
                .Run();
        }
        
        [Test]
        public void SendCorrectWeaponModifiers()
        {
            var onAssemblyModuleInstalled = new Subject<DroneModifiers>();
            
            var frame = AFrame(withWeaponSlotsAmount: 1, withWeight: 140);
            var droneInProgressRepository = 
                InMemoryDroneInProgressRepository(
                    withOriginalFrame: frame,
                    withActualFrameState: frame
                ); 

            var weaponModule = AWeaponModule(
                withDamage: 10, 
                withWeight: 10, 
                withWeaponType: WeaponType.Ballistic
            );

            var expectedDroneModifiers = frame.GetModifiers() + weaponModule.GetDroneModifiers();

            Given(AInstallAssemblyModule(
                    events: SomeEvents(withAssemblyModuleInstalled: onAssemblyModuleInstalled),
                    repositories: SomeRepositories(withDroneInProgressRepository: droneInProgressRepository)
                ))
                .When(action => action.Do(weaponModule))
                .Then(_ => onAssemblyModuleInstalled, it => it.Receives(expectedDroneModifiers))
                .Run();
        }
    }
}