﻿using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.SendDroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.MissionServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.MissionsRepositoryMother;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.SendDrone
{
    [TestFixture]
    public class SendDroneTests
    {

        [Test]
        public void SendOnDroneSent()
        {
            var onDroneSent = new Subject<Unit>();
            
            Given(ASendDrone(withEvents: SomeEvents(withDroneSent: onDroneSent)))
                .When(action => action.Do())
                .Then(_ => onDroneSent, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetDroneFromMissionBuilderRepository()
        {

            var missionBuilderRepository = AMissionBuilderRepository();

            Given(ASendDrone(
                    withRepositories: SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).GetDrone())
                .Run();
        }
        
        [Test]
        public void CallSetDroneToBusyState()
        {
            var dronesRepository = ADronesRepository();

            Given(ASendDrone(
                    withRepositories: SomeRepositories(withDronesRepository: dronesRepository)))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).SetDroneToTraveling(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallClearMissionBuilderRepository()
        {
            var missionBuilderRepository = AMissionBuilderRepository();

            Given(ASendDrone(
                    withRepositories: SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)))
                .When(action => action.Do())
                .Then(() => missionBuilderRepository.Received(1).Clear())
                .Run();
        }
        
        [Test]
        public void CallSaveNewMission_FromMissionsRepository()
        {
            var missionsRepository = AMissionsRepository();

            Given(ASendDrone(
                    withRepositories: SomeRepositories(withMissionsRepository: missionsRepository)
                ))
                .When(action => action.Do())
                .Then(() => missionsRepository.Received(1).SaveNewMission(Arg.Any<Mission>()))
                .Run();
        }
        
        [Test]
        public void CallCreateMission_InMissionService()
        {
            var missionService = AMissionService();

            Given(ASendDrone(
                    withServices: SomeServices(withMissionService: missionService)
                ))
                .When(action => action.Do())
                .Then(() => missionService.Received(1).CreateMission(Arg.Any<MissionOrders>()))
                .Run();
        }
        
        [Test]
        public void SetDroneToTravelingState()
        {
            var droneName = "Drone name";
            var drone = ADrone(withStatus: DroneStatus.Idle, withName: droneName);
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new[] {drone});

            Given(ASendDrone(
                    withRepositories: SomeRepositories(
                        withDronesRepository: dronesRepository,
                        withMissionBuilderRepository: AnInMemoryMissionBuilderRepository(withMissionOrders: AMissionOrders(), withDrone: drone))
                ))
                .When(action => action.Do())
                .Then( () => dronesRepository.GetDroneByName(droneName).status.Equals(DroneStatus.Traveling))
                .Run();
        }
    }
}

