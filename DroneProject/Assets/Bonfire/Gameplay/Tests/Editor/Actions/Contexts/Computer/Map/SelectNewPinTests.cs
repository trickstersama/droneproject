﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class SelectNewPinTests
    {
        [Test]
        public void SendOnNewPinSelected()
        {
            var onNewPinSelected = new Subject<Unit>();

            Given(new SelectNewPin(
                    SomeEvents(withNewPinSelected: onNewPinSelected),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onNewPinSelected, it => it.ReceivesSomething())
                .Run();
        }


        [Test]
        public void CallSaveDestinationBuilderRepository()
        {
            var repository = APinInProgressRepository();

            Given(new SelectNewPin(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).Initialize())
                .Run();
        }
    }
}