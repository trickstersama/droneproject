﻿using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EventMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.OpenComputerMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer
{
    [TestFixture]
    public class OpenComputerTests
    {
        [Test]
        public void SendOnOpenComputer()
        {
            var onOpenComputer = AnEvent<Unit>();
            
            Given(AOpenComputer(
                    withGameplayEvents: SomeEvents(withOpenComputer: onOpenComputer)
                ))
                .When(action => action.Do())
                .Then(_ => onOpenComputer, it => it.ReceivesSomething())
                .Run();
        }
    }
}