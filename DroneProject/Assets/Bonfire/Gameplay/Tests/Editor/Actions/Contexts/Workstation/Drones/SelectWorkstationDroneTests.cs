﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.DroneModifiersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DroneServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.TestSupport.Editor.BDD.Context;


namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Drones
{
    [TestFixture]
    public class SelectWorkstationDroneTests
    {

        [Test]
        public void CallSelectDroneForWorkshopOnRepository()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new SelectWorkstationDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do(new Drone()))
                .Then(() => dronesRepository.Received(1).SelectDroneForWorkshop(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallIsDroneDischargedFromDroneService()
        {
            var droneService = ADroneService();
            
            Given(new SelectWorkstationDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do(new Drone()))
                .Then(() => droneService.Received(1).IsDroneDischarged(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallIsDroneDamagedFromDroneService()
        {
            var droneService = ADroneService();
            
            Given(new SelectWorkstationDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do(new Drone()))
                .Then(() => droneService.Received(1).IsDroneDamaged(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void SendOnRechargeNeededWhenDroneIsDischarged()
        {
            var dischargedDrone = ADrone(withCharge: 50f);
            var onRechargeNeeded =  new Subject<Unit>();
            
            Given(new SelectWorkstationDrone(
                    SomeEvents(withRechargeNeeded:onRechargeNeeded),
                    SomeRepositories(),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do(dischargedDrone))
                .Then(_ => onRechargeNeeded, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnRechargeNeededWhenDroneIsFullyCharged()
        {
            var dischargedDrone = ADrone(withCharge: 100f);
            var onRechargeNeeded =  new Subject<Unit>();
            
            Given(new SelectWorkstationDrone(
                    SomeEvents(withRechargeNeeded: onRechargeNeeded),
                    SomeRepositories(),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do(dischargedDrone))
                .Then(_ => onRechargeNeeded, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void SendOnRepairNeededWhenDroneIsDamaged()
        {
            var onRepairNeeded = new Subject<Unit>();
            var damagedDrone = ADrone(withModifiers: ADroneModifiers(withHitPoints: 100), withDamageReceived: 50);

            
            Given(new SelectWorkstationDrone(
                    SomeEvents(withRepairNeeded: onRepairNeeded),
                    SomeRepositories(),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do(damagedDrone))
                .Then(_ => onRepairNeeded, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnRepairNeededWhenDroneIsFullHP()
        {
            var onRepairNeeded = new Subject<Unit>();
            var fullyHPDrone = ADrone(withModifiers: ADroneModifiers(withHitPoints: 49), withDamageReceived: 0);

            Given(new SelectWorkstationDrone(
                    SomeEvents(withRepairNeeded: onRepairNeeded),
                    SomeRepositories(),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do(fullyHPDrone))
                .Then(_ => onRepairNeeded, it => it.ReceivesNothing())
                .Run();
        }

        
    }
}