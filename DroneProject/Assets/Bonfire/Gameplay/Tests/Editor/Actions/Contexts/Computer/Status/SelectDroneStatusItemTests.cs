﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Status;
using Bonfire.Gameplay.Domain.ValueObjects;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EventMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Status
{
    [TestFixture]
    public class SelectDroneStatusItemTests
    {
        [Test]
        public void SendOnStatusDroneCardSelected()
        {
            var onDroneInStatusSelected = AnEvent<Drone>();

            Given(new SelectDroneCardInStatus(SomeEvents(withDroneInStatusSelected: onDroneInStatusSelected)))
                .When(action => action.Do(ADrone()))
                .Then(_ => onDroneInStatusSelected, it => it.ReceivesSomething())
                .Run();
        }
    }
}