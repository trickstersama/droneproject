﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class ChoosePinTypeTests
    {
        [Test]
        public void SendOnPinTypeChosen()
        {
            var onPinTypeChosen = new Subject<Destination>();
            
            Given(new ChoosePinType(
                    SomeEvents(withOnPinTypeChosen: onPinTypeChosen),
                    SomeRepositories()
                ))
                .When(action => action.Do(MissionType.Battle))
                .Then(_ => onPinTypeChosen, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallUpdateMissionTypeFromBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new ChoosePinType(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(MissionType.Battle))
                .Then(() => repository.Received(1).UpdateMissionType(Arg.Any<MissionType>()))
                .Run();
        }

        [Test]
        public void ReturnDestinationWithMissionType()
        {
            
            var onPinTypeChosen = new Subject<Destination>();
            var oldDestination = new Destination
            {
                missionType = MissionType.Battle, name = "Peter"
            };
            var repository = AnInMemoryPinInProgressRepository(withDestination: oldDestination);

            var newMissionType = MissionType.Loot; 
            var expectedDestination = new Destination
            {
                missionType = newMissionType, name = "Peter"
            };
            
            Given(new ChoosePinType(
                    SomeEvents(withOnPinTypeChosen: onPinTypeChosen),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(newMissionType))
                .Then(_ => onPinTypeChosen, it => it.Receives(expectedDestination))
                .Run();
        }
    }
}