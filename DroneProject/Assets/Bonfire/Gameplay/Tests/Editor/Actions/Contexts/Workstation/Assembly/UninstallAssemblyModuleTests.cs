﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DroneInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Assembly
{
    [TestFixture]
    public class UninstallAssemblyModuleTests
    {
        [Test]
        public void SendOnModuleUninstalled()
        {
            var OnAssemblyModuleUninstalled = new Subject<Unit>();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(withAssemblyModuleUninstalled: OnAssemblyModuleUninstalled),
                    SomeRepositories()
                ))
                .When(action => action.Do(ABatteryModule()))
                .Then(_ => OnAssemblyModuleUninstalled, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallReleaseSlotFromDroneInProgressRepository()
        {
            var repository = ADroneInProgressRepository();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(),
                    SomeRepositories(withDroneInProgressRepository: repository)
                ))
                .When(action => action.Do(ABatteryModule()))
                .Then(() => repository.Received(1).ReleaseSlot(Arg.Any<Module>()))
                .Run();
        }

        
        [Test]
        public void SendOnAssemblyModuleSlotsUpdated()
        {
            var onAssemblyModuleSlotsUpdated = new Subject<Frame>();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(withAssemblyModuleSlotsUpdated: onAssemblyModuleSlotsUpdated),
                    SomeRepositories()
                ))
                .When(action => action.Do(ABatteryModule()))
                .Then(_ => onAssemblyModuleSlotsUpdated, it => it.ReceivesSomething())
                .Run();
        }
                
        
        [Test]
        public void FrameUpdatesSlotsCorrectly()
        {
            var onAssemblyModuleSlotsUpdated = new Subject<Frame>();
            var repository = InMemoryDroneInProgressRepository(withActualFrameState: AFrame(withArmorSlotsAmount: 2));
            var expectedFrame = AFrame(withArmorSlotsAmount: 3);
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(withAssemblyModuleSlotsUpdated: onAssemblyModuleSlotsUpdated),
                    SomeRepositories(withDroneInProgressRepository: repository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleSlotsUpdated, it => it.Receives(expectedFrame))
                .Run();
        }

        [Test] 
        public void CallRemoveModuleFromProgressFromRepository()
        {
            var repository = ADroneInProgressRepository();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(),
                    SomeRepositories(withDroneInProgressRepository: repository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(() => repository.Received(1).RemoveModuleFromProgress(Arg.Any<Module>()))
                .Run();
        }
        
        [Test] 
        public void SendOnAssemblyModuleUninstalled()
        {
            var onAssemblyModuleUninstalled = new Subject<Unit>();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(withAssemblyModuleUninstalled: onAssemblyModuleUninstalled),
                    SomeRepositories()
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(_ => onAssemblyModuleUninstalled, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test] 
        public void SendUpdatedDroneModifiers()
        {
            var repository = ADroneInProgressRepository();
            
            Given(new UninstallAssemblyModule(
                    SomeEvents(),
                    SomeRepositories(withDroneInProgressRepository: repository)
                ))
                .When(action => action.Do(AnArmorModule()))
                .Then(() => repository.Received(1).RemoveModuleFromProgress(Arg.Any<Module>()))
                .Run();
        }
    }
}