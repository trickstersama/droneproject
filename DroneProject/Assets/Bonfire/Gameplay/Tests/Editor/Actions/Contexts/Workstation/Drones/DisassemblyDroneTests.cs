﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DroneServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Drones
{
    [TestFixture]
    public class DisassemblyDroneTests
    {
        [Test]
        public void SendOnDroneDisassembled()
        {
            var onDroneDisassembled = new Subject<Unit>();
            
            Given(new DisassemblyDrone(
                    SomeEvents(withDroneDisassembled: onDroneDisassembled),
                    SomeRepositories(),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(_ => onDroneDisassembled, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetModulesFromDroneService()
        {
            var droneService = ADroneService();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do())
                .Then(() => droneService.Received(1).GetModulesFromDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallGetFrameFromDroneService()
        {
            var droneService = ADroneService();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do())
                .Then(() => droneService.Received(1).GetFrameFromDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CallDroneInWorkshopFromRepository()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).RetrieveDroneInWorkshop())
                .Run();
        }
        
        [Test]
        public void CallSaveModulesToRepository()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(withComponentsRepository: componentsRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => componentsRepository.Received(1).SaveModules(Arg.Any<IEnumerable<Module>>()))
                .Run();
        }
        
        [Test]
        public void CallSaveFrameToRepository()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(withComponentsRepository: componentsRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => componentsRepository.Received(1).SaveFrame(Arg.Any<Frame>()))
                .Run();
        }
        
        [Test]
        public void CallRemoveFromRepository()
        {
            var dronesRepository = ADronesRepository();
            
            Given(new DisassemblyDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).Remove(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        [Ignore("SUS")]
        public void SaveComponentsAndDeleteDrone()
        {
            
            //given
            var modules = new List<Module>
            {
                ABatteryModule(withName: "battery 01"),
                AnArmorModule(withName: "armor 01"),
                AWeaponModule(withName: "weapon 01"),
                ASensorModule(withName: "sensor 01", withDamage: 10),
                AStorageModule(withName: "Storage 01", withReservedWeight: 1),
                ACPUModule(withName: "CPU 01"),
                ABatteryModule(withName: "battery 01")
            };
            
            var frame = AFrame(withName: "frame 01");
            
            var droneToDisassemble = ADroneWithModifiersFromParts(
                withModules: modules,
                withFrame: frame,
                withName: "Drone to delete"
            );
            var dronesRepository = AnInMemoryDronesRepository(withDroneInWorkshop: droneToDisassemble);
            var componentRepository = AnInMemoryComponentRepository();
            
            //workaround
            var allModulesSubject = Substitute.For<IObserver<IEnumerable<Module>>>();
            var framesSubject = Substitute.For<IObserver<IEnumerable<Frame>>>();
            
            var disassembleDrone = new DisassemblyDrone(
                SomeEvents(),
                SomeRepositories(
                    withDronesRepository: dronesRepository,
                    withComponentsRepository: componentRepository),
                SomeServices(withDroneService: ALocalDroneService())
                );

            //when
            disassembleDrone.Do();
            
            ObservableExtensions.Subscribe(componentRepository.GetAllModules(), allModulesSubject.OnNext);

            ObservableExtensions.Subscribe(componentRepository.GetFrames(), value =>framesSubject.OnNext(value));
            
            //then
            allModulesSubject.Received(1)
                .OnNext(Arg.Is<IEnumerable<Module>>(receivedModules => modules.SequenceEqual(receivedModules)));
            framesSubject.Received(1)
                .OnNext(Arg.Is<IEnumerable<Frame>>(receivedFrame => receivedFrame.SequenceEqual(new []{frame})));
        }
    }
}