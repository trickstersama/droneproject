﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class ChoosePinNameTests
    {
        [Test]
        public void SendOnPinNameChosen()
        {
            var OnPinNameChosen = new Subject<Destination>();
            
            Given(new ChoosePinName(
                    SomeEvents(withPinNameChosen: OnPinNameChosen),
                    SomeRepositories()
                ))
                .When(action => action.Do("name"))
                .Then(_ => OnPinNameChosen, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallUpdateNameFromBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new ChoosePinName(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do("name"))
                .Then(() => repository.Received(1).UpdateName(Arg.Any<string>()))
                .Run();
        }
        
        [Test]
        public void ReturnDestinationWithNewName()
        {
            var OnPinNameChosen = new Subject<Destination>();
            var oldDestination = new Destination
            {
                mapCoordinates = new MapCoordinates(4, 4),
                name = "oldName"
            };
            var newName = "newName";
            var repository = AnInMemoryPinInProgressRepository(oldDestination);
            
            var expected = new Destination
            {
                mapCoordinates = new MapCoordinates(4, 4),
                name = newName
            };
            
            Given(new ChoosePinName(
                    SomeEvents(withPinNameChosen: OnPinNameChosen),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(newName))
                .Then(_ => OnPinNameChosen, it => it.Receives(expected))
                .Run();
        }
    }
}