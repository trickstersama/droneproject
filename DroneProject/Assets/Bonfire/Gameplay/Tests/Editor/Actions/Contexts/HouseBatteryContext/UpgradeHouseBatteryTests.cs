using Bonfire.Gameplay.Domain.Actions.HouseBatteryContext;
using Bonfire.Gameplay.Domain.ValueObjects.House;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.HouseResourcesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.HouseBatteryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.HouseGameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.HouseBatteryContext
{
    [TestFixture]
    public class UpgradeHouseBatteryTests
    {
        [Test]
        public void SendOnHouseBatteryUpgraded()
        {
            var onHouseBatteryUpgraded = new Subject<Unit>();
            
            Given(new UpgradeHouseBattery(
                    SomeHouseEvents(withHouseBatteryUpgraded: onHouseBatteryUpgraded),
                    SomeRepositories()
                ))
                .When(action => action.Do(new HouseBattery()))
                .Then(_ => onHouseBatteryUpgraded, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallAddHouseBatteryFromHouseResourcesRepository()
        {
            var houseResourcesRepository = AHouseResourcesRepository();
            
            Given(new UpgradeHouseBattery(
                    SomeHouseEvents(),
                    SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
                ))
                .When(action => action.Do(new HouseBattery()))
                .Then( () => houseResourcesRepository.Received(1).AddHouseBattery(Arg.Any<HouseBattery>()))
                .Run();
        }
        
        [Test]
        public void StorageIsWithTotalAmount()
        {
            //given
            var oldHouseBattery = AHouseBattery(withStorageSize: 1000);
            var houseResourcesRepository = AnInMemoryHouseResourcesRepository(withHouseBatteries: new []{oldHouseBattery});
            var newHouseBattery = AHouseBattery(withStorageSize: 500);
            var expectedStorageAmount = oldHouseBattery.storageSize + newHouseBattery.storageSize;
            
            var action = new UpgradeHouseBattery(
                SomeHouseEvents(),
                SomeRepositories(withHouseResourcesRepository: houseResourcesRepository)
            );
                
            //When

            action.Do(newHouseBattery);
            
            //Then

            Assert.AreEqual(houseResourcesRepository.GetTotalStorage(), expectedStorageAmount);
        }
    }
}