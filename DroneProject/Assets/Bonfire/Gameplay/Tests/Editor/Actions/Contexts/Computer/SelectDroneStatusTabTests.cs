﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.SelectDroneStatusTabMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.EventMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer
{
    [TestFixture]
    public class SelectDroneStatusTabTests
    {
        [Test]
        public void SendOnSelectDroneStatusTab()
        {
            var onSelectDroneStatusTab = AnEvent<IEnumerable<Drone>>();
            
            Given(ASelectDroneStatusTab(
                    withGameplayEvents: SomeEvents(withDroneStatusTabSelected: onSelectDroneStatusTab)
                ))
                .When(action => action.Do())
                .Then(_ => onSelectDroneStatusTab,it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallReturnAllIdleDrones()
        {
            var droneRepository = ADronesRepository();

            Given(ASelectDroneStatusTab(
                    withGameplayRepositories: SomeRepositories(withDronesRepository: droneRepository)
                ))
                .When(action => action.Do())
                .Then(() => droneRepository.Received(1).RetrieveAllBusyDrones())
                .Run();
        }

        [Test]
        public void OnSelectDroneStatusReturnsBusyDrones()
        {
            var onSelectDroneStatusTab = AnEvent<IEnumerable<Drone>>();
            
            var drones = new[]{ADrone(withName: "drone01"), ADrone(withName: "drone02")};
            var expectedDrones = drones;
            var droneRepository = ADronesRepository(withBusyDrones: drones);
            
            Given(ASelectDroneStatusTab(
                    withGameplayEvents: SomeEvents(withDroneStatusTabSelected: onSelectDroneStatusTab),
                    withGameplayRepositories: SomeRepositories(withDronesRepository: droneRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onSelectDroneStatusTab,it => it.Receives(expectedDrones))
                .Run();
        }
    }
}