﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class SelectPinnedLocationTests
    {
        [Test]
        public void SendOnPinnedLocationSelected()
        {
            var OnPinnedLocationSelected = new Subject<Destination>();
            
            Given(new SelectPinnedLocation(SomeEvents(withPinnedLocationSelected: OnPinnedLocationSelected)))
                .When(action => action.Do(new Destination()))
                .Then(_ => OnPinnedLocationSelected, it => it.ReceivesSomething())
                .Run();
        }
    }
}