﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.Infrastructure;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.MapRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DestinationServiceMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class SaveNewPinTests
    {
        [Test]
        public void DoNotSendOnNewPinSavedWhenDestinationIsNotValid()
        {
            var onNewPinSaved = new Subject<Unit>();
            
            Given(new SaveNewPin(
                    SomeEvents(withNewPinSaved: onNewPinSaved),
                    SomeRepositories(),
                    SomeServices(withDestinationService: ADestinationService(withValidDestination: false))
                ))
                .When(action => action.Do())
                .Then(_  => onNewPinSaved, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void CallGetDestinationInBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new SaveNewPin(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository),
                    SomeServices(withDestinationService: ADestinationService(withValidDestination: true))
                ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).GetDestination())
                .Run();
        }
        
        [Test]
        public void CallAddNewDestinationToMapRepository()
        {
            var repository = AMapRepository();
            
            Given(new SaveNewPin(
                    SomeEvents(),
                    SomeRepositories(withMapRepository: repository),
                    SomeServices(withDestinationService: ADestinationService(withValidDestination: true))
                ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).AddNewDestination(Arg.Any<Destination>()))
                .Run();
        }
        
        [Test]
        public void CallClearOnDestinationBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new SaveNewPin(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository),
                    SomeServices(withDestinationService: ADestinationService(withValidDestination: true))
                ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).Clear())
                .Run();
        }
        
        [Test]
        public void CallIsDestinationValidFromService()
        {
            var service = ADestinationService(withValidDestination: true);
            
            Given(new SaveNewPin(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do())
                .Then(() => service.Received(1).IsValid(Arg.Any<Destination>()))
                .Run();
        }
        
        [Test]
        public void DoNotSendWhenDestinationIsMissingName()
        {
            var onNewPinSaved = new Subject<Unit>();
            var destination = new Destination()
            {
                name = "In",
                mapCoordinates = new MapCoordinates(1,1)
            };
            
            var repository = AnInMemoryPinInProgressRepository(withDestination: destination);
            
            Given(new SaveNewPin(
                    SomeEvents(withNewPinSaved: onNewPinSaved),
                    SomeRepositories(withPinInProgressRepository: repository),
                    SomeServices(withDestinationService: new UnityDestinationService())
                ))
                .When(action => action.Do())
                .Then(_  => onNewPinSaved, it => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void DoNotSendWhenDestinationIsMissingCoordinates()
        {
            var onNewPinSaved = new Subject<Unit>();
            var destination = new Destination()
            {
                name = "destination",
                mapCoordinates = new MapCoordinates(0,0)
            };
            
            var repository = AnInMemoryPinInProgressRepository(withDestination: destination);
            
            Given(new SaveNewPin(
                    SomeEvents(withNewPinSaved: onNewPinSaved),
                    SomeRepositories(withPinInProgressRepository: repository),
                    SomeServices(withDestinationService: new UnityDestinationService())
                ))
                .When(action => action.Do())
                .Then(_  => onNewPinSaved, it => it.ReceivesNothing())
                .Run();
        }
        [Test]
        public void SendWhenDestinationIsValid()
        {
            var onNewPinSaved = new Subject<Unit>();
            var destination = new Destination()
            {
                name = "complete",
                mapCoordinates = new MapCoordinates(1,1)
            };
            
            var repository = AnInMemoryPinInProgressRepository(withDestination: destination);
            
            Given(new SaveNewPin(
                    SomeEvents(withNewPinSaved: onNewPinSaved),
                    SomeRepositories(withPinInProgressRepository: repository),
                    SomeServices(withDestinationService: new UnityDestinationService())
                ))
                .When(action => action.Do())
                .Then(_  => onNewPinSaved, it => it.ReceivesSomething())
                .Run();
        }
    }
}