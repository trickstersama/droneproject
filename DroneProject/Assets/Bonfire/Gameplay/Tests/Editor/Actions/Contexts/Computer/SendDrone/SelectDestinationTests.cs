﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MapCoordinatesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.SendDrone
{
    [TestFixture]
    public class SelectDestinationTests
    {
        [Test]
        public void SendOnDestinationSelected()
        {
            var onDestinationSelected = new Subject<MissionOrders>();

            Given(new SelectDestination(
                    SomeEvents(withDestinationSelected: onDestinationSelected),
                    SomeRepositories(withMissionBuilderRepository: AMissionBuilderRepository())
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(_ => onDestinationSelected, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallUpdateDestination()
        {
            var onDestinationSelected = new Subject<MissionOrders>();
            var missionBuilderRepository = AMissionBuilderRepository();
            
            Given(new SelectDestination(
                    SomeEvents(withDestinationSelected: onDestinationSelected),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(() => missionBuilderRepository.Received(1).UpdateMapCoordinates(Arg.Any<MapCoordinates>()))
                .Run();
        }
        
        [Test]
        public void ReturnCorrectDestination()
        {
            var onDestinationSelected = new Subject<MissionOrders>();

            var missionBuilderRepository = AnInMemoryMissionBuilderRepository(
                    withMissionOrders: AMissionOrders(withMissionType: MissionType.Loot)
                );
            var mapCoordinates = AMapCoordinates();
            var expectedOrders = AMissionOrders(withMissionType: MissionType.Loot, withMapCoordinates: mapCoordinates);
            
            Given(new SelectDestination(
                    SomeEvents(withDestinationSelected: onDestinationSelected),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(_ => onDestinationSelected, it => it.Receives(expectedOrders))
                .Run();
        }
    }
}