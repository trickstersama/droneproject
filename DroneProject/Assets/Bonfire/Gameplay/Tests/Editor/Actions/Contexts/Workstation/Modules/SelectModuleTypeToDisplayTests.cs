﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Modules;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Modules
{
    //class used to build module STATS
    [TestFixture]
    public class SelectModuleTypeToDisplayTests
    {
        [Test]
        public void SendModuleTypeToDisplaySelected()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();

            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories()
                ))
                .When(action => action.Do(ModuleType.Armor))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetModulesOfType()
        {
            var moduleTypeSelected = ModuleType.Armor;
            var componentRepository = AComponentsRepository();
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(),
                    SomeRepositories(withComponentsRepository: componentRepository)
                    ))
                .When(action => action.Do(moduleTypeSelected))
                .Then(() => componentRepository.Received(1).GetModulesOfType(Arg.Any<ModuleType>()))
                .Run();
        }

        [Test]
        public void ReturnAllArmorModules()
        {
            var onModuleTypeToDisplaySelected = new Subject<IEnumerable<Module>>();

            var moduleTypeSelected = ModuleType.Armor;
            var modulesInRepository = new List<Module>
            {
                AWeaponModule(withName: "weapon1"),
                AnArmorModule(withName: "armor1"), 
                AnArmorModule(), 
                AWeaponModule()
            };
            
            var expectedModules = new List<Module>
            {
                AnArmorModule(withName: "armor1"), 
                AnArmorModule() 
            };
            
            var componentRepository = AnInMemoryComponentRepository(withModules:modulesInRepository);
            
            Given(new SelectModuleTypeToDisplay(
                    SomeEvents(withModuleTypeToDisplaySelected: onModuleTypeToDisplaySelected),
                    SomeRepositories(withComponentsRepository: componentRepository)
                ))
                .When(action => action.Do(moduleTypeSelected))
                .Then(_ => onModuleTypeToDisplaySelected, it => it.Receives(expectedModules))
                .Run(); 
        }
    }
}