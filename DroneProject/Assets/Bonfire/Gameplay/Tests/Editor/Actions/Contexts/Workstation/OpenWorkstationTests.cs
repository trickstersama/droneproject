﻿using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.WorkstationContext.OpenWorkstationMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation
{
    [TestFixture]
    public class OpenWorkstationTests
    {
        [Test]
        public void SendOnOpenWorkstation()
        {
            var OnOpenWorkstation = new Subject<Unit>();

            Given(AOpenWorkstation(withEvents: SomeEvents(withOpenWorkstation: OnOpenWorkstation)))
                .When(action => action.Do())
                .Then(_ => OnOpenWorkstation, it => it.ReceivesSomething())
                .Run();
        }
    }
}