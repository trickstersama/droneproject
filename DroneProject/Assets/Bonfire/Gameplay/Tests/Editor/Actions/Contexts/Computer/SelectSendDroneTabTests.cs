﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.SelectSendDroneTabMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer
{
    [TestFixture]
    public class SelectSendDroneTabTests
    {
        [Test]
        public void SendOnSendDroneTabSelected()
        {
            var onSendDroneTabSelected = new Subject<IEnumerable<Drone>>();

            Given(ASelectSendDroneTab(withEvents: SomeEvents(withSendDroneTabSelected: onSendDroneTabSelected)))
                .When(action => action.Do())
                .Then(_ => onSendDroneTabSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallRetrieveAllIdleDrones()
        {
            var dronesRepository = ADronesRepository();

            Given(ASelectSendDroneTab(withRepositories: SomeRepositories(withDronesRepository: dronesRepository)))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).RetrieveAllIdleDrones())
                .Run();
        }
        [Test]
        public void CallClearOnMissionBuilderRepository()
        {
            var dronesRepository = AMissionBuilderRepository();

            Given(ASelectSendDroneTab(withRepositories: SomeRepositories(withMissionBuilderRepository: dronesRepository)))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).Clear())
                .Run();
        }

        [Test]
        public void ReturnLoadedDronesOnTabSelected()
        {
            var onSendDroneTabSelected = new Subject<IEnumerable<Drone>>();
            
            var newDroneOne = ADrone(withName: "DroneOne", withStatus: DroneStatus.Idle);
            var newDroneTwo = ADrone(withName: "DroneTwo", withStatus: DroneStatus.Idle);
            var twoDronesList = new List<Drone>{newDroneOne, newDroneTwo};
            
            var dronesRepository = AnInMemoryDronesRepository(withDrones: twoDronesList);

            Given(ASelectSendDroneTab(
                    withEvents: SomeEvents(withSendDroneTabSelected: onSendDroneTabSelected),
                    withRepositories: SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onSendDroneTabSelected, it => it.Receives(twoDronesList))
                .Run();

        }
    }
}