﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DestinationMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.MapRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class SelectPinnedLocationsSectionTests
    {
        [Test]
        public void SendOnPinnedLocationsSelected()
        {
            var onPinnedLocationsSectionSelected = new Subject<IEnumerable<Destination>>();
            
            Given(new SelectPinnedLocationsSection(
                    SomeEvents(withPinnedLocationsSectionSelected: onPinnedLocationsSectionSelected),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onPinnedLocationsSectionSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallGetDestinationsFromRepository()
        {
            var repository = AMapRepository();
            Given(new SelectPinnedLocationsSection(
                    SomeEvents(),
                    SomeRepositories(withMapRepository: repository)
                ))
                .When(action => action.Do())
                .Then(() => repository.Received(1).RetrieveAllDestinations())
                .Run();
        }

        [Test]
        public void ReturnAllLoadedDestinations()
        {
            var onPinnedLocationsSectionSelected = new Subject<IEnumerable<Destination>>();
            var destinations = new[]
            {
                ADestination(withName: "pepe", withMapCoordinates: new MapCoordinates(2, 2)),
                ADestination(withName: "Equisde", withMapCoordinates: new MapCoordinates())
            };
            var expectedDestinations = destinations;
            var repository = AnInMemoryMapRepository(withDestinations: destinations);
            
            Given(new SelectPinnedLocationsSection(
                    SomeEvents(withPinnedLocationsSectionSelected: onPinnedLocationsSectionSelected),
                    SomeRepositories(withMapRepository: repository)
                ))
                .When(action => action.Do())
                .Then(_ => onPinnedLocationsSectionSelected, it => it.Receives(expectedDestinations))
                .Run();
        }
    }
}