﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DestinationServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DestinationMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class NewPinReadyToSaveTests
    {
        [Test]
        public void SendOnNewPinReadyToSaveWhenIsValid()
        {
            var onNewPinReadyToSave = new Subject<Unit>();
            var service = ADestinationService(withValidDestination: true);

            Given(new NewPinReadyToSave(
                    SomeEvents(withNewPinReadyToSave: onNewPinReadyToSave),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do(ADestination()))
                .Then(_ => onNewPinReadyToSave, it  => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnNewPinReadyToSaveWhenIsNotValid()
        {
            var onNewPinReadyToSave = new Subject<Unit>();
            var service = ADestinationService(withValidDestination: false);

            Given(new NewPinReadyToSave(
                    SomeEvents(withNewPinReadyToSave: onNewPinReadyToSave),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do(ADestination()))
                .Then(_ => onNewPinReadyToSave, it  => it.ReceivesNothing())
                .Run();
        }
        
        [Test]
        public void CallIsValidFromDestinationService()
        {
            var service = ADestinationService();
            Given(new NewPinReadyToSave(
                    SomeEvents(),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do(ADestination()))
                .Then(() => service.Received(1).IsValid(Arg.Any<Destination>()))
                .Run();
        }
        
        [Test]
        public void SendOnNewPinNotReadyToSaveWhenIsNotValid()
        {
            var onNewPinNotReadyToSave = new Subject<Unit>();
            var service = ADestinationService(withValidDestination: false);

            Given(new NewPinReadyToSave(
                    SomeEvents(withNewPinNotReadyToSave: onNewPinNotReadyToSave),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do(ADestination()))
                .Then(_ => onNewPinNotReadyToSave, it  => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void DoNotSendOnNewPinNotReadyToSaveWhenIsValid()
        {
            var onNewPinNotReadyToSave = new Subject<Unit>();
            var service = ADestinationService(withValidDestination: true);

            Given(new NewPinReadyToSave(
                    SomeEvents(withNewPinNotReadyToSave: onNewPinNotReadyToSave),
                    SomeServices(withDestinationService: service)
                ))
                .When(action => action.Do(ADestination()))
                .Then(_ => onNewPinNotReadyToSave, it  => it.ReceivesNothing())
                .Run();
        }
    }
}