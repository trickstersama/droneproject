﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Reactions.Workstation.Assembly;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Frames
{
    [TestFixture]
    public class SelectFramesTabTests
    {
        [Test]
        public void SendOnFramesTabSelected()
        {
            var onFramesTabSelected = new Subject<IEnumerable<Frame>>();
            
            Given(new SelectFramesTab(
                    SomeEvents(withFramesTabSelected: onFramesTabSelected),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onFramesTabSelected, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetFramesFromRepository()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(new SelectFramesTab(
                    SomeEvents(),
                    SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do())
                .Then(()  => componentsRepository.Received(1).GetFrames())
                .Run();
        }
        
        [Test]
        public void ReturnAllFramesInRepository()
        {
            var onFramesTabSelected = new Subject<IEnumerable<Frame>>();
            var framesInRepo = new[]
            {
                AFrame("Frame1"), AFullFrame(), AFrame("frame3")
            };
            var componentsRepository = AnInMemoryComponentRepository(withFrames: framesInRepo);
            
            var expectedFrames = new[]
            {
                AFrame("Frame1"), AFullFrame(), AFrame("frame3")
            }; 
            
            Given(new SelectFramesTab(
                    SomeEvents(withFramesTabSelected: onFramesTabSelected),
                    SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onFramesTabSelected, it => it.Receives(expectedFrames))
                .Run();
        }
    }
}