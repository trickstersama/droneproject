﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DestinationMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.MapRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer
{
    [TestFixture]
    public class SelectMapTabTests
    {
        [Test]
        public void SendOnMapTabSelected()
        {
            var onMapTabSelected = new Subject<IEnumerable<Destination>>();
            
            Given(new SelectMapTab(SomeEvents(withMapTabSelected: onMapTabSelected), SomeRepositories()))
                .When(action => action.Do())
                .Then(_ => onMapTabSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallRetrieveAllDestinations()
        {
            var onMapTabSelected = new Subject<IEnumerable<Destination>>();
            var mapRepository = AMapRepository();
            
            Given(new SelectMapTab(
                    SomeEvents(withMapTabSelected: onMapTabSelected),
                    SomeRepositories(withMapRepository: mapRepository)
                ))
                .When(action => action.Do())
                .Then(() => mapRepository.Received(1).RetrieveAllDestinations())
                .Run();
        }
        
        [Test]
        public void ReturnAllSavedDestinations()
        {
            var onMapTabSelected = new Subject<IEnumerable<Destination>>();
            var destinations = new[]
            {
                ADestination(withName: "caca"), 
                ADestination(withMapCoordinates: new MapCoordinates(1,2))
            };
            var mapRepository = AnInMemoryMapRepository(withDestinations: destinations);
            var expectedDestinations = new[]
            {
                ADestination(withName: "caca"), 
                ADestination(withMapCoordinates: new MapCoordinates(1,2))
            };
            
            Given(new SelectMapTab(
                    SomeEvents(withMapTabSelected: onMapTabSelected),
                    SomeRepositories(withMapRepository: mapRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onMapTabSelected, it => it.Receives(expectedDestinations))
                .Run();
        }
    }
}