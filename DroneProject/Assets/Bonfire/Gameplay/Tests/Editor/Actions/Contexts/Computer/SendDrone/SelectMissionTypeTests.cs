﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.SendDrone;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.MissionBuilderRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.SendDrone
{
    [TestFixture]
    public class SelectMissionTypeTests
    {
        [Test]
        public void SendOnMissionTypeSelected()
        {
            var onMissionTypeSelected = new Subject<MissionOrders>();

            Given(new SelectMissionType(
                    SomeEvents(withMissionTypeSelected: onMissionTypeSelected),
                    SomeRepositories(withMissionBuilderRepository: AMissionBuilderRepository())
                ))
                .When(action => action.Do(MissionType.Battle))
                .Then(_  => onMissionTypeSelected, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallUpdateMissionType()
        {
            var missionBuilderRepository = AMissionBuilderRepository();
            var onMissionTypeSelected = new Subject<MissionOrders>();
            var missionType = MissionType.Battle;

            Given(new SelectMissionType(
                    SomeEvents(withMissionTypeSelected: onMissionTypeSelected),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do(missionType))
                .Then(() => missionBuilderRepository.Received(1).UpdateMissionType(missionType))
                .Run();
        }
        
        [Test]
        public void ReturnMissionOrderWithMissionTypeUpdated()
        {
            var onMissionTypeSelected = new Subject<MissionOrders>();
            
            var missionBuilderRepository = AnInMemoryMissionBuilderRepository(withMissionOrders: AMissionOrders());
            var missionType = MissionType.Scout;

            var expectedMissionOrders = AMissionOrders(withMissionType: missionType);

            Given(new SelectMissionType(
                    SomeEvents(withMissionTypeSelected: onMissionTypeSelected),
                    SomeRepositories(withMissionBuilderRepository: missionBuilderRepository)
                ))
                .When(action => action.Do(missionType))
                .Then(_ => onMissionTypeSelected, it => it.Receives(expectedMissionOrders))
                .Run();
        }
    }
}