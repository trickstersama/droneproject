﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Frames;
using Bonfire.Gameplay.Domain.ValueObjects;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Frames
{
    [TestFixture]
    public class SelectFrameCardTests
    {
        [Test]
        public void SendOnFrameCardSelected()
        {
            var onFrameCardSelected = new Subject<Frame>();
            var incomingFrame = AFrame();
            var expectedFrame = incomingFrame;
            
            Given(new SelectFrameCard(SomeEvents(withFrameCardSelected: onFrameCardSelected)))
                .When(action => action.Do(incomingFrame))
                .Then(_ => onFrameCardSelected, it => it.Receives(expectedFrame))
                .Run();
        }
    }
}