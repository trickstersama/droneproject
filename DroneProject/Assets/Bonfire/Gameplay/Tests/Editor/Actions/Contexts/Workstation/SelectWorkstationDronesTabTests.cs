﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.ComputerContext;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation

{
    [TestFixture]
    public class SelectWorkstationDronesTabTests
    {
        [Test]
        public void SendOnDronesTabSelected()
        {
            var onWorkstationDronesTabSelected = new Subject<IEnumerable<Drone>>();
            
            Given(new SelectWorkstationDronesTab(
                    SomeEvents(withWorkstationDronesTabSelected: onWorkstationDronesTabSelected),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onWorkstationDronesTabSelected, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallRetrieveAllIdleDrones()
        {
            var onWorkstationDronesTabSelected = new Subject<IEnumerable<Drone>>();
            var dronesRepository = ADronesRepository();
            
            Given(new SelectWorkstationDronesTab(
                    SomeEvents(withWorkstationDronesTabSelected: onWorkstationDronesTabSelected),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).RetrieveAllIdleDrones())
                .Run();
        }
        
        [Test]
        public void ReturnStoredIdleDrones()
        {
            var onWorkstationDronesTabSelected = new Subject<IEnumerable<Drone>>();
            var dronesRepository = AnInMemoryDronesRepository(
                withDrones: new[]
                {
                    AFullyArmedDrone(withName: "arturo").ModifyThis(withStatus: DroneStatus.Idle),
                    ADrone(withName: "KuntaKinte", withStatus: DroneStatus.Busy)
                }
            );

            var expecteDrones = new[] {AFullyArmedDrone(withName: "arturo").ModifyThis(withStatus: DroneStatus.Idle)};
            
            Given(new SelectWorkstationDronesTab(
                    SomeEvents(withWorkstationDronesTabSelected: onWorkstationDronesTabSelected),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onWorkstationDronesTabSelected, it => it.Receives(expecteDrones))
                .Run();
        }
    }
}