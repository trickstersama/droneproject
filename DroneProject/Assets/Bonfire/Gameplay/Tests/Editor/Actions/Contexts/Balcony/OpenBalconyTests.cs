﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.BalconyContext;
using Bonfire.Gameplay.Domain.ValueObjects;
using Bonfire.Gameplay.Domain.ValueObjects.Mission;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Balcony.MissionProgressionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionResultsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.MissionOrdersMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.TimeOnMissionMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.MissionsRepositoryMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Balcony
{
    [TestFixture]
    public class OpenBalconyTests
    {
        [Test]
        public void SendOnBalconyOpened()
        {
            var onBalconyOpened = new Subject<IEnumerable<MissionResults>>();
        
            Given(new OpenBalcony(
                    SomeEvents(withBalconyOpened: onBalconyOpened),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onBalconyOpened, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallFinishedMissionsFrom_MissionsRepository()
        {
            var missionsRepository = AMissionsRepository();
        
            Given(new OpenBalcony(
                    SomeEvents(),
                    SomeRepositories(withMissionsRepository: missionsRepository)
                ))
                .When(action => action.Do())
                .Then(() => missionsRepository.Received(1).GetFinishedMissions())
                .Run();
        }
        
        [Test][Ignore("Not finished")]
        public void EmitDronesInBalcony()
        {
            var onBalconyOpened = new Subject<IEnumerable<MissionResults>>();
            
            var drone1 = ADrone(withStatus: DroneStatus.InBalcony, withName: "Caca");
            var drone2 = ADrone(withStatus: DroneStatus.InBalcony, withName: "pichi");
            var droneDamage = 50f;
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone1, drone2});
            var timeOnMission = ATimeOnMission(1);
            var missionProgression = AMissionProgression(withDroneDamage: droneDamage);
            
            var mission1 = AMission(
                withMissionOrders: AMissionOrders(
                    withDroneName: drone1.droneName,
                    withMissionType: MissionType.Battle
                ),  
                witMissionProgression: missionProgression,
                withTimeOnMission: timeOnMission
            );

            var mission2 = AMission(withMissionOrders: AMissionOrders(withDroneName: drone2.droneName));
            
            var missionResult1 = AMissionResult(
                withDroneName: drone1.droneName,
                withDroneDamage: 10f,
                withMissionType: MissionType.Battle,
                withTimeOnMission: timeOnMission
            );
            var expectedMissionResults = new[] { missionResult1 };
            
            Given(new OpenBalcony(
                    SomeEvents(withBalconyOpened: onBalconyOpened),
                    SomeRepositories(withDronesRepository: dronesRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onBalconyOpened, it => it.Receives(expectedMissionResults))
                .Run();
        }
    }

    public static class MissionProgressionMother
    {
        public static MissionProgression AMissionProgression(float withDroneDamage = 0)
        {
            return new MissionProgression(DroneDamage: withDroneDamage);
        }
    }
}
