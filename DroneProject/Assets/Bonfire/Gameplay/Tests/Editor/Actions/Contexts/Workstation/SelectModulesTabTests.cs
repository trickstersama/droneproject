﻿using System.Collections.Generic;
using Bonfire.Gameplay.Domain.Actions.WorkstationContext;
using Bonfire.Gameplay.Domain.ValueObjects.Modules;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.ComponentsRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.ModuleMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation
{
    [TestFixture]
    public class SelectModulesTabTests
    {
        [Test]
        public void SendOnModulesTabSelected()
        {
            var onModulesTabOpened = new Subject<IEnumerable<Module>>();

            Given(new SelectModulesTab(
                    SomeEvents(withModulesTabOpened: onModulesTabOpened),
                    SomeRepositories()
                ))
                .When(action => action.Do())
                .Then(_ => onModulesTabOpened, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetModulesFromComponentRepository()
        {
            var componentsRepository = AComponentsRepository();
            
            Given(new SelectModulesTab(
                    SomeEvents(),
                    SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do())
                .Then(() => componentsRepository.Received(1).GetAllModules())
                .Run();
        }
        
        [Test]
        public void ReturnStoredModules()
        {
            var onModulesTabOpened = new Subject<IEnumerable<Module>>();

            var availableModules = new[]
            {
                ABatteryModule("battery"),
                APropulsionModule(withName: "propulsion")
            };
            var expectedModules = new[]
            {
                ABatteryModule("battery"),
                APropulsionModule(withName: "propulsion")
            };
            var componentsRepository = AnInMemoryComponentRepository(withModules: availableModules);
            
            Given(new SelectModulesTab(
                    SomeEvents(withModulesTabOpened: onModulesTabOpened),
                    SomeRepositories(withComponentsRepository: componentsRepository)
                ))
                .When(action => action.Do())
                .Then(_ => onModulesTabOpened, it => it.Receives(expectedModules))
                .Run();
        }
    }
}