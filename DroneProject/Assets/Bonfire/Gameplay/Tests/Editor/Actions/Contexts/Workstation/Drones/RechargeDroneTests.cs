﻿using Bonfire.Gameplay.Domain.Actions.WorkstationContext.Drones;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayServicesMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Services.DroneServiceMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.FrameMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Workstation.Drones
{
    [TestFixture]
    public class RechargeDroneTests
    {
        [Test]
        public void SendOnDroneRecharged()
        {
            var onDroneRecharged = new Subject<Drone>();
            
            Given(new RechargeDrone(
                    SomeEvents(withDroneRecharged: onDroneRecharged),
                    SomeRepositories(),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(_ => onDroneRecharged, it => it.ReceivesSomething())
                .Run();
        }
        
        [Test]
        public void CallGetWorkshopDroneFromRepository()
        {
            var dronesRepository = ADronesRepository();
                
            Given(new RechargeDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).RetrieveDroneInWorkshop())
                .Run();
        }
        
        [Test]
        public void CallRechargeDroneInDroneService()
        {
            var droneService = ADroneService();
                
            Given(new RechargeDrone(
                    SomeEvents(),
                    SomeRepositories(),
                    SomeServices(withDroneService: droneService)
                ))
                .When(action => action.Do())
                .Then(() => droneService.Received(1).RechargeDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void CalUpdateDroneFromRepository()
        {
            var dronesRepository = ADronesRepository();
                
            Given(new RechargeDrone(
                    SomeEvents(),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices()
                ))
                .When(action => action.Do())
                .Then(() => dronesRepository.Received(1).UpdateDrone(Arg.Any<Drone>()))
                .Run();
        }
        
        [Test]
        public void ReturnRechargedDrone()
        {
            var onDroneRecharged = new Subject<Drone>();
            var dischargeDrone = ADrone(
                withCharge: 0,
                withFrame: AFullFrame(),
                withName: "TheRock",
                withDamageReceived: 99
            );

            var dronesRepository = AnInMemoryDronesRepository(withDroneInWorkshop: dischargeDrone);
            
            var expectedDrone = ADrone(
                withCharge: 100,
                withFrame: AFullFrame(),
                withName: "TheRock",
                withDamageReceived: 99
            );
            
            Given(new RechargeDrone(
                    SomeEvents(withDroneRecharged: onDroneRecharged),
                    SomeRepositories(withDronesRepository: dronesRepository),
                    SomeServices(withDroneService: ALocalDroneService())
                ))
                .When(action => action.Do())
                .Then(_ => onDroneRecharged, it => it.Receives(expectedDrone))
                .Run();
        }

        
    }
}