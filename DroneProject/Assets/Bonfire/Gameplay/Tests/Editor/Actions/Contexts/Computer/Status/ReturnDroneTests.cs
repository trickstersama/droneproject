﻿using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ComputerContext.ReturnDroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.ValueObjects.DroneMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.DronesRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Status
{
    [TestFixture]
    public class ReturnDroneTests
    {
        [Test]
        public void SendOnReturnDrone()
        {
            var onReturnDrone = new Subject<Drone>();

            Given(AReturnDrone(withEvents: SomeEvents(withReturnDrone: onReturnDrone)))
                .When(action => action.Do(ADrone()))
                .Then(_ => onReturnDrone, it => it.ReceivesSomething())
                .Run();
        }

        [Test]
        public void CallSetDroneToIdle()
        {
            var dronesRepository = ADronesRepository();

            Given(AReturnDrone(withRepositories: SomeRepositories(withDronesRepository: dronesRepository) ))
                .When(action => action.Do(ADrone()))
                .Then(() => dronesRepository.Received(1).SetDroneToIdle(Arg.Any<Drone>()))
                .Run();
        }

        [Test]
        public void SetBusyDroneToIdle()
        {   
            var onReturnDrone = new Subject<Drone>();
            
            var drone = ADrone(withStatus: DroneStatus.Busy);
            var dronesRepository = AnInMemoryDronesRepository(withDrones: new []{drone});
            var expectedDrone = ADrone(withStatus: DroneStatus.Idle);

            Given(AReturnDrone(
                    withRepositories: SomeRepositories(dronesRepository),
                    withEvents: SomeEvents(withReturnDrone: onReturnDrone)
                ))
                .When(action => action.Do(drone))
                .Then(_ => onReturnDrone, it => it.Receives(expectedDrone))
                .Run();

        }
    }
}