﻿using Bonfire.Gameplay.Domain.Actions.ComputerContext.Map;
using Bonfire.Gameplay.Domain.ValueObjects;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayEventsMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Repositories.PinInProgressRepositoryMother;
using static Bonfire.Gameplay.Tests.Editor.Mothers.Wrappers.GameplayRepositoriesMother;
using static Bonfire.TestSupport.Editor.BDD.Context;

namespace Bonfire.Gameplay.Tests.Editor.Actions.Contexts.Computer.Map
{
    [TestFixture]
    public class SelectNewPinCoordinatesTests
    {
        [Test]
        public void SendOnNewPinCoordinatesSelected()
        {
            var onNewPinCoordinatesSelected = new Subject<Destination>();
            
            Given(new SelectNewPinCoordinates(
                    SomeEvents(withNewPinCoordinatesSelected: onNewPinCoordinatesSelected),
                    SomeRepositories()
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(_ => onNewPinCoordinatesSelected, it => it.ReceivesSomething())
                .Run();
            
        }
                
        [Test]
        public void CallUpdateMapCoordinatesFromBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new SelectNewPinCoordinates(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(() => repository.Received(1).UpdateMapCoordinates(Arg.Any<MapCoordinates>()))
                .Run();
        }
        
        [Test]
        public void CallGetDestinationFromBuilderRepository()
        {
            var repository = APinInProgressRepository();
            
            Given(new SelectNewPinCoordinates(
                    SomeEvents(),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(new MapCoordinates()))
                .Then(() => repository.Received(1).GetDestination())
                .Run();
        }

        [Test]
        public void ReturnExpectedModifiedDestination()
        {
            var onNewPinCoordinatesSelected = new Subject<Destination>();
            var newCoordinates = new MapCoordinates(6, 6);
            var oldDestination = new Destination
            {
                missionType = MissionType.Battle
            };
            
            var repository = AnInMemoryPinInProgressRepository(withDestination: oldDestination);
            var expected = new Destination
            {
                missionType = oldDestination.missionType,
                mapCoordinates = newCoordinates
            };
            
            Given(new SelectNewPinCoordinates(
                    SomeEvents(withNewPinCoordinatesSelected: onNewPinCoordinatesSelected),
                    SomeRepositories(withPinInProgressRepository: repository)
                ))
                .When(action => action.Do(newCoordinates))
                .Then(_ => onNewPinCoordinatesSelected, it => it.Receives(expected))
                .Run();
        }
        
        
    }
}