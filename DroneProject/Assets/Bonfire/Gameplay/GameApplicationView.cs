﻿using Bonfire.Gameplay.Delivery.Views;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Balcony;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Computer;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.House;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.PopUps;
using Bonfire.Gameplay.Delivery.Views.UI.Contexts.Workstation;
using Bonfire.Gameplay.Delivery.Views.World;
using Bonfire.Gameplay.Domain.Data;
using UnityEngine;

namespace Bonfire.Gameplay
{
    public class GameApplicationView : MonoBehaviour
    {

        [Header("Game UI")] 
        [SerializeField] GameObject GameUI;

        [Header("UiElementViews")] 
        [SerializeField] BalconyView balconyContextView;
        [SerializeField] ComputerView computerView;
        [SerializeField] HouseBatteryView houseBatteryView;
        [SerializeField] WorkstationView workstationView;
        [SerializeField] PopUpsView popUpsView;

        [Header("World Views")] 
        [SerializeField] WorldGameplayView worldGameplayView;

        [SerializeField] TimelineView timelineView;
        
        void Start()
        {
            GameUI.gameObject.SetActive(true);
            InitializeGameplayViews();
        }

        CanvasViews CanvasGameplayViews => new CanvasViews
        (
            contextDirector : new ContextDirector(),
            balconyView: balconyContextView,
            houseBatteryView: houseBatteryView,
            workstationView: workstationView,
            popUpsView: popUpsView,
            computerView: computerView
        );

        WorldViews WorldGameplayViews => new WorldViews
        {
            WorldGameplayView = worldGameplayView,
            TimelineView = timelineView
        };
        
        void InitializeGameplayViews()
        {
            Context.Initialize(
                canvasViews: CanvasGameplayViews, 
                worldViews: WorldGameplayViews, 
                loadedDrones: ScriptableDataLoader.LoadDrones(), 
                loadedFrames: ScriptableDataLoader.LoadFrames(), 
                loadedModules: ScriptableDataLoader.LoadModules(), 
                loadedDestinations: ScriptableDataLoader.LoadDestinations(),
                loadedSolarGenerators: ScriptableDataLoader.LoadSolarGenerators(),
                loadedHouseBatteries: ScriptableDataLoader.LoadHouseBatteries(),
                loadedTimeCostsModifiers: ScriptableDataLoader.LoadTimeCostModifiers(),
                missionConfiguration: ScriptableDataLoader.LoadMissionConfiguration()
            );
        }

    }
}