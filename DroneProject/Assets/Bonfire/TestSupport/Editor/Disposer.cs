﻿using System;

namespace Bonfire.TestSupport.Editor
{
    internal class Disposer : IDisposable
    {

        readonly Action disposer;
        public Disposer(Action disposer) => this.disposer = disposer;

        public void Dispose() => disposer();
    }

    class EventShouldNotFail : Exception { }

    class EventShouldNotComplete : Exception { }
}