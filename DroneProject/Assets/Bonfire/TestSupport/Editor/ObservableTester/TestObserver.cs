﻿using System;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;

namespace Bonfire.TestSupport.Editor.ObservableTester
{
    public class TestObserver<T> : IObserver<T>
    {
        readonly List<T> receivedEvents = new List<T>();

        Exception error;
        bool completed;

        public void OnCompleted() => completed = true;

        public void OnError(Exception receivedError) => error = receivedError;

        public void OnNext(T receivedEvent) => receivedEvents.Add(receivedEvent);

        public bool HasCompleted => completed;

        public TestObserver<T> Receives(T expected)
        {
            Assert.Contains(expected, receivedEvents,
                $"Expected event: {expected} but received [{string.Join(", ", receivedEvents)}]");
            return this;
        }

        public TestObserver<T> ReceivesSomething(int times)
        {
            Assert.IsTrue(receivedEvents.Any() && receivedEvents.Count == times);
            return this;
        }
        public TestObserver<T> ReceivesSomething()
        {
            Assert.IsTrue(receivedEvents.Any());
            return this;
        }

        public TestObserver<T> ReceivesNothing()
        {
            Assert.IsEmpty(receivedEvents, "Expected: No events received. Actual: events were received");
            return this;
        }
    }
}