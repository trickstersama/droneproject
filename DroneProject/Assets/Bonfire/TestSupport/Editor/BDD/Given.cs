﻿using System;

namespace Bonfire.TestSupport.Editor.BDD
{
    public class Given <T>
    {
        readonly T action;//la accion que devolvio el context, que se cargo con el given y se va a usar en el when

        public Given(T action) => this.action = action; //constructor que asigna la accion nada mas

        public When<T> When(Action<T> doAction)
        {
            return new When<T>(action, doAction);
            // funcion when que se llama en el test
        }
        

        //devuelve un when del tipo de la accion y pasa un delegate void 
        
    }
}
