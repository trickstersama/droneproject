﻿namespace Bonfire.TestSupport.Editor.BDD
{
    public static class Context 
    {
        public static Given<T> Given<T>(T context) => new Given<T>(context);
    }
    //context es la accion
    //devuelve un given del tipo de la accion
}
