﻿using System;
using System.Collections.Generic;
using Bonfire.TestSupport.Editor.ObservableTester;
using UniRx;
using UnityEngine.Assertions;

namespace Bonfire.TestSupport.Editor.BDD
{
    public class When<T>
    {
        readonly T action;
        readonly Action<T> doAction;
        readonly List<Action> assertions = new List<Action>();

        ISubject<Unit> OnExpectedNeeded = new Subject<Unit>();
        public When(T action, Action<T> doAction)
        {
            this.action = action;
            this.doAction = doAction;
        }
/*
 
         public When<T> When(Action<T> doAction)
        {
            return new When<T>(action, doAction);
            // funcion when que se llama en el test
        }
 */
        public When<T> WhenDo<S>(Func<IObservable<S>> observable,  S expected)
        {

            OnExpectedNeeded
                .SelectMany(_ => observable.Invoke())
                .Subscribe(result => assertions.Add(() => Assert.AreEqual(result, expected)));

            return this;
        }
        public When<T> Then<S>(Func<T, IObservable<S>> eventSelector, Action<TestObserver<S>> assert)
        {
            var selectedEvent = eventSelector(action);
            var eventObserver = new TestObserver<S>();

            selectedEvent.Subscribe(eventObserver);

            assertions.Add(() => assert(eventObserver));

            return this;
        }

        public When<T> Then (Action assert)
        {
            assertions.Add(assert);
            return this;
        }
        public void Run()
        {
            doAction(action);
            OnExpectedNeeded.OnNext(Unit.Default);
            assertions.ForEach(assertion => assertion());
        }
    }
}