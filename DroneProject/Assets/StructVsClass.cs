using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Coding.Exercise;



namespace Coding.Exercise
{
    // TODO
    
    public class PhoneBook : IEnumerable<Contact>
    {
        readonly List<Contact> contacts;

        public PhoneBook(List<Contact> contacts)
        {
            this.contacts = contacts.ToList();
        }

        public IEnumerator<Contact> GetEnumerator()
        {
            return contacts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    public class Contact
    {
        readonly string name;
        readonly string number;

        public Contact(string name, string number)
        {
            this.name = name;
            this.number = number;
        }

        public void Call()
        {
            Console.WriteLine($"Calling to {name}. Phone number is {number}");
        }
    }
    public static class Program{
        static public void Main(string[] args){
            PhoneBook MyPhoneBook = new PhoneBook(Contacts);

            foreach (Contact contact in MyPhoneBook){
                contact.Call();
            }
        }

        static List<Contact> Contacts => new List<Contact>
        {
            new Contact("Andre", "435797087"),
            new Contact("Lisa", "435677087"),
            new Contact("Dine", "3457697087"),
            new Contact("Sofi", "4367697087")
        };
    }
}
/*
    CarStruct carClass;
    CarStruct carStruct;
    
    // Start is called before the first frame update
    void Start()
    {
        //populate meta
        PrinterMachine originalPrinterMachineText =
            new PrinterMachine() {message = "original printermachine basic text"};
        
        //populate class
        carClass = new CarStruct();
        carClass.id = 666;
        carClass.name = "Original class text";
        carClass.serialNumbers = new[] {6, 6, 6};
        carClass.actionsClass = new ActionsClass(new PrinterMachine {message = "original action class message! beep boop"});
        carClass.actionsStruct = new ActionsStruct(new PrinterMachine {message = "Original struct message! askdalks"});
        
        carStruct = new CarStruct();
        carStruct.id = 666;
        carStruct.name = "Original class text";
        carStruct.serialNumbers = new[] {6, 6, 6};
        carStruct.actionsClass = new ActionsClass(new PrinterMachine {message = "original action class message! beep boop"});
        carStruct.actionsStruct = new ActionsStruct(new PrinterMachine {message = "Original struct message! askdalks"});
        
        //tests
        //inicializar ambos, copiarlos e imprimir las diferencias
        //con:
        //enteros

        var newCarClass = carClass;
        newCarClass.id = 111;
        newCarClass.name = "text";
        newCarClass.serialNumbers = new[] {1,1,1};
        newCarClass.actionsClass = new ActionsClass(new PrinterMachine {message = "beep boop"});
        newCarClass.actionsStruct = new ActionsStruct(new PrinterMachine {message = "askdalks"});
        
        /*Debug.Log(
            $"->{carClass.id} -> {newCarClass.id} = {carClass.id == newCarClass.id} \n" +
            $"->{carClass.name} -> {newCarClass.name} = {carClass.name == newCarClass.name} \n" +
            $"->{carClass.serialNumbers[1]} -> {newCarClass.serialNumbers[1]} = {carClass.serialNumbers == newCarClass.serialNumbers} \n" +
            $"->{carClass.actionsClass.printerMachine.message} -> {newCarClass.actionsClass.printerMachine.message} = {carClass.actionsClass == newCarClass.actionsClass} \n" +
            $"->{carClass.actionsStruct.printerMachine.message} -> {newCarClass.actionsStruct.printerMachine.message} = {carClass.actionsStruct.printerMachine.message == newCarClass.actionsStruct.printerMachine.message} \n" 
        );

    }



    public class CarClass
    {
        public int id;
        public string name;
        public int[] serialNumbers;
        public ActionsClass actionsClass;
        public ActionsStruct actionsStruct;
    }

    public struct CarStruct
    {
        public int id;
        public string name;
        public int[] serialNumbers;
        public ActionsClass actionsClass;
        public ActionsStruct actionsStruct;
    }

    public struct ActionsStruct
    {
        public PrinterMachine printerMachine;

        public ActionsStruct(PrinterMachine printerMachine)
        {
            this.printerMachine = printerMachine;
        }
    }

    public class ActionsClass
    {
        public PrinterMachine printerMachine;

        public ActionsClass(PrinterMachine printerMachine)
        {
            this.printerMachine = printerMachine;
        }
    }

    public class PrinterMachine
    {
        public string message = "Printer printing! beep boop";
        public void Do()
        {
            Console.WriteLine(message);
        }
    }
*/
